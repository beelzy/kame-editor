// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include "multitests.h"
#include "../src/preview/bottomview.h"
#include "../src/preview/preview.h"
#include "../src/sourcefile.h"

class BottomViewTest : public QObject {
    Q_OBJECT

    private slots:
        void setScrollPos() {
            BottomView bottomview;

            QCOMPARE(bottomview.scrollpos(), 0);

            bottomview.setScrollPos(100);
            QCOMPARE(bottomview.scrollpos(), 100);
        };

        void onScrollPositionChange() {
            BottomView bottomview;

            bottomview.onScrollPositionChange(100);
            QCOMPARE(bottomview.scrollpos(), 0);

            SourceFile::bottomdraw = static_cast<int>(BottomView::DrawType::TEXTURE);
            bottomview.onScrollPositionChange(100);
            QCOMPARE(bottomview.scrollpos(), 100);

            SourceFile::bottomframe = static_cast<int>(BottomView::FrameType::SLOWSCROLL);
            bottomview.onScrollPositionChange(100);
            QCOMPARE(bottomview.scrollpos(), 100 * Preview::SLOWSCROLL_DAMPING);
        };

        void updateFrameType() {
            BottomView bottomview;

            bottomview.updateFrameType();
            QCOMPARE(bottomview.scrollpos(), 0);

            SourceFile::bottomdraw = static_cast<int>(BottomView::DrawType::TEXTURE);
            SourceFile::bottomframe = static_cast<int>(BottomView::FrameType::FASTSCROLL);
            bottomview.updateFrameType();
            QCOMPARE(bottomview.scrollpos(), 0);

            bottomview.setScrollPos(100);
            bottomview.updateFrameType();
            QCOMPARE(bottomview.scrollpos(), 100);
            SourceFile::bottomframe = static_cast<int>(BottomView::FrameType::SLOWSCROLL);

            bottomview.updateFrameType();
            QCOMPARE(bottomview.scrollpos(), 100 * Preview::SLOWSCROLL_DAMPING);
        };
};

TEST_DECLARE(BottomViewTest);
