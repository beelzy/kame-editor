// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include "multitests.h"
#include "../src/preview/arrowbutton.h"

class ArrowButtonTest : public QObject {
    Q_OBJECT

    private slots:
        void setDirection() {
            ArrowButton button;

            QCOMPARE(button.direction(), ArrowButton::Direction::LEFT);

            button.setDirection(ArrowButton::Direction::RIGHT);

            QCOMPARE(button.direction(), ArrowButton::Direction::RIGHT);
        };

        void updateVisibility() {
            ArrowButton button;

            button.updateVisibility(true, true);
            QCOMPARE(button.isVisible(), true);
            button.updateVisibility(true, false);
            QCOMPARE(button.isVisible(), true);
            button.updateVisibility(false, true);
            QCOMPARE(button.isVisible(), false);
            button.updateVisibility(false, false);
            QCOMPARE(button.isVisible(), false);

            button.setDirection(ArrowButton::Direction::RIGHT);

            button.updateVisibility(true, true);
            QCOMPARE(button.isVisible(), true);
            button.updateVisibility(false, true);
            QCOMPARE(button.isVisible(), true);
            button.updateVisibility(true, false);
            QCOMPARE(button.isVisible(), false);
            button.updateVisibility(false, false);
            QCOMPARE(button.isVisible(), false);
        };
};

TEST_DECLARE(ArrowButtonTest);
