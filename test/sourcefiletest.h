// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include "multitests.h"
#include "../src/sourcefile.h"

class SourceFileTest : public QObject {
    Q_OBJECT

    private slots:
        void reset() {
            SourceFile::reset();

            for (int i = 0; i < 13; i++) {
                QVERIFY(SourceFile::shorttitle[i].isEmpty());
                QVERIFY(SourceFile::longtitle[i].isEmpty());
                QVERIFY(SourceFile::publisher[i].isEmpty());
            }

            QVERIFY(SourceFile::smdhIconPath.isEmpty());
            QVERIFY(SourceFile::smdhSmallIconPath.isEmpty());

            for (int i = 0; i < 9; i++) {
                QCOMPARE(SourceFile::ratings[i]["age"], 0);
                QCOMPARE(SourceFile::ratings[i]["active"], false);
                QCOMPARE(SourceFile::ratings[i]["pending"], false);
                QCOMPARE(SourceFile::ratings[i]["noage"], false);
            }

            QCOMPARE(SourceFile::isRegionFree, false);
            QVERIFY(SourceFile::regions.isEmpty());
            QVERIFY(SourceFile::matchmakerid.isEmpty());
            QVERIFY(SourceFile::matchmakerbitid.isEmpty());

            QVERIFY(SourceFile::flags.isEmpty());
            QVERIFY(SourceFile::eulaversion.isEmpty());
            QCOMPARE(SourceFile::optimalbannerframe, 0.0);

            QVERIFY(SourceFile::streetpassid.isEmpty());

            QCOMPARE(SourceFile::topdraw, 0);
            QCOMPARE(SourceFile::topframe, 0);
            QCOMPARE(SourceFile::bottomdraw, 0);
            QCOMPARE(SourceFile::bottomframe, 0);

            QCOMPARE(SourceFile::toptexture.size(), 3);

            for (int i = 0; i < 3; i++) {
                QVERIFY(SourceFile::toptexture[i].isEmpty());
            }

            QVERIFY(SourceFile::topexttexture.isEmpty());

            QVERIFY(SourceFile::topcolors.isEmpty());

            QCOMPARE(SourceFile::bottomtexture.size(), 2);

            for (int i = 0; i < 2; i++) {
                QVERIFY(SourceFile::bottomtexture[i].isEmpty());
            }

            QVERIFY(SourceFile::bottomoutercolors.isEmpty());
            QVERIFY(SourceFile::bottominnercolors.isEmpty());

            QCOMPARE(SourceFile::foldericons.size(), 2);
            QCOMPARE(SourceFile::fileicons.size(), 2);

            for (int i = 0; i < 2; i++) {
                QVERIFY(SourceFile::foldericons[i].isEmpty());
                QVERIFY(SourceFile::fileicons[i].isEmpty());
            }

            QCOMPARE(SourceFile::cursorcolorEnabled, false);

            QCOMPARE(SourceFile::cursorcolors["dark"], QColor());
            QCOMPARE(SourceFile::cursorcolors["main"], QColor());
            QCOMPARE(SourceFile::cursorcolors["light"], QColor());
            QCOMPARE(SourceFile::cursorcolors["shadow"], QColor());

            QCOMPARE(SourceFile::foldercolorEnabled, false);

            QCOMPARE(SourceFile::foldercolors["dark"], QColor());
            QCOMPARE(SourceFile::foldercolors["main"], QColor());
            QCOMPARE(SourceFile::foldercolors["light"], QColor());
            QCOMPARE(SourceFile::foldercolors["shadow"], QColor());

            QCOMPARE(SourceFile::filecolorEnabled, false);

            QCOMPARE(SourceFile::filecolors["dark"], QColor());
            QCOMPARE(SourceFile::filecolors["main"], QColor());
            QCOMPARE(SourceFile::filecolors["light"], QColor());
            QCOMPARE(SourceFile::filecolors["shadow"], QColor());

            QCOMPARE(SourceFile::openclosecolorEnabled, false);

            QCOMPARE(SourceFile::opencolors["textshadowpos"], 0);
            QCOMPARE(SourceFile::opencolors["dark"], QColor());
            QCOMPARE(SourceFile::opencolors["main"], QColor());
            QCOMPARE(SourceFile::opencolors["light"], QColor());
            QCOMPARE(SourceFile::opencolors["glow"], QColor());
            QCOMPARE(SourceFile::opencolors["shadow"], QColor());
            QCOMPARE(SourceFile::opencolors["textshadow"], QColor());
            QCOMPARE(SourceFile::opencolors["textmain"], QColor());
            QCOMPARE(SourceFile::opencolors["textselected"], QColor());

            QCOMPARE(SourceFile::closecolors["textshadowpos"], 0);
            QCOMPARE(SourceFile::closecolors["dark"], QColor());
            QCOMPARE(SourceFile::closecolors["main"], QColor());
            QCOMPARE(SourceFile::closecolors["light"], QColor());
            QCOMPARE(SourceFile::closecolors["glow"], QColor());
            QCOMPARE(SourceFile::closecolors["shadow"], QColor());
            QCOMPARE(SourceFile::closecolors["textshadow"], QColor());
            QCOMPARE(SourceFile::closecolors["textmain"], QColor());
            QCOMPARE(SourceFile::closecolors["textselected"], QColor());

            QCOMPARE(SourceFile::folderbackbuttoncolorEnabled, false);

            QCOMPARE(SourceFile::folderbackbuttoncolors["textshadowpos"], 0);
            QCOMPARE(SourceFile::folderbackbuttoncolors["dark"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["main"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["light"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["glow"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["shadow"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["textshadow"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["textmain"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["textselected"], QColor());

            QCOMPARE(SourceFile::arrowbuttonbasecolorEnabled, false);

            QCOMPARE(SourceFile::arrowbuttonbasecolors["dark"], QColor());
            QCOMPARE(SourceFile::arrowbuttonbasecolors["main"], QColor());
            QCOMPARE(SourceFile::arrowbuttonbasecolors["light"], QColor());
            QCOMPARE(SourceFile::arrowbuttonbasecolors["shadow"], QColor());

            QCOMPARE(SourceFile::arrowbuttonarrowcolorEnabled, false);

            QCOMPARE(SourceFile::arrowbuttonarrowcolors["border"], QColor());
            QCOMPARE(SourceFile::arrowbuttonarrowcolors["unpressed"], QColor());
            QCOMPARE(SourceFile::arrowbuttonarrowcolors["pressed"], QColor());

            QCOMPARE(SourceFile::bottomcornercolorEnabled, false);

            QCOMPARE(SourceFile::bottomcornercolors["dark"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["main"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["light"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["shadow"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["iconmain"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["iconlight"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["icontextmain"], QColor());

            QCOMPARE(SourceFile::topcornercolorEnabled, false);

            QCOMPARE(SourceFile::topcornercolors["main"], QColor());
            QCOMPARE(SourceFile::topcornercolors["light"], QColor());
            QCOMPARE(SourceFile::topcornercolors["shadow"], QColor());
            QCOMPARE(SourceFile::topcornercolors["textmain"], QColor());

            QCOMPARE(SourceFile::folderviewcolorEnabled, false);

            QCOMPARE(SourceFile::folderviewcolors["dark"], QColor());
            QCOMPARE(SourceFile::folderviewcolors["main"], QColor());
            QCOMPARE(SourceFile::folderviewcolors["light"], QColor());
            QCOMPARE(SourceFile::folderviewcolors["shadow"], QColor());

            QCOMPARE(SourceFile::gametextcolorEnabled, false);
            QCOMPARE(SourceFile::gametextcolorHidden, false);

            QCOMPARE(SourceFile::gametextcolors["main"], QColor());
            QCOMPARE(SourceFile::gametextcolors["light"], QColor());
            QCOMPARE(SourceFile::gametextcolors["shadow"], QColor());
            QCOMPARE(SourceFile::gametextcolors["textmain"], QColor());

            QCOMPARE(SourceFile::demotextcolorEnabled, false);

            QCOMPARE(SourceFile::demotextcolors["main"], QColor());
            QCOMPARE(SourceFile::demotextcolors["textmain"], QColor());

            QVERIFY(SourceFile::bgm.isEmpty());

            QVERIFY(SourceFile::sfxes["cursor"].isEmpty());
            QVERIFY(SourceFile::sfxes["launch"].isEmpty());
            QVERIFY(SourceFile::sfxes["folder"].isEmpty());
            QVERIFY(SourceFile::sfxes["close"].isEmpty());
            QVERIFY(SourceFile::sfxes["frame0"].isEmpty());
            QVERIFY(SourceFile::sfxes["frame1"].isEmpty());
            QVERIFY(SourceFile::sfxes["frame2"].isEmpty());
            QVERIFY(SourceFile::sfxes["resume"].isEmpty());

            QVERIFY(SourceFile::deploypath.isEmpty());
        };

        void rgb565path() {
            QFileInfo input("mock/smdh.png");
            QFileInfo inputRGB565("mock/smdh.rgb565.png");
            QFileInfo inputNoRGB565("mock/smdh-small.png");
            QString rgb565Path = SourceFile::rgb565path(input.canonicalFilePath());

            QVERIFY(rgb565Path.endsWith(".rgb565.png"));

            QString normalPath = SourceFile::rgb565path(inputNoRGB565.canonicalFilePath());

            QVERIFY(!normalPath.endsWith(".rgb565.png"));

            QString rgb565FlatPath = SourceFile::rgb565path(inputRGB565.canonicalFilePath());

            QVERIFY(rgb565FlatPath.endsWith(".rgb565.png"));
            QVERIFY(!rgb565FlatPath.endsWith(".rgb565.png.rgb565.png"));
        };
};

TEST_DECLARE(SourceFileTest);
