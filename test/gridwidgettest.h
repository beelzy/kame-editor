// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include "multitests.h"
#include "../src/preview/gridwidget.h"

class GridWidgetTest : public QObject {
    Q_OBJECT

    private slots:
        void zoom() {
            GridWidget grid;
            GridWidget folder(NULL, true);

            QCOMPARE(grid.zoomLevel(), 0);
            QCOMPARE(folder.zoomLevel(), 1);

            grid.zoomIn();
            QCOMPARE(grid.zoomLevel(), 1);
            grid.zoomIn();
            QCOMPARE(grid.zoomLevel(), 2);
            grid.zoomIn();
            QCOMPARE(grid.zoomLevel(), 3);
            grid.zoomIn();
            QCOMPARE(grid.zoomLevel(), 4);
            grid.zoomIn();
            QCOMPARE(grid.zoomLevel(), 5);
            grid.zoomIn();
            QCOMPARE(grid.zoomLevel(), 5);

            grid.zoomOut();
            QCOMPARE(grid.zoomLevel(), 4);
            grid.zoomOut();
            QCOMPARE(grid.zoomLevel(), 3);
            grid.zoomOut();
            QCOMPARE(grid.zoomLevel(), 2);
            grid.zoomOut();
            QCOMPARE(grid.zoomLevel(), 1);
            grid.zoomOut();
            QCOMPARE(grid.zoomLevel(), 0);
            grid.zoomOut();
            QCOMPARE(grid.zoomLevel(), 0);

            folder.zoomIn();
            QCOMPARE(folder.zoomLevel(), 2);
            folder.zoomIn();
            QCOMPARE(folder.zoomLevel(), 3);
            folder.zoomIn();
            QCOMPARE(folder.zoomLevel(), 4);
            folder.zoomIn();
            QCOMPARE(folder.zoomLevel(), 5);
            folder.zoomIn();
            QCOMPARE(folder.zoomLevel(), 5);

            folder.zoomOut();
            QCOMPARE(folder.zoomLevel(), 4);
            folder.zoomOut();
            QCOMPARE(folder.zoomLevel(), 3);
            folder.zoomOut();
            QCOMPARE(folder.zoomLevel(), 2);
            folder.zoomOut();
            QCOMPARE(folder.zoomLevel(), 1);
            folder.zoomOut();
            QCOMPARE(folder.zoomLevel(), 1);
        };
};

TEST_DECLARE(GridWidgetTest);
