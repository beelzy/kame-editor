// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include "multitests.h"
#include "../src/dock/kameslider.h"

class KameSliderTest : public QObject {
    Q_OBJECT

    private slots:
        void setValue() {
            KameSlider slider;

            QCOMPARE(slider.value(), 0);

            slider.setValue(0.05);
            QVERIFY(qFuzzyCompare(slider.value(), 0.05f));
        };

        void setRange() {
            KameSlider slider;

            slider.setRange(-10, 10);
            slider.setValue(0.75);
            QCOMPARE(slider.value(), 0.75);

        };
};

TEST_DECLARE(KameSliderTest);
