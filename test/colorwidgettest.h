// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include "multitests.h"
#include "../src/dock/colorwidget.h"
#include "../src/sourcefile.h"

class ColorWidgetTest : public QObject {
    Q_OBJECT

    private slots:
        void reset() {
            ColorWidget colorwidget;

            SourceFile::reset();
            colorwidget.reset();

            QCOMPARE(colorwidget.slider("open")->value(), 0.5);
            QCOMPARE(colorwidget.slider("close")->value(), 0.5);
            QCOMPARE(colorwidget.slider("folderbackbutton")->value(), 0.5);

            QCOMPARE(SourceFile::cursorcolors["dark"], QColor());
            QCOMPARE(SourceFile::cursorcolors["main"], QColor());
            QCOMPARE(SourceFile::cursorcolors["light"], QColor());
            QCOMPARE(SourceFile::cursorcolors["shadow"], QColor());

            QCOMPARE(SourceFile::foldercolors["dark"], QColor());
            QCOMPARE(SourceFile::foldercolors["main"], QColor());
            QCOMPARE(SourceFile::foldercolors["light"], QColor());
            QCOMPARE(SourceFile::foldercolors["shadow"], QColor());

            QCOMPARE(SourceFile::filecolors["dark"], QColor());
            QCOMPARE(SourceFile::filecolors["main"], QColor());
            QCOMPARE(SourceFile::filecolors["light"], QColor());
            QCOMPARE(SourceFile::filecolors["shadow"], QColor());

            QCOMPARE(SourceFile::opencolors["textshadowpos"], 0);
            QCOMPARE(SourceFile::opencolors["dark"], QColor());
            QCOMPARE(SourceFile::opencolors["main"], QColor());
            QCOMPARE(SourceFile::opencolors["light"], QColor());
            QCOMPARE(SourceFile::opencolors["glow"], QColor());
            QCOMPARE(SourceFile::opencolors["shadow"], QColor());
            QCOMPARE(SourceFile::opencolors["textshadow"], QColor());
            QCOMPARE(SourceFile::opencolors["textmain"], QColor());
            QCOMPARE(SourceFile::opencolors["textselected"], QColor());

            QCOMPARE(SourceFile::closecolors["textshadowpos"], 0);
            QCOMPARE(SourceFile::closecolors["dark"], QColor());
            QCOMPARE(SourceFile::closecolors["main"], QColor());
            QCOMPARE(SourceFile::closecolors["light"], QColor());
            QCOMPARE(SourceFile::closecolors["glow"], QColor());
            QCOMPARE(SourceFile::closecolors["shadow"], QColor());
            QCOMPARE(SourceFile::closecolors["textshadow"], QColor());
            QCOMPARE(SourceFile::closecolors["textmain"], QColor());
            QCOMPARE(SourceFile::closecolors["textselected"], QColor());

            QCOMPARE(SourceFile::folderbackbuttoncolors["textshadowpos"], 0);
            QCOMPARE(SourceFile::folderbackbuttoncolors["dark"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["main"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["light"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["glow"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["shadow"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["textshadow"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["textmain"], QColor());
            QCOMPARE(SourceFile::folderbackbuttoncolors["textselected"], QColor());

            QCOMPARE(SourceFile::arrowbuttonbasecolors["dark"], QColor());
            QCOMPARE(SourceFile::arrowbuttonbasecolors["main"], QColor());
            QCOMPARE(SourceFile::arrowbuttonbasecolors["light"], QColor());
            QCOMPARE(SourceFile::arrowbuttonbasecolors["shadow"], QColor());

            QCOMPARE(SourceFile::arrowbuttonarrowcolors["border"], QColor());
            QCOMPARE(SourceFile::arrowbuttonarrowcolors["unpressed"], QColor());
            QCOMPARE(SourceFile::arrowbuttonarrowcolors["pressed"], QColor());

            QCOMPARE(SourceFile::bottomcornercolors["dark"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["main"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["light"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["shadow"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["iconmain"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["iconlight"], QColor());
            QCOMPARE(SourceFile::bottomcornercolors["icontextmain"], QColor());

            QCOMPARE(SourceFile::topcornercolors["main"], QColor());
            QCOMPARE(SourceFile::topcornercolors["light"], QColor());
            QCOMPARE(SourceFile::topcornercolors["shadow"], QColor());
            QCOMPARE(SourceFile::topcornercolors["textmain"], QColor());

            QCOMPARE(SourceFile::folderviewcolors["dark"], QColor());
            QCOMPARE(SourceFile::folderviewcolors["main"], QColor());
            QCOMPARE(SourceFile::folderviewcolors["light"], QColor());
            QCOMPARE(SourceFile::folderviewcolors["shadow"], QColor());

            QCOMPARE(SourceFile::gametextcolors["main"], QColor());
            QCOMPARE(SourceFile::gametextcolors["light"], QColor());
            QCOMPARE(SourceFile::gametextcolors["shadow"], QColor());
            QCOMPARE(SourceFile::gametextcolors["textmain"], QColor());

            QCOMPARE(SourceFile::demotextcolors["main"], QColor());
            QCOMPARE(SourceFile::demotextcolors["textmain"], QColor());

            QCOMPARE(SourceFile::recentlyModified, false);
        };

        void load() {
            ColorWidget colorwidget;

            SourceFile::opencolors["textshadowpos"] = 3.0;
            SourceFile::closecolors["textshadowpos"] = 3.0;
            SourceFile::folderbackbuttoncolors["textshadowpos"] = 3.0;

            colorwidget.load();

            QVERIFY(qFuzzyCompare(colorwidget.slider("open")->value(), 13.0f / 20.0f));
            QVERIFY(qFuzzyCompare(colorwidget.slider("close")->value(), 13.0f / 20.0f));
            QVERIFY(qFuzzyCompare(colorwidget.slider("folderbackbutton")->value(), 13.0f / 20.0f));

            QCOMPARE(SourceFile::opencolors["textshadowpos"], 3.0);
            QCOMPARE(SourceFile::closecolors["textshadowpos"], 3.0);
            QCOMPARE(SourceFile::folderbackbuttoncolors["textshadowpos"], 3.0);

            QCOMPARE(SourceFile::recentlyModified, false);
        };
};

TEST_DECLARE(ColorWidgetTest);
