// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QSettings>

#include "multitests.h"
#include "../src/consolewidget.h"
#include "../src/preview/preview.h"

class ConsoleWidgetTest : public QObject {
    Q_OBJECT

    private slots:
        void capturePreview() {
            ConsoleWidget widget;
            QPixmap preview = widget.capturePreview();

            QCOMPARE(preview.size(), QSize(Preview::TOP_SCREEN_WIDTH, Preview::TOP_SCREEN_HEIGHT + Preview::BOTTOM_SCREEN_HEIGHT));
        };

        void checkQuit() {
            ConsoleWidget widget;

            QCOMPARE(widget.checkQuit(), true);
        };

        void updateSkin() {
            ConsoleWidget widget;
            QSettings skin(":/resources/skins/2ds.conf", QSettings::IniFormat);

            widget.setSkinId("2ds");

            QCOMPARE(skin.value("buttons/smdhbutton.x").toInt(), widget.smdhButton()->x());
            QCOMPARE(skin.value("buttons/smdhbutton.y").toInt(), widget.smdhButton()->y());
            QCOMPARE(41, widget.smdhButton()->width());
            QCOMPARE(42, widget.smdhButton()->height());

            QCOMPARE(skin.value("buttons/colorsbutton.x").toInt(), widget.colorsButton()->x());
            QCOMPARE(skin.value("buttons/colorsbutton.y").toInt(), widget.colorsButton()->y());
            QCOMPARE(41, widget.colorsButton()->width());
            QCOMPARE(42, widget.colorsButton()->height());

            QCOMPARE(skin.value("buttons/texturesbutton.x").toInt(), widget.texturesButton()->x());
            QCOMPARE(skin.value("buttons/texturesbutton.y").toInt(), widget.texturesButton()->y());
            QCOMPARE(41, widget.texturesButton()->width());
            QCOMPARE(42, widget.texturesButton()->height());

            QCOMPARE(skin.value("buttons/audiobutton.x").toInt(), widget.audioButton()->x());
            QCOMPARE(skin.value("buttons/audiobutton.y").toInt(), widget.audioButton()->y());
            QCOMPARE(41, widget.audioButton()->width());
            QCOMPARE(42, widget.audioButton()->height());

            QCOMPARE(skin.value("dpad/x").toInt(), widget.pad()->x());
            QCOMPARE(skin.value("dpad/y").toInt(), widget.pad()->y());
            QCOMPARE(91, widget.pad()->width());
            QCOMPARE(91, widget.pad()->height());

            QCOMPARE(skin.value("menu/x").toInt(), widget.consoleMenu()->x());
            QCOMPARE(skin.value("menu/y").toInt(), widget.consoleMenu()->y());
            QCOMPARE(skin.value("menu/width").toInt(), widget.consoleMenu()->width());
            QCOMPARE(skin.value("menu/height").toInt(), widget.consoleMenu()->height());

            int topWidth = Preview::TOP_SCREEN_WIDTH;
            int topHeight = Preview::TOP_SCREEN_HEIGHT;
            QCOMPARE(skin.value("buttons/topscreen.x").toInt(), widget.topview()->x());
            QCOMPARE(skin.value("buttons/topscreen.y").toInt(), widget.topview()->y());
            QCOMPARE(topWidth, widget.topview()->width());
            QCOMPARE(topHeight, widget.topview()->height());

            int bottomWidth = Preview::BOTTOM_SCREEN_WIDTH;
            int bottomHeight = Preview::BOTTOM_SCREEN_HEIGHT;
            QCOMPARE(skin.value("buttons/bottomscreen.x").toInt(), widget.bottomview()->x());
            QCOMPARE(skin.value("buttons/bottomscreen.y").toInt(), widget.bottomview()->y());
            QCOMPARE(bottomWidth, widget.bottomview()->width());
            QCOMPARE(bottomHeight, widget.bottomview()->height());

            QCOMPARE(skin.value("buttons/bottomscreen.x").toInt(), widget.bottomwidget()->x());
            QCOMPARE(skin.value("buttons/bottomscreen.y").toInt(), widget.bottomwidget()->y());
            QCOMPARE(bottomWidth, widget.bottomwidget()->width());
            QCOMPARE(bottomHeight, widget.bottomwidget()->height());
        }
};

TEST_DECLARE(ConsoleWidgetTest);
