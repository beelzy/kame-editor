// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include "multitests.h"
#include "../src/preview/preview.h"

class PreviewTest : public QObject {
    Q_OBJECT

    private slots:
        void colormix() {
            QCOMPARE(Preview::colormix(QColor(Qt::red), QColor(Qt::green), 1.0), QColor(Qt::green));
            QCOMPARE(Preview::colormix(QColor(Qt::red), QColor(Qt::green), 0.0), QColor(Qt::red));
            QCOMPARE(Preview::colormix(QColor(Qt::red), QColor(Qt::green)), QColor("#7F7F00"));
        };
};

TEST_DECLARE(PreviewTest);
