// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include "multitests.h"
#include "../src/kametools.h"

class KameToolsTest : public QObject {
    Q_OBJECT

    private slots:
        void checkAvailable() {
            KameTools kametools;
            QSignalSpy spy(&kametools, SIGNAL(checkCompleted(bool)));

            kametools.checkAvailable();

            spy.wait();

            QCOMPARE(spy.count(), 1);
            QList<QVariant> arguments = spy.takeFirst();

            QVERIFY(arguments.at(0).toBool() == true);
        };

        void extractTheme() {
            KameTools kametools;
            QSignalSpy spy(&kametools, SIGNAL(themeExtracted(bool, QString)));

            QFileInfo input("mock/kame-editor.zip");
            QTemporaryDir output;

            kametools.extractTheme(input.canonicalFilePath(), output.path() + "/theme.rc", output.path());

            spy.wait();

            QCOMPARE(spy.count(), 1);
            QList<QVariant> arguments = spy.takeFirst();

            QVERIFY(arguments.at(0).toBool() == true);
        };

        void makeTheme() {
            KameTools kametools;
            QSignalSpy spy(&kametools, SIGNAL(themeDeployed(bool, QString)));

            QFileInfo input("mock/kameeditor.rc");
            QTemporaryDir output;

            kametools.makeTheme(input.canonicalFilePath(), output.path() + "/theme.zip", false);

            spy.wait();

            QCOMPARE(spy.count(), 1);
            QList<QVariant> arguments = spy.takeFirst();

            QVERIFY(arguments.at(0).toBool() == true);
        };

        void convertImageRGB565() {
            KameTools kametools;
            QSignalSpy spy(&kametools, SIGNAL(imageRGB565Converted(bool, QString)));

            QFileInfo input("mock/smdh.png");
            QTemporaryDir output;

            kametools.convertImageRGB565(input.canonicalFilePath(), output.path() + "/smdh.rgb565.png");

            spy.wait();

            QCOMPARE(spy.count(), 1);
            QList<QVariant> arguments = spy.takeFirst();

            QVERIFY(arguments.at(0).toBool() == true);

            QFileInfo outputFile(output.path() + "/smdh.rgb565.png");

            QVERIFY(outputFile.exists() == true);
        };
};

TEST_DECLARE(KameToolsTest);
