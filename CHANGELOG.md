# Changelog

## 1.4.0

* Update to Qt6
* Fix bug where fallback icon is not loaded correctly when not provided
* Various build improvements and updates to vgmstream
* Better audio file error handling
* Audio is optional after you add it the first time
* New widget locking feature
* Display audio file size in description

## 1.3.1

* Update file colors in folder view when editing theme colors
* Optional folder and file images can be cleared
* Disable empty dialogs when info icons have no dialog contents
* More accurate audio file size limits
* Provide more detailed explanation in tooltips for menu modes
* Share color dialog instance between swatch widgets in the same dock widget
* Double clicking no longer eats up single click events in accordion title
* Ensure vgmstream process is not prematurely terminated

## 1.3.0

* Resume playing bgm on application focus when openlid sfx is not defined
* Handle loaded images differently by generating dithered rgb565.png versions
* Upgrade to kame-tools v1.3.8
* No longer ask users for import target paths, and provide a configurable one automatically
* Add tooltips to filepaths in image and audio pickers
* Detect missing resources on loading themes and display a resolver
* Provide extra buttons for documentation and downloading texture templates in help dialogs
* Improve styling consistency
* Automatically fill missing required SMDH fields on deploy
* Use pixel sizes for fonts in preview. Hopefully, this will fix font size issues on other OSes.
* Automatically reload updated resources when app receives focus
* Add actual restart button to settings widget
* Change only modified color when selecting a swatch in the color dialog
* Fix QtColorWidget valgrind errors

## 1.2.8

* Fix deploy path when switching between zip and folder deploy
* Clear audio converter output path once conversion finishes
* Force preview images to be RGBA so Anemone can see them properly
* Improve preview appearance
* Top screen is only 400px wide
* Add missing includes so that it builds on more systems
* Correctly play BGM after openlid SFX plays
* Display SFX total limit instead of limiting per file
* Make menu items on certain skins more readable
* Provide help key visuals for other skins
* Correctly read WAV headers when multiple chunks are included
* Warn users with insufficient OpenGL versions

## 1.2.7

* Add extra setting to hide folder letter
* Implement extra folder deploy option
* Warn users if selected deploy folder already contains theme files
* Implement extra import from folder option
* Add option to include branding to preview screenshots
* Fix small icon path bug
* Accept jpeg files for textures

## 1.2.6

* Add autosave feature
* Make open/close and folder back button visibility icons update correctly on theme load
* Use ceilf instead of ceil when making certain operations with floats
* Don't save smallicon path if it doesn't exist
* Use correct position when capturing bottom screen preview
* Save color picker position after dragging it
* Persist animations pause/play when application closes
* Fix folder animation bug when icon selection changes

## 1.2.5

* Make import dialog file/folder select buttons green
* Add recently used color swatches to color picker
* Add core qt5 translations for standard buttons
* Fix audio looping bug so that seek doesn't land in the middle of a sample byte
* Display tooltips for selected files/folders in the import dialog
* Make dialog folder selection work properly on Windows

## 1.2.4

* Enable theme archive importing with kame-tools v1.3.2+
* Fix bugs related to hiding game text when disabled

## 1.2.3

* Implement color palette management
* Add missing hidden game text option
* Add optional small icon to SMDH optional section

## 1.2.2

* Add tooltip hints
* Disable SMDH region info editing
* Update translations

## 1.2.1

* Don't use reserved NOERROR enums
* Update translations
* Fix New 2DS XL frame

## 1.2.0

* Update credits
* Add BGM autoplay settings option
* Improve console menu functionality
* Fix long SFX warning duration
* Add warning about unsupported WAV bps (16 and 8 bit only because rstmcpp does not have encoding for anything else)
* Add skins for other 3DS consoles

## 1.1.0

* Add 3D folder to top screen
* Add settings widget
* Save hidden color values
* Add a configurable deploy path
* Save dock widget and window state

## 1.0.3

* Make code compatible with MinGW
* Add unit tests
* Color dialog no longer covers the preview window when it opens
* Use more QStylesheet colors
* Prevent loading another wav in the audio converter if a wav is already loading

## 1.0.2

* Certain textures are previewed with a black background if they contain transparency (this is what the 3DS does anyways; the image will still appear as is without any background in the image picker thumbnail).
* Pressed states of certain elements in the preview are displayed while you are picking a color.
* Added a German translation. Other translations are welcomed.
* Added a CircleCI container stub for building .deb packages.

## 1.0.1

* Code cleanup

## 1.0.0

* Add flag to disable top screen MSAA for certain graphics configurations
* Added a CircleCI script to make Mac builds
* Use vgmstream-cli canonical name instead of vgmstream_cli
* Add scripts for packaging Linux hicolor icons
