// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QApplication>

#include "consolewidget.h"
#include "preview/preview.h"
#include "preview/emptyicon.h"
#include "sourcefile.h"
#include "kamewidget.h"

/*
    TRANSLATOR ConsoleWidget

    Console Button - The four buttons on the right side of the console. Hover your cursor over them to see the tooltips.
    Power Button - Bottom button on the right side of the console. Used to quit the program.
    Console Joystick - Joystick on the left side of the console. Hover your cursor over it to see its tooltip.
*/

ConsoleWidget::ConsoleWidget(QWidget *parent) : QFrame(parent) {
    QSettings settings;
    _skinid = settings.value("skin", "3ds").toString();
    _widget = new QWidget(this);

    _smdhbutton = new ConsoleButton(this);
    _smdhbutton->setIcon(":/resources/skins/icons/smdh.png");
    _smdhbutton->setToolTip(tr("Show/Hide Theme Info Widget", "Console Button"));

    _colorsbutton = new ConsoleButton(this);
    _colorsbutton->setIcon(":/resources/skins/icons/colors.png");
    _colorsbutton->setToolTip(tr("Show/Hide Colors Widget", "Console Button"));

    _texturesbutton = new ConsoleButton(this);
    _texturesbutton->setIcon(":/resources/skins/icons/textures.png");
    _texturesbutton->setToolTip(tr("Show/Hide Textures and Screen Appearance Widget", "Console Button"));

    _audiobutton = new ConsoleButton(this);
    _audiobutton->setIcon(":/resources/skins/icons/audio.png");
    _audiobutton->setToolTip(tr("Show/Hide Audio Widget", "Console Button"));

    _consolepad = new ConsolePad(this);

    _powerbutton = new ConsoleButton(this);
    _powerbutton->setBase("powerbutton");
    _powerbutton->setIcon(":/resources/skins/icons/power.png");
    _powerbutton->setToolTip(tr("Quit", "Power Button"));
    _powerbutton->setCheckable(false);

    _animationbutton = new ConsoleJoystick(this);
    _animationbutton->setToolTip(tr("Play/Pause Animations", "Console Joystick"));

    _consolemenu = new ConsoleMenu(this);

    _topview = new TopView(_widget);

    _bottomview = new BottomView(this);
    _bottomview->setGeometry(0, 0, Preview::BOTTOM_SCREEN_WIDTH, Preview::BOTTOM_SCREEN_HEIGHT);

    _bottomwidget = new BottomWidget(this);
    _bottomwidget->setGeometry(0, 0, Preview::BOTTOM_SCREEN_WIDTH, Preview::BOTTOM_SCREEN_HEIGHT);

    _sizegrip = new QSizeGrip(_widget);

    updateSkin();

    connect(_animationbutton, &QAbstractButton::toggled, _topview, &TopView::setAnimated);
    connect(_animationbutton, &QAbstractButton::toggled, _bottomwidget->cursor(), &CursorWidget::setAnimated);
    connect(_animationbutton, &QAbstractButton::toggled, this, &ConsoleWidget::saveAnimationState);

    connect(_bottomwidget->gridwidget(), &GridWidget::iconChanged, _topview, &TopView::onIconChange);
    connect(_bottomwidget->gridwidget(), &GridWidget::scrollPositionChanged, _topview, &TopView::onScrollPositionChange);
    connect(_bottomwidget->gridwidget(), &GridWidget::pageScroll, _topview, &TopView::onPageScroll);
    connect(_bottomwidget->gridwidget(), &GridWidget::scrollPositionChanged, _bottomview, &BottomView::onScrollPositionChange);
    connect(_bottomwidget->gridwidget(), &GridWidget::pageScroll, _bottomview, &BottomView::onPageScroll);
    connect(_bottomwidget->gridwidget(), &GridWidget::zoomLevelChanged, _bottomview, &BottomView::onZoomLevelChanged);
    connect(_bottomwidget, &BottomWidget::folderStateChanged, _topview, &TopView::onFolderStateChanged);

    connect(_powerbutton, &QAbstractButton::clicked, this, &ConsoleWidget::onPowerButton);

    _bottomwidget->gridwidget()->setScreenIndex(0);
}

ConsoleWidget::~ConsoleWidget() {
    delete _animationbutton;
    delete _topview;
    delete _bottomview;
    delete _sizegrip;
    delete _widget;
    delete _powerbutton;
}

void ConsoleWidget::setSkinId(QString value) {
    _skinid = value;
    updateSkin();
}

void ConsoleWidget::updateSkin() {
    _smdhbutton->setSkinId(_skinid);
    _colorsbutton->setSkinId(_skinid);
    _texturesbutton->setSkinId(_skinid);
    _audiobutton->setSkinId(_skinid);
    _animationbutton->setSkinId(_skinid);
    _powerbutton->setSkinId(_skinid);
    _consolepad->setSkinId(_skinid);
    _consolemenu->setSkinId(_skinid);
    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);
    _pixmap.load(":/resources/skins/" + _skinid + "/frame.png");
    resize(_pixmap.size());
    setFixedSize(_pixmap.size());

    _widget->resize(_pixmap.size());
    _widget->setFixedSize(_pixmap.size());

    _sizegrip->setGeometry(_pixmap.size().width() - 20, _pixmap.size().height() - 20, 20, 20);

    _smdhbutton->setGeometry(skin.value("buttons/smdhbutton.x", 0).toInt(), skin.value("buttons/smdhbutton.y", 0).toInt(), _smdhbutton->width(), _smdhbutton->height());

    _colorsbutton->setGeometry(skin.value("buttons/colorsbutton.x", 0).toInt(), skin.value("buttons/colorsbutton.y", 0).toInt(), _colorsbutton->width(), _colorsbutton->height());

    _texturesbutton->setGeometry(skin.value("buttons/texturesbutton.x", 0).toInt(), skin.value("buttons/texturesbutton.y", 0).toInt(), _texturesbutton->width(), _texturesbutton->height());

    _audiobutton->setGeometry(skin.value("buttons/audiobutton.x", 0).toInt(), skin.value("buttons/audiobutton.y", 0).toInt(), _audiobutton->width(), _audiobutton->height());

    _consolepad->setGeometry(skin.value("dpad/x", 0).toInt(), skin.value("dpad/y", 0).toInt(), _consolepad->width(), _consolepad->height());

    _powerbutton->setGeometry(skin.value("buttons/power.x", 0).toInt(), skin.value("buttons/power.y", 0).toInt(), _powerbutton->width(), _powerbutton->height());
    _powerbutton->raise();

    _animationbutton->setGeometry(skin.value("buttons/joystick.x", 0).toInt(), skin.value("buttons/joystick.y", 0).toInt(), _animationbutton->width(), _animationbutton->height());

    _consolemenu->setGeometry(skin.value("menu/x", 0).toInt(), skin.value("menu/y", 0).toInt(), skin.value("menu/width", 0).toInt(), skin.value("menu/height", 0).toInt());

    _topview->setGeometry(skin.value("buttons/topscreen.x", 0).toInt(), skin.value("buttons/topscreen.y", 0).toInt(), Preview::TOP_SCREEN_WIDTH, Preview::TOP_SCREEN_HEIGHT);
    _topview->raise();

    _bottomview->move(skin.value("buttons/bottomscreen.x", 0).toInt(), skin.value("buttons/bottomscreen.y", 0).toInt());
    _bottomview->raise();

    _bottomview->show();

    _bottomwidget->move(skin.value("buttons/bottomscreen.x", 0).toInt(), skin.value("buttons/bottomscreen.y", 0).toInt());
    _bottomwidget->raise();
}

TopView *ConsoleWidget::topview() {
    return _topview;
}

BottomView *ConsoleWidget::bottomview() {
    return _bottomview;
}

BottomWidget *ConsoleWidget::bottomwidget() {
    return _bottomwidget;
}

ConsoleMenu *ConsoleWidget::consoleMenu() {
    return _consolemenu;
}

ConsolePad *ConsoleWidget::pad() {
    return _consolepad;
}

void ConsoleWidget::setAnimated(bool value) {
    bool play = false;
    if (value) {
        play = _animationbutton->isChecked();
    }
    _topview->setAnimated(play);
    _bottomwidget->cursor()->setAnimated(play);
}

void ConsoleWidget::saveAnimationState() {
    QSettings settings;
    settings.setValue("animations", _animationbutton->isChecked());
}

QPixmap ConsoleWidget::capturePreview() {
    QSettings settings;
    QPixmap top = QPixmap(Preview::TOP_SCREEN_WIDTH, Preview::TOP_SCREEN_HEIGHT);
    top.convertFromImage(_topview->grabFramebuffer());
    QPixmap bottom = QPixmap(Preview::BOTTOM_SCREEN_WIDTH, Preview::BOTTOM_SCREEN_HEIGHT);
    bottom.fill(QColor(Qt::black));
    render(&bottom, QPoint(), QRegion(_bottomview->geometry()));

    QPixmap preview = QPixmap(Preview::TOP_SCREEN_WIDTH, Preview::TOP_SCREEN_HEIGHT + Preview::BOTTOM_SCREEN_HEIGHT);
    preview.fill(QColor(Qt::transparent));
    QPainter p;
    p.begin(&preview);
    p.setRenderHints(QPainter::Antialiasing);
    p.drawPixmap(0, 0, top);
    p.fillRect(0, Preview::TOP_SCREEN_HEIGHT, Preview::TOP_SCREEN_WIDTH, Preview::BOTTOM_SCREEN_HEIGHT, QColor(Qt::black));
    p.drawPixmap((Preview::TOP_SCREEN_WIDTH - Preview::BOTTOM_SCREEN_WIDTH) / 2, Preview::TOP_SCREEN_HEIGHT, bottom);
    if (settings.contains("branding") && settings.value("branding").toBool()) {
        QPen pen;
        QFont font("Nimbus Sans");
        font.setPixelSize(16);
        font.setWeight(QFont::Bold);
        pen.setBrush(QBrush(Qt::white));
        pen.setWidth(2);
        QPainterPath path;
        path.addText(((Preview::TOP_SCREEN_WIDTH - Preview::BOTTOM_SCREEN_WIDTH) / 2) + 6, Preview::TOP_SCREEN_HEIGHT + Preview::BOTTOM_SCREEN_HEIGHT - 6, font, "Made with Kame Editor");
        p.strokePath(path, pen);
        p.fillPath(path, QBrush(Qt::black));
    }
    p.end();
    return preview;
}

void ConsoleWidget::onTopDrawTypeChanged(QString path) {

    int type = SourceFile::topdraw;

    switch(type) {
        case 0:
        default:
            _topview->setDrawType();
            break;
        case 1:
            _topview->setDrawType();
            _topview->setColor(qvariant_cast<QColor>(SourceFile::topcolors["color"]));
            _topview->setAlpha(SourceFile::topcolors["alpha"].toFloat());
            _topview->setGradientAlpha(SourceFile::topcolors["gradientalpha"].toFloat());
            _topview->setGradientBrightness(SourceFile::topcolors["gradientbrightness"].toFloat());
            _topview->setAlphaExt(SourceFile::topcolors["alphaext"].toFloat());
            _topview->setTexture(TopView::DEFAULT_TEXTURE);
            _topview->setTextureExt(TopView::DEFAULT_TEXTURE);
            break;
        case 2:
            _topview->setDrawType();
            _topview->setColor(qvariant_cast<QColor>(SourceFile::topcolors["color"]));
            _topview->setAlpha(SourceFile::topcolors["alpha"].toFloat());
            _topview->setGradientAlpha(SourceFile::topcolors["gradientalpha"].toFloat());
            _topview->setGradientBrightness(SourceFile::topcolors["gradientbrightness"].toFloat());
            _topview->setAlphaExt(SourceFile::topcolors["alphaext"].toFloat());
            _topview->setTexture(path);
            _topview->setTextureExt(SourceFile::topexttexture);
            break;
        case 3:
            if (!path.isEmpty()) {
                _topview->setFrameType();
                _topview->setDrawType();
                _topview->setTexture(SourceFile::rgb565path(path));
            }
            break;
    }
    _consolemenu->checkSourceFile();
}

void ConsoleWidget::onBottomDrawTypeChanged(QString path) {

    int type = SourceFile::bottomdraw;
    QColor color;

    switch(type) {
        case 0:
        default:
            _bottomview->updateDrawType();
            _bottomwidget->gridwidget()->setMainColor(GridWidget::DEFAULT_MAIN);
            _bottomwidget->gridwidget()->setShadowColor(GridWidget::DEFAULT_SHADOW);
            _bottomwidget->gridwidget()->setIconDarkColor(EmptyIcon::DEFAULT_DARK_COLOR);
            _bottomwidget->gridwidget()->setIconLightColor(EmptyIcon::DEFAULT_LIGHT_COLOR);
            _bottomwidget->gridwidget()->setTextureMode(false);
            break;
        case 1:
            _bottomview->updateDrawType();
            color = SourceFile::bottomoutercolors["main"];
            if (color.isValid()) {
                _bottomview->updateMainColor();
            }
            color = SourceFile::bottomoutercolors["dark"];
            if (color.isValid()) {
                _bottomview->updateDarkColor();
            }
            color = SourceFile::bottominnercolors["main"];
            if (color.isValid()) {
                _bottomwidget->gridwidget()->setMainColor(color);
            }
            color = SourceFile::bottominnercolors["shadow"];
            if (color.isValid()) {
                _bottomwidget->gridwidget()->setShadowColor(color);
            }
            color = SourceFile::bottominnercolors["dark"];
            if (color.isValid()) {
                _bottomwidget->gridwidget()->setIconDarkColor(color);
            }
            color = SourceFile::bottominnercolors["light"];
            if (color.isValid()) {
                _bottomwidget->gridwidget()->setIconLightColor(color);
            }
            _bottomwidget->gridwidget()->setTextureMode(false);
            break;
        case 2:
            if (!path.isEmpty()) {
                _bottomview->updateDrawType();
                _bottomview->updateFrameType();
                _bottomview->setTexture(QPixmap(SourceFile::rgb565path(path)));
                _bottomwidget->gridwidget()->setTextureMode(true);
            }
            break;
    }
    _consolemenu->checkSourceFile();
}

ConsoleButton *ConsoleWidget::smdhButton() {
    return _smdhbutton;
}

ConsoleButton *ConsoleWidget::colorsButton() {
    return _colorsbutton;
}

ConsoleButton *ConsoleWidget::texturesButton() {
    return _texturesbutton;
}

ConsoleButton *ConsoleWidget::audioButton() {
    return _audiobutton;
}

void ConsoleWidget::paintEvent(QPaintEvent *) {
    QPainter p(this);
    p.drawPixmap(0, 0, width(), height(), _pixmap);
}

void ConsoleWidget::onPowerButton() {
    QApplication::quit();
}

void ConsoleWidget::onRestart() {
    if (checkQuit()) {
        dynamic_cast<KameWidget*>(parent())->saveWindowState();
        QApplication::quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    }
}

bool ConsoleWidget::checkQuit() {
    if (SourceFile::recentlyModified) {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, tr("Theme has unsaved changes", "Title for the dialog"),
                tr("The current theme has unsaved changes. Would you like to save them before loading or starting a new one?", "Asks the user whether or not they want to save the current theme before quitting."),
                QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if(reply == QMessageBox::Save) {
            _consolemenu->saveSourceFile();
        }
        if (reply == QMessageBox::Save || reply == QMessageBox::Discard) {
            _consolemenu->deleteAutosave();
            return true;
        }
        return false;
    } else {
        _consolemenu->deleteAutosave();
        return true;
    }
}
