// This file is part of Kame Editor.
// Copyright (C) 2021 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QSettings>
#include <QFileInfo>

#include "kametools.h"
#include "semver.h"

int KameTools::LOSSY_VERSION[3] = {1, 3, 8};
int KameTools::MIN_VERSION[3] = {1, 3, 2};

KameTools::KameTools(QObject *parent) : QObject(parent) {
    _process = new QProcess();
}

KameTools::~KameTools() {
    delete _process;
}

void KameTools::checkAvailable() {
    connect(_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &KameTools::onCheckComplete);
    connect(_process, &QProcess::errorOccurred, this, &KameTools::onCheckError);
    _process->start("kame-tools", QStringList() << "-v");
}

void KameTools::extractTheme(QString input, QString output, QString resources) {
    connect(_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &KameTools::onThemeExtracted);
    QStringList cmd = QStringList() << "extract3dstheme" << "-i" << input << "-o" << output << "-r" << resources;
    QFileInfo inputpath = QFileInfo(input);
    if (inputpath.isDir()) {
        cmd << "-f" << "1";
    }
    _process->start("kame-tools", cmd);
}

void KameTools::makeTheme(QString input, QString output, bool isFolder) {
    connect(_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &KameTools::onThemeDeployed);
    QStringList cmd = QStringList() << "make3dstheme" << "-c" << input << "-o" << output << "-nd" << "1";
    if (isFolder) {
        cmd << "-n" << "1";
    }
    _process->start("kame-tools", cmd);
}

void KameTools::convertImageRGB565(QString input, QString output) {
    connect(_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &KameTools::onImageRGB565Converted);
    QStringList cmd = QStringList() << "convertrgb565" << "-i" << input << "-o" << output;
    _process->start("kame-tools", cmd);
}

void KameTools::onCheckComplete(int, QProcess::ExitStatus) {
    cleanupCheck();
    QString output = _process->readAllStandardOutput();
    QString version = output.trimmed().split("kame-tools v")[1];
    QStringList versionList = version.split(".");
    QSettings settings;
    settings.setValue("kame-tools/version/major", versionList[0].toInt());
    settings.setValue("kame-tools/version/minor", versionList[1].toInt());
    settings.setValue("kame-tools/version/patch", versionList[2].toInt());

    int toolsversion[3] = {versionList[0].toInt(), versionList[1].toInt(), versionList[2].toInt()};

    settings.setValue("kame-tools/available", true);
    settings.setValue("kame-tools/import", SemVer::semver_compare_version(toolsversion, KameTools::MIN_VERSION) >= 0);
    settings.setValue("kame-tools/nodither", SemVer::semver_compare_version(toolsversion, KameTools::LOSSY_VERSION) >= 0);
    _process->close();

    emit checkCompleted(true);
}

void KameTools::onCheckError(QProcess::ProcessError) {
    cleanupCheck();

    QSettings settings;
    settings.setValue("kame-tools/available", false);
    _process->close();

    emit checkCompleted(false);
}

void KameTools::cleanupCheck() {
    disconnect(_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &KameTools::onCheckComplete);
    disconnect(_process, &QProcess::errorOccurred, this, &KameTools::onCheckError);
}

void KameTools::onThemeExtracted(int exitCode, QProcess::ExitStatus exitStatus) {
    disconnect(_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &KameTools::onThemeExtracted);
    QString output = _process->readAllStandardOutput();
    emit themeExtracted(exitCode == 0 && exitStatus == QProcess::NormalExit, output);

    _process->close();
}

void KameTools::onThemeDeployed(int exitCode, QProcess::ExitStatus exitStatus) {
    disconnect(_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &KameTools::onThemeDeployed);
    QString output = _process->readAllStandardOutput();
    emit themeDeployed(exitCode == 0 && exitStatus == QProcess::NormalExit, output);

    _process->close();
}

void KameTools::onImageRGB565Converted(int exitCode, QProcess::ExitStatus exitStatus) {
    disconnect(_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &KameTools::onImageRGB565Converted);
    QString output = _process->readAllStandardOutput();
    _process->close();

    emit imageRGB565Converted(exitCode == 0 && exitStatus == QProcess::NormalExit, output); 
}
