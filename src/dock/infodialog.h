// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef INFODIALOG_H
#define INFODIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QHBoxLayout>

class InfoDialog : public QDialog {
    Q_OBJECT

    public:
        InfoDialog(QWidget *parent = 0);
        ~InfoDialog();

        void addWidget(QWidget *widget);
        void addLayout(QLayout *layout);
        void addHtml(QString html, int minWidth = 256, int maxWidth = QWIDGETSIZE_MAX);
        void showTemplatesButton();

    public slots:
        void show();

    protected:
        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void mouseReleaseEvent(QMouseEvent *event);
        void cleanupDownloadError();
        static QString TEXTURE_TEMPLATES;

    protected slots:
        void docsClicked();
        void templatesClicked();

    private:
        QHBoxLayout *_controlsLayout;
        QPushButton *_closebutton;
        QPushButton *_docsbutton;
        QPushButton *_templatesbutton;

        QPoint _mouseCoords;
        bool _mouseMoveAllowed;
};

#endif // INFODIALOG_H
