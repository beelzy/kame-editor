// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef AUDIOPICKER_H
#define AUDIOPICKER_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QProgressBar>
#include <QFileDialog>
#include <QSpinBox>
#include <QCheckBox>
#include <QDateTime>

#include "../preview/adpcmplayer.h"
#include "accordionwidget.h"
#include "infoicon.h"
#include "../preview/topview.h"

class AudioPicker : public QWidget {
    Q_OBJECT
    public:
        explicit AudioPicker(QWidget *parent = 0, int mode = 0);
        ~AudioPicker();

        enum class Mode {
            ADPCM,
            WAV
        };

        enum class WavError {
            INVALID,
            WRONGBPS,
            NOWAVERROR
        };

        void setTitle(QString value);
        void setTooltip(QString value);
        void setInfoDialog(QString content, QString title="", int minWidth = 256, int maxWidth=QWIDGETSIZE_MAX);
        bool isMuted();
        void enableLooping(bool value);
        void setAccordionWidget(AccordionWidget *widget);
        QString filepath();
        bool isPlaying();
        int loopstart();
        int loopend();
        bool loopEditEnabled();
        void invokeLoopEdit(bool value);
        void setPlayable(bool value);
        InfoIcon *info();
        int fileSize();
        bool fileChanged();
        bool isLoading();
        void reload();

    signals:
        void stateChanged(bool value);
        void muteChanged(bool value);
        void fileLoaded();
        void mediaEnd();

    public slots:
        void togglePlay();
        void toggleMute();
        void onSelectClicked();
        void loadFile(QString path);
        void stop();
        void acceptBCWAV(bool value);
        void play();
        void pause();
        void reset();

    protected:
        void drawPlay();
        void drawPause();
        void drawStop();
        void drawSpeaker();
        void drawMute();
        void drawWarning();
        void updateDescription(QString text);
        void buildWarningTooltip();

        QPixmap playIcon(QColor color);
        QPixmap pauseIcon(QColor color);
        QPixmap muteIcon(bool disabled = false);

    protected slots:
        void onFileLoaded();
        void onFileError();
        void onStreamError();
        WavError readHeader(QString path);
        void enableControls();
        void disableControls();
        void updateTrackbar(qint64 position);
        void updateDuration(qint64 duration);
        void resizeEvent(QResizeEvent *event);
        void enableLoopEdit(bool state);
        void clearAudio();

    private:
        AdpcmPlayer *_player;

        QLabel *_title;
        QLabel *_path;
        QPushButton *_select;
        QPushButton *_play;
        QPushButton *_stop;
        QPushButton *_mute;
        QPushButton *_reset;
        InfoIcon *_info;
        QProgressBar *_trackbar;
        QStringList _filters;
        QString _dialogtitle;
        QLabel *_description;
        QLabel *_warning;

        Mode _mode;
        QSpinBox *_loopstart;
        QSpinBox *_loopend;
        QCheckBox *_loopenabled;
        int _filesize;
        bool _isBcwav;
        QDateTime _lastChanged;

        bool _playable;
        bool _loading;
};

#endif // AUDIOPICKER_H
