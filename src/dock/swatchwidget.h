// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SWATCHWIDGET_H
#define SWATCHWIDGET_H

#include <QWidget>
#include <QLabel>

#include "QtColorWidgets/color_dialog.hpp"
#include "colorswatch.h"
#include "accordionwidget.h"

class SwatchWidget : public QWidget {
    Q_OBJECT
    public:
        explicit SwatchWidget(QWidget *parent = 0, ColorDialog *dialog = NULL);
        ~SwatchWidget();

        static const char *messages[];
        static QString message(int index);

        void setTitle(QString value);
        QColor color();

        void setShowAlpha(bool value);
        void setAccordionWidget(AccordionWidget *value);
        void setIndicator(QPixmap pixmap, QString tooltip);
        ColorSwatch *swatch();

    private:
        ColorSwatch *_swatch;
        QLabel *_title;
        QLabel *_indicator;
};

#endif // SWATCHWIDGET_H
