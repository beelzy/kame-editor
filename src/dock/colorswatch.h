// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef COLORSWATCH_H
#define COLORSWATCH_H

#include <QAbstractButton>
#include "QtColorWidgets/color_dialog.hpp"

#include "accordionwidget.h"

using namespace color_widgets;

class ColorSwatch : public QAbstractButton {
    Q_OBJECT
    public:
        explicit ColorSwatch(QWidget *parent = 0, ColorDialog *dialog = NULL);
        ~ColorSwatch();

        void setColor(QColor value);
        QColor color();
        void setShowAlpha(bool value);
        void setAccordionWidget(AccordionWidget *value);
        ColorDialog *dialog();

    signals:
        void stateChanged(bool isEnabled);
        void colorChanged(QColor color);

    public slots:
        void selectColor();
        void colorSelected(QColor color);
        void dialogCleanup(int result);

    protected:
        void paintEvent(QPaintEvent *event);

    private:
        QColor _color;
        bool _showAlpha;
        AccordionWidget *_accordionwidget;
        ColorDialog *_dialog;
};

#endif // COLORSWATCH_H
