// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>

#include "barsegment.h"

BarSegment::BarSegment(QWidget *parent, float value, QColor color) : QFrame(parent) {
    _color = color;
    _value = value;
}

void BarSegment::setValue(float value) {
    _value = value;
    repaint();
}

float BarSegment::value() {
    return _value;
}

void BarSegment::paintEvent(QPaintEvent *event) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    p.setBrush(_color);
    p.setPen(Qt::transparent);
    p.drawRect(contentsRect());
}
