// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPushButton>
#include <QListView>
#include <QStandardItemModel>
#include <QMessageBox>

#include "colorwidget.h"
#include "../sourcefile.h"
#include "icons/folder.h"

ColorWidget::ColorWidget(QWidget * parent) : AccordionWidget(parent) {
    _colorNames = QMap<QString, QString>();
    _colorNames["cursor"] = tr("Cursor", "Title for cursor color accordion.");
    _colorNames["folder"] = tr("3D Folder", "Title for 3D Folder accordion.");
    _colorNames["file"] = tr("File Frames", "Title for file frames accordion.");
    _colorNames["arrowbuttonbase"] = tr("Arrow Buttons (Base)", "Title for Arrow Buttons (Base) accordion.");
    _colorNames["arrowbuttonarrow"] = tr("Arrow Buttons (Arrow)", "Title for Arrow Buttons (Arrow) accordion.");
    _colorNames["openclose"] = tr("Open/Close Buttons", "Title for Open/Close Buttons accordion");
    _colorNames["gametext"] = tr("Game Text", "Title for game text accordion");
    _colorNames["folderview"] = tr("Folder View", "Title for folder view accordion.");
    _colorNames["folderbackbutton"] = tr("Folder Back Button", "Title for folder back button accordion.");
    _colorNames["topcorner"] = tr("Top Corner Buttons", "Title for top corner buttons accordion");
    _colorNames["bottomcorner"] = tr("Bottom Corner Buttons", "Title for bottom corner buttons accordion.");
    _colorNames["demotext"] = tr("Demo Text", "Title for demo text accordion.");
    _validPalettes = QMap<int, QVector<QString>>();
    _validPalettes[2] = QVector<QString>() << "demotext";
    _validPalettes[3] = QVector<QString>() << "arrowbuttonarrow";
    _validPalettes[4] = QVector<QString>() << "cursor" << "folder" << "file" << "arrowbuttonbase" << "gametext" << "topcorner" << "folderview";
    _validPalettes[7] = QVector<QString>() << "bottomcorner";

    _palettes = QMap<QString, QMap<QString, QColor>*>();
    _palettes["cursor"] = &SourceFile::cursorcolors;
    _palettes["folder"] = &SourceFile::foldercolors;
    _palettes["file"] = &SourceFile::filecolors;
    _palettes["arrowbuttonbase"] = &SourceFile::arrowbuttonbasecolors;
    _palettes["arrowbuttonarrow"] = &SourceFile::arrowbuttonarrowcolors;
    _palettes["gametext"] = &SourceFile::gametextcolors;
    _palettes["folderview"] = &SourceFile::folderviewcolors;
    _palettes["topcorner"] = &SourceFile::topcornercolors;
    _palettes["bottomcorner"] = &SourceFile::bottomcornercolors;
    _palettes["demotext"] = &SourceFile::demotextcolors;

    _enabledFlags = QMap<QString, bool*>();
    _enabledFlags["cursor"] = &SourceFile::cursorcolorEnabled;
    _enabledFlags["folder"] = &SourceFile::foldercolorEnabled;
    _enabledFlags["file"] = &SourceFile::filecolorEnabled;
    _enabledFlags["arrowbuttonbase"] = &SourceFile::arrowbuttonbasecolorEnabled;
    _enabledFlags["arrowbuttonarrow"] = &SourceFile::arrowbuttonarrowcolorEnabled;
    _enabledFlags["gametext"] = &SourceFile::gametextcolorEnabled;
    _enabledFlags["folderview"] = &SourceFile::folderviewcolorEnabled;
    _enabledFlags["topcorner"] = &SourceFile::topcornercolorEnabled;
    _enabledFlags["bottomcorner"] = &SourceFile::bottomcornercolorEnabled;
    _enabledFlags["demotext"] = &SourceFile::demotextcolorEnabled;
    _enabledFlags["folderbackbutton"] = &SourceFile::folderbackbuttoncolorEnabled;
    _enabledFlags["openclose"] = &SourceFile::openclosecolorEnabled;

    _warning = new Warning();
    _unknown = new UnknownIcon();

    _sliders = QMap<QString, KameSlider*>();
    _swatches = QMap<QString, QMap<QString, SwatchWidget*>>();

    ColorDialog *dialog = new ColorDialog();

    addPalette();
    addCursorColors(dialog);
    addFolderColors(dialog);
    addFileColors(dialog);
    addGameTextColors(dialog);
    addDemoTextColors(dialog);
    addArrowButtonBaseColors(dialog);
    addArrowButtonArrowColors(dialog);
    addOpenCloseColors(dialog);
    addFolderViewColors(dialog);
    addFolderBackButtonColors(dialog);
    addTopCornerButtonColors(dialog);
    addBottomCornerButtonColors(dialog);

    reset();
}

ColorWidget::~ColorWidget() {
    delete _warning;
    delete _unknown;
    delete _palette;
    delete _palettelabel;
    delete _assignOptions;
    delete _applyPalette;

    for(auto map : _swatches.keys()) {
        qDeleteAll(_swatches[map]);
    }
    _swatches.clear();

    qDeleteAll(_sliders);
}

ColorSwatch *ColorWidget::colorSwatch(QString layer, QString name) {
    return _swatches[layer][name]->swatch();
}

KameSlider *ColorWidget::slider(QString name) {
    return _sliders[name];
}

void ColorWidget::load() {
    bool showDefault = true;

    for (auto swatch : _swatches["cursor"].keys()) {
        if (SourceFile::cursorcolors[swatch].isValid()) {
            showDefault = !SourceFile::cursorcolorEnabled;
            _swatches["cursor"][swatch]->swatch()->setColor(SourceFile::cursorcolors[swatch]);
        } else {
            _swatches["cursor"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    SourceFile::cursorcolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["cursor"])->setIconVisibility(!showDefault);

    emit cursorColorChanged(showDefault);

    showDefault = true;
    for (auto swatch : _swatches["folder"].keys()) {
        if (SourceFile::foldercolors[swatch].isValid()) {
            showDefault = !SourceFile::foldercolorEnabled;
            _swatches["folder"][swatch]->swatch()->setColor(SourceFile::foldercolors[swatch]);
        } else {
            _swatches["folder"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    SourceFile::foldercolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["folder"])->setIconVisibility(!showDefault);

    emit folderColorChanged(showDefault);

    showDefault = true;
    for (auto swatch : _swatches["file"].keys()) {
        if (SourceFile::filecolors[swatch].isValid()) {
            showDefault = !SourceFile::filecolorEnabled;
            _swatches["file"][swatch]->swatch()->setColor(SourceFile::filecolors[swatch]);
        } else {
            _swatches["file"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    SourceFile::filecolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["file"])->setIconVisibility(!showDefault);

    emit fileColorChanged(showDefault);

    // Prevent signals from overwriting these
    float openshadowpos = SourceFile::opencolors["textshadowpos"].toFloat();
    float closeshadowpos = SourceFile::closecolors["textshadowpos"].toFloat();
    showDefault = true;
    for (auto swatch : _swatches["open"].keys()) {
        if (qvariant_cast<QColor>(SourceFile::opencolors[swatch]).isValid()) {
            showDefault = !SourceFile::openclosecolorEnabled;
            _swatches["open"][swatch]->swatch()->setColor(qvariant_cast<QColor>(SourceFile::opencolors[swatch]));
        } else {
            _swatches["open"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    for (auto swatch : _swatches["close"].keys()) {
        if (qvariant_cast<QColor>(SourceFile::closecolors[swatch]).isValid()) {
            showDefault = !SourceFile::openclosecolorEnabled;
            _swatches["close"][swatch]->swatch()->setColor(qvariant_cast<QColor>(SourceFile::closecolors[swatch]));
        } else {
            _swatches["close"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    _sliders["open"]->setValue((openshadowpos + 10) / 20.0);
    _sliders["close"]->setValue((closeshadowpos + 10) / 20.0);

    SourceFile::openclosecolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["openclose"])->setIconVisibility(!showDefault);

    emit opencloseColorChanged(showDefault);

    float shadowpos = SourceFile::folderbackbuttoncolors["textshadowpos"].toFloat();
    showDefault = true;
    for (auto swatch : _swatches["folderbackbutton"].keys()) {
        if (qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors[swatch]).isValid()) {
            showDefault = !SourceFile::folderbackbuttoncolorEnabled;
            _swatches["folderbackbutton"][swatch]->swatch()->setColor(qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors[swatch]));
        } else {
            _swatches["folderbackbutton"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    _sliders["folderbackbutton"]->setValue((shadowpos + 10) / 20.0);

    SourceFile::folderbackbuttoncolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["folderbackbutton"])->setIconVisibility(!showDefault);

    emit folderbackbuttonColorChanged(showDefault);

    showDefault = true;
    for (auto swatch : _swatches["arrowbuttonbase"].keys()) {
        if (SourceFile::arrowbuttonbasecolors[swatch].isValid()) {
            showDefault = !SourceFile::arrowbuttonbasecolorEnabled;
            _swatches["arrowbuttonbase"][swatch]->swatch()->setColor(SourceFile::arrowbuttonbasecolors[swatch]);
        } else {
            _swatches["arrowbuttonbase"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    SourceFile::arrowbuttonbasecolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["arrowbuttonbase"])->setIconVisibility(!showDefault);

    emit arrowbuttonbaseColorChanged(showDefault);

    showDefault = true;
    for (auto swatch : _swatches["arrowbuttonarrow"].keys()) {
        if (SourceFile::arrowbuttonarrowcolors[swatch].isValid()) {
            showDefault = !SourceFile::arrowbuttonarrowcolorEnabled;
            _swatches["arrowbuttonarrow"][swatch]->swatch()->setColor(SourceFile::arrowbuttonarrowcolors[swatch]);
        } else {
            _swatches["arrowbuttonarrow"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    SourceFile::arrowbuttonarrowcolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["arrowbuttonarrow"])->setIconVisibility(!showDefault);

    emit arrowbuttonarrowColorChanged(showDefault);

    showDefault = true;
    for (auto swatch : _swatches["bottomcorner"].keys()) {
        if (SourceFile::bottomcornercolors[swatch].isValid()) {
            showDefault = !SourceFile::bottomcornercolorEnabled;
            _swatches["bottomcorner"][swatch]->swatch()->setColor(SourceFile::bottomcornercolors[swatch]);
        } else {
            _swatches["bottomcorner"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    SourceFile::bottomcornercolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["bottomcorner"])->setIconVisibility(!showDefault);

    emit bottomcornerColorChanged(showDefault);

    showDefault = true;
    for (auto swatch : _swatches["topcorner"].keys()) {
        if (SourceFile::topcornercolors[swatch].isValid()) {
            showDefault = !SourceFile::topcornercolorEnabled;
            _swatches["topcorner"][swatch]->swatch()->setColor(SourceFile::topcornercolors[swatch]);
        } else {
            _swatches["topcorner"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    SourceFile::topcornercolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["topcorner"])->setIconVisibility(!showDefault);

    emit topcornerColorChanged(showDefault);

    showDefault = true;
    for (auto swatch : _swatches["folderview"].keys()) {
        if (SourceFile::folderviewcolors[swatch].isValid()) {
            showDefault = !SourceFile::folderviewcolorEnabled;
            _swatches["folderview"][swatch]->swatch()->setColor(SourceFile::folderviewcolors[swatch]);
        } else {
            _swatches["folderview"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    SourceFile::folderviewcolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["folderview"])->setIconVisibility(!showDefault);

    emit folderviewColorChanged(showDefault);

    showDefault = true;
    for (auto swatch : _swatches["gametext"].keys()) {
        if (SourceFile::gametextcolors[swatch].isValid()) {
            showDefault = !SourceFile::gametextcolorEnabled;
            _swatches["gametext"][swatch]->swatch()->setColor(SourceFile::gametextcolors[swatch]);
        } else {
            _swatches["gametext"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    SourceFile::gametextcolorEnabled = !showDefault;
    Qt::CheckState state = SourceFile::gametextcolorHidden ? Qt::Unchecked : showDefault ? Qt::PartiallyChecked : Qt::Checked;
    _accordion->getContentPane(_accordionindex["gametext"])->setIconState(state);

    emit gametextColorChanged(state);

    showDefault = true;
    for (auto swatch : _swatches["demotext"].keys()) {
        if (SourceFile::demotextcolors[swatch].isValid()) {
            showDefault = !SourceFile::demotextcolorEnabled;
            _swatches["demotext"][swatch]->swatch()->setColor(SourceFile::demotextcolors[swatch]);
        } else {
            _swatches["demotext"][swatch]->swatch()->setColor(QColor(Qt::transparent));
        }
    }
    SourceFile::demotextcolorEnabled = !showDefault;
    _accordion->getContentPane(_accordionindex["demotext"])->setIconVisibility(!showDefault);

    emit demotextColorChanged(showDefault);

    SourceFile::recentlyModified = false;
}

void ColorWidget::reset() {
    for (auto swatch : _swatches["cursor"].keys()) {
        _swatches["cursor"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    _accordion->getContentPane(_accordionindex["cursor"])->setIconVisibility(false);
    emit cursorColorChanged(true);

    for (auto swatch : _swatches["folder"].keys()) {
        _swatches["folder"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    _accordion->getContentPane(_accordionindex["folder"])->setIconVisibility(false);
    emit folderColorChanged(true);

    for (auto swatch : _swatches["file"].keys()) {
        _swatches["file"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    _accordion->getContentPane(_accordionindex["file"])->setIconVisibility(false);
    emit fileColorChanged(true);

    for (auto swatch : _swatches["open"].keys()) {
        _swatches["open"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    for (auto swatch : _swatches["close"].keys()) {
        _swatches["close"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }
    _sliders["open"]->setValue(0.5);
    _sliders["close"]->setValue(0.5);

    _accordion->getContentPane(_accordionindex["openclose"])->setIconVisibility(false);
    emit opencloseColorChanged(true);

    for (auto swatch : _swatches["folderbackbutton"].keys()) {
        _swatches["folderbackbutton"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }
    _sliders["folderbackbutton"]->setValue(0.5);

    _accordion->getContentPane(_accordionindex["folderbackbutton"])->setIconVisibility(false);
    emit folderbackbuttonColorChanged(true);

    for (auto swatch : _swatches["arrowbuttonbase"].keys()) {
        _swatches["arrowbuttonbase"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    _accordion->getContentPane(_accordionindex["arrowbuttonbase"])->setIconVisibility(false);
    emit arrowbuttonbaseColorChanged(true);

    for (auto swatch : _swatches["arrowbuttonarrow"].keys()) {
        _swatches["arrowbuttonarrow"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    _accordion->getContentPane(_accordionindex["arrowbuttonarrow"])->setIconVisibility(false);
    emit arrowbuttonarrowColorChanged(true);

    for (auto swatch : _swatches["bottomcorner"].keys()) {
        _swatches["bottomcorner"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    _accordion->getContentPane(_accordionindex["bottomcorner"])->setIconVisibility(false);
    emit bottomcornerColorChanged(true);

    for (auto swatch : _swatches["topcorner"].keys()) {
        _swatches["topcorner"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    _accordion->getContentPane(_accordionindex["topcorner"])->setIconVisibility(false);
    emit topcornerColorChanged(true);

    for (auto swatch : _swatches["folderview"].keys()) {
        _swatches["folderview"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    _accordion->getContentPane(_accordionindex["folderview"])->setIconVisibility(false);
    emit folderviewColorChanged(true);

    for (auto swatch : _swatches["gametext"].keys()) {
        _swatches["gametext"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    _accordion->getContentPane(_accordionindex["gametext"])->setIconState(Qt::PartiallyChecked);
    emit gametextColorChanged(1);

    for (auto swatch : _swatches["demotext"].keys()) {
        _swatches["demotext"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    _accordion->getContentPane(_accordionindex["demotext"])->setIconVisibility(false);
    emit demotextColorChanged(true);

    SourceFile::recentlyModified = false;
}

void ColorWidget::loadPalette() {
    QFileDialog palettedialog;
    createDialog(&palettedialog);
    palettedialog.setAcceptMode(QFileDialog::AcceptOpen);
    palettedialog.setWindowTitle(tr("Load Palette File...", "Dialog title when choosing to load a color palette."));

    if (palettedialog.exec()) {
        QString path = palettedialog.selectedFiles().first();

        QString title = tr("Failed to load palette", "Title for loading palette error message");
        QString message = tr("Not a valid palette. The file you have chosen is either corrupted or does not contain valid palette data.");
        QMessageBox msgBox(QMessageBox::Warning, title, message, QFlag(0), _palette);
        msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);

        _assignOptions->clear();
        _applyPalette->setEnabled(false);
        QFile file(path);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            msgBox.exec();
            return;
        }

        _palette->clear();

        while (!file.atEnd()) {
            QByteArray line = file.readLine();
            QColor color = QColor(QString(line).trimmed());
            if (color.isValid()) {
                _palette->addSwatch(color);
            } else {
                msgBox.exec();
                _palette->clear();
                _palettelabel->setText((tr("(No file selected)", "Palette file path text.")));
                return;
            }
        }

        _palettelabel->setText(path);
        int size = _palette->model()->rowCount();
        if (_validPalettes.contains(size)) {
            QVector<QString> validPalette = _validPalettes[size];
            for (int i = 0; i < validPalette.size(); i++) {
                _assignOptions->addItem(_colorNames[validPalette[i]], validPalette[i]);
            }
            _applyPalette->setEnabled(true);
        } else if (size == 8) {
            _assignOptions->addItem(_colorNames["folderbackbutton"], "folderbackbutton");
            _applyPalette->setEnabled(true);
        } else if (size == 16) {
            _assignOptions->addItem(_colorNames["openclose"], "openclose");
            _applyPalette->setEnabled(true);
        } else {
            _assignOptions->addItem(tr("(Not applicable for this palette)", "Combo box warning that the selected palette does not apply to any color sets."));
        }
    }
}

void ColorWidget::applyPalette() {
    QString key = _assignOptions->currentData().toString();

    bool *flag = _enabledFlags[key];
    *flag = true;
    if (key == "openclose") {
        QMap<QString, QColor> open = SourceFile::toQColorMap(SourceFile::opencolors);
        QMap<QString, QColor> close = SourceFile::toQColorMap(SourceFile::closecolors);

        int index = applyPaletteColors(&open);
        applyPaletteColors(&close, index);

        SourceFile::toQVariantMap(open, &SourceFile::opencolors);
        SourceFile::toQVariantMap(close, &SourceFile::closecolors);
    } else {
        QMap<QString, QColor> buttoncolor = SourceFile::toQColorMap(SourceFile::folderbackbuttoncolors);
        QMap<QString, QColor> *colors = _palettes.contains(key) ? _palettes[key] : &buttoncolor;

        applyPaletteColors(colors);

        if (key == "folderbackbutton") {
            SourceFile::toQVariantMap(*colors, &SourceFile::folderbackbuttoncolors);
        }
    }

    load();

    SourceFile::recentlyModified = true;
}

void ColorWidget::paletteQuickLoad() {
    loadPalette();

    int index = _assignOptions->findData(sender()->objectName());
    if (index >= 0) {
        _assignOptions->setCurrentIndex(index);
        applyPalette();
    }
}

void ColorWidget::savePalette() {
    QFileDialog palettedialog;
    createDialog(&palettedialog);
    palettedialog.setAcceptMode(QFileDialog::AcceptSave);
    palettedialog.setWindowTitle(tr("Save Palette...", "Dialog title when saving palette colors."));

    if (palettedialog.exec()) {
        QString path = palettedialog.selectedFiles().first();
        QFile file(path);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            return;
        }

        QTextStream out(&file);
        if (sender()->objectName() == "openclose") {
            savePaletteFile(&out, "open");
            savePaletteFile(&out, "close");
        } else {
            savePaletteFile(&out, sender()->objectName());
        }
    }
}

void ColorWidget::savePaletteFile(QTextStream *out, QString swatch) {
    QMap<QString, SwatchWidget*> swatches = _swatches[swatch];
    if (swatches.contains("dark")) {
        *out << swatches["dark"]->color().name() << "\n";
    }
    if (swatches.contains("main")) {
        *out << swatches["main"]->color().name() << "\n";
    }
    if (swatches.contains("light")) {
        *out << swatches["light"]->color().name() << "\n";
    }
    if (swatches.contains("glow")) {
        *out << swatches["glow"]->color().name() << "\n";
    }
    if (swatches.contains("shadow")) {
        *out << swatches["shadow"]->color().name() << "\n";
    }
    if (swatches.contains("textshadow")) {
        *out << swatches["textshadow"]->color().name() << "\n";
    }
    if (swatches.contains("textmain")) {
        *out << swatches["textmain"]->color().name() << "\n";
    }
    if (swatches.contains("textselected")) {
        *out << swatches["textselected"]->color().name() << "\n";
    }
    if (swatches.contains("iconmain")) {
        *out << swatches["iconmain"]->color().name() << "\n";
    }
    if (swatches.contains("iconlight")) {
        *out << swatches["iconlight"]->color().name() << "\n";
    }
    if (swatches.contains("icontextmain")) {
        *out << swatches["icontextmain"]->color().name() << "\n";
    }
    if (swatches.contains("border")) {
        *out << swatches["border"]->color().name() << "\n";
    }
    if (swatches.contains("unpressed")) {
        *out << swatches["unpressed"]->color().name() << "\n";
    }
    if (swatches.contains("pressed")) {
        *out << swatches["pressed"]->color().name() << "\n";
    }
}

int ColorWidget::applyPaletteColors(QMap<QString, QColor> *colors, int offset) {
    int index = offset;
    if (colors->contains("dark")) {
        (*colors)["dark"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("main")) {
        (*colors)["main"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("light")) {
        (*colors)["light"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("glow")) {
        (*colors)["glow"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("shadow")) {
        (*colors)["shadow"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("textshadow")) {
        (*colors)["textshadow"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("textmain")) {
        (*colors)["textmain"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("textselected")) {
        (*colors)["textselected"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("iconmain")) {
        (*colors)["iconmain"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("iconlight")) {
        (*colors)["iconlight"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("icontextmain")) {
        (*colors)["icontextmain"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("border")) {
        (*colors)["border"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("unpressed")) {
        (*colors)["unpressed"] = _palette->swatch(index);
        index++;
    }
    if (colors->contains("pressed")) {
        (*colors)["pressed"] = _palette->swatch(index);
        index++;
    }

    return index;
}

void ColorWidget::saveCursorColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["cursor"].keys()) {
            SourceFile::cursorcolors[swatch] = _swatches["cursor"][swatch]->color();
            SourceFile::cursorcolors[swatch].setAlphaF(1);
        }
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["cursor"])->setIconVisibility(true);
    emit cursorColorChanged(false);
}

void ColorWidget::saveFolderColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["folder"].keys()) {
            SourceFile::foldercolors[swatch] = _swatches["folder"][swatch]->color();
            SourceFile::foldercolors[swatch].setAlphaF(1);
        }
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["folder"])->setIconVisibility(true);
    emit folderColorChanged(false);
}

void ColorWidget::saveFileColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["file"].keys()) {
            SourceFile::filecolors[swatch] = _swatches["file"][swatch]->color();
            if (swatch != "shadow") {
                SourceFile::filecolors[swatch].setAlphaF(1);
            }
        }
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["file"])->setIconVisibility(true);
    emit fileColorChanged(false);
}

void ColorWidget::saveOpenCloseColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["open"].keys()) {
            QColor newcolor = _swatches["open"][swatch]->color();
            if (swatch != "shadow") {
                newcolor.setAlphaF(1);
            }
            SourceFile::opencolors[swatch] = newcolor;
        }
        for (auto swatch : _swatches["close"].keys()) {
            QColor newcolor = _swatches["close"][swatch]->color();
            if (swatch != "shadow") {
                newcolor.setAlphaF(1);
            }
            SourceFile::closecolors[swatch] = newcolor;
        }
        SourceFile::opencolors["textshadowpos"] = _sliders["open"]->value() * 20 - 10;
        SourceFile::closecolors["textshadowpos"] = _sliders["close"]->value() * 20 - 10;
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["openclose"])->setIconVisibility(true);
    emit opencloseColorChanged(false);
}

void ColorWidget::saveFolderBackButtonColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["folderbackbutton"].keys()) {
            QColor newcolor = _swatches["folderbackbutton"][swatch]->color();
            if (swatch != "shadow") {
                newcolor.setAlphaF(1);
            }
            SourceFile::folderbackbuttoncolors[swatch] = newcolor;
        }
        SourceFile::folderbackbuttoncolors["textshadowpos"] = _sliders["folderbackbutton"]->value() * 20 - 10;
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["folderbackbutton"])->setIconVisibility(true);
    emit folderbackbuttonColorChanged(false);
}

void ColorWidget::saveArrowButtonBaseColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["arrowbuttonbase"].keys()) {
            SourceFile::arrowbuttonbasecolors[swatch] = _swatches["arrowbuttonbase"][swatch]->color();
            if (swatch != "shadow") {
                SourceFile::arrowbuttonbasecolors[swatch].setAlphaF(1);
            }
        }
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["arrowbuttonbase"])->setIconVisibility(true);
    emit arrowbuttonbaseColorChanged(false);
}

void ColorWidget::saveArrowButtonArrowColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["arrowbuttonarrow"].keys()) {
            SourceFile::arrowbuttonarrowcolors[swatch] = _swatches["arrowbuttonarrow"][swatch]->color();
            if (swatch != "shadow") {
                SourceFile::arrowbuttonarrowcolors[swatch].setAlphaF(1);
            }
        }
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["arrowbuttonarrow"])->setIconVisibility(true);
    emit arrowbuttonarrowColorChanged(false);
}

void ColorWidget::saveBottomCornerColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["bottomcorner"].keys()) {
            SourceFile::bottomcornercolors[swatch] = _swatches["bottomcorner"][swatch]->color();
            SourceFile::bottomcornercolors[swatch].setAlphaF(1);
        }
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["bottomcorner"])->setIconVisibility(true);
    emit bottomcornerColorChanged(false);
}

void ColorWidget::saveTopCornerColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["topcorner"].keys()) {
            SourceFile::topcornercolors[swatch] = _swatches["topcorner"][swatch]->color();
            SourceFile::topcornercolors[swatch].setAlphaF(1);
        }
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["topcorner"])->setIconVisibility(true);
    emit topcornerColorChanged(false);
}

void ColorWidget::saveFolderViewColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["folderview"].keys()) {
            SourceFile::folderviewcolors[swatch] = _swatches["folderview"][swatch]->color();
            if (swatch != "shadow") {
                SourceFile::folderviewcolors[swatch].setAlphaF(1);
            }
        }
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["folderview"])->setIconVisibility(true);
    emit folderviewColorChanged(false);
}

void ColorWidget::saveGameTextColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["gametext"].keys()) {
            SourceFile::gametextcolors[swatch] = _swatches["gametext"][swatch]->color();
            if (swatch != "shadow") {
                SourceFile::gametextcolors[swatch].setAlphaF(1);
            }
        }
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["gametext"])->setIconVisibility(true);

    emit gametextColorChanged(SourceFile::gametextcolorHidden ? 0 : 2);
}

void ColorWidget::saveDemoTextColor(bool value) {
    if (value) {
        for (auto swatch : _swatches["demotext"].keys()) {
            SourceFile::demotextcolors[swatch] = _swatches["demotext"][swatch]->color();
            if (swatch != "shadow") {
                SourceFile::demotextcolors[swatch].setAlphaF(1);
            }
        }
        SourceFile::recentlyModified = true;
    }
    _accordion->getContentPane(_accordionindex["demotext"])->setIconVisibility(true);
    emit demotextColorChanged(false);
}

void ColorWidget::cursorVisibilityChanged(bool visible) {
    SourceFile::cursorcolorEnabled = visible;
    emit cursorColorChanged(!visible);
}

void ColorWidget::folderVisibilityChanged(bool visible) {
    SourceFile::foldercolorEnabled = visible;
    emit folderColorChanged(!visible);
}

void ColorWidget::fileVisibilityChanged(bool visible) {
    SourceFile::filecolorEnabled = visible;
    emit fileColorChanged(!visible);
}

void ColorWidget::opencloseVisibilityChanged(bool visible) {
    SourceFile::openclosecolorEnabled = visible;
    emit opencloseColorChanged(!visible);
}

void ColorWidget::folderbackbuttonVisibilityChanged(bool visible) {
    SourceFile::folderbackbuttoncolorEnabled = visible;
    emit folderbackbuttonColorChanged(!visible);
}

void ColorWidget::arrowButtonBaseVisibilityChanged(bool visible) {
    SourceFile::arrowbuttonbasecolorEnabled = visible;
    emit arrowbuttonbaseColorChanged(!visible);
}

void ColorWidget::arrowButtonArrowVisibilityChanged(bool visible) {
    SourceFile::arrowbuttonarrowcolorEnabled = visible;
    emit arrowbuttonarrowColorChanged(!visible);
}

void ColorWidget::bottomCornerVisibilityChanged(bool visible) {
    SourceFile::bottomcornercolorEnabled = visible;
    emit bottomcornerColorChanged(!visible);
}

void ColorWidget::topCornerVisibilityChanged(bool visible) {
    SourceFile::topcornercolorEnabled = visible;
    emit topcornerColorChanged(!visible);
}

void ColorWidget::folderViewVisibilityChanged(bool visible) {
    SourceFile::folderviewcolorEnabled = visible;
    emit folderviewColorChanged(!visible);
}

void ColorWidget::gameTextVisibilityChanged(int state) {
    SourceFile::gametextcolorEnabled = state == Qt::Checked;
    SourceFile::gametextcolorHidden = state == Qt::Unchecked;
    emit gametextColorChanged(state);
}

void ColorWidget::demoTextVisibilityChanged(bool visible) {
    SourceFile::demotextcolorEnabled = visible;
    emit demotextColorChanged(!visible);
}

void ColorWidget::createDialog(QFileDialog *dialog) {
    dialog->setDefaultSuffix("pal");
    dialog->setNameFilters(QStringList() << tr("Palette Files (*.pal)"));
}

void ColorWidget::addPalette() {
    _palettelabel = new QLabel(tr("(No file selected)", "Palette file path text."));
    QPushButton *select = new QPushButton();
    Folder *folder = new Folder();
    QPixmap pixmap = folder->icon();
    select->setIcon(pixmap);
    select->setIconSize(pixmap.rect().size());
    select->setObjectName("select");
    select->setStyleSheet(folder->selectStyle());
    select->setCursor(Qt::PointingHandCursor);
    QHBoxLayout *pathlayout = new QHBoxLayout();
    pathlayout->addWidget(_palettelabel, 1);
    pathlayout->addWidget(select);

    _palette = new PaletteView();

    QLabel *assign = new QLabel(tr("Assign to:", "Assign to palette text."));
    _assignOptions = new QComboBox();

    QHBoxLayout *assignLayout = new QHBoxLayout();
    assignLayout->addWidget(assign);
    assignLayout->addWidget(_assignOptions, 1);

    _applyPalette = new QPushButton(tr("Apply", "palette accordion apply button text"));
    _applyPalette->setEnabled(false);

    int index = createGroup(_accordion, tr("Palette Management", "Title for palette accordion."), tr("Color palette management.", "Tooltip for palette accordion."), false);
    QFrame *palette = getContent(_accordion, index);
    dynamic_cast<QVBoxLayout*>(palette->layout())->addLayout(pathlayout);
    palette->layout()->addWidget(_palette);
    dynamic_cast<QVBoxLayout*>(palette->layout())->addLayout(assignLayout);
    palette->layout()->addWidget(_applyPalette);
    _accordion->getContentPane(index)->setMaximumHeight(palette->sizeHint().height());
    delete folder;
    connect(select, &QPushButton::clicked, this, &ColorWidget::loadPalette);
    connect(_applyPalette, &QPushButton::clicked, this, &ColorWidget::applyPalette);
}

void ColorWidget::addCursorColors(ColorDialog *dialog) {
    SwatchWidget *dark = new SwatchWidget(0, dialog);
    dark->setTitle(tr("Dark", "Cursor dark color title"));
    dark->setAccordionWidget(this);
    SwatchWidget *main = new SwatchWidget(0, dialog);
    main->setTitle(tr("Main", "Cursor main color title"));
    main->setAccordionWidget(this);
    SwatchWidget *light = new SwatchWidget(0, dialog);
    light->setTitle(tr("Light", "Cursor light color title"));
    light->setAccordionWidget(this);
    SwatchWidget *glow = new SwatchWidget(0, dialog);
    glow->setTitle(tr("Glow", "Cursor glow color title"));
    glow->setAccordionWidget(this);

    int index = createGroup(_accordion, _colorNames["cursor"], tr("Colors for the bottom screen cursor.", "Tooltip for cursor color accordion."), true, true);
    _accordionindex["cursor"] = index;
    QFrame *cursor = getContent(_accordion, index);
    cursor->layout()->addWidget(dark);
    cursor->layout()->addWidget(main);
    cursor->layout()->addWidget(light);
    cursor->layout()->addWidget(glow);
    _accordion->getContentPane(index)->setMaximumHeight(cursor->sizeHint().height());
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("cursor");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("cursor");

    QMap<QString, SwatchWidget*> cursormap;
    cursormap["dark"] = dark;
    cursormap["main"] = main;
    cursormap["light"] = light;
    cursormap["glow"] = glow;

    _swatches["cursor"] = cursormap;

    connect(dark->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveCursorColor);
    connect(main->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveCursorColor);
    connect(light->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveCursorColor);
    connect(glow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveCursorColor);
    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::cursorVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addFolderColors(ColorDialog *dialog) {
    SwatchWidget *dark = new SwatchWidget(0, dialog);
    dark->setTitle(tr("Dark", "3D folder dark color title"));
    dark->setAccordionWidget(this);
    SwatchWidget *main = new SwatchWidget(0, dialog);
    main->setTitle(tr("Main", "3D folder main color title"));
    main->setAccordionWidget(this);
    SwatchWidget *light = new SwatchWidget(0, dialog);
    light->setTitle(tr("Light", "3D folder light color title"));
    light->setAccordionWidget(this);
    SwatchWidget *shadow = new SwatchWidget(0, dialog);
    shadow->setTitle(tr("Shadow", "3D folder shadow color title"));
    shadow->setIndicator(_warning->icon(), SwatchWidget::message(0));
    shadow->setAccordionWidget(this);

    int index = createGroup(_accordion, _colorNames["folder"], tr("Colors for the 3D folder in the top screen. Click for more details.", "Tooltip for 3D folder accordion."), true, true);
    _accordionindex["folder"] = index;
    QFrame *folder = getContent(_accordion, index);
    folder->layout()->addWidget(dark);
    folder->layout()->addWidget(main);
    folder->layout()->addWidget(light);
    folder->layout()->addWidget(shadow);
    _accordion->getContentPane(index)->setMaximumHeight(folder->sizeHint().height());
    //: 3D folder color detailed help
    _accordion->getContentPane(index)->setHeaderInfoDialog("<h3>" + tr("3D Folder Colors", "Title for 3D folder color detailed help") + "</h3><p><img src=\":resources/docs/3dfolder.png\"></img></p><ol><li>"
            + tr("Top Screen Preview") + "</li>"
                "<li>" + tr("Default Folder Colors (Only visible when folder textures are not used)") + "</li>"
                "<li>" + tr("Opened Folder Colors (Only visible when folder textures are not used)") + "</li></ol>", tr("3D Folder Colors Help", "Window title for 3D folder colors detailed help"));
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("folder");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("folder");

    QMap<QString, SwatchWidget*> foldermap;
    foldermap["dark"] = dark;
    foldermap["main"] = main;
    foldermap["light"] = light;
    foldermap["shadow"] = shadow;

    _swatches["folder"] = foldermap;

    connect(dark->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderColor);
    connect(main->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderColor);
    connect(light->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderColor);
    connect(shadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderColor);
    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::folderVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addFileColors(ColorDialog *dialog) {
    SwatchWidget *dark = new SwatchWidget(0, dialog);
    dark->setTitle(tr("Dark", "File dark color title"));
    dark->setAccordionWidget(this);
    SwatchWidget *main = new SwatchWidget(0, dialog);
    main->setTitle(tr("Main", "File main color title"));
    main->setAccordionWidget(this);
    SwatchWidget *light = new SwatchWidget(0, dialog);
    light->setTitle(tr("Light", "File light color title"));
    light->setAccordionWidget(this);
    SwatchWidget *shadow = new SwatchWidget(0, dialog);
    shadow->setTitle(tr("Shadow", "File shadow color title"));
    shadow->setAccordionWidget(this);
    shadow->setShowAlpha(true);

    int index = createGroup(_accordion, _colorNames["file"], tr("Colors for the frame around the file icons in the bottom screen. Click for more details.", "Tooltip for file frames accordion."), true, true);
    _accordionindex["file"] = index;
    QFrame *file = getContent(_accordion, index);
    file->layout()->addWidget(dark);
    file->layout()->addWidget(main);
    file->layout()->addWidget(light);
    file->layout()->addWidget(shadow);
    _accordion->getContentPane(index)->setMaximumHeight(file->sizeHint().height());
    //: File frames color detailed help
    _accordion->getContentPane(index)->setHeaderInfoDialog("<h3>" + tr("File Frames Colors", "Title for file frames color detailed help") + "</h3><p><img src=\":resources/docs/fileframes.png\"></img></p><ol><li>" + tr(
                "Top Screen Preview") + "</li>"
                "<li>" + tr("Folder File Colors (Default and Opened)") + "</li>"
                "<li>" + tr("File Frame Colors (Only visible on zoom levels where the corresponding large or small file textures are not used)") + "</li></ol>", tr("File Frames Colors Help", "Window title for file frames colors detailed help"));
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("file");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("file");

    QMap<QString, SwatchWidget*> filemap;
    filemap["dark"] = dark;
    filemap["main"] = main;
    filemap["light"] = light;
    filemap["shadow"] = shadow;

    _swatches["file"] = filemap;

    connect(dark->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFileColor);
    connect(main->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFileColor);
    connect(light->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFileColor);
    connect(shadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFileColor);
    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::fileVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addArrowButtonBaseColors(ColorDialog *dialog) {
    SwatchWidget *dark = new SwatchWidget(0, dialog);
    dark->setTitle(tr("Dark", "Arrow button base dark color title"));
    dark->setAccordionWidget(this);
    SwatchWidget *main = new SwatchWidget(0, dialog);
    main->setTitle(tr("Main", "Arrow button base main color title"));
    main->setAccordionWidget(this);
    SwatchWidget *light = new SwatchWidget(0, dialog);
    light->setTitle(tr("Light", "Arrow button base light color title"));
    light->setAccordionWidget(this);
    SwatchWidget *shadow = new SwatchWidget(0, dialog);
    shadow->setTitle(tr("Shadow", "Arrow button base shadow color title"));
    shadow->setAccordionWidget(this);
    shadow->setShowAlpha(true);
    shadow->setIndicator(_warning->icon(), SwatchWidget::message(0));
    
    int index = createGroup(_accordion, _colorNames["arrowbuttonbase"], tr("Background colors for the arrow buttons in the bottom screen.", "Tooltip for Arrow Buttons (Base) accordion."), true, true);
    _accordionindex["arrowbuttonbase"] = index;
    QFrame *arrowbutton = getContent(_accordion, index);
    arrowbutton->layout()->addWidget(dark);
    arrowbutton->layout()->addWidget(main);
    arrowbutton->layout()->addWidget(light);
    arrowbutton->layout()->addWidget(shadow);
    _accordion->getContentPane(index)->setMaximumHeight(arrowbutton->sizeHint().height());
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("arrowbuttonbase");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("arrowbuttonbase");

    QMap<QString, SwatchWidget*> arrowbuttonmap;
    arrowbuttonmap["dark"] = dark;
    arrowbuttonmap["main"] = main;
    arrowbuttonmap["light"] = light;
    arrowbuttonmap["shadow"] = shadow;

    _swatches["arrowbuttonbase"] = arrowbuttonmap;

    connect(dark->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveArrowButtonBaseColor);
    connect(main->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveArrowButtonBaseColor);
    connect(light->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveArrowButtonBaseColor);
    connect(shadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveArrowButtonBaseColor);
    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::arrowButtonBaseVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addArrowButtonArrowColors(ColorDialog *dialog) {
    SwatchWidget *border = new SwatchWidget(0, dialog);
    border->setTitle(tr("Border", "Border color title"));
    border->setAccordionWidget(this);
    SwatchWidget *unpressed = new SwatchWidget(0, dialog);
    unpressed->setTitle(tr("Unpressed", "Unpressed color title"));
    unpressed->setAccordionWidget(this);
    SwatchWidget *pressed = new SwatchWidget(0, dialog);
    pressed->setTitle(tr("Pressed", "Pressed color title"));
    pressed->setAccordionWidget(this);

    int index = createGroup(_accordion, _colorNames["arrowbuttonarrow"], tr("Colors for the icon on the arrow buttons in the bottom screen.", "Tooltip for Arrow Buttons (Arrow) accordion."), true, true);
    _accordionindex["arrowbuttonarrow"] = index;
    QFrame *arrowbutton = getContent(_accordion, index);
    arrowbutton->layout()->addWidget(border);
    arrowbutton->layout()->addWidget(unpressed);
    arrowbutton->layout()->addWidget(pressed);
    _accordion->getContentPane(index)->setMaximumHeight(arrowbutton->sizeHint().height());
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("arrowbuttonarrow");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("arrowbuttonarrow");

    QMap<QString, SwatchWidget*> arrowbuttonmap;
    arrowbuttonmap["border"] = border;
    arrowbuttonmap["unpressed"] = unpressed;
    arrowbuttonmap["pressed"] = pressed;

    _swatches["arrowbuttonarrow"] = arrowbuttonmap;

    connect(border->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveArrowButtonArrowColor);
    connect(unpressed->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveArrowButtonArrowColor);
    connect(pressed->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveArrowButtonArrowColor);
    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::arrowButtonArrowVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addOpenCloseColors(ColorDialog *dialog) {
    QFont font = QFont("Nimbus Sans", 11);
    QLabel *openbutton = new QLabel(tr("Open Button", "Title for Open button."));
    openbutton->setFont(font);

    KameSlider *opentextshadowpos = new KameSlider();
    opentextshadowpos->setTitle(tr("Text Shadow Position", "Text shadow position title for open button."));
    opentextshadowpos->setUnits("px");
    opentextshadowpos->setRange(-10, 10);
    opentextshadowpos->setValue(0.5);

    SwatchWidget *opendark = new SwatchWidget(0, dialog);
    opendark->setTitle(tr("Dark", "Open button dark color title."));
    opendark->setAccordionWidget(this);
    SwatchWidget *openmain = new SwatchWidget(0, dialog);
    openmain->setTitle(tr("Main", "Open button main color title."));
    openmain->setAccordionWidget(this);
    SwatchWidget *openlight = new SwatchWidget(0, dialog);
    openlight->setTitle(tr("Light", "Open button light color title."));
    openlight->setAccordionWidget(this);
    SwatchWidget *openshadow = new SwatchWidget(0, dialog);
    openshadow->setTitle(tr("Shadow", "Open button shadow color title."));
    openshadow->setShowAlpha(true);
    openshadow->setAccordionWidget(this);
    openshadow->setIndicator(_unknown->icon(), SwatchWidget::message(1));
    SwatchWidget *openglow = new SwatchWidget(0, dialog);
    openglow->setTitle(tr("Glow", "Open button glow color title."));
    openglow->setAccordionWidget(this);
    openglow->setIndicator(_unknown->icon(), SwatchWidget::message(1));
    SwatchWidget *opentextshadow = new SwatchWidget(0, dialog);
    opentextshadow->setTitle(tr("Text Shadow", "Open button text shadow color title."));
    opentextshadow->setAccordionWidget(this);
    SwatchWidget *opentextmain = new SwatchWidget(0, dialog);
    opentextmain->setTitle(tr("Text Main", "Open button text main color title."));
    opentextmain->setAccordionWidget(this);
    SwatchWidget *opentextselected = new SwatchWidget(0, dialog);
    opentextselected->setTitle(tr("Text Selected", "Open button text selected color title."));
    opentextselected->setAccordionWidget(this);

    QLabel *closebutton = new QLabel(tr("Close Button", "Title for Close Button"));
    closebutton->setFont(font);

    KameSlider *closetextshadowpos = new KameSlider();
    closetextshadowpos->setTitle(tr("Text Shadow Position", "Text shadow position title for close button."));
    closetextshadowpos->setUnits("px");
    closetextshadowpos->setRange(-10, 10);
    closetextshadowpos->setValue(0.5);

    SwatchWidget *closedark = new SwatchWidget(0, dialog);
    closedark->setTitle(tr("Dark", "Close button dark color title"));
    closedark->setAccordionWidget(this);
    SwatchWidget *closemain = new SwatchWidget(0, dialog);
    closemain->setTitle(tr("Main", "Close button main color title"));
    closemain->setAccordionWidget(this);
    SwatchWidget *closelight = new SwatchWidget(0, dialog);
    closelight->setTitle(tr("Light", "Close button light color title"));
    closelight->setAccordionWidget(this);
    SwatchWidget *closeshadow = new SwatchWidget(0, dialog);
    closeshadow->setTitle(tr("Shadow", "Close button shadow color title"));
    closeshadow->setAccordionWidget(this);
    closeshadow->setShowAlpha(true);
    closeshadow->setIndicator(_unknown->icon(), SwatchWidget::message(1));
    SwatchWidget *closeglow = new SwatchWidget(0, dialog);
    closeglow->setTitle(tr("Glow", "Close button glow color title"));
    closeglow->setAccordionWidget(this);
    closeglow->setIndicator(_unknown->icon(), SwatchWidget::message(1));
    SwatchWidget *closetextshadow = new SwatchWidget(0, dialog);
    closetextshadow->setTitle(tr("Text Shadow", "Close button text shadow color title"));
    closetextshadow->setAccordionWidget(this);
    SwatchWidget *closetextmain = new SwatchWidget(0, dialog);
    closetextmain->setTitle(tr("Text Main", "Close button text main color title"));
    closetextmain->setAccordionWidget(this);
    SwatchWidget *closetextselected = new SwatchWidget(0, dialog);
    closetextselected->setTitle(tr("Text Selected", "Close button text selected color title"));
    closetextselected->setAccordionWidget(this);

    int index = createGroup(_accordion, _colorNames["openclose"], tr("Colors for the bottom buttons in the bottom screen.", "Tooltip for Open/Close Buttons accordion"), true, true);
    _accordionindex["openclose"] = index;
    QFrame *openclose = getContent(_accordion, index);
    openclose->layout()->addWidget(openbutton);
    openclose->layout()->addWidget(opentextshadowpos);
    openclose->layout()->addWidget(opendark);
    openclose->layout()->addWidget(openmain);
    openclose->layout()->addWidget(openlight);
    openclose->layout()->addWidget(openshadow);
    openclose->layout()->addWidget(openglow);
    openclose->layout()->addWidget(opentextshadow);
    openclose->layout()->addWidget(opentextmain);
    openclose->layout()->addWidget(opentextselected);
    openclose->layout()->addWidget(closebutton);
    openclose->layout()->addWidget(closetextshadowpos);
    openclose->layout()->addWidget(closedark);
    openclose->layout()->addWidget(closemain);
    openclose->layout()->addWidget(closelight);
    openclose->layout()->addWidget(closeshadow);
    openclose->layout()->addWidget(closeglow);
    openclose->layout()->addWidget(closetextshadow);
    openclose->layout()->addWidget(closetextmain);
    openclose->layout()->addWidget(closetextselected);

    _accordion->getContentPane(index)->setMaximumHeight(openclose->sizeHint().height());
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("openclose");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("openclose");

    _sliders["open"] = opentextshadowpos;
    _sliders["close"] = closetextshadowpos;

    QMap<QString, SwatchWidget*> openmap;
    openmap["dark"] = opendark;
    openmap["main"] = openmain;
    openmap["light"] = openlight;
    openmap["glow"] = openglow;
    openmap["shadow"] = openshadow;
    openmap["textshadow"] = opentextshadow;
    openmap["textmain"] = opentextmain;
    openmap["textselected"] = opentextselected;

    _swatches["open"] = openmap;

    QMap<QString, SwatchWidget*> closemap;
    closemap["dark"] = closedark;
    closemap["main"] = closemain;
    closemap["light"] = closelight;
    closemap["glow"] = closeglow;
    closemap["shadow"] = closeshadow;
    closemap["textshadow"] = closetextshadow;
    closemap["textmain"] = closetextmain;
    closemap["textselected"] = closetextselected;

    _swatches["close"] = closemap;

    connect(opendark->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(openmain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(openlight->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(openglow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(openshadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(opentextshadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(opentextmain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(opentextselected->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(opentextshadowpos, &KameSlider::valueChanged, this, &ColorWidget::saveOpenCloseColor);

    connect(closedark->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(closemain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(closelight->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(closeglow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(closeshadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(closetextshadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(closetextmain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(closetextselected->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveOpenCloseColor);
    connect(closetextshadowpos, &KameSlider::valueChanged, this, &ColorWidget::saveOpenCloseColor);

    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::opencloseVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addGameTextColors(ColorDialog *dialog) {
    SwatchWidget *main = new SwatchWidget(0, dialog);
    main->setTitle(tr("Main", "Game text main color title"));
    main->setAccordionWidget(this);
    SwatchWidget *light = new SwatchWidget(0, dialog);
    light->setTitle(tr("Light", "Game text light color title"));
    light->setAccordionWidget(this);
    light->setIndicator(_unknown->icon(), SwatchWidget::message(1));
    SwatchWidget *shadow = new SwatchWidget(0, dialog);
    shadow->setTitle(tr("Shadow", "Game text shadow color title"));
    shadow->setAccordionWidget(this);
    shadow->setShowAlpha(true);
    shadow->setIndicator(_unknown->icon(), SwatchWidget::message(1));
    SwatchWidget *textmain = new SwatchWidget(0, dialog);
    textmain->setTitle(tr("Text", "Game text text color title"));
    textmain->setAccordionWidget(this);

    int index = createGroup(_accordion, _colorNames["gametext"], tr("Colors for the tooltip that appears above bottom screen icons on the highest zoom level. Click for more details.", "Tooltip for game text accordion"), true, true);
    _accordionindex["gametext"] = index;
    QFrame *gametext = getContent(_accordion, index);
    gametext->layout()->addWidget(main);
    gametext->layout()->addWidget(light);
    gametext->layout()->addWidget(shadow);
    gametext->layout()->addWidget(textmain);
    _accordion->getContentPane(index)->setMaximumHeight(gametext->sizeHint().height());
    //: Game text colors detailed help
    _accordion->getContentPane(index)->setHeaderInfoDialog("<h3>" + tr("Game Text Colors", "Title for game text color detailed help") + "</h3><p><img src=\":resources/docs/gametext.png\"></img></p><ol><li>"
            + tr("Game Text") + "</li>"
                "<li>" + tr("Only visible on the largest zoom level. Click to change it.") + "</li></ol>", tr("Game Text Colors Help", "Window title for game text colors detailed help"));
    _accordion->getContentPane(index)->clickableFrame()->visibleIcon()->setTristate(true);
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("gametext");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("gametext");

    QMap<QString, SwatchWidget*> gametextmap;
    gametextmap["main"] = main;
    gametextmap["light"] = light;
    gametextmap["shadow"] = shadow;
    gametextmap["textmain"] = textmain;

    _swatches["gametext"] = gametextmap;

    connect(main->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveGameTextColor);
    connect(light->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveGameTextColor);
    connect(shadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveGameTextColor);
    connect(textmain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveGameTextColor);

    connect(_accordion->getContentPane(index), &ContentPane::visibilityStateChanged, this, &ColorWidget::gameTextVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addFolderViewColors(ColorDialog *dialog) {
    SwatchWidget *bgdark = new SwatchWidget(0, dialog);
    bgdark->setTitle(tr("Dark", "Folder view dark color title"));
    bgdark->setAccordionWidget(this);
    SwatchWidget *bgmain = new SwatchWidget(0, dialog);
    bgmain->setTitle(tr("Main", "Folder view main color title"));
    bgmain->setAccordionWidget(this);
    SwatchWidget *bglight = new SwatchWidget(0, dialog);
    bglight->setTitle(tr("Light", "Folder view light color title"));
    bglight->setAccordionWidget(this);
    SwatchWidget *bgshadow = new SwatchWidget(0, dialog);
    bgshadow->setTitle(tr("Shadow", "Folder view shadow color title"));
    bgshadow->setAccordionWidget(this);
    bgshadow->setShowAlpha(true);

    int index = createGroup(_accordion, _colorNames["folderview"], tr("Colors for the bottom screen when you open a folder. Click for more details.", "Tooltip for folder view accordion."), true, true);
    _accordionindex["folderview"] = index;
    QFrame *folder = getContent(_accordion, index);
    folder->layout()->addWidget(bgdark);
    folder->layout()->addWidget(bgmain);
    folder->layout()->addWidget(bglight);
    folder->layout()->addWidget(bgshadow);
    _accordion->getContentPane(index)->setMaximumHeight(folder->sizeHint().height());
    //: Folder view and back button colors detailed help
    _accordion->getContentPane(index)->setHeaderInfoDialog("<h3>" + tr("Displaying Folder View and Back Button Colors", "Title for folder view and back button colors detailed help") + "</h3><p><img src=\":resources/docs/folderview.png\"></img></p><ol><li>" + tr("Select any folder.") + "</li>"
                "<li>" + tr("Click on the open button.") + "</li></ol>", tr("Folder View and Back Button Colors Help", "Window title for folder view and back button colors detailed help"));
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("folderview");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("folderview");

    QMap<QString, SwatchWidget*> folderviewmap;
    folderviewmap["dark"] = bgdark;
    folderviewmap["main"] = bgmain;
    folderviewmap["light"] = bglight;
    folderviewmap["shadow"] = bgshadow;

    _swatches["folderview"] = folderviewmap;

    connect(bgdark->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderViewColor);
    connect(bgmain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderViewColor);
    connect(bglight->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderViewColor);
    connect(bgshadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderViewColor);

    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::folderViewVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addFolderBackButtonColors(ColorDialog *dialog) {
    KameSlider *textshadowpos = new KameSlider();
    textshadowpos->setTitle(tr("Text Shadow Position", "Text shadow position for folder back button."));
    textshadowpos->setUnits("px");
    textshadowpos->setRange(-10, 10);
    textshadowpos->setValue(0.5);

    SwatchWidget *dark = new SwatchWidget(0, dialog);
    dark->setTitle(tr("Dark", "Folder back button dark color title"));
    dark->setAccordionWidget(this);
    SwatchWidget *main = new SwatchWidget(0, dialog);
    main->setTitle(tr("Main", "Folder back button main color title"));
    main->setAccordionWidget(this);
    SwatchWidget *light = new SwatchWidget(0, dialog);
    light->setTitle(tr("Light", "Folder back button light color title"));
    light->setAccordionWidget(this);
    SwatchWidget *shadow = new SwatchWidget(0, dialog);
    shadow->setTitle(tr("Shadow", "Folder back button shadow color title"));
    shadow->setAccordionWidget(this);
    shadow->setShowAlpha(true);
    shadow->setIndicator(_unknown->icon(), SwatchWidget::message(1));
    SwatchWidget *glow = new SwatchWidget(0, dialog);
    glow->setTitle(tr("Glow", "Folder back button glow color title"));
    glow->setAccordionWidget(this);
    glow->setIndicator(_unknown->icon(), SwatchWidget::message(1));
    SwatchWidget *textshadow = new SwatchWidget(0, dialog);
    textshadow->setTitle(tr("Text Shadow", "Folder back button text shadow color title"));
    textshadow->setAccordionWidget(this);
    SwatchWidget *textmain = new SwatchWidget(0, dialog);
    textmain->setTitle(tr("Text Main", "Folder back button text main color title"));
    textmain->setAccordionWidget(this);
    SwatchWidget *textselected = new SwatchWidget(0, dialog);
    textselected->setTitle(tr("Text Selected", "Folder back button text selected color title"));
    textselected->setAccordionWidget(this);

    int index = createGroup(_accordion, _colorNames["folderbackbutton"], tr("Colors for the folder view back button. Click for more details.", "Tooltip for folder back button accordion."), true, true);
    _accordionindex["folderbackbutton"] = index;
    QFrame *folder = getContent(_accordion, index);
    folder->layout()->addWidget(textshadowpos);
    folder->layout()->addWidget(dark);
    folder->layout()->addWidget(main);
    folder->layout()->addWidget(light);
    folder->layout()->addWidget(shadow);
    folder->layout()->addWidget(glow);
    folder->layout()->addWidget(textshadow);
    folder->layout()->addWidget(textmain);
    folder->layout()->addWidget(textselected);
    _accordion->getContentPane(index)->setMaximumHeight(folder->sizeHint().height());
    _accordion->getContentPane(index)->setHeaderInfoDialog("<h3>" + tr("Displaying Folder View and Back Button Colors", "Title for folder view and back button colors detailed help") + "</h3><p><img src=\":resources/docs/folderview.png\"></img></p><ol><li>" + tr("Select any folder.") + "</li>"
                "<li>" + tr("Click on the open button.") + "</li></ol>", tr("Folder View and Back Button Colors Help", "Window title for folder view and back button colors detailed help"));
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("folderbackbutton");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("folderbackbutton");

    QMap<QString, SwatchWidget*> backbuttonmap;
    backbuttonmap["dark"] = dark;
    backbuttonmap["main"] = main;
    backbuttonmap["light"] = light;
    backbuttonmap["glow"] = glow;
    backbuttonmap["shadow"] = shadow;
    backbuttonmap["textshadow"] = textshadow;
    backbuttonmap["textmain"] = textmain;
    backbuttonmap["textselected"] = textselected;

    _swatches["folderbackbutton"] = backbuttonmap;

    _sliders["folderbackbutton"] = textshadowpos;

    connect(dark->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderBackButtonColor);
    connect(main->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderBackButtonColor);
    connect(light->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderBackButtonColor);
    connect(glow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderBackButtonColor);
    connect(shadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderBackButtonColor);
    connect(textshadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderBackButtonColor);
    connect(textmain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderBackButtonColor);
    connect(textselected->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveFolderBackButtonColor);
    connect(textshadowpos, &KameSlider::valueChanged, this, &ColorWidget::saveFolderBackButtonColor);

    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::folderbackbuttonVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addTopCornerButtonColors(ColorDialog *dialog) {
    SwatchWidget *main = new SwatchWidget(0, dialog);
    main->setTitle(tr("Main", "Top corner button main color title"));
    main->setAccordionWidget(this);
    SwatchWidget *light = new SwatchWidget(0, dialog);
    light->setTitle(tr("Light", "Top corner button light color title"));
    light->setAccordionWidget(this);
    light->setIndicator(_unknown->icon(), SwatchWidget::message(1));
    SwatchWidget *shadow = new SwatchWidget(0, dialog);
    shadow->setTitle(tr("Shadow", "Top corner button shadow color title"));
    shadow->setAccordionWidget(this);
    SwatchWidget *textmain = new SwatchWidget(0, dialog);
    textmain->setTitle(tr("Text", "Top corner button text color title"));
    textmain->setAccordionWidget(this);

    int index = createGroup(_accordion, _colorNames["topcorner"], tr("Colors for the corner buttons in the top screen. Click for more details.", "Tooltip for top corner buttons accordion"), true, true);
    _accordionindex["topcorner"] = index;
    QFrame *topcorner = getContent(_accordion, index);
    topcorner->layout()->addWidget(main);
    topcorner->layout()->addWidget(light);
    topcorner->layout()->addWidget(shadow);
    topcorner->layout()->addWidget(textmain);
    _accordion->getContentPane(index)->setMaximumHeight(topcorner->sizeHint().height());
    //: Top corner button colors detailed help
    _accordion->getContentPane(index)->setHeaderInfoDialog("<h3>" + tr("Top Corner Button Colors", "Title for top corner button colors detailed help") + "</h3><p><img src=\":resources/docs/topcorner.png\"></img></p><ol><li>"
            + tr("Top Corner Buttons") + "</li>"
                "<li>" + tr("In this preview, select any folder or empty slot to see them.") + "</li></ol>", tr("Top Corner Button Colors Help", "Window title for top corner button colors detailed help"));
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("topcorner");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("topcorner");

    QMap<QString, SwatchWidget*> topcornermap;
    topcornermap["main"] = main;
    topcornermap["light"] = light;
    topcornermap["shadow"] = shadow;
    topcornermap["textmain"] = textmain;

    _swatches["topcorner"] = topcornermap;

    connect(main->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveTopCornerColor);
    connect(light->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveTopCornerColor);
    connect(shadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveTopCornerColor);
    connect(textmain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveTopCornerColor);

    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::topCornerVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addBottomCornerButtonColors(ColorDialog *dialog) {
    SwatchWidget *dark = new SwatchWidget(0, dialog);
    dark->setTitle(tr("Dark", "Bottom corner button dark color title"));
    dark->setAccordionWidget(this);
    SwatchWidget *main = new SwatchWidget(0, dialog);
    main->setTitle(tr("Main", "Bottom corner button main color title"));
    main->setAccordionWidget(this);
    SwatchWidget *light = new SwatchWidget(0, dialog);
    light->setTitle(tr("Light", "Bottom corner button light color title"));
    light->setAccordionWidget(this);
    SwatchWidget *shadow = new SwatchWidget(0, dialog);
    shadow->setTitle(tr("Shadow", "Bottom corner button shadow color title"));
    shadow->setAccordionWidget(this);
    shadow->setIndicator(_unknown->icon(), SwatchWidget::message(1));
    SwatchWidget *iconmain = new SwatchWidget(0, dialog);
    iconmain->setTitle(tr("Icon Main", "Bottom corner button icon main color title"));
    iconmain->setAccordionWidget(this);
    SwatchWidget *iconlight = new SwatchWidget(0, dialog);
    iconlight->setTitle(tr("Icon Light", "Bottom corner button icon light color title"));
    iconlight->setAccordionWidget(this);
    SwatchWidget *icontextmain = new SwatchWidget(0, dialog);
    icontextmain->setTitle(tr("Icon Text Main", "Bottom corner button icon text main color title"));
    icontextmain->setAccordionWidget(this);
    icontextmain->setIndicator(_unknown->icon(), SwatchWidget::message(1));

    int index = createGroup(_accordion, _colorNames["bottomcorner"], tr("Colors for the corner buttons on the bottom screen.", "Tooltip for bottom corner buttons accordion."), true, true);
    _accordionindex["bottomcorner"] = index;
    QFrame *bottomcorner = getContent(_accordion, index);
    bottomcorner->layout()->addWidget(dark);
    bottomcorner->layout()->addWidget(main);
    bottomcorner->layout()->addWidget(light);
    bottomcorner->layout()->addWidget(shadow);
    bottomcorner->layout()->addWidget(iconmain);
    bottomcorner->layout()->addWidget(iconlight);
    bottomcorner->layout()->addWidget(icontextmain);

    _accordion->getContentPane(index)->setMaximumHeight(bottomcorner->sizeHint().height());
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("bottomcorner");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("bottomcorner");

    QMap<QString, SwatchWidget*> bottomcornermap;
    bottomcornermap["dark"] = dark;
    bottomcornermap["main"] = main;
    bottomcornermap["light"] = light;
    bottomcornermap["shadow"] = shadow;
    bottomcornermap["iconmain"] = iconmain;
    bottomcornermap["iconlight"] = iconlight;
    bottomcornermap["icontextmain"] = icontextmain;

    _swatches["bottomcorner"] = bottomcornermap;

    connect(dark->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveBottomCornerColor);
    connect(main->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveBottomCornerColor);
    connect(light->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveBottomCornerColor);
    connect(shadow->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveBottomCornerColor);
    connect(iconmain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveBottomCornerColor);
    connect(iconlight->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveBottomCornerColor);
    connect(icontextmain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveBottomCornerColor);

    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::bottomCornerVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}

void ColorWidget::addDemoTextColors(ColorDialog *dialog) {
    SwatchWidget *main = new SwatchWidget(0, dialog);
    main->setTitle(tr("Main", "Demo text main color title"));
    main->setAccordionWidget(this);
    SwatchWidget *textmain = new SwatchWidget(0, dialog);
    textmain->setTitle(tr("Text", "Demo text text color title"));
    textmain->setAccordionWidget(this);

    int index = createGroup(_accordion, _colorNames["demotext"], tr("Colors for the text that appears at the bottom of the top screen when certain apps in the bottom screen are selected. Click for more details.", "Tooltip for demo text accordion."), true, true);
    _accordionindex["demotext"] = index;
    QFrame *demo = getContent(_accordion, index);
    demo->layout()->addWidget(main);
    demo->layout()->addWidget(textmain);
    _accordion->getContentPane(index)->setMaximumHeight(demo->sizeHint().height());
    //: Demo text colors detailed help
    _accordion->getContentPane(index)->setHeaderInfoDialog("<h3>" + tr("Demo Text Colors", "Title for demo text color detailed help") + "</h3><p><img src=\":resources/docs/demotext.png\"></img></p><ol><li>"
            + tr("Demo Text") + "</li>"
                "<li>" + tr("Only appears when certain items are selected. In this preview, any file icon will have a demo text, but not every app on a real device has a demo text.") + "</li></ol>", tr("Demo Text Colors Help", "Window title for demo text colors detailed help"));
    _accordion->getContentPane(index)->clickableFrame()->loadIcon()->setObjectName("demotext");
    _accordion->getContentPane(index)->clickableFrame()->saveIcon()->setObjectName("demotext");

    QMap<QString, SwatchWidget*> demotextmap;
    demotextmap["main"] = main;
    demotextmap["textmain"] = textmain;

    _swatches["demotext"] = demotextmap;

    connect(main->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveDemoTextColor);
    connect(textmain->swatch(), &ColorSwatch::stateChanged, this, &ColorWidget::saveDemoTextColor);

    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &ColorWidget::demoTextVisibilityChanged);
    connect(_accordion->getContentPane(index)->clickableFrame()->loadIcon(), &QAbstractButton::clicked, this, &ColorWidget::paletteQuickLoad);
    connect(_accordion->getContentPane(index)->clickableFrame()->saveIcon(), &QAbstractButton::clicked, this, &ColorWidget::savePalette);
}
