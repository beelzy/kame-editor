// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef KAMEDOCKWIDGET_H
#define KAMEDOCKWIDGET_H

#include <QDockWidget>
#include <QFrame>
#include <QSizeGrip>

#include "overlaywidget.h"

class KameDockWidget : public QDockWidget {
    Q_OBJECT

    public:
        explicit KameDockWidget(QWidget *contents, QString title, QWidget *parent = 0);
        ~KameDockWidget();

    public slots:
        void setEnabled(bool value);

    protected:
        void resizeEvent(QResizeEvent *event) override;
        void mouseMoveEvent(QMouseEvent *event) override;

    protected slots:
        void onDockLocationChanged(Qt::DockWidgetArea area);
        void onScrollBarRangeChanged(int min, int max);
        void onScrollBarValueChanged(int value);

    private:
        QFrame *_contentFrame;
        QWidget *_contents;
        OverlayWidget *_overlay;
        QSizeGrip *_sizegrip;
        void updateSizeGrip();
};

#endif // KAMEDOCKWIDGET_H
