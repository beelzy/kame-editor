// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SMDHWIDGET_H
#define SMDHWIDGET_H

#include <QVBoxLayout>
#include <QCheckBox>
#include <QListView>
#include <QDoubleSpinBox>
#include <QStringListModel>

#include "accordionwidget.h"
#include "kameslider.h"
#include "imagepicker.h"

class SMDHWidget : public AccordionWidget {
    Q_OBJECT

    public:
        explicit SMDHWidget(QWidget *parent = 0);
        ~SMDHWidget();

    public slots:
        void onActive();
        void resetFields();
        void loadFields();
        void onKameTools();

    signals:
        void smdhRequiredFieldsChanged();

    protected:
        void addRequiredFields();
        void addOptionalFields();
        void connectRatings();
        void disconnectRatings();
        QVBoxLayout *createTextField(QString title, bool isMultiline = false);

    protected slots:
        void shorttitleLocaleChanged(int index);
        void shorttitleChanged(const QString &text);
        void longtitleLocaleChanged(int index);
        void longtitleChanged(const QString &text);
        void publisherLocaleChanged(int index);
        void publisherChanged(const QString &text);
        void ratingsRegionChanged(int region);
        void iconChanged(bool value);
        void smallIconChanged(bool value);
        void onAgeLimitChanged(int value);
        void onActiveChanged(int state);
        void onPendingChanged(int state);
        void onNoAgeChanged(int state);
        void regionChanged(const QModelIndex &index);
        void regionFreeChanged(bool state);
        void flagsChanged(bool state);
        void optimalBannerFrameChanged(double value);
        void matchMakerIdChanged(const QString &text);
        void matchMakerBitIdChanged(const QString &text);
        void eulaVersionChanged(const QString &text);
        void streetpassChanged(const QString &text);

    private:
        QLineEdit *_shorttitle;
        QLineEdit *_longtitle;
        QLineEdit *_publisher;
        KameSlider *_agelimit;
        QCheckBox *_active;
        QCheckBox *_pending;
        QCheckBox *_noage;
        QListView *_region;
        QCheckBox *_regionfree;
        QGridLayout *_flagslayout;
        QLineEdit *_matchmaker;
        QLineEdit *_matchmakerbit;
        QLineEdit *_eulaversion;
        QDoubleSpinBox *_optimalbannerframe;
        QLineEdit *_streetpass;
        QStringListModel *_regionModel;
        QRegularExpressionValidator *_rxValidator;

        ImagePicker *_iconpicker;
        ImagePicker *_smalliconpicker;

        int _shorttitleLocale;
        int _longtitleLocale;
        int _publisherLocale;
        int _ratingsRegion;
};

#endif //SMDHWIDGET_H
