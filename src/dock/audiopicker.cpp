// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTime>
#include <QtEndian>
#include <QMessageBox>

#include "audiopicker.h"
#include "icons/cross.h"
#include "icons/folder.h"
#include "icons/speaker.h"
#include "icons/warning.h"
#include "kameaccordion/kameaccordion.h"
#include "../color.h"
#include "infodialog.h"
#include "../resourcesdialog.h"

/*
    TRANSLATOR AudioPicker

    Used to select audio files (BGM, SFX) in the Audio Dock Widget.
*/

AudioPicker::AudioPicker(QWidget * parent, int mode) : QWidget(parent), _mode(static_cast<Mode>(mode)) {
    _isBcwav = false;
    _playable = true;
    _filesize = 0;
    _title = new QLabel();
    _path = new QLabel(tr("(No file selected)", "Displayed when no file in the audio picker is selected."));
    _player = new AdpcmPlayer(this);
    _info = new InfoIcon();
    Folder *folder = new Folder();
    Cross *cross = new Cross();
    QIcon icon(folder->icon());
    _select = new QPushButton();
    _play  = new QPushButton();
    _stop = new QPushButton();
    _mute = new QPushButton();
    _reset = new QPushButton();
    _trackbar = new QProgressBar();
    _description = new QLabel();
    _warning = new QLabel();

    _select->setIcon(icon);
    _select->setIconSize(folder->icon().rect().size());

    _select->setObjectName("select");
    _select->setStyleSheet(folder->selectStyle());
    _select->setCursor(Qt::PointingHandCursor);

    drawPlay();
    drawStop();
    drawSpeaker();
    _play->setObjectName("play");
    _play->setStyleSheet(folder->playStyle());
    _play->setCursor(Qt::PointingHandCursor);
    _play->setCheckable(true);

    _stop->setObjectName("stop");
    _stop->setStyleSheet(folder->stopStyle());
    _stop->setCursor(Qt::PointingHandCursor);

    _mute->setCheckable(true);
    _mute->setChecked(true);
    _mute->setStyleSheet("background: none; outline: none; border: none;");
    _mute->setCursor(Qt::PointingHandCursor);

    QIcon crossicon(cross->icon());
    _reset->setIcon(crossicon);
    _reset->setIconSize(cross->icon().rect().size());

    _reset->setObjectName("reset");
    _reset->setStyleSheet("QPushButton#reset {border: none; background-color: none; outline: none; padding: 0;}");
    _reset->setCursor(Qt::PointingHandCursor);

    Warning *warning = new Warning();
    _warning->setPixmap(warning->icon());
    _warning->hide();

    delete warning;
    delete cross;

    _play->setEnabled(false);
    _stop->setEnabled(false);
    _mute->setEnabled(false);
    _reset->setEnabled(false);

    _trackbar->setTextVisible(false);
    _trackbar->setFixedHeight(4);
    QPalette pal = _trackbar->palette();
    pal.setColor(QPalette::Highlight, Color::MAIN);
    _trackbar->setPalette(pal);

    acceptBCWAV(false);

    _description->setWordWrap(true);
    _description->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);

    KameAccordion *accordion = new KameAccordion(this);

    ContentPane *header = new ContentPane(tr("Information", "Title for the audio picker meta data accordion."));
    header->setVisibleIconVisibility(false);
    header->setDarkMode(true);

    int index = accordion->addContentPane(header);

    if (index != -1) {
        QFrame *contentFrame = accordion->getContentPane(index)->getContentFrame();
        contentFrame->setLayout(new QVBoxLayout());
        contentFrame->layout()->addWidget(_description);
        accordion->getContentPane(index)->setMaximumHeight(0);
    }

    QVBoxLayout *vlayout = new QVBoxLayout();

    QHBoxLayout *titlelayout = new QHBoxLayout();
    QHBoxLayout *hlayout = new QHBoxLayout();
    QHBoxLayout *controlslayout = new QHBoxLayout();
    
    titlelayout->addWidget(_title);
    titlelayout->addWidget(_info);
    titlelayout->addWidget(_mute);
    titlelayout->addWidget(_warning);
    titlelayout->addWidget(_reset);
    titlelayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Preferred));

    hlayout->addWidget(_select);
    hlayout->addWidget(_path, 1);

    controlslayout->addWidget(_stop);
    controlslayout->addWidget(_play);
    controlslayout->addWidget(_trackbar, 1);

    vlayout->addLayout(titlelayout);
    vlayout->addLayout(hlayout);
    vlayout->addLayout(controlslayout);
    if (_mode == Mode::WAV) {
        _mute->hide();

        _loopenabled = new QCheckBox("Looping");
        _loopenabled->setChecked(true);

        _loopstart = new QSpinBox();
        _loopend = new QSpinBox();

        QLabel *startlabel = new QLabel(tr("Start", "Loop start text in the audio converter section."));
        QLabel *endlabel = new QLabel(tr("End", "Loop end text in the audio converter section."));

        QHBoxLayout *looplayout = new QHBoxLayout();

        looplayout->addWidget(startlabel);
        looplayout->addWidget(_loopstart);
        looplayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Preferred));
        looplayout->addWidget(endlabel);
        looplayout->addWidget(_loopend);

        vlayout->addWidget(_loopenabled);
        vlayout->addLayout(looplayout);

        _loopstart->setEnabled(false);
        _loopend->setEnabled(false);
    }
    vlayout->addWidget(accordion);
    setLayout(vlayout);

    connect(_select, &QAbstractButton::clicked, this, &AudioPicker::onSelectClicked);
    connect(_play, &QAbstractButton::toggled, this, &AudioPicker::togglePlay);
    connect(_stop, &QAbstractButton::clicked, this, &AudioPicker::stop);
    if (_mode == Mode::ADPCM) {
        connect(_mute, &QAbstractButton::toggled, this, &AudioPicker::toggleMute);
    } else {
        connect(_loopenabled, &QCheckBox::stateChanged, this, &AudioPicker::enableLoopEdit);
    }
    connect(_reset, &QAbstractButton::clicked, this, &AudioPicker::clearAudio);
    connect(_player, &AdpcmPlayer::fileLoaded, this, &AudioPicker::onFileLoaded);
    connect(_player, &AdpcmPlayer::fileError, this, &AudioPicker::onFileError);
    connect(_player, &AdpcmPlayer::streamError, this, &AudioPicker::onStreamError);
    connect(_player, &AdpcmPlayer::positionChanged, this, &AudioPicker::updateTrackbar);
    connect(_player, &AdpcmPlayer::durationChanged, this, &AudioPicker::updateDuration);
    connect(_player, &AdpcmPlayer::mediaEnd, this, &AudioPicker::stop);

    delete folder;
}

AudioPicker::~AudioPicker() {
    delete _warning;
    delete _mute;
    delete _reset;
    delete _description;
    delete _trackbar;
    delete _info;
    delete _play;
    delete _stop;
    delete _select;
    delete _player;
    if (_mode == Mode::WAV) {
        delete _loopenabled;
        delete _loopstart;
        delete _loopend;
    }
    delete _path;
    delete _title;
}

void AudioPicker::setAccordionWidget(AccordionWidget *widget) {
    connect(this, &AudioPicker::stateChanged, widget, &AccordionWidget::onAvailabilityChanged);
}

QString AudioPicker::filepath() {
    return _player->filepath();
}

int AudioPicker::loopstart() {
    return _loopstart->value();
}

int AudioPicker::loopend() {
    return _loopend->value();
}

void AudioPicker::setTitle(QString value) {
    _title->setText(value);
}

void AudioPicker::setTooltip(QString value) {
    _info->setToolTip(value);
}

void AudioPicker::setInfoDialog(QString content, QString title, int minWidth, int maxWidth) {
    InfoDialog *infodialog = new InfoDialog(this);
    infodialog->setWindowTitle(title);
    infodialog->addHtml(content, minWidth, maxWidth);

    connect(_info, &QAbstractButton::clicked, infodialog, &InfoDialog::show);
}

InfoIcon *AudioPicker::info() {
    return _info;
}

int AudioPicker::fileSize() {
    return _filesize;
}

void AudioPicker::setPlayable(bool value) {
    _playable = value;
    if (_playable) {
        _play->setChecked(false);
        if (!_player->filepath().isEmpty()) {
            enableControls();
        } else {
            disableControls();
        }
    } else {
        drawWarning();
        disableControls();
        _player->setVgmstreamAvailable(false);
    }
}

void AudioPicker::enableLooping(bool value) {
    _player->setLooping(value);
}

void AudioPicker::invokeLoopEdit(bool value) {
    _loopenabled->setChecked(value);
}

void AudioPicker::enableLoopEdit(bool state) {
    if (!_player->filepath().isEmpty()) {
        _loopstart->setEnabled(state);
        _loopend->setEnabled(state);
    }
}

bool AudioPicker::loopEditEnabled() {
    return _loopenabled->isChecked();
}

bool AudioPicker::isMuted() {
    return !_mute->isChecked();
}

bool AudioPicker::isPlaying() {
    return _player->isPlaying();
}

void AudioPicker::drawWarning() {
    Warning *warning = new Warning();
    QColor color = Color::LIGHT;
    color.setAlpha(0x80);
    Warning *disabled = new Warning(color);
    QIcon icon(warning->icon());
    icon.addPixmap(disabled->icon(), QIcon::Disabled);
    _play->setIcon(icon);
    _play->setToolTip(tr("vgmstream-cli cannot be found. You will still be able to manage your theme's audio, but Kame Editor cannot preview them.", "This warning appears in a tooltip in a ! icon where the play button was if the program cannot find vgmstream-cli."));

    delete warning;
    delete disabled;
}

void AudioPicker::drawPlay() {
    QIcon icon(playIcon(Color::LIGHT));
    QColor color = Color::LIGHT;
    color.setAlpha(0x80);
    icon.addPixmap(playIcon(color), QIcon::Disabled);
    _play->setIcon(icon);
    _play->setToolTip("");
}

void AudioPicker::drawPause() {
    QIcon icon(pauseIcon(Color::LIGHT));
    QColor color = Color::LIGHT;
    color.setAlpha(0x80);
    icon.addPixmap(pauseIcon(color), QIcon::Disabled);
    _play->setIcon(icon);
    _play->setToolTip("");
}

void AudioPicker::drawSpeaker() {
    Speaker *speaker = new Speaker();
    QIcon icon(speaker->icon());

    QPixmap pixmap = QPixmap(16, 16);
    pixmap.fill(QColor(Qt::transparent));

    QPainter p;
    p.begin(&pixmap);
    p.setOpacity(0.5);

    p.drawPixmap(0, 0, speaker->icon());
    p.end();

    icon.addPixmap(pixmap, QIcon::Disabled);
    _mute->setIcon(icon);

    delete speaker;
}

void AudioPicker::drawMute() {
    QIcon icon(muteIcon());
    icon.addPixmap(muteIcon(true), QIcon::Disabled);
    _mute->setIcon(icon);
}

void AudioPicker::buildWarningTooltip() {
    _warning->setToolTip("");
    if (!_isBcwav) {
        if (_filesize > 3371008) {
            _warning->show();
            _warning->setToolTip(tr("WARNING: Your selected .bcstm file is over 3.3MB in size. Please optimize your file before including it or else it will not be included during deploy.", "This warning shows up in a tooltip in a ! icon near the title if users select a bgm file that's over 3.3MB."));
        }
    }
    if (!_player->errors().isEmpty()) {
        _warning->show();
        QStringList tooltip = QStringList() << _warning->toolTip() << _player->errors();
        tooltip.removeAll("");
        _warning->setToolTip(tooltip.join("\n"));
    }
    if (_warning->toolTip().isEmpty()) {
        _warning->hide();
    }
}

QPixmap AudioPicker::muteIcon(bool disabled) {
    Speaker *speaker = new Speaker();

    QPainter p;
    QPixmap pixmap = QPixmap(16, 16);
    pixmap.fill(QColor(Qt::transparent));
    p.begin(&pixmap);

    p.setRenderHints(QPainter::Antialiasing);
    if (disabled) {
        p.setOpacity(0.5);
    }

    p.drawPixmap(0, 0, speaker->icon());
    QPainterPath path;
    path.moveTo(0, 0);
    path.lineTo(16, 16);
    path.moveTo(16, 0);
    path.lineTo(0, 16);

    QPen pen = QPen(QColor("#AF0E0E"));
    pen.setWidth(3);
    pen.setCapStyle(Qt::FlatCap);
    p.setPen(pen);
    p.setBrush(QColor(Qt::transparent));

    p.drawPath(path);
    p.end();

    delete speaker;

    return pixmap;
}

QPixmap AudioPicker::playIcon(QColor color) {
    QPainter p;
    QPixmap pixmap = QPixmap(12, 12);
    pixmap.fill(QColor(Qt::transparent));
    p.begin(&pixmap);
    p.setRenderHints(QPainter::Antialiasing);

    QPainterPath path;
    float offsetX = (12 - 6 * sqrt(3)) / 2.0;
    path.moveTo(offsetX, 0);
    path.lineTo(offsetX + 6 * sqrt(3), 6);
    path.lineTo(offsetX, 12);
    path.lineTo(offsetX, 0);

    p.setBrush(color);
    p.setPen(QColor(Qt::transparent));
    p.drawPath(path);
    p.end();
    return pixmap;
}

QPixmap AudioPicker::pauseIcon(QColor color) {
    QPainter p;
    QPixmap pixmap = QPixmap(16, 16);
    pixmap.fill(QColor(Qt::transparent));
    p.begin(&pixmap);
    p.setRenderHints(QPainter::Antialiasing);

    p.setBrush(color);
    p.setPen(QColor(Qt::transparent));
    p.drawRect(3, 3, 3, 10);
    p.drawRect(8, 3, 3, 10);
    p.end();
    return pixmap;
}

void AudioPicker::drawStop() {
    QPainter p;
    QPixmap pixmap = QPixmap(10, 10);
    pixmap.fill(Color::MAIN);
    QPixmap disabled = QPixmap(10, 10);
    QColor color = Color::MAIN;
    color.setAlpha(0x80);
    disabled.fill(color);
    QIcon icon(pixmap);
    icon.addPixmap(disabled, QIcon::Disabled);

    _stop->setIcon(icon);
}

void AudioPicker::togglePlay() {
    if (_play->isChecked()) {
        drawPause();
        _player->play();
    } else {
        drawPlay();
        _player->pause();
    }
    update();
}

void AudioPicker::toggleMute() {
    if (_mute->isChecked()) {
        drawSpeaker();
        enableControls();
    } else {
        drawMute();
        disableControls();
        _play->setChecked(false);
    }
    emit muteChanged(_mute->isChecked());
}

void AudioPicker::onSelectClicked() {
    QString selectedFilter;
    emit stateChanged(false);
    QFileDialog filedialog;
    filedialog.setAcceptMode(QFileDialog::AcceptOpen);
    filedialog.setNameFilters(_filters);
    filedialog.setWindowTitle(_dialogtitle);
    if (filedialog.exec()) {
        QString fileName = filedialog.selectedFiles().first();

        loadFile(fileName);
    }
    emit stateChanged(true);
}

bool AudioPicker::fileChanged() {
    QFileInfo fileInfo(_player->filepath());
    return !_player->filepath().isEmpty() && (_lastChanged < fileInfo.lastModified() || _filesize != fileInfo.size());
}

bool AudioPicker::isLoading() {
    return _loading;
}

void AudioPicker::reload() {
    loadFile(_player->filepath());
}

void AudioPicker::loadFile(QString path) {
    _play->setChecked(false);
    _filesize = QFileInfo(path).size();
    if (_mode == Mode::ADPCM) {
        disableControls();
        _loading = true;
        _select->setEnabled(false);
        _player->loadFile(path);
        buildWarningTooltip();
    } else {
        WavError result = readHeader(path);
        if (result == WavError::NOWAVERROR) {
            disableControls();
            _loading = true;
            _select->setEnabled(false);
            _player->loadFile(path);
        } else {
            QString title = result == WavError::INVALID ? tr("Failed to load file", "Title for WAV loading error message") : tr("Wrong Bits per Sample", "Title for WAV loading error message");
            QString message = result == WavError::INVALID ? tr("Not a valid WAV file. The file you have chosen is either corrupted or does not contain a WAV header.", "This message appears when users select an audio file that isn't really a WAV.") : tr("Incompatible bits per sample. The file you have chosen needs to have a bits per sample of 8 or 16.", "This message appears when users select a WAV file that doesn't have 8 or 16 bits per sample.");
            QMessageBox msgBox(QMessageBox::Warning, title, message, QFlag(0), this);
            msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
            msgBox.exec();
            path = _path->text();
        }
    }
    _path->setText(path);
    _path->setToolTip(path);
}

void AudioPicker::stop() {
    _trackbar->setValue(0);
    _play->setChecked(false);
    _player->reset();

    emit mediaEnd();
}

void AudioPicker::pause() {
    _play->setChecked(false);
}

void AudioPicker::acceptBCWAV(bool value) {
    _isBcwav = value;
    if (_isBcwav) {
        _filters = ResourcesDialog::FILETYPES["sfx"].filters;
        _dialogtitle = tr("SFX File Selection", "Dialog title for selecting SFX files.");
    } else if (_mode == Mode::WAV) {
        _filters << tr("WAV Audio Files (*.wav)");
        _dialogtitle = tr("WAV Input File Selection", "Dialog title for selecting WAV files for conversion.");
    } else {
        _filters = ResourcesDialog::FILETYPES["bgm"].filters;
        _dialogtitle = tr("BGM File Selection", "Dialog title for selecting BGM files.");
    }
}

void AudioPicker::reset() {
    _play->setChecked(false);
    if (_mode == Mode::ADPCM) {
        _mute->setChecked(true);
        _mute->setEnabled(false);
    }
    _reset->setEnabled(false);
    _filesize = 0;
    _player->unload();
    _warning->hide();
    disableControls();
    _path->setText(tr("(No file selected)"));
    _path->setToolTip("");
    _description->setText("");
}

void AudioPicker::clearAudio() {
    reset();
    emit stateChanged(true);
    emit fileLoaded();
}

void AudioPicker::play() {

    if (_mute->isChecked() && !filepath().isEmpty() && _playable) {
        _play->setChecked(true);
    }
}

AudioPicker::WavError AudioPicker::readHeader(QString path) {
    QFile m_WAVFile;
    m_WAVFile.setFileName(path);
    m_WAVFile.open(QIODevice::ReadOnly);

    QByteArray strm;
    QVector<double> m_DATA;

    strm = m_WAVFile.read(4);//RIFF
    if (strm != "RIFF") {
        m_WAVFile.close();
        return WavError::INVALID;
    }
    m_WAVFile.read(4);
    strm = m_WAVFile.read(4);//WAVE
    if (strm != "WAVE") {
        m_WAVFile.close();
        return WavError::INVALID;
    }
    while(!m_WAVFile.atEnd()) {
        strm = m_WAVFile.read(4);
        if (strm == "fmt ") {
            m_WAVFile.read(4);
            m_WAVFile.read(2);
            m_WAVFile.read(2);
            m_WAVFile.read(8);
            m_WAVFile.read(2);
            strm = m_WAVFile.read(2);
            int bps = qFromLittleEndian<quint32>(strm);
            if (bps != 16 && bps != 8) {
                m_WAVFile.close();
                return WavError::WRONGBPS;
            }
            m_WAVFile.close();
            return WavError::NOWAVERROR;
        } else if (strm == "data") {
            m_WAVFile.close();
            return WavError::INVALID;
        }
    }
    return WavError::INVALID;
}

void AudioPicker::updateDescription(QString text) {
    _description->setText(text);

    QFontMetrics metrics = QFontMetrics(QFont());
    QRect descriptionRect = metrics.boundingRect(QRect(0, 0, _description->width(), QWIDGETSIZE_MAX), Qt::TextWordWrap, _description->text());
    _description->setMinimumHeight(descriptionRect.height());
    dynamic_cast<ContentPane*>(_description->parent()->parent()->parent())->setMaximumHeight(descriptionRect.height());
}

void AudioPicker::onFileLoaded() {
    _loading = false;
    _lastChanged = QFileInfo(_player->filepath()).lastModified();
    _description->setText(_player->description());
    updateDescription(_player->description());

    if (_mode == Mode::WAV) {
        _loopstart->setMaximum(_player->samples());
        _loopend->setMaximum(_player->samples());
        _loopend->setValue(_player->samples());
    }

    updateDuration(_player->total());

    enableControls();
    _select->setEnabled(true);
    _reset->setEnabled(true);

    emit fileLoaded();
}

void AudioPicker::onFileError() {
    _loading = false;

    updateDescription(_player->description());
    _select->setEnabled(true);
    _reset->setEnabled(true);

    emit fileLoaded();
}

void AudioPicker::onStreamError() {
    buildWarningTooltip();
    _select->setEnabled(true);
    _reset->setEnabled(true);
}

void AudioPicker::enableControls() {
    if (_playable) {
        _play->setEnabled(true);
        _stop->setEnabled(true);
        _mute->setEnabled(true);
    }
    if (_mode == Mode::WAV) {
        _loopstart->setEnabled(true);
        _loopend->setEnabled(true);
    }
}

void AudioPicker::disableControls() {
    _play->setEnabled(false);
    _stop->setEnabled(false);
    if (_mode == Mode::WAV) {
        _loopstart->setEnabled(false);
        _loopend->setEnabled(false);
    }
}

void AudioPicker::updateDuration(qint64 duration) {
    _trackbar->setMaximum(duration);
}

void AudioPicker::updateTrackbar(qint64 position) {
    // Unfortunately, the portaudio callback is on a different thread, so it may still try to update the
    // position, even after telling it to stop.
    if (_player->isPlaying() || position == 0) {
        _trackbar->setValue(position);
    }
    if (_player->filepath().isEmpty()) {
        _trackbar->setValue(0);
    }
}

void AudioPicker::resizeEvent(QResizeEvent *) {
    QFontMetrics metrics = QFontMetrics(QFont());
    QRect descriptionRect = metrics.boundingRect(QRect(0, 0, _description->width(), QWIDGETSIZE_MAX), Qt::TextWordWrap, _description->text());
    _description->setMinimumHeight(descriptionRect.height());
    dynamic_cast<ContentPane*>(_description->parent()->parent()->parent())->setMaximumHeight(_description->height());
}
