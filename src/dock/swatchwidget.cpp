// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QHBoxLayout>

#include "swatchwidget.h"

/*
    TRANSLATOR SwatchWidget

    Unused Color - Tooltip that appears next to a ! icon in a color swatch.
    Unknown Color - Tooltip that appears next to a (?) icon in a color swatch.
*/
const char *SwatchWidget::messages[] = {
    QT_TRANSLATE_NOOP("SwatchWidget", "This color appears to be unused. You can modify it, and it will be saved into your theme, but it will not appear in the preview and may not show up on your 3DS."),
    QT_TRANSLATE_NOOP("SwatchWidget", "It is unknown what this color does exactly, so it will not show up in the preview. It may show up on your 3DS or in other editors.")
};

SwatchWidget::SwatchWidget(QWidget *parent, ColorDialog *dialog) : QWidget(parent) {
    QHBoxLayout *layout = new QHBoxLayout();

    _swatch = new ColorSwatch(this, dialog);
    _title = new QLabel("");
    _indicator = new QLabel("");
    QFontMetrics metrics = QFontMetrics(QFont());
    _title->setMinimumHeight(metrics.height());

    layout->addWidget(_swatch, Qt::AlignVCenter);
    layout->addWidget(_indicator, 0, Qt::AlignVCenter);
    layout->addWidget(_title, Qt::AlignVCenter);

    setLayout(layout);
}

SwatchWidget::~SwatchWidget() {
    delete _title;
    delete _indicator;
    delete _swatch;
}

QString SwatchWidget::message(int index) {
    return tr(messages[index]);
}

void SwatchWidget::setTitle(QString value) {
    _title->setText(value);
}

void SwatchWidget::setIndicator(QPixmap pixmap, QString tooltip) {
    _indicator->setPixmap(pixmap);
    _indicator->setToolTip(tooltip);
}

QColor SwatchWidget::color() {
    return _swatch->color();
}

void SwatchWidget::setShowAlpha(bool value) {
    _swatch->setShowAlpha(value);
}

void SwatchWidget::setAccordionWidget(AccordionWidget *obj) {
    _swatch->setAccordionWidget(obj);
}

ColorSwatch *SwatchWidget::swatch() {
    return _swatch;
}
