// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include "accordionwidget.h"

AccordionWidget::AccordionWidget(QWidget * parent) : QObject(parent) {
    _accordion = new KameAccordion(parent);
    _accordion->setMultiActive(true);
    _dockWidget = nullptr;
}

AccordionWidget::~AccordionWidget() {
    _accordion->deleteLater();
    if (_dockWidget != nullptr) {
        _dockWidget->deleteLater();
    }
}

KameAccordion *AccordionWidget::contents() {
    return _accordion;
}

int AccordionWidget::createGroup(KameAccordion *accordion, QString title, QString tooltip, bool visibilityIcon, bool restoreOptions) {
    ContentPane *header = new ContentPane(title);
    header->setHeaderTooltip(tooltip);
    header->setVisibleIconVisibility(visibilityIcon);
    if (restoreOptions) {
        header->clickableFrame()->loadIcon()->show();
        header->clickableFrame()->saveIcon()->show();
        header->clickableFrame()->loadIcon()->setToolTip(tr("Quick load palette", "tooltip for accordion quick load palette"));
        header->clickableFrame()->saveIcon()->setToolTip(tr("Save colors to palette", "tooltip for accordion save palette"));
    }
    int index = accordion->addContentPane(header);

    if (index != -1) {
        QFrame *contentFrame = accordion->getContentPane(index)->getContentFrame();
        contentFrame->setLayout(new QVBoxLayout());
    }
    return index;
}

QFrame *AccordionWidget::getContent(KameAccordion *accordion, int index) {
    if (index != -1) {
        return accordion->getContentPane(index)->getContentFrame();
    }
    return 0;
}

void AccordionWidget::setDockWidget(KameDockWidget *widget) {
    _dockWidget = widget;
}

void AccordionWidget::onAvailabilityChanged(bool isEnabled) {
    _dockWidget->setEnabled(isEnabled);
}
