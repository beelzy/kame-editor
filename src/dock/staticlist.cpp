// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QMouseEvent>

#include "staticlist.h"

StaticList::StaticList(QWidget *parent) : QListView(parent) {
    setAlternatingRowColors(true);
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
}

void StaticList::setModel(QAbstractItemModel *model) {
    QAbstractItemView::setModel(model);

    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setSelectionMode(QAbstractItemView::NoSelection);
    setMinimumHeight(20 * model->rowCount()); //Qt5 won't tell us what the correct row height is...
}

void StaticList::mousePressEvent(QMouseEvent *event) {
    event->ignore();
}

void StaticList::mouseMoveEvent(QMouseEvent *event) {
    event->ignore();
}

void StaticList::mouseReleaseEvent(QMouseEvent *event) {
    event->ignore();
}
