// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QVBoxLayout>
#include <QMouseEvent>
#include <QLabel>
#include <QDesktopServices>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>

#include "infodialog.h"
#include "../color.h"
#include "../resourcesdialog.h"

QString InfoDialog::TEXTURE_TEMPLATES = QT_TRANSLATE_NOOP("InfoDialog", "Texture Templates");

InfoDialog::InfoDialog(QWidget * parent) : QDialog(parent) {
    _mouseMoveAllowed = false;
    setModal(false);

    _closebutton = new QPushButton(tr("Close", "Close button in info dialogs"));
    _closebutton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

    _docsbutton = new QPushButton(tr("Documentation", "Documentation button in info dialogs"));
    _docsbutton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    _templatesbutton = new QPushButton(TEXTURE_TEMPLATES);
    _templatesbutton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    _templatesbutton->hide();

    _controlsLayout = new QHBoxLayout();
    _controlsLayout->addWidget(_closebutton);
    _controlsLayout->addWidget(_docsbutton);
    _controlsLayout->addWidget(_templatesbutton);
    _controlsLayout->addStretch();

    QVBoxLayout *layout = new QVBoxLayout();
    layout->addLayout(_controlsLayout);

    setLayout(layout);

    connect(_closebutton, &QAbstractButton::clicked, this, &QDialog::accept);
    connect(_docsbutton, &QAbstractButton::clicked, this, &InfoDialog::docsClicked);
    connect(_templatesbutton, &QAbstractButton::clicked, this, &InfoDialog::templatesClicked);
}

InfoDialog::~InfoDialog() {
    delete _controlsLayout;
}

void InfoDialog::addWidget(QWidget *widget) {
    QVBoxLayout *vlayout = dynamic_cast<QVBoxLayout*>(layout());
    vlayout->insertWidget(vlayout->indexOf(_controlsLayout), widget);
}

void InfoDialog::addLayout(QLayout *newlayout) {
    QVBoxLayout *vlayout = dynamic_cast<QVBoxLayout*>(layout());
    vlayout->insertLayout(vlayout->indexOf(_controlsLayout), newlayout);
}

void InfoDialog::addHtml(QString html, int minWidth, int maxWidth) {
    QLabel *info = new QLabel(html);
    info->setAttribute(Qt::WA_TransparentForMouseEvents, true);
    info->setWordWrap(true);
    info->setMinimumWidth(minWidth);
    info->setMaximumWidth(maxWidth);
    info->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

    addWidget(info);
}

void InfoDialog::show() {
    QWidget::show();
    raise();
    activateWindow();
}

void InfoDialog::showTemplatesButton() {
    _templatesbutton->show();
}

void InfoDialog::docsClicked() {
    QDesktopServices::openUrl(QUrl("https://gitlab.com/beelzy/kame-editor/-/wikis/home"));
}

void InfoDialog::templatesClicked() {
    QString path = QFileDialog::getSaveFileName(this, tr("Download Texture Templates...", "Dialogue title when choosing to download templates"), "kame-editor_templates.zip", ResourcesDialog::FILETYPES["zip"].filters[0]);
    if (!path.isEmpty()) {
        QFile templates(":/templates/kame-editor_templates.zip");

        QFile dest(path);
        dest.remove();

        if (!templates.copy(path)) {
            QMessageBox msgBox(QMessageBox::Critical, tr("File Write Error", "Dialog title when attempting to open texture templates file for download failed"), tr("Unable to write to destination file for texture templates download: %1").arg(templates.errorString()), QFlag(0), this);
            msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
            msgBox.exec();
        }
    }
}

void InfoDialog::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton && !_closebutton->underMouse()) {
        _mouseMoveAllowed = true;
        _mouseCoords = event->pos();
    } else {
        _mouseMoveAllowed = false;
    }
}

void InfoDialog::mouseMoveEvent(QMouseEvent *event) {
    if (_mouseMoveAllowed && !_closebutton->underMouse()) {
        move(event->globalPos().x() - _mouseCoords.x(), event->globalPos().y() - _mouseCoords.y());
    }
}

void InfoDialog::mouseReleaseEvent(QMouseEvent *) {
    _mouseMoveAllowed = false;
}
