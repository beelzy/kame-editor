// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QHBoxLayout>
#include <QGridLayout>
#include <QComboBox>
#include <QLineEdit>
#include <QTextEdit>
#include <QSettings>
#include <QFileInfo>

#include "smdhwidget.h"
#include "../sourcefile.h"


SMDHWidget::SMDHWidget(QWidget * parent) : AccordionWidget(parent) {
    _shorttitleLocale = 0;
    _longtitleLocale = 0;
    _publisherLocale = 0;
    _ratingsRegion = 0;

    addRequiredFields();
    addOptionalFields();
}

SMDHWidget::~SMDHWidget() {
    delete _shorttitle;
    delete _longtitle;
    delete _publisher;
    delete _agelimit;
    delete _active;
    delete _pending;
    delete _noage;
    delete _iconpicker;
    delete _smalliconpicker;
    delete _regionfree;
    QItemSelectionModel *m = _region->selectionModel();
    _region->setSelectionModel(0);
    delete m;
    _region->setModel(0);
    delete _regionModel;
    delete _region;
    delete _matchmaker;
    delete _matchmakerbit;
    delete _flagslayout;
    _eulaversion->setValidator(0);
    delete _rxValidator;
    delete _eulaversion;
    delete _optimalbannerframe;
    delete _streetpass;
}

void SMDHWidget::onActive() {
    if (_iconpicker->fileChanged()) {
        _iconpicker->reload();
    }

    if (_smalliconpicker->fileChanged()) {
        _iconpicker->reload();
    }
}

void SMDHWidget::loadFields() {
    QFileInfo fileInfo(SourceFile::smdhIconPath);
    if (!fileInfo.isDir() && fileInfo.exists()) {
        _iconpicker->setImage(SourceFile::smdhIconPath);
    } else {
        _iconpicker->reset();
    }
    QFileInfo smallFileInfo(SourceFile::smdhSmallIconPath);
    if (!smallFileInfo.isDir() && smallFileInfo.exists()) {
        _smalliconpicker->setImage(SourceFile::smdhSmallIconPath);
    } else {
        _smalliconpicker->reset();
    }
    _shorttitle->setText(SourceFile::shorttitle[_shorttitleLocale]);
    _longtitle->setText(SourceFile::longtitle[_longtitleLocale]);
    _publisher->setText(SourceFile::publisher[_publisherLocale]);

    disconnectRatings();
    QMap<QString, QVariant> rating = SourceFile::ratings[_ratingsRegion];
    _agelimit->setValue(rating["age"].toFloat() / 15.0);
    _active->setChecked(rating["active"].toBool());
    _pending->setChecked(rating["pending"].toBool());
    _noage->setChecked(rating["noage"].toBool());

    _regionfree->setChecked(SourceFile::isRegionFree);
    if (SourceFile::isRegionFree) {
        _region->setEnabled(false);
    } else {
        _region->setEnabled(true);
        _region->clearSelection();
        for(int i = 0; i < SourceFile::REGION.size(); i++) {
            if (SourceFile::regions.contains(SourceFile::REGION[i])) {
                _region->selectionModel()->select(_region->model()->index(i, 0), QItemSelectionModel::Select);
            }
        }
    }
    connectRatings();

    for (int i = 0; i < _flagslayout->count(); i++) {
        dynamic_cast<QCheckBox*>(_flagslayout->itemAt(i)->widget())->setChecked(SourceFile::flags.contains(SourceFile::FLAGS[i]));
    }

    _matchmaker->setText(SourceFile::matchmakerid);
    _matchmakerbit->setText(SourceFile::matchmakerbitid);

    _eulaversion->setText(SourceFile::eulaversion);
    _optimalbannerframe->setValue(SourceFile::optimalbannerframe);
    _streetpass->setText(SourceFile::streetpassid);
}

void SMDHWidget::resetFields() {
    _iconpicker->reset();
    _shorttitle->setText("");
    _longtitle->setText("");
    _publisher->setText("");
    _smalliconpicker->reset();
    disconnectRatings();
    _agelimit->setValue(0);
    _active->setChecked(false);
    _pending->setChecked(false);
    _noage->setChecked(false);
    connectRatings();
    _region->clearSelection();
    _region->setEnabled(true);
    _regionfree->setChecked(false);
    _matchmaker->setText("");
    _matchmakerbit->setText("");
    for (int i = 0; i < _flagslayout->count(); i++) {
        dynamic_cast<QCheckBox*>(_flagslayout->itemAt(i)->widget())->setChecked(false);
    }
    _eulaversion->setText("");
    _optimalbannerframe->setValue(0);
    _streetpass->setText("");
}

void SMDHWidget::shorttitleLocaleChanged(int index) {
    SourceFile::shorttitle[_shorttitleLocale] = _shorttitle->text();
    _shorttitle->setText(SourceFile::shorttitle[index]);
    _shorttitleLocale = index;
}

void SMDHWidget::shorttitleChanged(const QString &text) {
    SourceFile::shorttitle[_shorttitleLocale] = text;
    emit smdhRequiredFieldsChanged();
    SourceFile::recentlyModified = true;
}

void SMDHWidget::longtitleLocaleChanged(int index) {
    SourceFile::longtitle[_longtitleLocale] = _longtitle->text();
    SourceFile::longtitle[_longtitleLocale] = _longtitle->text();
    _longtitle->setText(SourceFile::longtitle[index]);
    _longtitleLocale = index;
}

void SMDHWidget::longtitleChanged(const QString &text) {
    SourceFile::longtitle[_longtitleLocale] = text;
    emit smdhRequiredFieldsChanged();
    SourceFile::recentlyModified = true;
}

void SMDHWidget::publisherLocaleChanged(int index) {
    SourceFile::publisher[_publisherLocale] = _publisher->text();
    SourceFile::publisher[_publisherLocale] = _publisher->text();
    _publisher->setText(SourceFile::publisher[index]);
    _publisherLocale = index;
}

void SMDHWidget::publisherChanged(const QString &text) {
    SourceFile::publisher[_publisherLocale] = text;
    emit smdhRequiredFieldsChanged();
    SourceFile::recentlyModified = true;
}

void SMDHWidget::iconChanged(bool) {
    SourceFile::smdhIconPath = _iconpicker->filepath();
    emit smdhRequiredFieldsChanged();
    SourceFile::recentlyModified = true;
}

void SMDHWidget::smallIconChanged(bool) {
    SourceFile::smdhSmallIconPath = _smalliconpicker->filepath();
    SourceFile::recentlyModified = true;
}

void SMDHWidget::ratingsRegionChanged(int region) {
    SourceFile::ratings[_ratingsRegion]["age"] = _agelimit->value() * 15.0;
    SourceFile::ratings[_ratingsRegion]["active"] = _active->isChecked();
    SourceFile::ratings[_ratingsRegion]["pending"] = _pending->isChecked();
    SourceFile::ratings[_ratingsRegion]["noage"] = _noage->isChecked();
    disconnectRatings();
    _agelimit->setValue(SourceFile::ratings[region]["age"].toFloat() / 15.0);
    _active->setChecked(SourceFile::ratings[region]["active"].toBool());
    _pending->setChecked(SourceFile::ratings[region]["pending"].toBool());
    _noage->setChecked(SourceFile::ratings[region]["noage"].toBool());
    _ratingsRegion = region;
    connectRatings();
}

void SMDHWidget::onAgeLimitChanged(int value) {
    SourceFile::ratings[_ratingsRegion]["age"] = value;
    SourceFile::recentlyModified = true;
}

void SMDHWidget::onActiveChanged(int) {
    SourceFile::ratings[_ratingsRegion]["active"] = _active->isChecked();
    SourceFile::recentlyModified = true;
}

void SMDHWidget::onPendingChanged(int) {
    SourceFile::ratings[_ratingsRegion]["pending"] = _pending->isChecked();
    SourceFile::recentlyModified = true;
}

void SMDHWidget::onNoAgeChanged(int) {
    SourceFile::ratings[_ratingsRegion]["noage"] = _noage->isChecked();
    SourceFile::recentlyModified = true;
}

void SMDHWidget::regionFreeChanged(bool state) {
    _region->setEnabled(!state);
    SourceFile::isRegionFree = state;
    SourceFile::recentlyModified = true;
}

void SMDHWidget::regionChanged(const QModelIndex &) {
    SourceFile::regions = QList<QString>();
    QList<QModelIndex> selection = _region->selectionModel()->selectedIndexes();
    for (int i = 0; i < selection.size(); i++) {
        QModelIndex select = selection[i];
        SourceFile::regions << SourceFile::REGION[select.row()];
    }
    SourceFile::recentlyModified = true;
}

void SMDHWidget::flagsChanged(bool state) {
    QCheckBox *checkbox = dynamic_cast<QCheckBox*>(QObject::sender());
    int index = _flagslayout->indexOf(checkbox);
    if (state && !SourceFile::flags.contains(SourceFile::FLAGS[index])) {
        SourceFile::flags.append(SourceFile::FLAGS[index]);
    } else {
        int sourceindex = SourceFile::flags.indexOf(SourceFile::FLAGS[index]);
        if (sourceindex >= 0) {
            SourceFile::flags.removeAt(sourceindex);
        }
    }
    SourceFile::recentlyModified = true;
}

void SMDHWidget::optimalBannerFrameChanged(double value) {
    SourceFile::optimalbannerframe = float(value);
    SourceFile::recentlyModified = true;
}

void SMDHWidget::matchMakerIdChanged(const QString &text) {
    SourceFile::matchmakerid = text;
    SourceFile::recentlyModified = true;
}

void SMDHWidget::matchMakerBitIdChanged(const QString &text) {
    SourceFile::matchmakerbitid = text;
    SourceFile::recentlyModified = true;
}

void SMDHWidget::eulaVersionChanged(const QString &text) {
    SourceFile::eulaversion = text;
    SourceFile::recentlyModified = true;
}

void SMDHWidget::streetpassChanged(const QString &text) {
    SourceFile::streetpassid = text;
    SourceFile::recentlyModified = true;
}

QVBoxLayout *SMDHWidget::createTextField(QString title, bool isMultiline) {
    QVBoxLayout *layout = new QVBoxLayout();
    layout->setContentsMargins(0, 0, 0, 0);

    QLabel *titleLabel = new QLabel(title);

    QHBoxLayout *hlayout = new QHBoxLayout();
    hlayout->setContentsMargins(0, 0, 0, 0);

    QStringList languages = QList<QString>() << tr("Default", "Language selection for SMDH titles/publisher")
        << tr("Japanese", "Language selection for SMDH titles/publisher")
        << tr("English", "Language selection for SMDH titles/publisher")
        << tr("French", "Language selection for SMDH titles/publisher")
        << tr("German", "Language selection for SMDH titles/publisher")
        << tr("Italian", "Language selection for SMDH titles/publisher")
        << tr("Spanish", "Language selection for SMDH titles/publisher")
        << tr("Simplified Chinese", "Language selection for SMDH titles/publisher")
        << tr("Korean", "Language selection for SMDH titles/publisher")
        << tr("Dutch", "Language selection for SMDH titles/publisher")
        << tr("Portuguese", "Language selection for SMDH titles/publisher")
        << tr("Russian", "Language selection for SMDH titles/publisher")
        << tr("Traditional Chinese", "Language selection for SMDH titles/publisher");

    QComboBox *locale = new QComboBox();
    locale->addItems(languages);

    if (isMultiline) {
        QTextEdit *textarea = new QTextEdit();
        textarea->setAcceptRichText(false);
        hlayout->addWidget(textarea, 1);
    } else {
        QLineEdit *inputfield = new QLineEdit();
        hlayout->addWidget(inputfield, 1);
    }

    hlayout->addWidget(locale, 0, Qt::AlignTop);

    layout->addWidget(titleLabel);
    layout->addLayout(hlayout);

    return layout;
}

void SMDHWidget::onKameTools() {
    QSettings settings;
    bool dither = settings.value("kame-tools/available").toBool() && settings.value("kame-tools/nodither").toBool();
    _iconpicker->setDither(dither);
    _smalliconpicker->setDither(dither);
}

void SMDHWidget::addRequiredFields() {
    _iconpicker = new ImagePicker();
    _iconpicker->setAccordionWidget(this);
    _iconpicker->setTitle(tr("Icon", "Title for SMDH icon image selection."));
    _iconpicker->setTooltip(tr("Icon used for the entry displayed in Anemone 3DS.", "Tooltip for SMDH icon image selection."));
    _iconpicker->setDimensions(QSize(48, 48));

    QVBoxLayout *titlefield = createTextField(tr("Short Title", "Title for SMDH short title."));
    _shorttitle = dynamic_cast<QLineEdit*>(dynamic_cast<QHBoxLayout*>(titlefield->itemAt(1))->itemAt(0)->widget());
    QVBoxLayout *descriptionfield = createTextField(tr("Long Title", "Title for SMDH long title."));
    _longtitle = dynamic_cast<QLineEdit*>(dynamic_cast<QHBoxLayout*>(descriptionfield->itemAt(1))->itemAt(0)->widget());
    QVBoxLayout *publisherfield = createTextField(tr("Publisher", "Title for SMDH publisher."));
    _publisher = dynamic_cast<QLineEdit*>(dynamic_cast<QHBoxLayout*>(publisherfield->itemAt(1))->itemAt(0)->widget());
    
    int index = createGroup(_accordion, tr("Required Fields", "Title for Theme Info widget required fields accordion."), tr("Required information metadata.", "Tooltip for Theme Info widget required fields accordion."), false);

    QFrame *required = getContent(_accordion, index);
    required->layout()->addWidget(_iconpicker);
    dynamic_cast<QVBoxLayout*>(required->layout())->addLayout(titlefield);
    dynamic_cast<QVBoxLayout*>(required->layout())->addLayout(descriptionfield);
    dynamic_cast<QVBoxLayout*>(required->layout())->addLayout(publisherfield);
    _accordion->getContentPane(index)->setMaximumHeight(required->sizeHint().height());

    connect(_iconpicker, &ImagePicker::stateChanged, this, &SMDHWidget::iconChanged);
    connect(dynamic_cast<QComboBox*>(dynamic_cast<QHBoxLayout*>(titlefield->itemAt(1))->itemAt(1)->widget()), QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SMDHWidget::shorttitleLocaleChanged);
    connect(dynamic_cast<QLineEdit*>(dynamic_cast<QHBoxLayout*>(titlefield->itemAt(1))->itemAt(0)->widget()), &QLineEdit::textEdited, this, &SMDHWidget::shorttitleChanged);
    connect(dynamic_cast<QComboBox*>(dynamic_cast<QHBoxLayout*>(descriptionfield->itemAt(1))->itemAt(1)->widget()), QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SMDHWidget::longtitleLocaleChanged);
    connect(dynamic_cast<QLineEdit*>(dynamic_cast<QHBoxLayout*>(descriptionfield->itemAt(1))->itemAt(0)->widget()), &QLineEdit::textEdited, this, &SMDHWidget::longtitleChanged);
    connect(dynamic_cast<QComboBox*>(dynamic_cast<QHBoxLayout*>(publisherfield->itemAt(1))->itemAt(1)->widget()), QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SMDHWidget::publisherLocaleChanged);
    connect(dynamic_cast<QLineEdit*>(dynamic_cast<QHBoxLayout*>(publisherfield->itemAt(1))->itemAt(0)->widget()), &QLineEdit::textEdited, this, &SMDHWidget::publisherChanged);
}

void SMDHWidget::addOptionalFields() {
    _smalliconpicker = new ImagePicker();
    _smalliconpicker->setAccordionWidget(this);
    _smalliconpicker->setTitle(tr("Small Icon", "Title for SMDH small icon image selection."));
    _smalliconpicker->setTooltip(tr("Small icon used for the entry displayed in Anemone 3DS.", "Tooltip for SMDH small icon image selection."));
    _smalliconpicker->setDimensions(QSize(24, 24));
    _smalliconpicker->setClearable(true);

    int index = createGroup(_accordion, tr("Optional Fields", "Title for Theme Info widget optional fields accordion."), tr("Optional information metadata.", "Tooltip for Theme Info widget optional fields accordion."), false);

    QFont font = QFont("Nimbus Sans", 11);
    QFrame *optional = getContent(_accordion, index);

    QLabel *ratingsLabel = new QLabel("Ratings");
    ratingsLabel->setFont(font);

    QStringList ratingslist = QList<QString>() << "CERO (" + tr("Japan", "Name of the country/region for which the SMDH rating field belongs to.") + ")"
        << "ESRB (" + tr("USA", "Name of the country/region for which the SMDH rating field belongs to.") + ")"
        << "USK (" + tr("Germany", "Name of the country/region for which the SMDH rating field belongs to.") + ")"
        << "PEGI GEN (" + tr("Europe", "Name of the country/region for which the SMDH rating field belongs to.") + ")"
        << "PEGI PRT (" + tr("Portugal", "Name of the country/region for which the SMDH rating field belongs to.") + ")"
        << "PEGI BBFC (" + tr("UK", "Name of the country/region for which the SMDH rating field belongs to.") + ")"
        << "COB (" + tr("Australia", "Name of the country/region for which the SMDH rating field belongs to.") + ")"
        << "GRB (" + tr("South Korea", "Name of the country/region for which the SMDH rating field belongs to.") + ")"
        << "CGSRR (" + tr("Taiwan", "Name of the country/region for which the SMDH rating field belongs to.") + ")";

    QComboBox *ratingregion = new QComboBox();
    ratingregion->addItems(ratingslist);
    
    _agelimit = new KameSlider();
    _agelimit->setTitle(tr("Age Limit", "Title for age limit in SMDH rating."));
    _agelimit->setUnits(tr("years", "Units for the age limit in SMDH rating."));
    _agelimit->setRange(0, 15);
    _agelimit->setStep(1);

    QGridLayout *ratingslayout = new QGridLayout();

    _active = new QCheckBox(tr("Active", "Option for SMDH rating."));
    _pending = new QCheckBox(tr("Rating Pending", "Option for SMDH rating."));
    _noage = new QCheckBox(tr("No Age Restriction", "Option for SMDH rating."));

    ratingslayout->setContentsMargins(0, 0, 0, 10);
    ratingslayout->addWidget(_active, 0, 0);
    ratingslayout->addWidget(_pending, 0, 1);
    ratingslayout->addWidget(_noage, 0, 2);

    QLabel *regionLabel = new QLabel(tr("Region", "Title for SMDH region."));
    regionLabel->setFont(font);
    _regionfree = new QCheckBox(tr("Region Free", "Option for SMDH region free."));
    _region = new QListView();
    _regionModel = new QStringListModel(QStringList()
            << tr("Japan", "Region selection for SMDH.")
            << tr("North America", "Region selection for SMDH.")
            << tr("Europe", "Region selection for SMDH.")
            << tr("Australia", "Region selection for SMDH.")
            << tr("China", "Region selection for SMDH.")
            << tr("Korea", "Region selection for SMDH.")
            << tr("Taiwan", "Region selection for SMDH."));
    _region->setModel(_regionModel);

    _region->setEditTriggers(QAbstractItemView::NoEditTriggers);
    _region->setSelectionMode(QAbstractItemView::ExtendedSelection);
    _region->setAlternatingRowColors(true);

    QLabel *matchmakerLabel = new QLabel(tr("Match Maker ID"));

    _matchmaker = new QLineEdit();

    QLabel *matchmakerbitLabel = new QLabel(tr("Match Maker BIT ID"));

    _matchmakerbit = new QLineEdit();

    QLabel *eulaversionLabel = new QLabel(tr("EULA Version", "Label for EULA version in SMDH."));
    _eulaversion = new QLineEdit();
    QRegularExpression regex("^(\\d+\\.)?(\\d+\\.)?(\\*|\\d+)$");
    _rxValidator = new QRegularExpressionValidator(regex);
    _eulaversion->setValidator(_rxValidator);
    
    _flagslayout = new QGridLayout();
    _flagslayout->setContentsMargins(0, 0, 0, 0);

    QLabel *flagsLabel = new QLabel(tr("Flags", "Title for flag options in SMDH."));
    flagsLabel->setFont(font);
    QCheckBox *visible = new QCheckBox(tr("Visible", "SMDH flag option"));
    QCheckBox *autoboot = new QCheckBox(tr("Autoboot", "SMDH flag option"));
    QCheckBox *allow3d = new QCheckBox(tr("Allow 3D", "SMDH flag option"));
    QCheckBox *requireeula = new QCheckBox(tr("Require EULA", "SMDH flag option"));
    QCheckBox *autosave = new QCheckBox(tr("Autosave", "SMDH flag option"));
    QCheckBox *extendedbanner = new QCheckBox(tr("Extended Banner", "SMDH flag option"));
    QCheckBox *ratingrequired = new QCheckBox(tr("Rating Required", "SMDH flag option"));
    QCheckBox *savedata = new QCheckBox(tr("Save Data", "SMDH flag option"));
    QCheckBox *recordusage = new QCheckBox(tr("Record Usage", "SMDH flag option"));
    QCheckBox *nosavebackups = new QCheckBox(tr("No Save Backups", "SMDH flag option"));
    QCheckBox *new3ds = new QCheckBox(tr("New 3DS", "SMDH flag option"));

    _flagslayout->setContentsMargins(0, 0, 0, 10);
    _flagslayout->addWidget(visible);
    _flagslayout->addWidget(autoboot, 0, 1);
    _flagslayout->addWidget(allow3d, 1, 0);
    _flagslayout->addWidget(requireeula, 1, 1);
    _flagslayout->addWidget(autosave, 2, 0);
    _flagslayout->addWidget(extendedbanner, 2, 1);
    _flagslayout->addWidget(ratingrequired, 3, 0);
    _flagslayout->addWidget(savedata, 3, 1);
    _flagslayout->addWidget(recordusage, 4, 0);
    _flagslayout->addWidget(nosavebackups, 4, 1);
    _flagslayout->addWidget(new3ds, 5, 0);

    QLabel *bnrLabel = new QLabel(tr("Optimal Animation Default Frame (for BNR)", "Title for SMDH option."));
    bnrLabel->setFont(font);
    _optimalbannerframe = new QDoubleSpinBox();
    _optimalbannerframe->setMaximum(0xffffffff);

    QLabel *streetpassLabel = new QLabel(tr("CEC (StreetPass) ID", "Title for SMDH option."));
    _streetpass = new QLineEdit();

    optional->layout()->addWidget(_smalliconpicker);
    optional->layout()->addWidget(ratingsLabel);
    optional->layout()->addWidget(ratingregion);
    optional->layout()->addWidget(_agelimit);
    dynamic_cast<QVBoxLayout*>(optional->layout())->addLayout(ratingslayout);
    optional->layout()->addWidget(regionLabel);
    optional->layout()->addWidget(_regionfree);
    optional->layout()->addWidget(_region);
    dynamic_cast<QVBoxLayout*>(optional->layout())->addSpacerItem(new QSpacerItem(0, 15));
    optional->layout()->addWidget(matchmakerLabel);
    optional->layout()->addWidget(_matchmaker);
    optional->layout()->addWidget(matchmakerbitLabel);
    optional->layout()->addWidget(_matchmakerbit);
    dynamic_cast<QVBoxLayout*>(optional->layout())->addSpacerItem(new QSpacerItem(0, 15));
    optional->layout()->addWidget(flagsLabel);
    dynamic_cast<QVBoxLayout*>(optional->layout())->addLayout(_flagslayout);
    optional->layout()->addWidget(eulaversionLabel);
    optional->layout()->addWidget(_eulaversion);
    dynamic_cast<QVBoxLayout*>(optional->layout())->addSpacerItem(new QSpacerItem(0, 15));
    optional->layout()->addWidget(bnrLabel);
    optional->layout()->addWidget(_optimalbannerframe);
    dynamic_cast<QVBoxLayout*>(optional->layout())->addSpacerItem(new QSpacerItem(0, 15));
    optional->layout()->addWidget(streetpassLabel);
    optional->layout()->addWidget(_streetpass);
    _accordion->getContentPane(index)->setMaximumHeight(optional->sizeHint().height());

    connect(_smalliconpicker, &ImagePicker::stateChanged, this, &SMDHWidget::smallIconChanged);
    connect(ratingregion, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SMDHWidget::ratingsRegionChanged);
    connectRatings();
    connect(_regionfree, &QCheckBox::stateChanged, this, &SMDHWidget::regionFreeChanged);
    connect(_region, &QAbstractItemView::clicked, this, &SMDHWidget::regionChanged);
    connect(visible, &QCheckBox::stateChanged, this, &SMDHWidget::flagsChanged);
    connect(allow3d, &QCheckBox::stateChanged, this, &SMDHWidget::flagsChanged);
    connect(requireeula, &QCheckBox::stateChanged, this, &SMDHWidget::flagsChanged);
    connect(autosave, &QCheckBox::stateChanged, this, &SMDHWidget::flagsChanged);
    connect(extendedbanner, &QCheckBox::stateChanged, this, &SMDHWidget::flagsChanged);
    connect(ratingrequired, &QCheckBox::stateChanged, this, &SMDHWidget::flagsChanged);
    connect(savedata, &QCheckBox::stateChanged, this, &SMDHWidget::flagsChanged);
    connect(recordusage, &QCheckBox::stateChanged, this, &SMDHWidget::flagsChanged);
    connect(nosavebackups, &QCheckBox::stateChanged, this, &SMDHWidget::flagsChanged);
    connect(new3ds, &QCheckBox::stateChanged, this, &SMDHWidget::flagsChanged);
    connect(_optimalbannerframe, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SMDHWidget::optimalBannerFrameChanged);
    connect(_matchmaker, &QLineEdit::textEdited, this, &SMDHWidget::matchMakerIdChanged);
    connect(_matchmakerbit, &QLineEdit::textEdited, this, &SMDHWidget::matchMakerBitIdChanged);
    connect(_eulaversion, &QLineEdit::textEdited, this, &SMDHWidget::eulaVersionChanged);
    connect(_streetpass, &QLineEdit::textEdited, this, &SMDHWidget::streetpassChanged);
}

void SMDHWidget::connectRatings() {
    connect(_agelimit, &KameSlider::valueChanged, this, &SMDHWidget::onAgeLimitChanged);
    connect(_active, &QCheckBox::stateChanged, this, &SMDHWidget::onActiveChanged);
    connect(_pending, &QCheckBox::stateChanged, this, &SMDHWidget::onPendingChanged);
    connect(_noage, &QCheckBox::stateChanged, this, &SMDHWidget::onNoAgeChanged);
}

void SMDHWidget::disconnectRatings() {
    disconnect(_agelimit, &KameSlider::valueChanged, this, &SMDHWidget::onAgeLimitChanged);
    disconnect(_active, &QCheckBox::stateChanged, this, &SMDHWidget::onActiveChanged);
    disconnect(_pending, &QCheckBox::stateChanged, this, &SMDHWidget::onPendingChanged);
    disconnect(_noage, &QCheckBox::stateChanged, this, &SMDHWidget::onNoAgeChanged);
}
