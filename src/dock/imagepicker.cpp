// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QDebug>

#include "imagepicker.h"
#include "icons/folder.h"
#include "icons/cross.h"
#include "icons/warning.h"
#include "../sourcefile.h"
#include "../resourcesdialog.h"

ImagePicker::ImagePicker(QWidget * parent) : QWidget(parent) {
    _rgb565warning = tr("This image appears to already be in RGB565 color mode, so it has been loaded without being dithered. If you want to dither it, change the file extension to exclude \".rgb565\" and load it again.", "Warn users that they're using an image that may already have been dithered, so Kame Editor does not dither it.");
    _title = new QLabel();
    _infodialog = new InfoDialog(this);
    _infoicon = new InfoIcon();
    _select = new QPushButton();
    _reset = new QPushButton();
    Warning *warning = new Warning();
    Folder *folder = new Folder();
    Cross *cross = new Cross();
    _warning = new QLabel();
    _warning->setPixmap(warning->icon());
    _warning->hide();

    QPixmap pixmap = folder->icon();
    QIcon icon(pixmap);
    _select->setIcon(icon);
    _select->setIconSize(pixmap.rect().size());

    QIcon crossicon(cross->icon());
    _reset->setIcon(crossicon);
    _reset->setIconSize(cross->icon().rect().size());

    _select->setObjectName("select");
    _select->setStyleSheet(folder->selectStyle());
    _select->setCursor(Qt::PointingHandCursor);

    _reset->setObjectName("reset");
    _reset->setStyleSheet(
            "QPushButton#reset {border: none; background-color: none; outline: none; padding: 0;}"
            );
    _reset->setCursor(Qt::PointingHandCursor);
    _reset->hide();

    _dither = false;
    _kametools = new KameTools();

    _fileinput = new QLabel(tr("(No file selected)", "Displayed near the file selection icon when no file is selected."));
    _thumbnail = new ThumbnailWidget();
    _caption = new QLabel("");
    QFont font = QFont("Nimbus Sans", 6);
    _caption->setFont(font);

    QHBoxLayout *titlelayout = new QHBoxLayout();
    titlelayout->addWidget(_title);
    titlelayout->addWidget(_infoicon);
    titlelayout->addWidget(_warning);
    titlelayout->addWidget(_reset);
    titlelayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Preferred));

    QHBoxLayout *hlayout = new QHBoxLayout();

    QVBoxLayout *thumblayout = new QVBoxLayout();
    thumblayout->addWidget(_thumbnail);
    thumblayout->addWidget(_caption, 1, Qt::AlignCenter);

    hlayout->addLayout(thumblayout);
    hlayout->addWidget(_fileinput, 1);
    hlayout->addWidget(_select);

    QVBoxLayout *vlayout = new QVBoxLayout();
    vlayout->setContentsMargins(0, 0, 0, 0);
    vlayout->addLayout(titlelayout);
    vlayout->addLayout(hlayout, 1);

    setLayout(vlayout);

    connect(_select, &QAbstractButton::clicked, this, &ImagePicker::onSelectClicked);
    connect(_thumbnail, &QAbstractButton::clicked, this, &ImagePicker::onSelectClicked);

    delete warning;
    delete folder;
    delete cross;
}

ImagePicker::~ImagePicker() {
    delete _thumbnail;
    delete _select;
    delete _reset;
    delete _fileinput;
    delete _title;
    delete _warning;
    delete _infoicon;
    delete _caption;
    delete _infodialog;
    delete _kametools;
}

void ImagePicker::setAccordionWidget(AccordionWidget *widget) {
    connect(this, &ImagePicker::stateChanged, widget, &AccordionWidget::onAvailabilityChanged);
}

void ImagePicker::setTitle(QString value) {
    _title->setText(value);
}

void ImagePicker::setTooltip(QString value) {
    _infoicon->setToolTip(value);
}

void ImagePicker::setInfoDialog(QString value, QString title, int minWidth) {
    disconnect(_infoicon, &QAbstractButton::clicked, _infodialog, &InfoDialog::show);
    connect(_infoicon, &QAbstractButton::clicked, _infodialog, &InfoDialog::show);
    _infodialog->setWindowTitle(title);
    _infodialog->addHtml(value, minWidth);
    _infodialog->showTemplatesButton();
}

void ImagePicker::setDimensions(QSize size) {
    _caption->setText(QString::number(size.width()) + "x" + QString::number(size.height()));
    _thumbnail->setDimensions(size);
    if (_thumbnail->image().isNull()) {
        _fileinput->setText(tr("(No file selected)"));
        _fileinput->setToolTip("");
        _filepath = "";
    }
}

void ImagePicker::onSelectClicked() {
    emit stateChanged(false);

    QString selectedFilter;
    QFileDialog filedialog;
    filedialog.setAcceptMode(QFileDialog::AcceptOpen);
    filedialog.setNameFilters(ResourcesDialog::FILETYPES["texture"].filters);
    filedialog.setWindowTitle(tr("Texture Image Selection", "Title for image selection dialog"));
    if (filedialog.exec()) {
        QString fileName = filedialog.selectedFiles().first();
        if (isImageValid(fileName)) {
            if (_dither) {
                QFileInfo pathInfo(fileName);
                if (pathInfo.completeSuffix().endsWith("rgb565.png")) {
                    _setImage(fileName);
                    emit stateChanged(true);
                    _warning->setToolTip(_rgb565warning);
                    _warning->show();
                } else {
                    qDebug() << "dither" << fileName;
                    connect(_kametools, &KameTools::imageRGB565Converted, this, &ImagePicker::onUserDithered);

                    _filepath = fileName;
                    QString ditherpath = pathInfo.dir().path() + "/" + pathInfo.completeBaseName() + ".rgb565.png";
                    _kametools->convertImageRGB565(fileName, ditherpath);
                    _warning->hide();
                }
            } else {
                _setImage(fileName);
                emit stateChanged(true);
            }
        }
        emit stateChanged(true);
    } else {
        emit stateChanged(true);
    }
}

bool ImagePicker::isImageValid(QString path, bool isUser) {
    return !path.isEmpty() && _thumbnail->validateDimensions(path, isUser);
}

bool ImagePicker::fileChanged() {
    QFileInfo fileInfo(_filepath);
    return !_filepath.isEmpty() && (_lastChanged < fileInfo.lastModified() || _filesize != fileInfo.size());
}

void ImagePicker::reload() {
    setImage(_filepath, true);
    // Prevents infinite message box errors
    setFileData(_filepath);
}

void ImagePicker::setImage(QString path, bool isUser) {
    if (isImageValid(path, isUser)) {
        _setImage(path);

        QFileInfo fileInfo(path);
        if (_dither) {
            if (fileInfo.completeSuffix().endsWith("rgb565.png")) {
                _warning->setToolTip(_rgb565warning);
                _warning->show();
            } else {
                QFile file(fileInfo.dir().path() + "/" + fileInfo.completeBaseName() + ".rgb565.png");
                qDebug() << "dither" << path;
                connect(_kametools, &KameTools::imageRGB565Converted, this, &ImagePicker::onDithered);

                _filepath = path;
                QString ditherpath = fileInfo.dir().path() + "/" + fileInfo.completeBaseName() + ".rgb565.png";
                _kametools->convertImageRGB565(path, ditherpath);
                _warning->hide();
            }
        }
    }
}

void ImagePicker::_setImage(QString path) {
    _thumbnail->setImage(SourceFile::rgb565path(path));
    _fileinput->setText(path);
    _fileinput->setToolTip(path);
    _filepath = path;
    setFileData(path);
}

void ImagePicker::setFileData(QString path) {
    QFileInfo fileInfo(path);
    _lastChanged = fileInfo.lastModified();
    _filesize = fileInfo.size();
}

void ImagePicker::onUserDithered(bool success, QString output) {
    disconnect(_kametools, &KameTools::imageRGB565Converted, this, &ImagePicker::onUserDithered);

    _onDithered(success, output);

    emit stateChanged(true);
}

void ImagePicker::onDithered(bool success, QString output) {
    disconnect(_kametools, &KameTools::imageRGB565Converted, this, &ImagePicker::onDithered);

    _onDithered(success, output);

    emit imageDithered();
}

void ImagePicker::_onDithered(bool success, QString output) {
    if (!success) {
        return;
    }
    _setImage(_filepath);
    qDebug() << output;
}

void ImagePicker::setClearable(bool value) {
    if (value) {
        _reset->show();
        connect(_reset, &QAbstractButton::clicked, this, &ImagePicker::clearImage);
    } else {
        _reset->hide();
        disconnect(_reset, &QAbstractButton::clicked, this, &ImagePicker::clearImage);
    }
}

void ImagePicker::reset() {
    _thumbnail->reset();
    _filepath = "";
    _fileinput->setText(tr("(No file selected)"));
    _fileinput->setToolTip("");
    _warning->hide();
    update();
}

void ImagePicker::clearImage() {
    reset();
    emit stateChanged(true);
}

QString ImagePicker::filepath() {
    return _filepath;
}

void ImagePicker::setDither(bool value) {
    _dither = value;
}
