// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FOLDER_H
#define FOLDER_H

#include <QObject>
#include <QPixmap>
#include <QPainter>
#include <QPainterPath>

#include "../../color.h"

class Folder : public QObject {
    Q_OBJECT
    public:
        Folder(QObject *parent = 0) : QObject(parent) {
            _icon = new QPixmap(16, 16);
            _icon->fill(QColor(Qt::transparent));
            QPainter *p = new QPainter(_icon);

            p->setRenderHints(QPainter::Antialiasing);

            QPainterPath path;
            path.moveTo(0, 3);
            path.lineTo(0, 14);
            path.moveTo(16, 14);
            path.lineTo(15, 4);
            path.lineTo(8, 4);
            path.lineTo(7, 3);
            path.lineTo(0, 3);

            QPen pen = QPen(Color::LIGHT);
            pen.setWidth(2);
            pen.setCosmetic(true);
            p->setPen(pen);
            p->drawPath(path);

            path = QPainterPath();
            path.moveTo(0, 5);
            path.lineTo(16, 5);
            path.moveTo(0, 14);
            path.lineTo(16, 14);

            pen.setWidth(1);
            
            p->setPen(pen);
            p->drawPath(path);

            p->end();
            delete p;
        }
        ~Folder() {
            delete _icon;
        }

        QString selectStyle() {
            QColor lighter = Color::MAIN.lighter(110);
            return "QPushButton#select {background-color: " + Color::MAIN.name() + "; border: none; border-radius: 3px; padding: 5px; outline: none;}"
            "QPushButton#select:hover {background-color: " + lighter.name() + ";}"
            "QPushButton#select:disabled {background-color: rgba(74, 154, 90, 128);}";
        }

        QString playStyle() {
            QColor lighter = Color::MAIN.lighter(110);
            return "QPushButton#play {background-color: " + Color::MAIN.name() + "; border: none; border-radius: 3px; padding: 5px; outline: none;}"
            "QPushButton#play:hover {background-color: " + lighter.name() + ";}"
            "QPushButton#play:disabled {background-color: rgba(74, 154, 90, 128);}";
        }

        QString stopStyle() {
            QColor lighter = Color::DARK.lighter(110);
            return "QPushButton#stop {background-color: " + Color::DARK.name() + "; border: 1px solid " + Color::MAIN.name() + "; border-radius: 3px; padding: 5px; outline: none;}"
                "QPushButton#stop:hover {background-color: " + lighter.name() + ";}"
                "QPushButton#stop:disabled {border-color: rgba(74, 154, 90, 128); color: " + Color::MAIN.name() + ";}";
        }

        QPixmap icon() {
            return *_icon;
        }

    private:
        QPixmap *_icon;
};

#endif // FOLDER_H
