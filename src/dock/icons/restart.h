// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef RESTART_H
#define RESTART_H

#include <QObject>
#include <QPixmap>
#include <QPainter>
#include <QPainterPath>

#include "../../color.h"

class Restart : public QObject {
    Q_OBJECT
    public:
        Restart(QColor color=QColor(Color::LIGHT)) : QObject() {
            _icon = new QPixmap(16, 16);
            _icon->fill(QColor(Qt::transparent));
            QPainter *p = new QPainter(_icon);

            p->setRenderHints(QPainter::Antialiasing);

            QPainterPath path;
            path.moveTo(6, 2);
            path.arcTo(1, 1, 12, 12, 135, 270);

            QPen pen = QPen(color);
            pen.setWidth(1);
            pen.setCosmetic(true);

            p->setPen(pen);
            p->setBrush(QColor(Qt::transparent));
            p->drawPath(path);

            path = QPainterPath();
            path.moveTo(10, 2);
            path.lineTo(14, 2);
            path.lineTo(10, 6);
            path.lineTo(10, 2);

            p->setPen(QPen(QColor(Qt::transparent)));
            p->setBrush(color);

            p->drawPath(path);

            p->end();
            delete p;
        }
        ~Restart() {
            delete _icon;
        }

        QPixmap icon() {
            return *_icon;
        }

    private:
        QPixmap *_icon;
};

#endif // RESTART_H
