// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CROSS_H
#define CROSS_H

#include <QObject>
#include <QPixmap>
#include <QPainter>
#include <QPainterPath>

class Cross : public QObject {
    Q_OBJECT
    public:
        Cross(QObject *parent = 0) : QObject(parent) {
            _icon = new QPixmap(16, 16);
            _icon->fill(QColor(Qt::transparent));
            QPainter *p = new QPainter(_icon);

            p->setRenderHints(QPainter::Antialiasing);

            QPen pen = QPen(QColor("#AF0E0E"));
            pen.setWidth(3);
            pen.setJoinStyle(Qt::MiterJoin);
            pen.setCapStyle(Qt::FlatCap);
            pen.setCosmetic(true);

            p->setPen(pen);
            p->setBrush(QColor(Qt::transparent));

            QPainterPath path;
            path.moveTo(4, 4);
            path.lineTo(12, 12);
            path.moveTo(12, 4);
            path.lineTo(4, 12);

            p->drawPath(path);

            p->end();
            delete p;
        }
        ~Cross() {
            delete _icon;
        }

        QPixmap icon() {
            return *_icon;
        }
    private:
        QPixmap *_icon;
};

#endif //CROSS_H
