// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ARROW_H
#define ARROW_H

#include <QObject>
#include <QPixmap>
#include <QPainter>
#include <QPainterPath>

#include "../../color.h"

class Arrow : public QObject {
    Q_OBJECT
    public:
        Arrow(QColor color = Color::LIGHT) : QObject() {
            _icon = new QPixmap(16, 16);
            _icon->fill(QColor(Qt::transparent));
            QPainter *p = new QPainter(_icon);

            p->setRenderHints(QPainter::Antialiasing);

            QPainterPath path;
            path.moveTo(4, 6);
            path.lineTo(8, 10);
            path.lineTo(12, 6);

            QPen pen = QPen(color);
            pen.setWidth(2);
            pen.setJoinStyle(Qt::MiterJoin);
            pen.setCapStyle(Qt::FlatCap);
            pen.setCosmetic(true);

            p->setPen(pen);
            p->drawPath(path);

            p->end();
            delete p;
        }
        ~Arrow() {
            delete _icon;
        }

        QPixmap down() {
            return *_icon;
        }

        QPixmap right() {
            return _icon->transformed(QTransform().rotate(-90));
        }

    private:
        QPixmap *_icon;
};

#endif // ARROW_H
