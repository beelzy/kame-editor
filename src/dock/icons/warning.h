// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef WARNING_H
#define WARNING_H

#include <math.h>
#include <QObject>
#include <QPixmap>
#include <QPainter>
#include <QPainterPath>

#include "../../color.h"

class Warning : public QObject {
    Q_OBJECT
    public:
        Warning(QColor color = Color::LIGHT) : QObject() {
            _icon = new QPixmap(16, 16);
            _icon->fill(QColor(Qt::transparent));
            QPainter *p = new QPainter(_icon);

            p->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

            float triangleHeight = 8 * sqrt(3);
            QPainterPath path;
            path.moveTo(8, (16 - triangleHeight) / 2.0);
            path.lineTo(16, 16 - (16 - triangleHeight) / 2.0);
            path.lineTo(0, 16 - (16 - triangleHeight) / 2.0);
            path.lineTo(8, (16 - triangleHeight) / 2.0);

            QPen pen = QPen(color);
            pen.setWidth(1.5);
            pen.setJoinStyle(Qt::RoundJoin);
            pen.setCosmetic(true);
            p->setPen(pen);
            p->setBrush(QColor(Qt::transparent));
            p->drawPath(path);
            p->setFont(QFont("Nimbus Sans", 8, QFont::Bold));
            p->drawText(6, 13, "!");

            p->end();
            delete p;
        }
        ~Warning() {
            delete _icon;
        }

        QPixmap icon() {
            return *_icon;
        }

    private:
        QPixmap *_icon;
};

#endif // WARNING_H
