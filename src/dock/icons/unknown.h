// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef UNKNOWN_H
#define UNKNOWN_H

#include <QObject>
#include <QPixmap>
#include <QPainter>

#include "../../color.h"

class UnknownIcon : public QObject {
    Q_OBJECT
    public:
        UnknownIcon() : QObject() {
            _icon = new QPixmap(16, 16);
            _icon->fill(QColor(Qt::transparent));
            QPainter *p = new QPainter(_icon);

            p->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

            QPen pen = QPen(Color::LIGHT);
            pen.setWidth(1.5);
            pen.setCosmetic(true);
            p->setPen(pen);
            p->drawEllipse(1, 1, 14, 14);
            p->setFont(QFont("Nimbus Sans", 8, QFont::Bold));
            p->drawText(5, 12, "?");

            p->end();
            delete p;
        }
        ~UnknownIcon() {
            delete _icon;
        }

        QPixmap icon() {
            return *_icon;
        }

    private:
        QPixmap *_icon;
};

#endif // UNKNOWN_H
