// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DISK_H
#define DISK_H

#include <QObject>
#include <QPixmap>
#include <QPainter>
#include <QPainterPath>

#include "../../color.h"

class Disk : public QObject {
    Q_OBJECT
    public:
        Disk(QObject *parent = 0) : QObject(parent) {
            _icon = new QPixmap(14, 14);
            _icon->fill(QColor(Qt::transparent));
            QPainter *p = new QPainter(_icon);

            p->setRenderHints(QPainter::Antialiasing);

            QPainterPath path;
            path.moveTo(0, 0);
            path.lineTo(14, 0);
            path.lineTo(14, 14);
            path.lineTo(3, 14);
            path.lineTo(0, 11);
            path.lineTo(0, 0);

            p->setPen(QColor(Qt::transparent));
            p->setBrush(Color::LIGHT);
            p->drawPath(path);

            QPainterPath label;
            label.moveTo(2, 0);
            label.lineTo(2, 5);
            label.lineTo(12, 5);
            label.lineTo(12, 0);

            p->setPen(Color::MAIN);
            p->setBrush(QColor(Qt::transparent));
            p->drawPath(label);

            QPainterPath bottom;
            bottom.addRect(4, 8, 8, 6);

            p->setPen(QColor(Qt::transparent));
            p->setBrush(Color::MAIN);
            p->drawPath(bottom);

            p->end();
            delete p;
        }
        ~Disk() {
            delete _icon;
        }

        QPixmap icon() {
            return *_icon;
        }

    private:
        QPixmap *_icon;
};

#endif // DISK_H
