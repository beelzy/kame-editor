// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef LOGO_H
#define LOGO_H

#include <math.h>
#include <QObject>
#include <QPixmap>
#include <QPainter>
#include <QPainterPath>

class Logo : public QObject {
    Q_OBJECT
    public:
        Logo() : QObject() {
        }

        const float RATIO = 1.2037037037;
        const float HCURVE = 0.34782608695652173;
        const float VCURVE = 0.2753623188405797;
        const float HEXHRATIO = 0.17391304347826086;
        const float INTERSECT = 0.65;

        QPainterPath base(float width) {
            float height = width * RATIO;

            QPainterPath path;

            path.moveTo(width / 2.0, 0);
            path.cubicTo(width / 2.0 + width * HCURVE, 0, width, height / 2.0 - height * VCURVE, width, height / 2.0);
            path.cubicTo(width, height / 2.0 + height * VCURVE, width / 2.0 + width * HCURVE, height, width / 2.0, height);
            path.cubicTo(width / 2.0 - width * HCURVE, height, 0, height / 2.0 + height * VCURVE, 0, height / 2.0);
            path.cubicTo(0, height / 2.0 - height * VCURVE, width / 2.0 - width * HCURVE, 0, width / 2.0, 0);

            return path;
        }

        QPainterPath lattice(float width) {
            float height = width * RATIO;
            float triangle = (width - width * HEXHRATIO * 2) / 4.0;

            QPolygonF hexagon;
            hexagon << QPointF(width * HEXHRATIO, height / 2.0) << QPointF(width * HEXHRATIO + triangle, height / 2.0 - triangle * sqrt(3)) << QPointF(width * HEXHRATIO + triangle * 3, height / 2.0 - triangle * sqrt(3)) << QPointF(width - width * HEXHRATIO, height / 2.0) << QPointF(HEXHRATIO * width + triangle * 3, height / 2.0 + triangle * sqrt(3)) << QPointF(HEXHRATIO * width + triangle, height / 2.0 + triangle * sqrt(3)) << QPointF(width * HEXHRATIO, height / 2.0);

            QPainterPath path;

            path.addPolygon(hexagon);
            path.moveTo(width * HEXHRATIO + triangle * 3, height / 2.0 - triangle * sqrt(3));
            path.lineTo(curveIntersect(QPointF(width, height / 2.0), QPointF(width, height / 2.0 - height * VCURVE), QPointF(width / 2.0 + width * HCURVE, 0), QPointF(width / 2.0, 0)));
            path.moveTo(width * HEXHRATIO + triangle * 3, height / 2.0 + triangle * sqrt(3));
            path.lineTo(curveIntersect(QPointF(width, height / 2.0), QPointF(width, height / 2.0 + height * VCURVE), QPointF(width / 2.0 + width * HCURVE, height), QPointF(width / 2.0, height)));
            path.moveTo(width * HEXHRATIO + triangle, height / 2.0 + triangle * sqrt(3));
            path.lineTo(curveIntersect(QPointF(0, height / 2.0), QPointF(0, height / 2.0 + height * VCURVE), QPointF(width / 2.0 - width * HCURVE, height),  QPointF(width / 2.0, height)));
            path.moveTo(width * HEXHRATIO + triangle, height / 2.0 - triangle * sqrt(3));
            path.lineTo(curveIntersect(QPointF(0, height / 2.0), QPointF(0, height / 2.0 - height * VCURVE), QPointF(width / 2.0 - width * HCURVE, 0), QPointF(width / 2.0, 0)));

            path.moveTo(width * HEXHRATIO, height / 2.0);
            path.lineTo(0, height / 2.0);
            path.moveTo(width - width * HEXHRATIO, height / 2.0);
            path.lineTo(width, height / 2.0);
            return path;
        }

    private:
        QPointF curveIntersect(QPointF p0, QPointF p1, QPointF p2, QPointF p3) {
            float x = pow((1 - INTERSECT), 3) * p0.x() + 3 * pow((1 - INTERSECT), 2) * INTERSECT * p1.x() + 3 * (1 - INTERSECT) * pow(INTERSECT, 2) * p2.x() + pow(INTERSECT, 3) * p3.x();
            float y = pow((1 - INTERSECT), 3) * p0.y() + 3 * pow((1 - INTERSECT), 2) * INTERSECT * p1.y() + 3 * (1 - INTERSECT) * pow(INTERSECT, 2) * p2.y() + pow(INTERSECT, 3) * p3.y();
            return QPointF(x, y);
        }
};

#endif // LOGO_H
