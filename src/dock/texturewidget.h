// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef TEXTUREWIDGET_H
#define TEXTUREWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QMap>
#include "QtColorWidgets/color_dialog.hpp"

#include "accordionwidget.h"
#include "imagepicker.h"
#include "swatchwidget.h"
#include "kameslider.h"
#include "../preview/foldericon.h"
#include "../preview/gridicon.h"

class TextureWidget : public AccordionWidget {
    Q_OBJECT
    public:
        explicit TextureWidget(QWidget *parent = 0);
        ~TextureWidget();

        enum class ImageType {
            SINGLE = 0,
            SCROLL = 1,
            TILING = 2
        };

        int getTopFrameType();
        int getBottomFrameType();
        ColorSwatch *topColorSwatch();
        ColorSwatch *bottomColorSwatch(QString layer, QString name);

    public slots:
        void reset();
        void load();
        void onKameTools();
        void onActive();

    signals:
        void topDrawTypeChanged(QString path);
        void topColorChanged(QColor color);
        void bottomDrawTypeChanged(QString path);
        void folderImageChanged(FolderIcon::State state, QPixmap image);
        void fileImageChanged(GridIcon::Size, QPixmap image);

        void setFolderVisibility(bool visible);
        void setFileVisibility(bool visible);

    protected:
        void addTopScreenTextures(ColorDialog *dialog);
        void addBottomScreenTextures(ColorDialog *dialog);
        void addTopScreenColors(ColorDialog *dialog);
        void addBottomScreenColors(ColorDialog *dialog);
        void addFolderTextures();
        void addFileTextures();
        void setTopTextureDimensions();
        void setBottomTextureDimensions();

    protected slots:
        void onTopDrawTypeChanged(int selection);
        void onBottomDrawTypeChanged(int selection);
        void onTopFrameTypeChanged(int selection);
        void onBottomFrameTypeChanged(int selection);
        void onTopTextureChanged(bool value);
        void onTopTextureExtChanged(bool value);
        void onBottomTextureChanged(bool value);
        void _onTopTextureChanged();
        void _onBottomTextureChanged();
        void onTopColorChanged(bool value);
        void onBottomColorChanged(bool value);
        void onTopValuesChanged(int value);
        void onFolderOpenTextureChanged(bool value);
        void onFolderCloseTextureChanged(bool value);
        void folderVisibilityChanged(bool value);
        void onFileLargeTextureChanged(bool value);
        void onFileSmallTextureChanged(bool value);
        void fileVisibilityChanged(bool value);

        void setTopEmptyData();
        void setTopColorData();
        QString setTopColorTextureData();
        QString setTopTextureData();

        void setBottomEmptyData();
        void setBottomColorData();
        QString setBottomTextureData();

    private:
        QComboBox *_topdrawtype;
        QComboBox *_topframetype;
        QComboBox *_bottomdrawtype;
        QComboBox *_bottomframetype;

        ImagePicker *_toptexture;
        ImagePicker *_toptextureext;
        ImagePicker *_bottomtexture;

        QWidget *_topcolorgroup;
        QWidget *_topframegroup;

        QWidget *_bottomcolorgroup;
        QWidget *_bottomframegroup;

        ImagePicker *_folderopen;
        ImagePicker *_folderclose;
        ImagePicker *_filelarge;
        ImagePicker *_filesmall;

        SwatchWidget *_topscreenbg;
        KameSlider *_gradientslider;
        KameSlider *_opacityslider;
        KameSlider *_altopacityslider;
        KameSlider *_gradientcolorslider;

        QMap<QString, QMap<QString, SwatchWidget*>> _bottomswatches;
        bool _folderEnabled;
        bool _fileEnabled;
};

#endif // TEXTUREWIDGET_H
