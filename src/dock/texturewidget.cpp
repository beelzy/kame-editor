// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QLabel>
#include <QVBoxLayout>
#include <QSettings>
#include <QFileInfo>

#include "texturewidget.h"
#include "../sourcefile.h"
#include "icons/unknown.h"
#include "../preview/topview.h"
#include "../preview/bottomview.h"

TextureWidget::TextureWidget(QWidget * parent) : AccordionWidget(parent) {
    ColorDialog *dialog = new ColorDialog();
    addTopScreenTextures(dialog);
    addBottomScreenTextures(dialog);
    addFolderTextures();
    addFileTextures();
    _folderEnabled = true;
    _fileEnabled = true;
}

TextureWidget::~TextureWidget() {
    delete _topdrawtype;
    delete _topframetype;
    delete _bottomdrawtype;
    delete _bottomframetype;

    delete _topscreenbg;
    delete _gradientslider;
    delete _opacityslider;
    delete _altopacityslider;
    delete _gradientcolorslider;
    delete _topcolorgroup;
    delete _bottomcolorgroup;

    delete _toptexture;
    delete _toptextureext;
    delete _bottomtexture;

    delete _topframegroup;
    delete _bottomframegroup;

    delete _folderopen;
    delete _folderclose;

    delete _filelarge;
    delete _filesmall;

    for(auto map : _bottomswatches.keys()) {
        qDeleteAll(_bottomswatches[map]);
    }
    _bottomswatches.clear();
}

void TextureWidget::load() {
    // Prevent signals from overwriting what we intend to load
    int topdraw = SourceFile::topdraw;
    int bottomdraw = SourceFile::bottomdraw;
    int topframe = SourceFile::topframe;
    int bottomframe = SourceFile::bottomframe;
    QString topextpath = SourceFile::topexttexture;
    QString toppath = SourceFile::toptexture[getTopFrameType()];
    QString bottompath = SourceFile::bottomtexture[getBottomFrameType()];

    QColor topbgcolor = qvariant_cast<QColor>(SourceFile::topcolors["color"]);
    float gradientalpha = SourceFile::topcolors["gradientalpha"].toFloat();
    float alpha = SourceFile::topcolors["alpha"].toFloat();
    float alphaext = SourceFile::topcolors["alphaext"].toFloat();
    float gradientbrightness = SourceFile::topcolors["gradientbrightness"].toFloat();

    _topscreenbg->swatch()->setColor(topbgcolor);
    _gradientslider->setValue(gradientalpha);
    _opacityslider->setValue(alpha);
    _altopacityslider->setValue(alphaext);
    _gradientcolorslider->setValue(gradientbrightness);

    for (auto swatch : _bottomswatches["outer"].keys()) {
        _bottomswatches["outer"][swatch]->swatch()->setColor(qvariant_cast<QColor>(SourceFile::bottomoutercolors[swatch]));
    }
    for (auto swatch : _bottomswatches["inner"].keys()) {
        _bottomswatches["inner"][swatch]->swatch()->setColor(qvariant_cast<QColor>(SourceFile::bottominnercolors[swatch]));
    }

    SourceFile::topexttexture = topextpath;

    if (toppath.isEmpty()) {
        _toptexture->reset();
    } else {
        _toptexture->setImage(toppath, false);
    }
    if (topextpath.isEmpty()) {
        _toptextureext->reset();
    } else {
        _toptextureext->setImage(topextpath, false);
    }
    if (bottompath.isEmpty()) {
        _bottomtexture->reset();
    } else {
        _bottomtexture->setImage(bottompath, false);
    }

    _topdrawtype->setCurrentIndex(topdraw);
    _topframetype->setCurrentIndex(topframe);
    _bottomdrawtype->setCurrentIndex(bottomdraw);
    _bottomframetype->setCurrentIndex(bottomframe);

    QFileInfo fileInfo(SourceFile::foldericons[0]);
    if (!fileInfo.isDir() && fileInfo.exists()) {
        _folderclose->setImage(SourceFile::foldericons[0]);
    } else {
        _folderclose->reset();
    }
    fileInfo = QFileInfo(SourceFile::foldericons[1]);
    if (!fileInfo.isDir() && fileInfo.exists()) {
        _folderopen->setImage(SourceFile::foldericons[1]);
    } else {
        _folderopen->reset();
    }
    fileInfo = QFileInfo(SourceFile::fileicons[0]);
    if (!fileInfo.isDir() && fileInfo.exists()) {
        _filelarge->setImage(SourceFile::fileicons[0]);
    } else {
        _filelarge->reset();
    }
    fileInfo = QFileInfo(SourceFile::fileicons[1]);
    if (!fileInfo.isDir() && fileInfo.exists()) {
        _filesmall->setImage(SourceFile::fileicons[1]);
    } else {
        _filesmall->reset();
    }

    // setCurrentIndex doesn't emit anything if it's not different from before...
    emit topDrawTypeChanged(SourceFile::toptexture[getTopFrameType()]);
    emit bottomDrawTypeChanged(SourceFile::bottomtexture[getBottomFrameType()]);

    emit folderImageChanged(FolderIcon::State::OPEN, QPixmap(_folderopen->filepath()));
    emit folderImageChanged(FolderIcon::State::CLOSE, QPixmap(_folderclose->filepath()));

    emit fileImageChanged(GridIcon::Size::LARGE, QPixmap(_filelarge->filepath()));
    emit fileImageChanged(GridIcon::Size::SMALL, QPixmap(_filesmall->filepath()));

    emit setFolderVisibility(true);
    emit setFileVisibility(true);

    SourceFile::recentlyModified = false;
}

void TextureWidget::reset() {
    _toptexture->reset();
    _toptextureext->reset();
    _bottomtexture->reset();
    _topdrawtype->setCurrentIndex(0);
    _topframetype->setCurrentIndex(0);
    _bottomdrawtype->setCurrentIndex(0);
    _bottomframetype->setCurrentIndex(0);

    _folderopen->reset();
    _folderclose->reset();
    _filelarge->reset();
    _filesmall->reset();

    _topscreenbg->swatch()->setColor(QColor(Qt::transparent));
    _gradientslider->setValue(0);
    _opacityslider->setValue(1.0);
    _altopacityslider->setValue(1.0);
    _gradientcolorslider->setValue(1.0);

    for (auto swatch : _bottomswatches["outer"].keys()) {
        _bottomswatches["outer"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }
    for (auto swatch : _bottomswatches["inner"].keys()) {
        _bottomswatches["inner"][swatch]->swatch()->setColor(QColor(Qt::transparent));
    }

    emit topDrawTypeChanged("");
    emit bottomDrawTypeChanged("");

    emit folderImageChanged(FolderIcon::State::OPEN, QPixmap());
    emit folderImageChanged(FolderIcon::State::CLOSE, QPixmap());

    emit fileImageChanged(GridIcon::Size::LARGE, QPixmap());
    emit fileImageChanged(GridIcon::Size::SMALL, QPixmap());

    emit setFolderVisibility(true);
    emit setFileVisibility(true);

    SourceFile::recentlyModified = false;
}

void TextureWidget::onActive() {
    if (_toptexture->fileChanged()) {
        _toptexture->reload();
        emit topDrawTypeChanged(SourceFile::toptexture[getTopFrameType()]);
    }
    if (_toptextureext->fileChanged()) {
        _toptextureext->reload();
        emit topDrawTypeChanged(SourceFile::toptexture[getTopFrameType()]);
    }
    if (_bottomtexture->fileChanged()) {
        _bottomtexture->reload();
        emit bottomDrawTypeChanged(SourceFile::bottomtexture[getBottomFrameType()]);
    }

    if (_folderclose->fileChanged()) {
        _folderclose->reload();
        emit folderImageChanged(FolderIcon::State::CLOSE, QPixmap(_folderclose->filepath()));
    }
    if (_folderopen->fileChanged()) {
        _folderopen->reload();
        emit folderImageChanged(FolderIcon::State::OPEN, QPixmap(_folderopen->filepath()));
    }
    if (_filelarge->fileChanged()) {
        _filelarge->reload();
        emit fileImageChanged(GridIcon::Size::LARGE, QPixmap(_filelarge->filepath()));
    }
    if (_filesmall->fileChanged()) {
        _filesmall->reload();
        emit fileImageChanged(GridIcon::Size::SMALL, QPixmap(_filesmall->filepath()));
    }
}

void TextureWidget::onKameTools() {
    QSettings settings;
    bool dither = settings.value("kame-tools/available").toBool() && settings.value("kame-tools/nodither").toBool();
    _toptexture->setDither(dither);
    _bottomtexture->setDither(dither);
}

void TextureWidget::setTopEmptyData() {
    SourceFile::topdraw = _topdrawtype->currentIndex();
}

void TextureWidget::setTopColorData() {
    SourceFile::topdraw = _topdrawtype->currentIndex();
    SourceFile::topcolors["color"] = _topscreenbg->color();
    SourceFile::topcolors["alpha"] = _opacityslider->value();
    SourceFile::topcolors["gradientalpha"] = _gradientslider->value();
    SourceFile::topcolors["gradientbrightness"] = _gradientcolorslider->value();
    SourceFile::topcolors["alphaext"] = _altopacityslider->value();
}

QString TextureWidget::setTopColorTextureData() {
    setTopColorData();
    SourceFile::topexttexture = _toptextureext->filepath();
    return SourceFile::toptexture[2];
}

QString TextureWidget::setTopTextureData() {
    SourceFile::topdraw = _topdrawtype->currentIndex();
    SourceFile::topframe = _topframetype->currentIndex();
    return SourceFile::toptexture[getTopFrameType()];
}

void TextureWidget::onTopDrawTypeChanged(int selection) {
    TopView::DrawType drawtype = static_cast<TopView::DrawType>(selection);
    QString path;
    if (drawtype == TopView::DrawType::COLOR) {
        _toptexture->hide();
        _toptextureext->hide();
        _topcolorgroup->show();
        _topframegroup->hide();
        setTopColorData();
    } else if (drawtype == TopView::DrawType::COLORTEXTURE) {
        _toptexture->show();
        setTopTextureDimensions();
        _toptextureext->show();
        _topcolorgroup->show();
        _topframegroup->hide();
        path = setTopColorTextureData();
    } else if (drawtype == TopView::DrawType::TEXTURE) {
        _toptexture->show();
        setTopTextureDimensions();
        _toptextureext->hide();
        _topcolorgroup->hide();
        _topframegroup->show();
        path = setTopTextureData();
    } else {
        _toptexture->hide();
        _toptextureext->hide();
        _topcolorgroup->hide();
        _topframegroup->hide();
        setTopEmptyData();
    }
    SourceFile::recentlyModified = true;
    emit topDrawTypeChanged(path);
}

void TextureWidget::onTopColorChanged(bool) {
    if (static_cast<TopView::DrawType>(_topdrawtype->currentIndex()) == TopView::DrawType::COLOR) {
        setTopColorData();
        emit topDrawTypeChanged("");
    } else {
        emit topDrawTypeChanged(setTopColorTextureData());
    }
    SourceFile::recentlyModified = true;
}

void TextureWidget::onBottomColorChanged(bool) {
    setBottomColorData();
    SourceFile::recentlyModified = true;
    emit bottomDrawTypeChanged("");
}

void TextureWidget::onTopValuesChanged(int) {
    if (static_cast<TopView::DrawType>(_topdrawtype->currentIndex()) == TopView::DrawType::COLOR) {
        setTopColorData();
        emit topDrawTypeChanged("");
    } else {
        emit topDrawTypeChanged(setTopColorTextureData());
    }
    SourceFile::recentlyModified = true;
}

void TextureWidget::onTopFrameTypeChanged(int) {
    setTopTextureDimensions();
    emit topDrawTypeChanged(setTopTextureData());
    SourceFile::recentlyModified = true;
}

void TextureWidget::setBottomEmptyData() {
    SourceFile::bottomdraw = _bottomdrawtype->currentIndex();
}

void TextureWidget::setBottomColorData() {
    SourceFile::bottomdraw = _bottomdrawtype->currentIndex();
    for (auto swatch : _bottomswatches["outer"].keys()) {
        SourceFile::bottomoutercolors[swatch] = _bottomswatches["outer"][swatch]->color();
    }
    for (auto swatch : _bottomswatches["inner"].keys()) {
        SourceFile::bottominnercolors[swatch] = _bottomswatches["inner"][swatch]->color();
    }
}

QString TextureWidget::setBottomTextureData() {
    SourceFile::bottomdraw = _bottomdrawtype->currentIndex();
    SourceFile::bottomframe = _bottomframetype->currentIndex();
    return SourceFile::bottomtexture[getBottomFrameType()];
}

void TextureWidget::onBottomDrawTypeChanged(int selection) {
    BottomView::DrawType drawtype = static_cast<BottomView::DrawType>(selection);
    QString path;
    if (drawtype == BottomView::DrawType::COLOR) {
        _bottomcolorgroup->show();
        _bottomtexture->hide();
        _bottomframegroup->hide();
        setBottomColorData();
    } else if (drawtype == BottomView::DrawType::TEXTURE) {
        _bottomcolorgroup->hide();
        setBottomTextureDimensions();
        _bottomtexture->show();
        _bottomframegroup->show();
        path = setBottomTextureData();
    } else {
        _bottomcolorgroup->hide();
        _bottomtexture->hide();
        _bottomframegroup->hide();
        setBottomEmptyData();
    }
    SourceFile::recentlyModified = true;
    emit bottomDrawTypeChanged(path);
}

void TextureWidget::onBottomFrameTypeChanged(int) {
    setBottomTextureDimensions();
    emit bottomDrawTypeChanged(setBottomTextureData());
    SourceFile::recentlyModified = true;
}

int TextureWidget::getTopFrameType() {
    int index = std::max(std::min(_topframetype->currentIndex(), 1), 0);
    if (_topdrawtype->currentIndex() == 2) {
        index = 2;
    }
    return index;
}

int TextureWidget::getBottomFrameType() {
    return std::min(std::max(_bottomframetype->currentIndex(), 0), 1);
}

ColorSwatch *TextureWidget::topColorSwatch() {
    return _topscreenbg->swatch();
}

ColorSwatch *TextureWidget::bottomColorSwatch(QString layer, QString name) {
    return _bottomswatches[layer][name]->swatch();
}

void TextureWidget::setTopTextureDimensions() {
    int index = getTopFrameType(); 
    switch (index) {
        case static_cast<int>(ImageType::SINGLE):
            _toptexture->setDimensions(QSize(512, 256));
            break;
        case static_cast<int>(ImageType::SCROLL):
            _toptexture->setDimensions(QSize(1024, 256));
            break;
        case static_cast<int>(ImageType::TILING):
        default:
            _toptexture->setDimensions(QSize(64, 64));
            break;
    }
    _toptexture->setImage(SourceFile::toptexture[index]);
}

void TextureWidget::setBottomTextureDimensions() {
    int index = getBottomFrameType();
    switch (index) {
        case static_cast<int>(ImageType::SINGLE):
            _bottomtexture->setDimensions(QSize(512, 256));
            break;
        case static_cast<int>(ImageType::SCROLL):
        default:
            _bottomtexture->setDimensions(QSize(1024, 256));
            break;
    }
    _bottomtexture->setImage(SourceFile::bottomtexture[index]);
}

void TextureWidget::onTopTextureChanged(bool) {
    _onTopTextureChanged();

    SourceFile::recentlyModified = true;
}

void TextureWidget::_onTopTextureChanged() {
    int index = getTopFrameType();
    SourceFile::toptexture[index] = _toptexture->filepath();
    if (static_cast<TopView::DrawType>(_topdrawtype->currentIndex()) == TopView::DrawType::TEXTURE) {
        emit topDrawTypeChanged(setTopTextureData());
    } else {
        emit topDrawTypeChanged(setTopColorTextureData());
    }
}

void TextureWidget::onTopTextureExtChanged(bool) {
    emit topDrawTypeChanged(setTopColorTextureData());
    SourceFile::recentlyModified = true;
}

void TextureWidget::onBottomTextureChanged(bool) {
    _onBottomTextureChanged();

    SourceFile::recentlyModified = true;
}

void TextureWidget::_onBottomTextureChanged() {
    SourceFile::bottomtexture[getBottomFrameType()] = _bottomtexture->filepath();
    emit bottomDrawTypeChanged(setBottomTextureData());
}

void TextureWidget::onFolderOpenTextureChanged(bool value) {
    if (value && _folderEnabled) {
        QString path = _folderopen->filepath().isEmpty() ? "" : _folderopen->filepath();
        SourceFile::foldericons[1] = path;
        emit folderImageChanged(FolderIcon::State::OPEN, QPixmap(path));
        SourceFile::recentlyModified = true;
    }
}

void TextureWidget::onFolderCloseTextureChanged(bool value) {
    if (value && _folderEnabled) {
        QString path = _folderclose->filepath().isEmpty() ? "" : _folderclose->filepath();
        SourceFile::foldericons[0] = path;
        emit folderImageChanged(FolderIcon::State::CLOSE, QPixmap(path));
        SourceFile::recentlyModified = true;
    }
}

void TextureWidget::folderVisibilityChanged(bool value) {
    _folderEnabled = value;
    if (value) {
        onFolderOpenTextureChanged(true);
        onFolderCloseTextureChanged(true);
    } else {
        SourceFile::foldericons = QList<QString>() << "" << "";
        emit folderImageChanged(FolderIcon::State::OPEN, QPixmap());
        emit folderImageChanged(FolderIcon::State::CLOSE, QPixmap());
        SourceFile::recentlyModified = true;
    }
}

void TextureWidget::onFileLargeTextureChanged(bool value) {
    if (value && _fileEnabled) {
        QString path = _filelarge->filepath();
        SourceFile::fileicons[0] = path;
        emit fileImageChanged(GridIcon::Size::LARGE, QPixmap(path));
        SourceFile::recentlyModified = true;
    }
}

void TextureWidget::onFileSmallTextureChanged(bool value) {
    if (value && _fileEnabled) {
        QString path = _filesmall->filepath();
        SourceFile::fileicons[1] = path;
        emit fileImageChanged(GridIcon::Size::SMALL, QPixmap(path));
        SourceFile::recentlyModified = true;
    }
}

void TextureWidget::fileVisibilityChanged(bool value) {
    _fileEnabled = value;
    if (value) {
        onFileLargeTextureChanged(true);
        onFileSmallTextureChanged(true);
    } else {
        SourceFile::fileicons = QList<QString>() << "" << "";
        emit fileImageChanged(GridIcon::Size::LARGE, QPixmap());
        emit fileImageChanged(GridIcon::Size::SMALL, QPixmap());
        SourceFile::recentlyModified = true;
    }
}

void TextureWidget::addTopScreenTextures(ColorDialog *dialog) {
    
    QLabel *topdrawlabel = new QLabel(tr("Draw Type", "Top screen draw type selection"));
    QLabel *topframelabel = new QLabel(tr("Frame Type", "Top screen frame type selection"));

    _topdrawtype = new QComboBox();
    _topdrawtype->addItems(QStringList()
        << tr("None", "Top screen draw type selection option")
        << tr("Color", "Top screen draw type selection option")
        << tr("Color Texture", "Top screen draw type selection option")
        << tr("Texture", "Top screen draw type selection option")
        );

    _toptexture = new ImagePicker();
    _toptexture->setAccordionWidget(this);
    _toptexture->setTitle(tr("Image", "Title for top screen images. Used by top screen frame types TEXTURE and COLOR TEXTURE (main texture)."));
    QString info = "<h3>" + tr("Top Screen with Texture Draw Type", "Title caption for detailed top texture help") + "</h3><p><img src=\":resources/docs/toptexture-single.png\"></img><br><img src=\":/resources/docs/toptexture-scroll.png\"></img></p><ol><li>" + tr("Frame Type Single") + "</li>"
                "<li>" + tr("Frame Type Fast Scroll or Slow Scroll") + "</li>"
                "<li>" + tr("Single Frame Image Dimensions") + "</li>"
                "<li>" + tr("Single Frame Visible Region (green = visible, gray = not visible)") + "</li>"
                "<li>" + tr("3D Holographic region bleed") + "</li>"
                "<li>" + tr("Scrolling Frames Image Dimensions") + "</li>"
                "<li>" + tr("Scrolling Frames Visible Region (green = visible, gray = not visible)") + "</li></ol><h3>" + tr("Top Screen with Color Texture Draw Type", "Title caption for detailed top texture help") + "</h3>" +
            tr("Tiling images are grayscale images where white is used to represent opaque pixels, and black is used for transparent pixels. For best results, do not use images with transparent pixels.", "Detailed information about top color textures.");
    QString title = tr("Top Screen Texture Help", "Title for detailed top texture help");
    _toptexture->setTooltip(tr("Image file for the top screen background, or the moving tiles pattern for the color texture draw type. Click for more details.", "Tooltip for top screen main texture selection."));
    //: Top screen texture detailed help
    _toptexture->setInfoDialog(info, title, 512);
    _toptexture->setDimensions(QSize(512, 256));

    _toptextureext= new ImagePicker();
    _toptextureext->setAccordionWidget(this);
    _toptextureext->setTitle(tr("Image Extra", "Title for top screen ext image. Only used in top screen frame type COLOR TEXTURE."));
    _toptextureext->setTooltip(tr("Image file for the static tiles.", "Tooltip for top screen ext texture selection."));
    _toptextureext->setInfoDialog(info, title, 512);
    _toptextureext->setDimensions(QSize(64, 64));
    _toptextureext->setClearable(true);

    addTopScreenColors(dialog);

    _topframetype = new QComboBox();
    _topframetype->addItems(QStringList()
            << tr("Single", "Top screen frame type selection option")
            << tr("Fast Scroll", "Top screen frame type selection option")
            << tr("Slow Scroll", "Top screen frame type selection option")
            );

    _topframegroup = new QWidget();

    InfoIcon *frameinfo = new InfoIcon();
    //: Top frame type tooltip explanation
    frameinfo->setToolTip("<ul>"
                "<li>" + tr("Single - Display texture as is") + "</li>"
                "<li>" + tr("Fast Scroll - A longer texture scrolls over the screen") + "</li>"
                "<li>" + tr("Slow Scroll - Same as fast scroll, but scrolling speed is reduced to 0.8x") + "</li></ul>");

    QHBoxLayout *frametitlelayout = new QHBoxLayout();
    frametitlelayout->addWidget(topframelabel);
    frametitlelayout->addWidget(frameinfo);
    frametitlelayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Preferred));

    QVBoxLayout *topframelayout = new QVBoxLayout();
    topframelayout->setContentsMargins(0, 0, 0, 0);

    topframelayout->addLayout(frametitlelayout);
    topframelayout->addWidget(_topframetype);

    _topframegroup->setLayout(topframelayout);

    int index = createGroup(_accordion, tr("Top Screen", "Title for top screen accordion."), tr("Textures for the top screen.", "Tooltip for top screen accordion."), false);

    QFrame *topscreen = getContent(_accordion, index);
    topscreen->layout()->addWidget(topdrawlabel);
    topscreen->layout()->addWidget(_topdrawtype);
    topscreen->layout()->addWidget(_toptexture);
    topscreen->layout()->addWidget(_toptextureext);
    topscreen->layout()->addWidget(_topcolorgroup);
    topscreen->layout()->addWidget(_topframegroup);
    _accordion->getContentPane(index)->setMaximumHeight(topscreen->sizeHint().height());

    _toptexture->hide();
    _toptextureext->hide();
    _topcolorgroup->hide();
    _topframegroup->hide();

    connect(_topdrawtype, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &TextureWidget::onTopDrawTypeChanged);
    connect(_topframetype, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &TextureWidget::onTopFrameTypeChanged);
    connect(_toptexture, &ImagePicker::stateChanged, this, &TextureWidget::onTopTextureChanged);
    connect(_toptexture, &ImagePicker::imageDithered, this, &TextureWidget::_onTopTextureChanged);
    connect(_toptextureext, &ImagePicker::stateChanged, this, &TextureWidget::onTopTextureExtChanged);
    connect(_topscreenbg->swatch(), &ColorSwatch::stateChanged, this, &TextureWidget::onTopColorChanged);
    connect(_gradientslider, &KameSlider::valueChanged, this, &TextureWidget::onTopValuesChanged);
    connect(_opacityslider, &KameSlider::valueChanged, this, &TextureWidget::onTopValuesChanged);
    connect(_altopacityslider, &KameSlider::valueChanged, this, &TextureWidget::onTopValuesChanged);
    connect(_gradientcolorslider, &KameSlider::valueChanged, this, &TextureWidget::onTopValuesChanged);
}

void TextureWidget::addTopScreenColors(ColorDialog *dialog) {
    _topscreenbg = new SwatchWidget(0, dialog);
    _topscreenbg->setTitle(tr("Background Color", "Top screen background color text in frame types COLOR and COLOR TEXTURE."));
    _topscreenbg->setAccordionWidget(this);

    QFont font = QFont("Nimbus Sans", 10);

    _gradientslider = new KameSlider();
    _gradientslider->setTitle(tr("Gradient", "Top screen gradient text in frame types COLOR and COLOR TEXTURE."));
    _gradientslider->setUnits("%");
    _gradientslider->setRange(0, 100);

    _opacityslider = new KameSlider();
    _opacityslider->setTitle(tr("Opacity", "Top screen opacity text in frame types COLOR and COLOR TEXTURE."));
    _opacityslider->setUnits("%");
    _opacityslider->setRange(0, 100);
    _opacityslider->setValue(1.0);

    _altopacityslider = new KameSlider();
    _altopacityslider->setTitle(tr("Static Texture Opacity", "Top screen static texture opacity in frame types COLOR and COLOR TEXTURE."));
    _altopacityslider->setUnits("%");
    _altopacityslider->setRange(0, 100);
    _altopacityslider->setValue(1.0);

    _gradientcolorslider = new KameSlider();
    _gradientcolorslider->setTitle(tr("Gradient Brightness", "Top screen gradient brightness in frame types COLOR and COLOR TEXTURE."));
    _gradientcolorslider->setUnits("%");
    _gradientcolorslider->setRange(0, 100);
    _gradientcolorslider->setValue(1.0);

    _topcolorgroup = new QWidget();
    QVBoxLayout *layout = new QVBoxLayout();
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(_topscreenbg);
    layout->addWidget(_gradientslider);
    layout->addWidget(_opacityslider);
    layout->addWidget(_altopacityslider);
    layout->addWidget(_gradientcolorslider);
    _topcolorgroup->setLayout(layout);
}

void TextureWidget::addBottomScreenTextures(ColorDialog *dialog) {
    QVBoxLayout *bottomframelayout = new QVBoxLayout();

    bottomframelayout->setContentsMargins(0, 0, 0, 0);

    QLabel *bottomdrawlabel = new QLabel(tr("Draw Type", "Bottom screen draw type selection"));
    QLabel *bottomframelabel = new QLabel(tr("Frame Type", "Bottom screen frame type selection"));

    _bottomdrawtype = new QComboBox();
    _bottomdrawtype->addItems(QStringList()
            << tr("None", "Bottom screen draw type selection option")
            << tr("Color", "Bottom screen draw type selection option")
            << tr("Texture", "Bottom screen draw type selection option")
            );

    addBottomScreenColors(dialog);

    _bottomtexture = new ImagePicker();
    _bottomtexture->setAccordionWidget(this);
    _bottomtexture->setTitle(tr("Image", "Title for bottom screen images. Used by bottom screen frame type TEXTURE."));
    _bottomtexture->setTooltip(tr("Image file for the bottom screen background. Click for more details.", "Tooltip for bottom screen texture selection."));
    //: Bottom screen texture detailed help
    _bottomtexture->setInfoDialog("<h3>" + tr("Bottom Screen with Texture Draw Type", "Title caption for detailed bottom texture help") + "</h3><p><img src=\":resources/docs/bottomtexture-single.png\"></img><br><img src=\":/resources/docs/bottomtexture-scroll.png\"></img><br><img src=\":/resources/docs/bottomtexture-page.png\"></img></p><ol><li>" + tr("Frame Type Single") + "</li>"
                "<li>" + tr("Frame Type Fast Scroll or Slow Scroll") + "</li>"
                "<li>" + tr("Frame Type Page Scroll or Bounce Scroll") + "</li>"
                "<li>" + tr("Single Frame Image Dimensions") + "</li>"
                "<li>" + tr("Single Frame Visible Region (green = visible, gray = not visible)") + "</li>"
                "<li>" + tr("Fast or Slow Scroll Frame Image Dimensions") + "</li>"
                "<li>" + tr("Fast or Slow Scroll Frame Visible Region (green = visible, gray = not visible)") + "</li>"
                "<li>" + tr("Offset image left 45 pixels to align with top screen (must also have the same frame type)") + "</li>"
                "<li>" + tr("Page or Bounce Scroll Frame 0 (within the dotted lines)") + "</li>"
                "<li>" + tr("Page or Bounce Scroll Frame 1 (within the dotted lines)") + "</li>"
                "<li>" + tr("Page or Bounce Scroll Frame 2 (within the dotted lines)") + "</li>"
                "<li>" + tr("Page or Bounce Scroll Frames Image Dimensions") + "</li>"
                "<li>" + tr("Page or Bounce Scroll Frames Visible Region (green = visible, gray = not visible)") + "</li></ol>", tr("Bottom Screen Texture Help", "Title for detailed bottom texture help"), 512);
    _bottomtexture->setDimensions(QSize(512, 256));

    _bottomframetype = new QComboBox();
    _bottomframetype->addItems(QStringList()
            << tr("Single", "Bottom screen frame type selection option")
            << tr("Fast Scroll", "Bottom screen frame type selection option")
            << tr("Slow Scroll", "Bottom screen frame type selection option")
            << tr("Page Scroll", "Bottom screen frame type selection option")
            << tr("Bounce Scroll", "Bottom screen frame type selection option")
            );

    _bottomframegroup = new QWidget();

    InfoIcon *frameinfo = new InfoIcon();
    frameinfo->setToolTip("<ul>"
            //: Bottom frame type explanation
            "<li>" + tr("Single - Display texture as is") + "</li>"
            "<li>" + tr("Fast Scroll - A longer texture scrolls over the screen") + "</li>"
                "<li>" + tr("Slow Scroll - Same as fast scroll, but scrolling speed is reduced to 0.8x") + "</li>"
                "<li>" + tr("Page Scroll - Scrolling/dragging through the bottom screen causes a frame animation to play. The order of the frames is 0, 1, 2, 0, 1, 2...") + "</li>"
                "<li>" + tr("Bounce Scroll - Same as Page Scroll, but the order of the frames is 0, 1, 2, 1, 0...") + "</li></ul>");

    QHBoxLayout *frametitlelayout = new QHBoxLayout();
    frametitlelayout->addWidget(bottomframelabel);
    frametitlelayout->addWidget(frameinfo);
    frametitlelayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Preferred));

    bottomframelayout->addLayout(frametitlelayout);
    bottomframelayout->addWidget(_bottomframetype);

    _bottomframegroup->setLayout(bottomframelayout);

    int index = createGroup(_accordion, tr("Bottom Screen", "Title for bottom screen accordion."), tr("Textures for the bottom screen.", "Tooltip for bottom screen accordion."), false);
    QFrame *bottomscreen = getContent(_accordion, index);
    bottomscreen->layout()->addWidget(bottomdrawlabel);
    bottomscreen->layout()->addWidget(_bottomdrawtype);
    bottomscreen->layout()->addWidget(_bottomcolorgroup);
    bottomscreen->layout()->addWidget(_bottomtexture);
    bottomscreen->layout()->addWidget(_bottomframegroup);
    _accordion->getContentPane(index)->setMaximumHeight(bottomscreen->sizeHint().height());

    _bottomcolorgroup->hide();
    _bottomtexture->hide();
    _bottomframegroup->hide();

    connect(_bottomdrawtype, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &TextureWidget::onBottomDrawTypeChanged);
    connect(_bottomframetype, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &TextureWidget::onBottomFrameTypeChanged);
    connect(_bottomtexture, &ImagePicker::stateChanged, this, &TextureWidget::onBottomTextureChanged);
    connect(_bottomtexture, &ImagePicker::imageDithered, this, &TextureWidget::_onBottomTextureChanged);
}

void TextureWidget::addBottomScreenColors(ColorDialog *dialog) {
    QFont font = QFont("Nimbus Sans", 11);
    QLabel *inner = new QLabel(tr("Inner Frame", "Title for inner frame colors in the bottom screen with frame type COLOR."));
    inner->setFont(font);

    SwatchWidget *dark = new SwatchWidget(0, dialog);
    dark->setTitle(tr("Dark", "Bottom screen inner frame dark color title"));
    dark->setAccordionWidget(this);
    SwatchWidget *main = new SwatchWidget(0, dialog);
    main->setTitle(tr("Main", "Bottom screen inner frame main color title"));
    main->setAccordionWidget(this);
    SwatchWidget *light = new SwatchWidget(0, dialog);
    light->setTitle(tr("Light", "Bottom screen inner frame light color title"));
    light->setAccordionWidget(this);
    SwatchWidget *shadow = new SwatchWidget(0, dialog);
    shadow->setTitle(tr("Shadow", "Bottom screen inner frame shadow color title"));
    shadow->setAccordionWidget(this);
    shadow->setShowAlpha(true);

    QLabel *outer = new QLabel(tr("Background", "Title for background colors in the bottom screen with frame type COLOR."));
    outer->setFont(font);

    SwatchWidget *darkouter = new SwatchWidget(0, dialog);
    darkouter->setTitle(tr("Dark", "Bottom screen background dark color title"));
    darkouter->setAccordionWidget(this);
    SwatchWidget *mainouter = new SwatchWidget(0, dialog);
    mainouter->setTitle(tr("Main", "Bottom screen background main color title"));
    mainouter->setAccordionWidget(this);
    SwatchWidget *lightouter = new SwatchWidget(0, dialog);
    lightouter->setTitle(tr("Light", "Bottom screen background light color title"));
    UnknownIcon *unknown = new UnknownIcon();
    lightouter->setIndicator(unknown->icon(), SwatchWidget::message(1));
    lightouter->setAccordionWidget(this);

    QVBoxLayout *layout = new QVBoxLayout();
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(inner);
    layout->addWidget(dark);
    layout->addWidget(main);
    layout->addWidget(light);
    layout->addWidget(shadow);
    layout->addWidget(outer);
    layout->addWidget(darkouter);
    layout->addWidget(mainouter);
    layout->addWidget(lightouter);

    _bottomcolorgroup = new QWidget();
    _bottomcolorgroup->setLayout(layout);

    QMap<QString, SwatchWidget*> innerMap;
    QMap<QString, SwatchWidget*> outerMap;

    connect(dark->swatch(), &ColorSwatch::stateChanged, this, &TextureWidget::onBottomColorChanged);
    connect(main->swatch(), &ColorSwatch::stateChanged, this, &TextureWidget::onBottomColorChanged);
    connect(light->swatch(), &ColorSwatch::stateChanged, this, &TextureWidget::onBottomColorChanged);
    connect(shadow->swatch(), &ColorSwatch::stateChanged, this, &TextureWidget::onBottomColorChanged);

    connect(darkouter->swatch(), &ColorSwatch::stateChanged, this, &TextureWidget::onBottomColorChanged);
    connect(mainouter->swatch(), &ColorSwatch::stateChanged, this, &TextureWidget::onBottomColorChanged);
    connect(lightouter->swatch(), &ColorSwatch::stateChanged, this, &TextureWidget::onBottomColorChanged);

    innerMap["dark"] = dark;
    innerMap["main"] = main;
    innerMap["light"] = light;
    innerMap["shadow"] = shadow;

    outerMap["dark"] = darkouter;
    outerMap["main"] = mainouter;
    outerMap["light"] = lightouter;

    _bottomswatches["inner"] = innerMap;
    _bottomswatches["outer"] = outerMap;

    delete unknown;
}

void TextureWidget::addFolderTextures() {
    _folderopen = new ImagePicker();
    _folderopen->setAccordionWidget(this);
    _folderopen->setTitle(tr("Open", "Title for open folder image selection."));
    _folderopen->setDimensions(QSize(128, 64));
    _folderopen->setTooltip(tr("Image for the folder icon when it is opened. Click for more details.", "Tooltip for open folder image selection."));
    //: Open folder texture detailed help
    _folderopen->setInfoDialog("<h3>" + tr("Folder Texture: Opened", "Title caption for detailed folder texture help.") + "</h3><p><img src=\":/resources/docs/folder-open.png\"></img></p><ol><li>"
            + tr("Folder Foreground Texture (green)") + "</li>"
                "<li>" + tr("Folder Background Texture (gray)") + "</li></ol>" + tr("Exact templates can be found at %1.", "Opened folder texture detailed help note").arg("https://gitlab.io/beelzy/kame-editor"), tr("Folder Texture Help", "Window title for folder texture detailed help"));
    _folderopen->setClearable(true);

    _folderclose = new ImagePicker();
    _folderclose->setAccordionWidget(this);
    _folderclose->setTitle(tr("Default", "Title for closed folder image selection."));
    _folderclose->setDimensions(QSize(128, 64));
    _folderclose->setTooltip(tr("Image for the folder icon when it is closed. Click for more details.", "Tooltip for closed folder image selection."));
    //: Default folder texture detailed help
    _folderclose->setInfoDialog("<h3>" + tr("Folder Texture: Default", "Title caption for detailed folder texture help.") + "</h3><p><img src=\":/resources/docs/folder-close.png\"></img></p><ol><li>"
            + tr("Folder Foreground Texture (green)") + "</li>"
                "<li>" + tr("Folder Background Texture (gray)") + "</li></ol>" + tr("Exact templates can be found at %1.", "Default folder texture detailed help note").arg("https://gitlab.io/beelzy/kame-editor"), tr("Folder Texture Help", "Window title for folder texture detailed help"));
    _folderclose->setClearable(true);

    int index = createGroup(_accordion, tr("Folder Icons", "Title for folder icons accordion."), tr("Textures for the folder icons in the bottom screen.", "Tooltip for folder icons accordion."), true);
    QFrame *folder = getContent(_accordion, index);
    folder->layout()->addWidget(_folderclose);
    folder->layout()->addWidget(_folderopen);
    _accordion->getContentPane(index)->setMaximumHeight(folder->sizeHint().height());

    connect(_folderopen, &ImagePicker::stateChanged, this, &TextureWidget::onFolderOpenTextureChanged);
    connect(_folderclose, &ImagePicker::stateChanged, this, &TextureWidget::onFolderCloseTextureChanged);
    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &TextureWidget::folderVisibilityChanged);
    connect(this, &TextureWidget::setFolderVisibility, _accordion->getContentPane(index), &ContentPane::setIconVisibility);
}

void TextureWidget::addFileTextures() {
    _filelarge = new ImagePicker();
    _filelarge->setAccordionWidget(this);
    _filelarge->setTitle(tr("Large", "Title for large file frame image selection."));
    _filelarge->setDimensions(QSize(64, 128));
    _filelarge->setTooltip(tr("Image for the file icon when bottom screen grid zoom is large enough. Click for more details.", "Tooltip for large file frame image selection."));
    //: Large file icon detailed help
    _filelarge->setInfoDialog("<h3>" + tr("Large File Icon", "Title caption for large file icon detailed help.") + "</h3><p><img src=\":/resources/docs/large-icon.png\"></img></p><ol><li>"
            + tr("Icon Frame Region (gray)") + "</li>"
                "<li>" + tr("Icon Region (green)") + "</li></ol>" + tr("The 3DS renders icon frames with rounded corners, so some portion of the corners will not be visible. The icon is mirrored horizontally, so the image contains only the right half of the icon. The icon may also be scaled to fit in different zoom levels.", "Large file icon detailed help note"), tr("Large File Icon Help", "Window title for large file icon detailed help"));
    _filelarge->setClearable(true);

    _filesmall = new ImagePicker();
    _filesmall->setAccordionWidget(this);
    _filesmall->setTitle(tr("Small", "Title for small file frame image selection."));
    _filesmall->setDimensions(QSize(32, 64));
    _filesmall->setTooltip(tr("Image for the file icon when bottom screen grid zoom is small enough.", "Tooltip for small file frame image selection."));
    //: Small file icon detailed help
    _filesmall->setInfoDialog("<h3>" + tr("Small File Icon", "Title caption for small file icon detailed help.") + "</h3><p><img src=\":/resources/docs/small-icon.png\"></img></p><ol><li>"
            + tr("Icon Frame Region (gray)") + "</li>"
                "<li>" + tr("Icon Region (green)") + "</li></ol>" + tr("The 3DS renders icon frames with rounded corners, so some portion of the corners will not be visible. The icon is mirrored horizontally, so the image contains only the right half of the icon. The icon may also be scaled to fit in different zoom levels.", "Small file icon detailed help note"), tr("Small File Icon Help", "Window title for small file icon detailed help"));
    _filesmall->setClearable(true);

    int index = createGroup(_accordion, tr("File Icons", "Title for file icons accordion."), tr("Textures for the file icons in the bottom screen.", "Tooltip for file icons accordion."), true);
    QFrame *file = getContent(_accordion, index);
    file->layout()->addWidget(_filelarge);
    file->layout()->addWidget(_filesmall);
    _accordion->getContentPane(index)->setMaximumHeight(file->sizeHint().height());

    connect(_filelarge, &ImagePicker::stateChanged, this, &TextureWidget::onFileLargeTextureChanged);
    connect(_filesmall, &ImagePicker::stateChanged, this, &TextureWidget::onFileSmallTextureChanged);
    connect(_accordion->getContentPane(index), &ContentPane::visibilityToggled, this, &TextureWidget::fileVisibilityChanged);
    connect(this, &TextureWidget::setFileVisibility, _accordion->getContentPane(index), &ContentPane::setIconVisibility);
}
