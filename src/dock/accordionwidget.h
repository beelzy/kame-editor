// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ACCORDIONWIDGET_H
#define ACCORDIONWIDGET_H

#include <QObject>

#include "kameaccordion/kameaccordion.h"
#include "kamedockwidget.h"

class AccordionWidget : public QObject {
    public:
        explicit AccordionWidget(QWidget *parent = 0);
        ~AccordionWidget();

        KameAccordion *contents();
        void setDockWidget(KameDockWidget *widget);

    public slots:
        void onAvailabilityChanged(bool isEnabled);

    protected:
        int createGroup(KameAccordion *accordion, QString title, QString tooltip, bool visibilityIcon, bool restoreOptions=false);
        QFrame *getContent(KameAccordion *accordion, int index);
        KameAccordion *_accordion;

    private:
        KameDockWidget *_dockWidget;
};

#endif // ACCORDIONWIDGET_H
