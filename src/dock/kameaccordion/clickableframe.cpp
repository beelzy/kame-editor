// This file is part of qAccordion. An Accordion widget for Qt
// Copyright © 2015, 2017 Christian Rapp <0x2a at posteo dot org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "clickableframe.h"
#include "../icons/arrow.h"
#include "../../color.h"

ClickableFrame::ClickableFrame(QString header, QWidget *parent,
                               Qt::WindowFlags f)
    : iconLabel(nullptr),
      nameLabel(nullptr),
      header(std::move(header)),
      QFrame(parent, f)
{
    darkMode = false;
    this->setAttribute(Qt::WA_Hover, true);
    this->headerTrigger = TRIGGER::SINGLECLICK;
    this->setCursor(Qt::PointingHandCursor);
    QColor background = Color::MAIN;
    QColor lighter = background.lighter(110);
    this->normalStylesheet = "";
    this->hoverStylesheet = "background-color: " + lighter.name() + ";";
    this->initFrame();
}

void ClickableFrame::toggleVisibility(bool visible) {
    QColor background = Color::MAIN;
    QColor lighter = background.lighter(110);
    QString property = darkMode ? "background: none; color" : "background-color";

    if (visible) {
        this->normalStylesheet = property + ": " + background.name() + ";";
    } else {
        background = QColor("#3A7C54");
        lighter = background.lighter(110);
        this->normalStylesheet = property + ": " + background.name() + ";";
    }
    this->hoverStylesheet = property + ": " + lighter.name() + ";";
    this->setNormalStylesheet(this->normalStylesheet);
}

void ClickableFrame::changeVisibilityState(int state) {
    this->toggleVisibility(state == Qt::Checked);
}

void ClickableFrame::setTrigger(ClickableFrame::TRIGGER tr)
{
    this->headerTrigger = tr;
    if (tr != TRIGGER::NONE) {
        this->setCursor(Qt::PointingHandCursor);
    } else {
        this->setCursor(Qt::ForbiddenCursor);
    }
}

ClickableFrame::TRIGGER ClickableFrame::getTrigger()
{
    return this->headerTrigger;
}

void ClickableFrame::setHeader(QString header)
{
    this->header = std::move(header);
    this->nameLabel->setText(this->header);
}

QString ClickableFrame::getHeader() { return this->header; }

void ClickableFrame::setIcon(const QPixmap &icon)
{
    this->iconLabel->setPixmap(icon);
}

//void ClickableFrame::setIconPosition(ClickableFrame::ICON_POSITION pos) {}

void ClickableFrame::setNormalStylesheet(QString stylesheet)
{
    this->normalStylesheet = std::move(stylesheet);
    this->setStyleSheet(this->normalStylesheet);
}

QString ClickableFrame::getNormalStylesheet() { return this->normalStylesheet; }

void ClickableFrame::setHoverStylesheet(QString stylesheet)
{
    this->hoverStylesheet = std::move(stylesheet);
}

QString ClickableFrame::getHoverStylesheet() { return this->hoverStylesheet; }

InfoIcon* ClickableFrame::info() {
    return infoIcon;
}

EyeIcon* ClickableFrame::visibleIcon() {
    return eyeIcon;
}

QPushButton* ClickableFrame::loadIcon() {
    return _loadIcon;
}

QPushButton* ClickableFrame::saveIcon() {
    return _saveIcon;
}

void ClickableFrame::setDarkMode(bool value) {
    darkMode = value;
    QColor green = Color::MAIN;
    QColor lighter = green.lighter(110);
    if (darkMode) {
        Arrow *arrow = new Arrow(Color::MAIN);
        this->iconLabel->setPixmap(arrow->right());
        this->normalStylesheet = "background: none;";
        this->hoverStylesheet = "background: none; color: " + lighter.name() + ";";
        delete arrow;
    } else {
        this->normalStylesheet = "";
        this->hoverStylesheet = "background-color: " + lighter.name() + ";";
    }
}

bool ClickableFrame::darkmode() {
    return darkMode;
}

void ClickableFrame::initFrame()
{
    this->setSizePolicy(QSizePolicy::Policy::Preferred,
                        QSizePolicy::Policy::Fixed);
    this->setLayout(new QHBoxLayout());

    QFont font = QFont("Nimbus Sans", 10);

    this->iconLabel = new QLabel();
    Arrow *arrow = new Arrow();
    this->iconLabel->setPixmap(arrow->right());
    this->layout()->addWidget(this->iconLabel);

    this->nameLabel = new QLabel();
    this->nameLabel->setFont(font);
    nameLabel->setText(this->header);
    this->layout()->addWidget(nameLabel);
    this->eyeIcon = new EyeIcon();
    this->infoIcon = new InfoIcon();
    this->_loadIcon = new QPushButton();
    this->_saveIcon = new QPushButton();
    Folder *folder = new Folder();
    Disk *disk = new Disk();
    QPixmap pixmap = folder->icon();
    QPixmap save = disk->icon();
    _loadIcon->setIcon(QIcon(pixmap));
    _saveIcon->setIcon(QIcon(save));

    dynamic_cast<QHBoxLayout *>(this->layout())->addStretch();

    dynamic_cast<QHBoxLayout *>(this->layout())->addWidget(this->_loadIcon, 0, Qt::AlignRight);
    dynamic_cast<QHBoxLayout *>(this->layout())->addWidget(this->_saveIcon, 0, Qt::AlignRight);
    dynamic_cast<QHBoxLayout *>(this->layout())->addWidget(this->infoIcon, 0, Qt::AlignRight);
    dynamic_cast<QHBoxLayout *>(this->layout())->addWidget(this->eyeIcon, 0, Qt::AlignRight);

    this->eyeIcon->hide();
    this->infoIcon->hide();
    this->_loadIcon->hide();
    this->_saveIcon->hide();

    this->setStyleSheet(this->normalStylesheet);

    connect(this->eyeIcon, &QAbstractButton::toggled, this, &ClickableFrame::toggleVisibility);
    connect(this->eyeIcon, &QCheckBox::stateChanged, this, &ClickableFrame::changeVisibilityState);

    delete arrow;
    delete folder;
}

void ClickableFrame::mousePressEvent(QMouseEvent *event)
{
    if (this->headerTrigger == TRIGGER::SINGLECLICK) {
        emit this->triggered(event->pos());
        event->accept();
    } else {
        event->ignore();
    }
}

void ClickableFrame::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (this->headerTrigger == TRIGGER::DOUBLECLICK) {
        emit this->triggered(event->pos());
        event->accept();
    } else {
        QWidget::mouseDoubleClickEvent(event);
    }
}

// TODO: No Stylesheet change when TRIGGER::NONE?
void ClickableFrame::enterEvent(QEnterEvent *event)
{
    if (this->headerTrigger != TRIGGER::NONE) {
        this->setStyleSheet(this->hoverStylesheet);
    }
}

void ClickableFrame::leaveEvent(QEvent *event)
{
    if (this->headerTrigger != TRIGGER::NONE) {
        this->setStyleSheet(this->normalStylesheet);
    }
}
