// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include <QPainter>

#include "eyeicon.h"
#include "../../color.h"
#include "../icons/cross.h"

EyeIcon::EyeIcon(QWidget *parent) : QCheckBox(parent) {
    setFixedSize(16, 16);
    setCheckable(true);
    setChecked(true);
}

void EyeIcon::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    QPainterPath path;
    path.moveTo(1, 8);

    QPen pen = QPen(Color::LIGHT);
    pen.setWidth(1.5);
    pen.setCosmetic(true);

    if (!isChecked() || (isTristate() && checkState() == Qt::PartiallyChecked)) {
        path.cubicTo(QPointF(4, 10), QPointF(12, 10), QPointF(15, 8));
    } else {
        path.cubicTo(QPointF(6, 2), QPointF(10, 2), QPointF(15, 8));
        path.cubicTo(QPointF(10, 14), QPointF(6, 14), QPointF(1, 8));

        p.setPen(pen);
        p.setBrush(QColor(Qt::transparent));
        p.drawPath(path);

        path = QPainterPath();
        path.addEllipse(5.5, 5.5, 5, 5);

        pen.setWidth(2);
    }

    p.setPen(pen);
    p.drawPath(path);

    if (isTristate() && !isChecked()) {
        Cross *cross = new Cross();
        p.drawPixmap(0, 0, 16, 16, cross->icon());
        delete cross;
    }
}
