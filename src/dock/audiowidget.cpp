// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <portaudio.h>
#include <QMessageBox>
#include <QSettings>

#include "audiowidget.h"
#include "icons/folder.h"
#include "icons/warning.h"
#include "../sourcefile.h"
#include "../color.h"
#include "infodialog.h"

const char *AudioWidget::noFile = QT_TRANSLATE_NOOP("AudioWidget", "(No file selected)");

AudioWidget::AudioWidget(QWidget * parent) : AccordionWidget(parent) {
    _bgmstate = false;
    _inputstate = false;
    _rstmcppFound = true;
    _state["cursor"] = false;
    _state["launch"] = false;
    _state["folder"] = false;
    _state["close"] = false;
    _state["frame0"] = false;
    _state["frame1"] = false;
    _state["frame2"] = false;
    _state["resume"] = false;

    _progress = nullptr;
    _format = Format::BCSTM;
    _converter = new QProcess();

    if (Pa_Initialize() == paNoError) {
        addConverter();
        addBGM();
        addSFX();

        checkProcesses();
    }
}

AudioWidget::~AudioWidget() {
    Pa_Terminate();
    delete _rstmcppCheck;
    delete _vgmstreamCheck;
    delete _input;
    delete _formatbox;
    delete _selectoutput;
    delete _output;
    delete _bgmassignment;
    delete _sfxassignment;
    delete _convert;
    delete _converter;

    delete _placeholder;
    delete _totalsfx;
    delete _totalsfxwarning;

    delete _bgm;

    qDeleteAll(_sfxes);
}

void AudioWidget::checkProcesses() {
    _vgmstreamCheck = new QProcess();
    _rstmcppCheck = new QProcess();

    connect(_vgmstreamCheck, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AudioWidget::vgmstreamSuccess);
    connect(_vgmstreamCheck, &QProcess::errorOccurred, this, &AudioWidget::vgmstreamError);
    connect(_rstmcppCheck, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AudioWidget::rstmcppSuccess);
    connect(_rstmcppCheck, &QProcess::errorOccurred, this, &AudioWidget::rstmcppError);

    _vgmstreamCheck->start("vgmstream-cli", QStringList());
    _rstmcppCheck->start("rstmcpp", QStringList());
}

void AudioWidget::vgmstreamSuccess(int, QProcess::ExitStatus) {
    _vgmstreamCheck->close();
    disconnect(_vgmstreamCheck, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AudioWidget::vgmstreamSuccess);
    disconnect(_vgmstreamCheck, &QProcess::errorOccurred, this, &AudioWidget::vgmstreamError);
}

void AudioWidget::rstmcppSuccess(int, QProcess::ExitStatus) {
    _rstmcppCheck->close();
    disconnect(_rstmcppCheck, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AudioWidget::rstmcppSuccess);
    disconnect(_rstmcppCheck, &QProcess::errorOccurred, this, &AudioWidget::rstmcppError);
}

void AudioWidget::vgmstreamError(QProcess::ProcessError) {
    _vgmstreamCheck->close();
    _input->setPlayable(false);
    _bgm->setPlayable(false);
    for (auto key : _sfxes.keys()) {
        _sfxes[key]->setPlayable(false);
    }
    disconnect(_vgmstreamCheck, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AudioWidget::vgmstreamSuccess);
    disconnect(_vgmstreamCheck, &QProcess::errorOccurred, this, &AudioWidget::vgmstreamError);
}

void AudioWidget::rstmcppError(QProcess::ProcessError) {
    _rstmcppCheck->close();
    _rstmcppFound = false;
    QColor color = Color::MAIN;
    color.setAlpha(0x80);
    Warning *warning = new Warning(color);
    QIcon icon(warning->icon());
    icon.addPixmap(warning->icon(), QIcon::Disabled);
    _convert->setEnabled(false);
    _convert->setIcon(icon);
    _convert->setToolTip(tr("rstmcpp could not be found. Audio file conversion is not possible without it.", "This message is in a tooltip in an icon in the convert button if the program can't find rstmcpp."));

    delete warning;
    disconnect(_rstmcppCheck, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AudioWidget::rstmcppSuccess);
    disconnect(_rstmcppCheck, &QProcess::errorOccurred, this, &AudioWidget::rstmcppError);
}

AudioPicker *AudioWidget::picker(QString name) {
    if (name == "bgm") {
        return _bgm;
    } else {
        return _sfxes[name];
    }
}

void AudioWidget::pauseAll() {
    _inputstate = _input->isPlaying();
    _input->pause();
    _bgmstate = _bgm->isPlaying();
    _bgm->pause();
    for (auto key : _sfxes.keys()) {
        _state[key] = _sfxes[key]->isPlaying();
        _sfxes[key]->pause();
    }
}

void AudioWidget::restoreAll() {
    disconnect(_sfxes["resume"], &AudioPicker::mediaEnd, this, &AudioWidget::restoreAll);
    if (_bgmstate && !_bgm->isLoading()) {
        _bgm->play();
    }
    if (_inputstate) {
        _input->play();
    }
    for (auto key : _sfxes.keys()) {
        if (_state[key]) {
            _sfxes[key]->play();
        }
    }
}

void AudioWidget::resumeLoaded() {
    _resumeloaded = true;
    disconnect(_sfxes["resume"], &AudioPicker::fileLoaded, this, &AudioWidget::resumeLoaded);
    _sfxes["resume"]->play();
    connect(_sfxes["resume"], &AudioPicker::mediaEnd, this, &AudioWidget::restoreAll);
}

void AudioWidget::bgmLoaded() {
    disconnect(_bgm, &AudioPicker::fileLoaded, this, &AudioWidget::bgmLoaded);
    if (!_sfxes["resume"]->isPlaying() && _bgmstate && _resumeloaded) {
        _bgm->play();
    } else {
        _bgm->pause();
    }
}

void AudioWidget::playResume() {
    if (_bgm->fileChanged()) {
        connect(_bgm, &AudioPicker::fileLoaded, this, &AudioWidget::bgmLoaded);
        _bgm->reload();
    }
    if (_input->fileChanged()) {
        _input->reload();
        _inputstate = false;
    }
    for (auto key : _sfxes.keys()) {
        if (key != "resume" && _sfxes[key]->fileChanged()) {
            _sfxes[key]->reload();
            _state[key] = false;
        }
    }
    _resumeloaded = false;
    if (_sfxes["resume"]->fileChanged()) {
        connect(_sfxes["resume"], &AudioPicker::fileLoaded, this, &AudioWidget::resumeLoaded);
        _sfxes["resume"]->reload();
    } else {
        if (_sfxes["resume"]->filepath().isEmpty() || _sfxes["resume"]->isMuted()) {
            restoreAll();
        } else {
            resumeLoaded();
        }
    }
}

void AudioWidget::enableBGMAutoplay(bool enabled) {
    if (enabled) {
        connect(_bgm, &AudioPicker::fileLoaded, _bgm, &AudioPicker::play);
    } else {
        disconnect(_bgm, &AudioPicker::fileLoaded, _bgm, &AudioPicker::play);
    }
}

void AudioWidget::reset() {
    _bgm->reset();
    for (auto key : _sfxes.keys()) {
        _sfxes[key]->reset();
    }

    updateSFXTotal();
    SourceFile::recentlyModified = false;
}

void AudioWidget::load() {
    QFileInfo fileInfo(SourceFile::bgm);
    if (!fileInfo.isDir() && fileInfo.exists()) {
        _bgm->loadFile(SourceFile::bgm);
    } else {
        _bgm->reset();
    }

    for (auto key : _sfxes.keys()) {
        fileInfo = QFileInfo(SourceFile::sfxes[key]);
        if (!fileInfo.isDir() && fileInfo.exists()) {
            _sfxes[key]->loadFile(SourceFile::sfxes[key]);
        } else {
            _sfxes[key]->reset();
        }
    }

    SourceFile::recentlyModified = false;
}

void AudioWidget::bgmChanged(bool value) {
    if (value) {
        SourceFile::bgm = _bgm->filepath();

        SourceFile::recentlyModified = true;
    }
}

void AudioWidget::toggleBGM(bool value) {
    if (value) {
        SourceFile::bgm = _bgm->filepath();
    } else {
        SourceFile::bgm = "";
    }

    SourceFile::recentlyModified = true;
}

void AudioWidget::cursorChanged(bool value) {
    if (value) {
        SourceFile::sfxes["cursor"] = _sfxes["cursor"]->filepath();

        SourceFile::recentlyModified = true;
    }
}

void AudioWidget::toggleCursor(bool value) {
    if (value) {
        SourceFile::sfxes["cursor"] = _sfxes["cursor"]->filepath();
    } else {
        SourceFile::sfxes["cursor"] = "";
    }

    SourceFile::recentlyModified = true;
}

void AudioWidget::launchChanged(bool value) {
    if (value) {
        SourceFile::sfxes["launch"] = _sfxes["launch"]->filepath();

        SourceFile::recentlyModified = true;
    }
}

void AudioWidget::toggleLaunch(bool value) {
    if (value) {
        SourceFile::sfxes["launch"] = _sfxes["launch"]->filepath();
    } else {
        SourceFile::sfxes["launch"] = "";
    }

    SourceFile::recentlyModified = true;
}

void AudioWidget::folderChanged(bool value) {
    if (value) {
        SourceFile::sfxes["folder"] = _sfxes["folder"]->filepath();

        SourceFile::recentlyModified = true;
    }
}

void AudioWidget::toggleFolder(bool value) {
    if (value) {
        SourceFile::sfxes["folder"] = _sfxes["folder"]->filepath();
    } else {
        SourceFile::sfxes["folder"] = "";
    }

    SourceFile::recentlyModified = true;
}

void AudioWidget::closeChanged(bool value) {
    if (value) {
        SourceFile::sfxes["close"] = _sfxes["close"]->filepath();

        SourceFile::recentlyModified = true;
    }
}

void AudioWidget::toggleClose(bool value) {
    if (value) {
        SourceFile::sfxes["close"] = _sfxes["close"]->filepath();
    } else {
        SourceFile::sfxes["close"] = "";
    }

    SourceFile::recentlyModified = true;
}

void AudioWidget::frame0Changed(bool value) {
    if (value) {
        SourceFile::sfxes["frame0"] = _sfxes["frame0"]->filepath();

        SourceFile::recentlyModified = true;
    }
}

void AudioWidget::toggleFrame0(bool value) {
    if (value) {
        SourceFile::sfxes["frame0"] = _sfxes["frame0"]->filepath();
    } else {
        SourceFile::sfxes["frame0"] = "";
    }

    SourceFile::recentlyModified = true;
}

void AudioWidget::frame1Changed(bool value) {
    if (value) {
        SourceFile::sfxes["frame1"] = _sfxes["frame1"]->filepath();

        SourceFile::recentlyModified = true;
    }
}

void AudioWidget::toggleFrame1(bool value) {
    if (value) {
        SourceFile::sfxes["frame1"] = _sfxes["frame1"]->filepath();
    } else {
        SourceFile::sfxes["frame1"] = "";
    }

    SourceFile::recentlyModified = true;
}

void AudioWidget::frame2Changed(bool value) {
    if (value) {
        SourceFile::sfxes["frame2"] = _sfxes["frame2"]->filepath();

        SourceFile::recentlyModified = true;
    }
}

void AudioWidget::toggleFrame2(bool value) {
    if (value) {
        SourceFile::sfxes["frame2"] = _sfxes["frame2"]->filepath();
    } else {
        SourceFile::sfxes["frame2"] = "";
    }

    SourceFile::recentlyModified = true;
}

void AudioWidget::resumeChanged(bool value) {
    if (value) {
        SourceFile::sfxes["resume"] = _sfxes["resume"]->filepath();

        SourceFile::recentlyModified = true;
    }
}

void AudioWidget::toggleResume(bool value) {
    if (value) {
        SourceFile::sfxes["resume"] = _sfxes["resume"]->filepath();
    } else {
        SourceFile::sfxes["resume"] = "";
    }

    SourceFile::recentlyModified = true;
}

void AudioWidget::updateSFXTotal() {
    double total = 0;

    for (auto key : _sfxes.keys()) {
        total += _sfxes[key]->fileSize();
    }

    _placeholder->setMaxValue(std::max(int(total), 0x2dc00));

    for (auto key : _sfxes.keys()) {
        _placeholder->updateSfx(key, _sfxes[key]->fileSize());
    }

    _totalsfx->setText(QString::number(int(total / 1024)) + "/183KB");
    _totalsfxwarning->setVisible(total > 0x2dc00);
}

void AudioWidget::changeFormat(int index) {
    _format = static_cast<Format>(index);
    if (_format == Format::BCWAV) {
        _bgmassignment->hide();
        _sfxassignment->show();
        _input->invokeLoopEdit(false);
    } else {
        _bgmassignment->show();
        _sfxassignment->hide();
        _input->invokeLoopEdit(true);
    }
    if (!_destpath.isEmpty()) {
        QFileInfo info(_destpath);
        _destpath = info.dir().filePath(info.baseName() + "." + (_format == Format::BCSTM ? "bcstm" : "bcwav"));
        _output->setText(_destpath);
    }
}

void AudioWidget::inputChanged(bool value) {
    if (value && _rstmcppFound) {
        _convert->setEnabled(!_input->filepath().isEmpty() && !_destpath.isEmpty());
    }
}

void AudioWidget::selectOutput() {
    QFileDialog *dialog = new QFileDialog();
    dialog->setAcceptMode(QFileDialog::AcceptSave);
    dialog->setWindowTitle(tr("Select BCSTM/BCWAV Destination", "Title for the file dialog when selecting the destination path for audio conversion."));
    QString formatname = _format == Format::BCSTM ? "bcstm" : "bcwav";
    dialog->setDefaultSuffix(formatname);
    dialog->setNameFilters(QStringList() << tr(qPrintable("Nintendo Audio Files (*." + formatname + ")")));

    if (dialog->exec()) {
        _destpath = dialog->selectedFiles().first();
        _output->setText(_destpath);
    }

    if (_rstmcppFound) {
        _convert->setEnabled(!_destpath.isEmpty() && !_input->filepath().isEmpty());
    }
}

void AudioWidget::convertClicked() {
    QStringList args = QStringList() << _input->filepath() << _destpath;
    if (_input->loopstart() < _input->loopend() && _input->loopEditEnabled()) {
        QString loops = "-l" + QString::number(_input->loopstart()) + "-" + QString::number(_input->loopend());
        args = QStringList() << loops << args;
    }

    connect(_converter, &QProcess::readyReadStandardOutput, this, &AudioWidget::updateProgress);
    connect(_converter, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AudioWidget::convertFinished);
    _converter->start("rstmcpp", args);
    QProgressBar *bar = new QProgressBar();
    QPalette pal = bar->palette();
    pal.setColor(QPalette::Highlight, Color::MAIN);
    bar->setPalette(pal);
    if (_progress != nullptr) {
        delete _progress;
    }
    _progress = new QProgressDialog();
    _progress->setLabelText(tr("Converting...", "Indicates in a message box that the audio is being converted."));
    _progress->setBar(bar);
    _progress->setWindowModality(Qt::WindowModal);
    _progress->setRange(0, 78);
    _progress->setMinimumDuration(0);
    _progress->setCancelButton(0);
    _progress->setValue(0);
}

void AudioWidget::updateProgress() {
    QString output = _converter->readAllStandardOutput();
    _progress->setValue(output.count("#"));
}

void AudioWidget::convertFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    disconnect(_converter, &QProcess::readyReadStandardOutput, this, &AudioWidget::updateProgress);
    disconnect(_converter, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AudioWidget::convertFinished);
    _progress->setValue(78);
    QString format = _format == Format::BCSTM ? "BCSTM" : "BCWAV";
    QMessageBox msgBox(QMessageBox::NoIcon, tr("Audio Conversion Results", "Title for the messagebox displaying the results."), tr("%1 file successfully converted.", "Message to display when audio conversion succeeds.").arg(format), QFlag(0), _accordion);
    if (exitCode != 0 || exitStatus != QProcess::NormalExit) {
        msgBox.setText(tr("Audio conversion failed.", "Messagebox gets displayed when audio conversion fails."));
    } else {
        if (_format == Format::BCSTM) {
            if (_bgmassignment->currentIndex() > 0) {
                _bgm->loadFile(_destpath);
                bgmChanged(true);
            }
        } else {
                Assign assign = static_cast<Assign>(_sfxassignment->currentIndex());
                switch(assign) {
                    case Assign::CURSOR:
                        _sfxes["cursor"]->loadFile(_destpath);
                        cursorChanged(true);
                        break;
                    case Assign::LAUNCH:
                        _sfxes["launch"]->loadFile(_destpath);
                        launchChanged(true);
                        break;
                    case Assign::FOLDER:
                        _sfxes["folder"]->loadFile(_destpath);
                        folderChanged(true);
                        break;
                    case Assign::CLOSE:
                        _sfxes["close"]->loadFile(_destpath);
                        closeChanged(true);
                        break;
                    case Assign::FRAME0:
                        _sfxes["frame0"]->loadFile(_destpath);
                        frame0Changed(true);
                        break;
                    case Assign::FRAME1:
                        _sfxes["frame1"]->loadFile(_destpath);
                        frame1Changed(true);
                        break;
                    case Assign::FRAME2:
                        _sfxes["frame2"]->loadFile(_destpath);
                        frame2Changed(true);
                        break;
                    case Assign::RESUME:
                        _sfxes["resume"]->loadFile(_destpath);
                        resumeChanged(true);
                        break;
                    case Assign::NONE:
                    default:
                        break;
                }
        }
    }
    _destpath = "";
    _output->setText(tr(noFile));
    _convert->setEnabled(false);
    _converter->close();
    msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
    msgBox.exec();
}

void AudioWidget::addConverter() {
    _input = new AudioPicker(0, static_cast<int>(AudioPicker::Mode::WAV));
    _input->setTitle(tr("Source File", "Title for the audio converter input file."));
    _input->setTooltip(tr("Source WAV file to convert. Loop values are in samples. Please note that WAV files must be in 8 or 16-bit encoding. For best results, use stereo channel WAVs. You can make the necessary conversions in programs like Audacity or ffmpeg.", "Tooltip that appears when hovering over the title icon for the coverter."));
    _input->setAccordionWidget(this);

    QLabel *destlabel = new QLabel(tr("Destination:", "Text indicating where the output selection for the audio converter apppears."));

    _selectoutput = new QPushButton();
    Folder *folder = new Folder();
    QIcon icon(folder->icon());
    _selectoutput->setIcon(icon);
    _selectoutput->setIconSize(folder->icon().rect().size());

    _selectoutput->setObjectName("select");
    _selectoutput->setStyleSheet(folder->selectStyle());
    _selectoutput->setCursor(Qt::PointingHandCursor);

    _output = new QLabel(tr(noFile));

    QHBoxLayout *selectlayout = new QHBoxLayout();
    selectlayout->addWidget(_selectoutput);
    selectlayout->addWidget(_output, 1);

    QLabel *formatlabel = new QLabel(tr("Format:", "Text in the audio converter that indicates where the options for changing the converter format are."));

    _formatbox = new QComboBox();
    _formatbox->addItems(QStringList() << "BCSTM (BGM)" << "BCWAV (SFX)");

    QLabel *assignlabel = new QLabel(tr("Assign to:", "Text in the audio converter that indicates where the options for changing the audio assignment start."));

    _bgmassignment = new QComboBox();
    _bgmassignment->addItems(QStringList() << tr("Do not assign", "Initial option in the combobox for assigning the bgm after conversion.") << "BGM");

    _sfxassignment = new QComboBox();
    _sfxassignment->addItems(QStringList() << tr("Do not assign", "Initial option in the combobx for assigning the sfx after conversion.") << "Cursor" << "Launch" << "Folder" << "Close" << "Frame 0" << "Frame 1" << "Frame 2" << "Open Lid");

    _sfxassignment->hide();

    _convert = new QPushButton();
    _convert->setText(tr("Convert", "Button for starting audio conversion."));
    _convert->setEnabled(false);

    int index = createGroup(_accordion, tr("Converter", "Title for audio converter accordion."), tr("Converts .wav files to .bcstm or .bcwav.", "Tooltip for audio converter accordion"), false);
    QFrame *converterframe = getContent(_accordion, index);
    converterframe->layout()->addWidget(_input);
    converterframe->layout()->addWidget(formatlabel);
    converterframe->layout()->addWidget(_formatbox);
    converterframe->layout()->addWidget(destlabel);
    dynamic_cast<QVBoxLayout*>(converterframe->layout())->addLayout(selectlayout);
    converterframe->layout()->addWidget(assignlabel);
    converterframe->layout()->addWidget(_bgmassignment);
    converterframe->layout()->addWidget(_sfxassignment);
    converterframe->layout()->addWidget(_convert);
    _accordion->getContentPane(index)->setMaximumHeight(_input->height() + formatlabel->height() + _formatbox->height() + destlabel->height() + _selectoutput->height() + assignlabel->height() + _bgmassignment->height() + _convert->height());

    connect(_formatbox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &AudioWidget::changeFormat);
    connect(_selectoutput, &QAbstractButton::clicked, this, &AudioWidget::selectOutput);
    connect(_input, &AudioPicker::stateChanged, this, &AudioWidget::inputChanged);
    connect(_convert, &QAbstractButton::clicked, this, &AudioWidget::convertClicked);

    delete folder;
}

void AudioWidget::addBGM() {
    _bgm = new AudioPicker();
    _bgm->setTitle("BGM");
    _bgm->setTooltip(tr("Background music track", "Tooltip describes what the bgm audiopicker is for."));
    _bgm->setAccordionWidget(this);

    int index = createGroup(_accordion, tr("Background Music", "Title for BGM accordion"), tr("Looping music track that plays in the background. In .bcstm/.cstm format.", "Tooltip that appears in the Background Music accordion."), false);
    QFrame *bgmframe = getContent(_accordion, index);
    bgmframe->layout()->addWidget(_bgm);
    _accordion->getContentPane(index)->setMaximumHeight(_bgm->height());

    connect(_bgm, &AudioPicker::stateChanged, this, &AudioWidget::bgmChanged);
    connect(_bgm, &AudioPicker::muteChanged, this, &AudioWidget::toggleBGM);

    QSettings settings;
    if (settings.contains("bgmautoplay") && settings.value("bgmautoplay").toBool()) {
        connect(_bgm, &AudioPicker::fileLoaded, _bgm, &AudioPicker::play);
    }
}

void AudioWidget::addSFX() {
    QLabel *totalsfxTitle = new QLabel(tr("SFX Limit", "Title for SFX limit section"));

    _placeholder = new SfxBar();

    QWidget *container = new QWidget();
    container->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    QVBoxLayout *layout = new QVBoxLayout();
    container->setLayout(layout);
    container->layout()->addWidget(_placeholder);
    container->layout()->setSpacing(0);
    container->layout()->setContentsMargins(QMargins(0, 0, 0, 0));
    container->setMaximumHeight(10);

    _totalsfx = new QLabel("0/183KB");
    _totalsfxwarning = new QLabel();

    Warning *warning = new Warning();
    _totalsfxwarning->setPixmap(warning->icon());

    delete warning;

    _totalsfxwarning->setToolTip(tr("The total size of all the SFXes in this theme is over the 183KB limit. Your theme may not have sound effects on deploy.", "Tooltip warning when users have a total SFX size > 183KB."));
    _totalsfxwarning->hide();

    QHBoxLayout *totalTitleLayout = new QHBoxLayout();
    totalTitleLayout->addWidget(totalsfxTitle);
    totalTitleLayout->addWidget(_totalsfxwarning);
    totalTitleLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Preferred));

    QHBoxLayout *totalLayout = new QHBoxLayout();
    totalLayout->addWidget(container);
    totalLayout->addWidget(_totalsfx);

    //: SFX detailed help
    InfoDialog *infodialog = new InfoDialog(_accordion);

    infodialog->setWindowTitle(tr("SFX Help", "Window title for SFX detailed help"));
    infodialog->addHtml("<h3>" + tr("Triggering Sound Effects", "Title for sfx detailed help") + "</h3><p><img src=\":/resources/docs/sfx1.png\"></img> <img src=\":/resources/docs/sfx2.png\"></img></p><ol><li>" + tr(
            "Launch") + "</li>"
            "<li>" + tr("Close") + "</li>"
            "<li>" + tr("Folder (On real devices, the folder SFX can also be played while clicking on certain buttons, not just for creating folders.)") + "</li>"
            "<li>" + tr("Frame 0 - 2 (Plays while scrolling/dragging)") + "</li></ol>", 490);

    AudioPicker *cursor = new AudioPicker();
    cursor->setTitle(tr("Cursor", "Cursor audio title"));
    cursor->setTooltip(tr("Sound that plays when you move the cursor.", "Tooltip for the cursor audio picker."));
    cursor->setAccordionWidget(this);
    cursor->acceptBCWAV(true);
    cursor->enableLooping(false);

    AudioPicker *launch = new AudioPicker();
    launch->setTitle(tr("Launch", "Launch audio title"));
    launch->setTooltip(tr("Sound that plays when opening an application. Click for more details.", "Tooltip for the launch audio picker."));
    launch->setAccordionWidget(this);
    launch->acceptBCWAV(true);
    launch->enableLooping(false);
    connect(launch->info(), &QAbstractButton::clicked, infodialog, &InfoDialog::show);

    AudioPicker *folder = new AudioPicker();
    folder->setTitle(tr("Folder", "Folder audio title"));
    folder->setTooltip(tr("Sound that plays when creating a folder. Click for more details.", "Tooltip for the folder audio picker."));
    folder->setAccordionWidget(this);
    folder->acceptBCWAV(true);
    folder->enableLooping(false);
    connect(folder->info(), &QAbstractButton::clicked, infodialog, &InfoDialog::show);

    AudioPicker *close = new AudioPicker();
    close->setTitle(tr("Close", "Close audio title"));
    close->setTooltip(tr("Sound that plays when closing an application. Click for more details.", "Tooltip for the close sound audio picker."));
    close->setAccordionWidget(this);
    close->acceptBCWAV(true);
    close->enableLooping(false);
    connect(close->info(), &QAbstractButton::clicked, infodialog, &InfoDialog::show);

    AudioPicker *frame0 = new AudioPicker();
    frame0->setTitle(tr("Frame 0", "Frame 0 audio title"));
    frame0->setTooltip(tr("Sound that plays when scrolling between bottom screens. Applies to any bottom screen frame type. Click for more details.", "Tooltip for page scrolling sound audio picker."));
    frame0->setAccordionWidget(this);
    frame0->acceptBCWAV(true);
    frame0->enableLooping(false);
    connect(frame0->info(), &QAbstractButton::clicked, infodialog, &InfoDialog::show);

    AudioPicker *frame1 = new AudioPicker();
    frame1->setTitle(tr("Frame 1", "Frame 1 audio title"));
    frame1->setTooltip(tr("Sound that plays when scrolling between bottom screens. Applies to any bottom screen frame type. Click for more details.", "Tooltip for page scrolling sound audio picker."));
    frame1->setAccordionWidget(this);
    frame1->acceptBCWAV(true);
    frame1->enableLooping(false);
    connect(frame1->info(), &QAbstractButton::clicked, infodialog, &InfoDialog::show);

    AudioPicker *frame2 = new AudioPicker();
    frame2->setTitle(tr("Frame 2", "Frame 2 audio title"));
    frame2->setTooltip(tr("Sound that plays when scrolling between bottom screens. Applies to any bottom screen frame type. Click for more details.", "Tooltip for page scrolling sound audio picker."));
    frame2->setAccordionWidget(this);
    frame2->acceptBCWAV(true);
    frame2->enableLooping(false);
    connect(frame2->info(), &QAbstractButton::clicked, infodialog, &InfoDialog::show);

    AudioPicker *resume = new AudioPicker();
    resume->setTitle(tr("Open Lid", "Open lid audio title"));
    resume->setTooltip(tr("Sound that plays when opening the 3DS. In this preview, it will play when Kame Editor receives focus.", "Tooltip for open lid audio picker."));
    resume->setAccordionWidget(this);
    resume->acceptBCWAV(true);
    resume->enableLooping(false);

    int index = createGroup(_accordion, tr("Sound Effects", "Title for SFX accordion."), tr("Sound effects for various theme actions. In .bcwav/.cwav format.", "Tooltip for SFX accordion."), false);
    QFrame *sfxframe = getContent(_accordion, index);
    dynamic_cast<QBoxLayout*>(sfxframe->layout())->addLayout(totalTitleLayout);
    dynamic_cast<QBoxLayout*>(sfxframe->layout())->addLayout(totalLayout);
    sfxframe->layout()->addWidget(cursor);
    sfxframe->layout()->addWidget(launch);
    sfxframe->layout()->addWidget(folder);
    sfxframe->layout()->addWidget(close);
    sfxframe->layout()->addWidget(frame0);
    sfxframe->layout()->addWidget(frame1);
    sfxframe->layout()->addWidget(frame2);
    sfxframe->layout()->addWidget(resume);
    _accordion->getContentPane(index)->setMaximumHeight(cursor->height() * 8);

    _sfxes["cursor"] = cursor;
    _sfxes["launch"] = launch;
    _sfxes["folder"] = folder;
    _sfxes["close"] = close;
    _sfxes["frame0"] = frame0;
    _sfxes["frame1"] = frame1;
    _sfxes["frame2"] = frame2;
    _sfxes["resume"] = resume;

    for (auto key : _sfxes.keys()) {
        _placeholder->addSfx(key, _sfxes[key]->fileSize());
    }

    ContentPane* pane = _accordion->getContentPane(index);

    connect(cursor, &AudioPicker::stateChanged, this, &AudioWidget::cursorChanged);
    connect(cursor, &AudioPicker::muteChanged, this, &AudioWidget::toggleCursor);
    connect(cursor, &AudioPicker::fileLoaded, this, &AudioWidget::updateSFXTotal);
    connect(launch, &AudioPicker::stateChanged, this, &AudioWidget::launchChanged);
    connect(launch, &AudioPicker::muteChanged, this, &AudioWidget::toggleLaunch);
    connect(launch, &AudioPicker::fileLoaded, this, &AudioWidget::updateSFXTotal);
    connect(folder, &AudioPicker::stateChanged, this, &AudioWidget::folderChanged);
    connect(folder, &AudioPicker::muteChanged, this, &AudioWidget::toggleFolder);
    connect(folder, &AudioPicker::fileLoaded, this, &AudioWidget::updateSFXTotal);
    connect(close, &AudioPicker::stateChanged, this, &AudioWidget::closeChanged);
    connect(close, &AudioPicker::muteChanged, this, &AudioWidget::toggleClose);
    connect(close, &AudioPicker::fileLoaded, this, &AudioWidget::updateSFXTotal);
    connect(frame0, &AudioPicker::stateChanged, this, &AudioWidget::frame0Changed);
    connect(frame0, &AudioPicker::muteChanged, this, &AudioWidget::toggleFrame0);
    connect(frame0, &AudioPicker::fileLoaded, this, &AudioWidget::updateSFXTotal);
    connect(frame1, &AudioPicker::stateChanged, this, &AudioWidget::frame1Changed);
    connect(frame1, &AudioPicker::muteChanged, this, &AudioWidget::toggleFrame1);
    connect(frame1, &AudioPicker::fileLoaded, this, &AudioWidget::updateSFXTotal);
    connect(frame2, &AudioPicker::stateChanged, this, &AudioWidget::frame2Changed);
    connect(frame2, &AudioPicker::muteChanged, this, &AudioWidget::toggleFrame2);
    connect(frame2, &AudioPicker::fileLoaded, this, &AudioWidget::updateSFXTotal);
    connect(resume, &AudioPicker::stateChanged, this, &AudioWidget::resumeChanged);
    connect(resume, &AudioPicker::muteChanged, this, &AudioWidget::toggleResume);
    connect(resume, &AudioPicker::fileLoaded, this, &AudioWidget::updateSFXTotal);

    updateSFXTotal();
}
