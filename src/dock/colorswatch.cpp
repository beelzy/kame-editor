// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QFontMetrics>
#include <QGuiApplication>
#include <QScreen>
#include <QCursor>

#include "../kamewidget.h"
#include "colorswatch.h"
#include "../color.h"

ColorSwatch::ColorSwatch(QWidget *parent, ColorDialog *dialog) : QAbstractButton(parent) {
    _showAlpha = false;
    _color = QColor(Qt::transparent);
    QFontMetrics metrics = QFontMetrics(QFont());
    setMinimumSize(QSize(metrics.height(), metrics.height()));
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    _dialog = dialog ? dialog : new ColorDialog();
    _dialog->setAlphaEnabled(_showAlpha);

    connect(this, &QAbstractButton::clicked, this, &ColorSwatch::selectColor);
}

ColorSwatch::~ColorSwatch() {
    if (_dialog) {
        _dialog->deleteLater();
    }
}

void ColorSwatch::setColor(QColor value) {
    _color = value;

    update();
}

QColor ColorSwatch::color() {
    return _color;
}

void ColorSwatch::setShowAlpha(bool value) {
    _showAlpha = value;
    _dialog->setAlphaEnabled(value);
}

void ColorSwatch::colorSelected(QColor color) {
    setColor(color);
}

void ColorSwatch::dialogCleanup(int) {
    disconnect(_dialog, &ColorDialog::colorSelected, this, &ColorSwatch::colorSelected);
    disconnect(_dialog, &QDialog::finished, this, &ColorSwatch::dialogCleanup);
    disconnect(_dialog, &ColorDialog::colorChanged, this, &ColorSwatch::colorChanged);
    emit stateChanged(true);
    QSettings settings;
    settings.setValue("colorpicker.position", _dialog->geometry());
}

void ColorSwatch::setAccordionWidget(AccordionWidget *value) {
    _accordionwidget = value;
    connect(this, &ColorSwatch::stateChanged, _accordionwidget, &AccordionWidget::onAvailabilityChanged);
}

ColorDialog *ColorSwatch::dialog() {
    return _dialog;
}

void ColorSwatch::selectColor() {
    connect(_dialog, &ColorDialog::colorSelected, this, &ColorSwatch::colorSelected);
    connect(_dialog, &QDialog::finished, this, &ColorSwatch::dialogCleanup);
    connect(_dialog, &ColorDialog::colorChanged, this, &ColorSwatch::colorChanged);
    emit stateChanged(false);
    KameWidget *root = dynamic_cast<KameWidget*>(window());
    if (root == nullptr) {
        root = dynamic_cast<KameWidget*>(window()->parent());
    }
    QRect rect = _dialog->geometry();
    QSettings settings;
    if (settings.contains("colorpicker.position")) {
        QRect saved = settings.value("colorpicker.position").toRect();
        rect.setX(saved.x());
        rect.setY(saved.y());
    } else {
        QRect parentRect = root->geometry();
#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
        QRect screen = QGuiApplication::screenAt(QCursor::pos())->geometry();
#else
        QRect screen = QGuiApplication::primaryScreen()->geometry();
#endif
        QRect console = root->consolewidget()->geometry();
        console.moveTo(QPoint(parentRect.x() + console.x(), parentRect.y() + console.y()));
        QRect bottomRect = root->consolewidget()->bottomview()->geometry();
        bottomRect.moveTo(QPoint(console.x() + bottomRect.x(), console.y() + bottomRect.y()));
        int posX = 0;
        if (bottomRect.x() - screen.x() > screen.x() + screen.width() - bottomRect.x() - bottomRect.width()) {
            int offsetX = console.x() - screen.x() >= rect.width() ? console.x() : bottomRect.x();
            posX = offsetX - rect.width();
        } else {
            int offsetX = screen.x() + screen.width() - console.x() - console.width() >= rect.width() ? console.x() + console.width() : bottomRect.x() + bottomRect.width();
            posX = offsetX;
        }
        rect.moveTo(QPoint(posX, console.y() + (console.height() - rect.height()) / 2));
    }
    _dialog->setGeometry(rect);
    _dialog->showColor(_color);
}

void ColorSwatch::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    p.fillRect(0, 0, width(), height(), QColor("#303030"));

    p.fillRect(0, 0, width() / 2, height() / 2, QColor("#404040"));
    p.fillRect(width() / 2, height() / 2, width() / 2, height() / 2, QColor("#404040"));

    p.setPen(Color::LIGHT);
    p.setBrush(_color);
    p.drawRect(0, 0, width(), height());
}
