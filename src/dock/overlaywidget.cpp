// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QEvent>
#include <QResizeEvent>

#include "overlaywidget.h"
#include "../color.h"

OverlayWidget::OverlayWidget(QWidget *parent) : QWidget(parent) {
   setAttribute(Qt::WA_NoSystemBackground); 
   newParent();
}

bool OverlayWidget::eventFilter(QObject *obj, QEvent *event) {
    if (obj == parent()) {
        if (event->type() == QEvent::Resize) {
            resize(static_cast<QResizeEvent*>(event)->size());
        } else if (event->type() == QEvent::ChildAdded) {
            raise();
        }
    }
    return QWidget::eventFilter(obj, event);
}

bool OverlayWidget::event(QEvent *event) {
    if (event->type() == QEvent::ParentAboutToChange) {
        if (parent()) parent()->removeEventFilter(this);
    }
    else if (event->type() == QEvent::ParentChange)
        newParent();
    return QWidget::event(event);
}

void OverlayWidget::paintEvent(QPaintEvent *) {
    QPainter p(this);
    QColor color = Color::DARK;
    color.setAlpha(0xD8);
    p.fillRect(rect(), color);
}

void OverlayWidget::newParent() {
    if (!parent()) return;
    parent()->installEventFilter(this);
    raise();
}
