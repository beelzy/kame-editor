// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef COLORWIDGET_H
#define COLORWIDGET_H

#include <QFileDialog>
#include <QComboBox>

#include "QtColorWidgets/color_dialog.hpp"
#include "accordionwidget.h"
#include "kameslider.h"
#include "swatchwidget.h"
#include "paletteview.h"
#include "icons/warning.h"
#include "icons/unknown.h"

class ColorWidget : public AccordionWidget {
    Q_OBJECT

    public:
        explicit ColorWidget(QWidget *parent = 0);
        ~ColorWidget();

        ColorSwatch *colorSwatch(QString layer, QString name);
        KameSlider *slider(QString name);

    public slots:
        void reset();
        void load();

    signals:
        void cursorColorChanged(bool defaultvalue);
        void folderColorChanged(bool defaultvalue);
        void fileColorChanged(bool defaultvalue);
        void opencloseColorChanged(bool defaultvalue);
        void folderbackbuttonColorChanged(bool defaultvalue);
        void arrowbuttonbaseColorChanged(bool defaultvalue);
        void arrowbuttonarrowColorChanged(bool defaultvalue);
        void bottomcornerColorChanged(bool defaultvalue);
        void topcornerColorChanged(bool defaultvalue);
        void folderviewColorChanged(bool defaultvalue);
        void gametextColorChanged(int state);
        void demotextColorChanged(bool defaultvalue);

    protected:
        void addPalette();
        void addCursorColors(ColorDialog *dialog);
        void addFolderColors(ColorDialog *dialog);
        void addFileColors(ColorDialog *dialog);
        void addArrowButtonBaseColors(ColorDialog *dialog);
        void addArrowButtonArrowColors(ColorDialog *dialog);
        void addOpenCloseColors(ColorDialog *dialog);
        void addGameTextColors(ColorDialog *dialog);
        void addFolderViewColors(ColorDialog *dialog);
        void addFolderBackButtonColors(ColorDialog *dialog);
        void addTopCornerButtonColors(ColorDialog *dialog);
        void addBottomCornerButtonColors(ColorDialog *dialog);
        void addDemoTextColors(ColorDialog *dialog);
        void createDialog(QFileDialog *dialog);

        int applyPaletteColors(QMap<QString, QColor> *colors, int offset=0);
        void savePaletteFile(QTextStream *stream, QString swatch);

    protected slots:
        void loadPalette();
        void applyPalette();
        void paletteQuickLoad();
        void savePalette();

        void saveCursorColor(bool value);
        void cursorVisibilityChanged(bool visible);

        void saveFolderColor(bool value);
        void folderVisibilityChanged(bool visible);

        void saveFileColor(bool value);
        void fileVisibilityChanged(bool visible);

        void saveOpenCloseColor(bool value);
        void opencloseVisibilityChanged(bool visible);

        void saveFolderBackButtonColor(bool value);
        void folderbackbuttonVisibilityChanged(bool visible);

        void saveArrowButtonBaseColor(bool value);
        void arrowButtonBaseVisibilityChanged(bool visible);

        void saveArrowButtonArrowColor(bool value);
        void arrowButtonArrowVisibilityChanged(bool visible);

        void saveBottomCornerColor(bool value);
        void bottomCornerVisibilityChanged(bool visible);

        void saveTopCornerColor(bool value);
        void topCornerVisibilityChanged(bool visible);

        void saveFolderViewColor(bool value);
        void folderViewVisibilityChanged(bool visible);

        void saveGameTextColor(bool value);
        void gameTextVisibilityChanged(int state);

        void saveDemoTextColor(bool value);
        void demoTextVisibilityChanged(bool visible);

    private:
        QMap<QString, QMap<QString, SwatchWidget*>> _swatches;
        QMap<QString, KameSlider*> _sliders;
        QMap<QString, int> _accordionindex;
        Warning *_warning;
        UnknownIcon *_unknown;

        PaletteView *_palette;
        QLabel *_palettelabel;

        QMap<QString, QString> _colorNames;
        QMap<int, QVector<QString>> _validPalettes;
        QMap<QString, QMap<QString, QColor>*> _palettes;
        QMap<QString, bool*> _enabledFlags;
        QComboBox *_assignOptions;
        QPushButton *_applyPalette;
};

#endif // COLORWIDGET_H
