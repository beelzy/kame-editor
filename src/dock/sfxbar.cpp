// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include <QPainter>
#include <QPainterPath>

#include "sfxbar.h"
#include "../color.h"

const QColor SfxBar::COLOR1 = QColor("#56c7ff");
const QColor SfxBar::COLOR2 = QColor("#679dff");
const QColor SfxBar::COLOR3 = QColor("#7b72fd");
const QColor SfxBar::COLOR4 = QColor("#a15eff");
const QColor SfxBar::COLOR5 = QColor("#cb5ffa");
const QColor SfxBar::COLOR6 = QColor("#e778d5");
const QColor SfxBar::COLOR7 = QColor("#f0c377");
const QColor SfxBar::COLOR8 = QColor("#fff156");

const char *SfxBar::tooltips[] = {
    QT_TRANSLATE_NOOP("Cursor", "Cursor"),
    QT_TRANSLATE_NOOP("Launch", "Launch"),
    QT_TRANSLATE_NOOP("Folder", "Folder"),
    QT_TRANSLATE_NOOP("Close", "Close"),
    QT_TRANSLATE_NOOP("Frame0", "Frame0"),
    QT_TRANSLATE_NOOP("Frame1", "Frame1"),
    QT_TRANSLATE_NOOP("Frame2", "Frame2"),
    QT_TRANSLATE_NOOP("Resume", "Resume")
};

SfxBar::SfxBar(QWidget *parent) : QFrame(parent) {
    _colors.append(SfxBar::COLOR1);
    _colors.append(SfxBar::COLOR2);
    _colors.append(SfxBar::COLOR3);
    _colors.append(SfxBar::COLOR4);
    _colors.append(SfxBar::COLOR5);
    _colors.append(SfxBar::COLOR6);
    _colors.append(SfxBar::COLOR7);
    _colors.append(SfxBar::COLOR8);
    _layout = new QHBoxLayout();
    setLayout(_layout);
    _layout->setSpacing(0);
    _layout->setContentsMargins(QMargins(1, 0, 1, 0));
    _translations["cursor"] = 0;
    _translations["launch"] = 1;
    _translations["folder"] = 2;
    _translations["close"] = 3;
    _translations["frame0"] = 4;
    _translations["frame1"] = 5;
    _translations["frame2"] = 6;
    _translations["resume"] = 7;
}

SfxBar::~SfxBar() {
    delete _layout;
    qDeleteAll(_sfxes);
}

void SfxBar::setMaxValue(float value) {
    _maxValue = value;
}

void SfxBar::addSfx(QString key, float value) {
    _sfxes[key] = new BarSegment(0, value, _colors[_sfxes.size() % 8]);
    _layout->addWidget(_sfxes[key]);
    _layout->setAlignment(Qt::AlignLeft);
    updateBarSize(key);
    updateToolTip(key);
}

void SfxBar::updateSfx(QString key, float value) {
    if (_sfxes.contains(key)) {
        _sfxes[key]->setValue(value);
        updateBarSize(key);
        updateToolTip(key);
    }
}

void SfxBar::updateToolTip(QString key) {
    if (_sfxes.contains(key)) {
        float value = _sfxes[key]->value();
        _sfxes[key]->setToolTip(tr(tooltips[_translations[key]]) + ": "
                + QString::number(int(100 * value / 1024)/ 100.0) + "KB, "
                + QString::number(int(100 * value / 0x2dc00)) + "%");
    }
}

void SfxBar::updateBarSize(QString key) {
    int maxWidth = std::max(0, contentsRect().width() - _sfxes[key]->geometry().x() - 2);
    int width = _maxValue ? std::min(float(maxWidth), ceil(contentsRect().width() * _sfxes[key]->value() / _maxValue)) : 0;
    _sfxes[key]->setFixedSize(QSize(width, std::max(0, contentsRect().height() - 2)));
}

void SfxBar::updateAll() {
    for (auto key : _sfxes.keys()) {
        updateBarSize(key);
    }
}

void SfxBar::paintEvent(QPaintEvent *) {
    updateAll();
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    QPainterPath path;

    path.addRoundedRect(0, 0, contentsRect().width(), 10, 3, 3);

    p.setPen(QColor(Color::MAIN));
    p.setBrush(QColor(Qt::transparent));
    p.drawPath(path);
}
