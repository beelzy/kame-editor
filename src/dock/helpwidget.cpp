// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QStringListModel>
#include <QSettings>
#include <QMouseEvent>
#include <QSizePolicy>

#include "helpwidget.h"
#include "staticlist.h"

HelpWidget::HelpWidget(QWidget * parent) : InfoDialog(parent) {
    setWindowTitle(tr("Help", "Title for help overlay"));
    QPixmap pixmap(":/resources/docs/keyvisual-3ds.png");
    _keyvisual = new QLabel();
    _keyvisual->setPixmap(pixmap);

    QLabel *tooltips = new QLabel(tr("All interactive elements on the console frame have help tooltips.", "Console tooltip hint in help overlay."));

    QStringList stringList;
    stringList << tr("1) Menu Options", "Help overlay description")
        << tr("2) Change Menu Options", "Help overlay description")
        << tr("3) 3DS Top Screen Preview", "Help overlay description")
        << tr("4) 3DS Bottom Screen Preview", "Help overlay description")
        << tr("5) Show/Hide Theme Info Dock Widget", "Help overlay description")
        << tr("6) Show/Hide Colors Dock Widget", "Help overlay description")
        << tr("7) Show/Hide Textures and Screen Appearance Dock Widget", "Help overlay description")
        << tr("8) Show/Hide Audio Dock Widget", "Help overlay description")
        << tr("9) Quit Application", "Help overlay description")
        << tr("10) Show/Hide Application Information", "Help overlay description")
        << tr("11) Show/Hide This Help Overlay", "Help overlay description")
        << tr("12) Show/Hide Settings Widget", "Help overlay description")
        << tr("13) Play/Pause Preview Animations", "Help overlay description")
        << tr("14) Dock Widgets can be docked or tabbed", "Help overlay description")
        << tr("15) Dockable regions indicated with a dotted line", "Help overlay description")
        << tr("16) Hover over (i) icons for help tooltips", "Help overlay description")
        << tr("17) Show Custom/Default Styles", "Help overlay description");

    if (stringList.size() % 2 == 1) {
        stringList << "";
    }

    QStringListModel *model = new QStringListModel();
    model->setStringList(stringList.mid(0, stringList.size() / 2));
    StaticList *list = new StaticList();
    list->setModel(model);

    QStringListModel *model2 = new QStringListModel();
    model2->setStringList(stringList.mid(stringList.size() / 2, stringList.size() / 2));
    StaticList *list2 = new StaticList();
    list2->setModel(model2);

    QHBoxLayout *listlayout = new QHBoxLayout();
    listlayout->setContentsMargins(0, 0, 0, 0);
    listlayout->setSpacing(0);
    listlayout->addWidget(list, 1);
    listlayout->addWidget(list2, 1);

    addWidget(_keyvisual);
    addWidget(tooltips);
    addLayout(listlayout);

    showTemplatesButton();
}

HelpWidget::~HelpWidget() {
    delete _keyvisual;
}

void HelpWidget::setVisible(bool value) {
    QWidget::setVisible(value);
    QSettings settings;
    QPixmap pixmap(":/resources/docs/keyvisual-" + settings.value("skin").toString() + ".png");
    _keyvisual->setPixmap(pixmap);

    emit visibilityChanged(value);
}
