// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QLabel>
#include <QVBoxLayout>
#include <QDebug>

#include "aboutwidget.h"
#include "kameaccordion/kameaccordion.h"
#include "../config.h"
#include "icons/logo.h"
#include "../preview/preview.h"
#include "../color.h"

AboutWidget::AboutWidget(QWidget * parent) : QFrame(parent) {
    _logo = new Logo();
    QWidget *content = new QWidget();
    content->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);

    QFont titleFont = QFont("Nimbus Sans", 36);
    QFont small = QFont("Nimbus Sans", 8);

    QFontMetrics titlemetrics = QFontMetrics(titleFont);
    QFontMetrics metrics = QFontMetrics(QFont());

    QPixmap logo = QPixmap(titlemetrics.horizontalAdvance("Kame Editor") + 120, 70);
    logo.fill(QColor(Qt::transparent));
    QPainter p;

    p.begin(&logo);

    p.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    QPen pen = QPen(Color::LIGHT);
    pen.setWidth(8);
    pen.setJoinStyle(Qt::RoundJoin);

    QPainterPath text;
    text.addText(4, 52, titleFont, "Kame");
    text.addText(logo.width() - titlemetrics.horizontalAdvance("Editor") - 4, 52, titleFont, "Editor");

    p.strokePath(text, pen);
    p.fillPath(text, QBrush(Color::MAIN));

    p.translate(titlemetrics.horizontalAdvance("Kame") + 58, -4);
    p.rotate(30);
    pen.setWidth(5);
    p.setPen(pen);
    p.setBrush(Color::MAIN);
    p.drawPath(_logo->base(52));

    pen.setWidth(3);
    p.setPen(pen);
    p.setBrush(QColor(Qt::transparent));
    p.drawPath(_logo->lattice(52));

    p.end();

    QLabel *title = new QLabel();
    title->setPixmap(logo);

    QLabel *version = new QLabel("v" + QString(VERSION) + " GPLv3+");
    version->setFont(small);

    QLabel *hash = new QLabel("git hash: " + QString(GIT_VERSION));
    hash->setFont(small);

    QString creditsString = QObject::tr("Programming and graphics: %1\n"
            "Made with Qt5 and PortAudio.\n"
            "Other frameworks and libraries:\n"
            "%2\n"
            "Special Thanks:\n"
            "%3", "Credits contents").arg("beelzy").arg("qAccordion, Qt Color Widgets Color Dialog, miniz, tinyobjloader").arg("Steveice10, usagirei, exelix11, libertyernie, jackoalan, ihaveamac, luigoalma, Theme Plaza, Nintendo Homebrew Discord");

    _creditsContent = new QLabel(creditsString);
    _creditsContent->setWordWrap(true);

    KameAccordion *credits = new KameAccordion(this);

    ContentPane *header = new ContentPane(QObject::tr("Credits", "Title for Credits accordion in about widget."));
    int index = credits->addContentPane(header);

    if (index != -1) {
        QFrame *contentFrame = credits->getContentPane(index)->getContentFrame();
        credits->getContentPane(index)->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        contentFrame->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        contentFrame->setLayout(new QVBoxLayout());
        contentFrame->layout()->setSizeConstraint(QLayout::SetMinimumSize);
        contentFrame->layout()->addWidget(_creditsContent);
    }

    QVBoxLayout *vlayout = new QVBoxLayout();
    vlayout->setAlignment(Qt::AlignTop);

    vlayout->addWidget(title, 1, Qt::AlignHCenter);
    vlayout->addWidget(version, 1, Qt::AlignHCenter);
    vlayout->addWidget(hash, 1, Qt::AlignHCenter);
    vlayout->addWidget(credits);

    content->setLayout(vlayout);

    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(content, 1, Qt::AlignTop);
    setLayout(layout);
}

AboutWidget::~AboutWidget() {
    delete _creditsContent;
    delete _logo;
}

void AboutWidget::resizeEvent(QResizeEvent *) {
    QFontMetrics metrics = QFontMetrics(QFont());
    QRect creditsRect = metrics.boundingRect(QRect(0, 0, _creditsContent->width(), QWIDGETSIZE_MAX), Qt::TextWordWrap, _creditsContent->text());
    _creditsContent->setMinimumHeight(creditsRect.height());
}

void AboutWidget::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    QPen pen = QPen(Preview::colormix(Color::DARK, Color::MAIN, 0.1));
    pen.setWidth(24);
    pen.setCapStyle(Qt::FlatCap);
    p.setPen(pen);
    p.setBrush(QColor(Qt::transparent));
    p.rotate(30);
    p.translate(50, -200);
    p.drawPath(_logo->base(500));
    pen.setWidth(16);
    p.setPen(pen);
    p.drawPath(_logo->lattice(500));
    p.translate(-50, 200);
    p.rotate(-30);

    QLinearGradient gradient = QLinearGradient(QPointF(0, 0), QPointF(0, height()));
    gradient.setColorAt(0, QColor(Qt::transparent));
    gradient.setColorAt(1, Color::DARK);

    p.fillRect(0, 0, width(), height(), QBrush(gradient));
}
