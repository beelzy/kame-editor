// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef AUDIOWIDGET_H
#define AUDIOWIDGET_H

#include <QComboBox>
#include <QSpinBox>
#include <QProgressDialog>

#include "sfxbar.h"
#include "accordionwidget.h"
#include "audiopicker.h"

class AudioWidget : public AccordionWidget {
    Q_OBJECT

    public:
        explicit AudioWidget(QWidget *parent = 0);
        ~AudioWidget();

        static const char *noFile;

        enum class Format {
            BCSTM,
            BCWAV
        };

        enum class Assign {
            NONE,
            CURSOR,
            LAUNCH,
            FOLDER,
            CLOSE,
            FRAME0,
            FRAME1,
            FRAME2,
            RESUME
        };

    public slots:
        void reset();
        void load();
        AudioPicker *picker(QString name);
        void pauseAll();
        void restoreAll();
        void playResume();
        void enableBGMAutoplay(bool enabled);

    protected:
        void addConverter();
        void addBGM();
        void addSFX();
        void checkProcesses();

    protected slots:
        void bgmChanged(bool value);
        void toggleBGM(bool value);
        void cursorChanged(bool value);
        void toggleCursor(bool value);
        void launchChanged(bool value);
        void toggleLaunch(bool value);
        void folderChanged(bool value);
        void toggleFolder(bool value);
        void closeChanged(bool value);
        void toggleClose(bool value);
        void frame0Changed(bool value);
        void toggleFrame0(bool value);
        void frame1Changed(bool value);
        void toggleFrame1(bool value);
        void frame2Changed(bool value);
        void toggleFrame2(bool value);
        void resumeChanged(bool value);
        void toggleResume(bool value);

        void resumeLoaded();
        void bgmLoaded();

        void updateSFXTotal();

        void changeFormat(int index);
        void selectOutput();
        void inputChanged(bool value);
        void convertClicked();
        void updateProgress();
        void convertFinished(int exitCode, QProcess::ExitStatus exitStatus);

        void vgmstreamSuccess(int exitCode, QProcess::ExitStatus exitStatus);
        void rstmcppSuccess(int exitCode, QProcess::ExitStatus exitStatus);
        void vgmstreamError(QProcess::ProcessError error);
        void rstmcppError(QProcess::ProcessError error);

    private:
        AudioPicker *_bgm;
        QMap<QString, AudioPicker*> _sfxes;
        bool _inputstate;
        bool _bgmstate;
        bool _resumeloaded;
        QMap<QString, bool> _state;

        SfxBar * _placeholder;
        QLabel *_totalsfx;
        QLabel *_totalsfxwarning;

        AudioPicker *_input;
        QComboBox *_formatbox;
        QComboBox *_bgmassignment;
        QComboBox *_sfxassignment;
        QPushButton *_selectoutput;
        QLabel *_output;
        QPushButton *_convert;
        QString _destpath;

        Format _format;
        QProcess *_converter;
        QProgressDialog *_progress;
        bool _rstmcppFound;

        QProcess *_vgmstreamCheck;
        QProcess *_rstmcppCheck;
};

#endif // AUDIOWIDGET_H
