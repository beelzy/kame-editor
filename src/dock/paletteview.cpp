// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>

#include "paletteview.h"
#include "../color.h"

PaletteView::PaletteView(QWidget * parent) : QListView(parent) {
    _model = new QStandardItemModel(this);
    setAlternatingRowColors(true);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setSelectionMode(QAbstractItemView::NoSelection);
    setIconSize(QSize(16, 16));
    setModel(_model);
}

PaletteView::~PaletteView() {
    delete _model;
}

void PaletteView::addSwatch(QColor color) {
    QPixmap swatch(iconSize().width(), iconSize().height());
    QPainter p;
    p.begin(&swatch);
    p.fillRect(0, 0, swatch.width(), swatch.height(), QColor("#303030"));
    p.fillRect(0, 0, swatch.width() / 2, swatch.height() / 2, QColor("#404040"));
    p.fillRect(swatch.width() / 2, swatch.height() / 2, swatch.width() / 2, swatch.height() / 2, QColor("#404040"));

    p.setPen(Color::LIGHT);
    p.setBrush(color);
    p.drawRect(0, 0, swatch.width()-1, swatch.height()-1);
    p.end();

    QStandardItem *item = new QStandardItem(QIcon(swatch), color.name());
    _model->setItem(_model->rowCount(), 0, item);
}

void PaletteView::clear() {
   _model->removeRows(0, _model->rowCount());
}

QColor PaletteView::swatch(int index) {
    QModelIndex modelIndex = _model->index(index, 0);
    if (modelIndex.isValid()) {
        return QColor(_model->data(modelIndex).toString());
    }
    return QColor();
}
