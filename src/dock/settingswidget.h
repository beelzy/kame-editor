// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QFrame>
#include <QStringList>
#include <QComboBox>
#include <QCheckBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QListView>
#include <QStringListModel>
#include <QTemporaryDir>

class SettingsWidget : public QFrame {
    Q_OBJECT
    public:
        explicit SettingsWidget(QWidget *parent = 0);
        ~SettingsWidget();

        static const QStringList LOCALE;
        static const QStringList SKIN;
        bool autosaveFilesPending;

    signals:
        void deployPathUpdated();
        void bgmAutoplayChanged(bool enabled);
        void folderLetterChanged(bool enabled);
        void skinChanged(QString skin);
        void autosaveChanged(int interval);
        void autosaveRestored(QString path);
        void deployTypeChanged(bool enabled);
        void restart();

    public slots:
        void updatePath();
        void resetPath();
        void resetImportPath();
        void autosaveDeleted();

    protected:
        QPixmap restartIcon();
        QHBoxLayout *restartLayout(QLabel *label);
        void saveSettings();
        void resetSettings();
        void onRestart();
        void enableSave(int index);
        void selectPath();
        void selectImportPath();
        void selectSkin(int index);
        void toggleBGMAutoplay(bool enabled);
        void toggleFolderLetter(bool enabled);
        void toggleAutosave(bool enabled);
        void toggleDeployType(bool enabled);
        void toggleImportType(bool enabled);
        void toggleImportEnabled(bool enabled);
        QStringList findAutosaveFiles();
        void restoreAutosave();
        void deleteAutosave();
        void autosaveItemSelected(const QModelIndex index);
        void toggleBranding(bool enabled);

    private:
        QComboBox *_languageselect;
        QComboBox *_skinselect;
        QCheckBox *_importtype;
        QCheckBox *_importenabled;
        QPushButton *_importselect;
        QLabel *_importlabel;
        QCheckBox *_deploytype;
        QPushButton *_deployselect;
        QLabel *_deploylabel;
        QCheckBox *_bgmautoplay;
        QCheckBox *_folderletter;
        QCheckBox *_autosaveenabled;
        QSpinBox *_autosaveinterval;
        QListView *_autosavelist;
        QStringListModel *_autosavemodel;
        QPushButton *_autosaverestore;
        QPushButton *_autosavedelete;
        QPushButton *_savebutton;
        QPushButton *_resetbutton;
        QPushButton *_restartbutton;
        QCheckBox *_branding;
        QTemporaryDir *_tempimportdir;

        bool _isfolderimport;
        bool _isimportenabled;
        QString _importpath;
        QString _deploypath;
        QString _defaultimportpath;
        QString _defaultpath;
        bool _isbgmautoplay;
        bool _showfolderletter;
        bool _isbranding;
        QString _autosavefilepath;
};

#endif // SETTINGSWIDGET_H
