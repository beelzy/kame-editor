// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include <QPainter>
#include <QMessageBox>

#include "thumbnailwidget.h"
#include "../color.h"

ThumbnailWidget::ThumbnailWidget(QWidget *parent) : QAbstractButton(parent) {
    setMinimumSize(QSize(64, 64));
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void ThumbnailWidget::setDimensions(QSize size) {
    QSize oldsize = _dimensions;
    _dimensions = size;
    float ratio = float(size.width()) / size.height();
    int minsize = std::min(size.height(), 64);
    setMinimumSize(minsize * ratio, minsize);
    if (oldsize != _dimensions) {
        _image = QImage();
    }
}

bool ThumbnailWidget::validateDimensions(QString path, bool isUser) {
    QImage test(path);
    if (test.width() == _dimensions.width() && test.height() == _dimensions.height()) {
        return true;
    } 
    if (isUser) {
        QMessageBox msgBox(QMessageBox::Warning, tr("Incorrect Texture Image Size", "Title for error message."), tr("The image you have selected is the wrong dimensions. Please use an image with a resolution of %1 x %2.", "This message shows up when you try to select an image in the texture widget that's not the correct dimensions.").arg(QString::number(_dimensions.width())).arg(QString::number(_dimensions.height())), QFlag(0), this);
        msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
        msgBox.exec();
    }
    return false;
}

void ThumbnailWidget::setImage(QString path) {
    QImage test(path);
    _image = test;
    update();
}

void ThumbnailWidget::reset() {
    _image = QImage();
}

QImage ThumbnailWidget::image() {
    return _image;
}

void ThumbnailWidget::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    p.fillRect(0, 0, width(), height(), QColor("#303030"));
    
    float tilesize = 12;
    int htiles = ceil(width() / (tilesize * 2));
    int vtiles = ceil(height() / tilesize);

    for (int i = 0; i < htiles + 1; i++) {
        for (int j = 0; j < vtiles + 1; j++) {
            p.fillRect(i * tilesize * 2 - (j % 2) * tilesize, j * tilesize, tilesize, tilesize, QColor("#404040"));
        }
    }

    if (!_image.isNull()) {
        p.drawImage(0, 0, _image.scaledToHeight(height()));
    }

    if (underMouse()){
        QColor color = Color::MAIN;
        color.setAlpha(0x80);
        p.fillRect(0, 0, width(), height(), color);
    };

    p.setBrush(QColor(Qt::transparent));
    p.setPen(Color::LIGHT);
    p.drawRect(0, 0, width(), height());
}
