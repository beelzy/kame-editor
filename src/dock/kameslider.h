// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef KAMESLIDER_H
#define KAMESLIDER_H

#include <QWidget>
#include <QSlider>
#include <QLabel>

class KameSlider : public QWidget {
    Q_OBJECT
    public:
        explicit KameSlider(QWidget *parent = 0);
        ~KameSlider();

        void setTitle(QString value);
        void setUnits(QString value);
        void setRange(float min, float max);
        void setValue(float value);
        void setStep(float value);
        float value();
        void updateSlider();

    signals:
        void valueChanged(int value);

    public slots:
        void onValueChanged(int value);

    protected:
        void paintEvent(QPaintEvent *event);

    private:
        QSlider *_slider;
        QLabel *_maxlabel;
        QLabel *_minlabel;
        QLabel *_currentlabel;
        QLabel *_titlelabel;
        QString _units;
        float _scaleFactor;
        float _shift;
        float _totalrange;
        float _stepsize;
};

#endif // KAMESLIDER_H
