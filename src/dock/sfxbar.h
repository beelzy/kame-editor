// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SFXBAR_H
#define SFXBAR_H

#include <QFrame>
#include <QMap>
#include <QHBoxLayout>
#include <QVector>

#include "barsegment.h"

class SfxBar : public QFrame {
    Q_OBJECT

    public:
        static const QColor COLOR1;
        static const QColor COLOR2;
        static const QColor COLOR3;
        static const QColor COLOR4;
        static const QColor COLOR5;
        static const QColor COLOR6;
        static const QColor COLOR7;
        static const QColor COLOR8;

        static const char* tooltips[];

        explicit SfxBar(QWidget *parent = 0);
        ~SfxBar();

        void setMaxValue(float value);
        void addSfx(QString key, float value);
        void updateSfx(QString key, float value);

    protected:
        void paintEvent(QPaintEvent *event);
        void updateBarSize(QString key);
        void updateToolTip(QString key);
        void updateAll();

    private:
        float _maxValue;
        QMap<QString, BarSegment*> _sfxes;
        QHBoxLayout *_layout;

        QVector<QColor> _colors;
        QMap<QString, int> _translations;
};

#endif // SFXBAR_H
