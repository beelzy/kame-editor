// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QVBoxLayout>
#include <QComboBox>
#include <QSettings>
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>
#include <QDebug>

#include "settingswidget.h"
#include "icons/restart.h"
#include "icons/folder.h"
#include "../sourcefile.h"
#include "../color.h"

const QStringList SettingsWidget::LOCALE = QStringList() << "en_US" << "de_DE";
const QStringList SettingsWidget::SKIN = QStringList() << "3ds" << "2ds" << "3dsxl" << "new3dsxl" << "new2dsxl";

SettingsWidget::SettingsWidget(QWidget * parent) : QFrame(parent) {
    QVBoxLayout *layout = new QVBoxLayout();

    QLabel *language = new QLabel(tr("Language: ", "Language selection title"));
    QHBoxLayout *languagelayout = restartLayout(language);

    _languageselect = new QComboBox();
    _languageselect->addItems(QStringList() << tr("English", "Language option") << tr("German", "Language option"));

    int localeIndex = std::max(static_cast<int>(LOCALE.indexOf(QLocale::system().name())), 0);
    QSettings settings;
    if (settings.contains("locale")) {
        localeIndex = LOCALE.indexOf(settings.value("locale").toString());
    }
    settings.setValue("currentLocale", LOCALE[localeIndex]);
    _languageselect->setCurrentIndex(localeIndex);

    QLabel *skin = new QLabel(tr("Console Skin: ", "Title for selecting console appearance."));
    _skinselect = new QComboBox();
    _skinselect->addItems(QStringList() << tr("3DS", "Console skin option") << tr("2DS", "Console skin option") << tr("3DS XL", "Console skin option") << tr("New 3DS XL", "Console skin option") << tr("New 2DS XL", "Console skin option"));

    int skinIndex = 0;
    if (settings.contains("skin")) {
        skinIndex = SKIN.indexOf(settings.value("skin").toString());
    }
    _skinselect->setCurrentIndex(skinIndex);

    _bgmautoplay = new QCheckBox(tr("Autoplay BGM", "Settings option for enabling autoplaying BGMs"));
    _isbgmautoplay = settings.contains("bgmautoplay") ? settings.value("bgmautoplay").toBool() : false;
    _bgmautoplay->setChecked(_isbgmautoplay);

    _folderletter = new QCheckBox(tr("Show folder letter", "Settings option for displaying first letter on bottom screen folders."));
    _showfolderletter = settings.contains("folderletter") ? settings.value("folderletter").toBool() : true;
    _folderletter->setChecked(_showfolderletter);

    _tempimportdir = new QTemporaryDir();
    if (!_tempimportdir->isValid()) {
        qDebug() << _tempimportdir->errorString();
    }
    QDir(_tempimportdir->path()).mkdir("imports");
    _defaultimportpath = _tempimportdir->path() + "/imports";
    settings.setValue("import/tmp", _tempimportdir->path());
    _isimportenabled = settings.contains("import/path") && !settings.value("import/path").toString().isEmpty();
    _importenabled = new QCheckBox(tr("Custom Import Path: ", "Title for import path"));
    _importenabled->setChecked(_isimportenabled);
    _importenabled->setToolTip(tr("Custom location to save source files for imported themes. If it's disabled, the editor picks a temporary location for these, and will be removed when the app closes. Enable this if you want to keep the source files.", "Tooltip to explain to users the point of having a custom import path."));
    _importtype = new QCheckBox(tr("Import from Folder", "Title for whether to import from folder or zip"));
    _importtype->setToolTip(tr("Import themes from a folder instead of from a zip file.", "Tooltip for import from folder option"));
    _isfolderimport = settings.contains("import/folder") ? settings.value("import/folder").toBool() : false;
    _importtype->setChecked(_isfolderimport);
    Folder *folder = new Folder();
    QIcon icon(folder->icon());
    _importselect = new QPushButton();
    _importselect->setIcon(icon);
    _importselect->setIconSize(folder->icon().rect().size());

    _importselect->setObjectName("select");
    _importselect->setStyleSheet(folder->selectStyle());
    _importselect->setCursor(Qt::PointingHandCursor);

    _importselect->setEnabled(_importenabled->isChecked());

    _importlabel = new QLabel();
    if (!settings.contains("import/path") || settings.value("import/path").toString().isEmpty()) {
        resetImportPath();
    } else {
        _importpath = settings.value("import/path").toString();
        _importlabel->setText(_importpath);
    }

    QHBoxLayout *importlayout = new QHBoxLayout();
    importlayout->addWidget(_importselect);
    importlayout->addWidget(_importlabel, 1);

    QLabel *deploy = new QLabel(tr("Deploy Path: ", "Title for deploy path"));
    _deploytype = new QCheckBox(tr("Deploy Target to Folder", "Title for deploy target"));
    _deployselect = new QPushButton();
    _deployselect->setIcon(icon);
    _deployselect->setIconSize(folder->icon().rect().size());

    _deployselect->setObjectName("select");
    _deployselect->setStyleSheet(folder->selectStyle());
    _deployselect->setCursor(Qt::PointingHandCursor);

    _deploylabel = new QLabel();
    toggleDeployType(false);

    QHBoxLayout *deploylayout = new QHBoxLayout();
    deploylayout->addWidget(_deployselect);
    deploylayout->addWidget(_deploylabel, 1);

    autosaveFilesPending = false;
    _autosaveenabled = new QCheckBox(tr("Autosave", "autosave label in settings widget"));
    QLabel *autosaveIntervalLabel = new QLabel(tr("Autosave interval:", "Autosave interval text in settings widget"));
    QLabel *autosaveIntervalTime = new QLabel(tr("min(s)", "autosave interval in minutes"));
    _autosaveinterval = new QSpinBox();

    if (settings.contains("autosave")) {
        _autosaveenabled->setChecked(true);
        _autosaveinterval->setValue(settings.value("autosave").toInt());
    } else {
        _autosaveinterval->setEnabled(false);
    }

    QLabel *autosaveListLabel = new QLabel(tr("Untitled Autosave Files", "Title for list of untitled autosave files"));

    _autosavelist = new QListView();
    _autosavemodel = new QStringListModel(findAutosaveFiles());
    _autosavelist->setModel(_autosavemodel);
    _autosavelist->setEditTriggers(QAbstractItemView::NoEditTriggers);
    _autosavelist->setSelectionMode(QAbstractItemView::SingleSelection);
    _autosavelist->setAlternatingRowColors(true);

    _autosaverestore = new QPushButton(tr("Restore", "button for restoring autosave files"));
    _autosavedelete = new QPushButton(tr("Delete", "button for deleting autosave files"));
    _autosaverestore->setEnabled(false);
    _autosavedelete->setEnabled(false);

    if (_autosavemodel->rowCount() > 0) {
        QMessageBox msgBox(QMessageBox::Warning, tr("Untitled Autosave Files Pending", "Title for message box warning users about existing autosave files"), tr("Autosave files from previously unsaved rc files have been cached. Please look in the settings widget to resolve them."), QFlag(0), this);
        msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
        msgBox.exec();
        autosaveFilesPending = true;
    }

    QHBoxLayout *autosavelayout = new QHBoxLayout();
    autosavelayout->addWidget(autosaveIntervalLabel);
    autosavelayout->addWidget(_autosaveinterval);
    autosavelayout->addWidget(autosaveIntervalTime);

    QHBoxLayout *autosavebuttonlayout = new QHBoxLayout();
    autosavebuttonlayout->addWidget(_autosaverestore);
    autosavebuttonlayout->addWidget(_autosavedelete);

    _savebutton = new QPushButton();
    _savebutton->setText(tr("Save"));
    _savebutton->setEnabled(false);

    _resetbutton = new QPushButton();
    _resetbutton->setText(tr("Reset"));
    _resetbutton->setEnabled(false);

    _restartbutton = new QPushButton();
    _restartbutton->setText(tr("Restart"));
    _restartbutton->hide();

    _branding = new QCheckBox(tr("Branded Preview", "Option to add branding to preview screenshots"));
    _branding->setToolTip(tr("Preview screenshot contains additional \"Made with Kame Editor\" text.", "Tooltip for branded preview option"));
    _isbranding = settings.contains("branding") ? settings.value("branding").toBool() : false;
    _branding->setChecked(_isbranding);

    layout->addLayout(languagelayout);
    layout->addWidget(_languageselect);
    layout->addWidget(skin);
    layout->addWidget(_skinselect);
    layout->addWidget(_bgmautoplay);
    layout->addWidget(_folderletter);
    layout->addWidget(_importtype);
    layout->addWidget(_importenabled);
    layout->addLayout(importlayout);
    layout->addWidget(deploy);
    layout->addWidget(_deploytype);
    layout->addLayout(deploylayout);
    layout->addWidget(_autosaveenabled);
    layout->addLayout(autosavelayout);
    layout->addWidget(autosaveListLabel);
    layout->addWidget(_autosavelist);
    layout->addLayout(autosavebuttonlayout);
    layout->addWidget(_branding);
    layout->addWidget(_savebutton);
    layout->addWidget(_resetbutton);
    layout->addWidget(_restartbutton);

    setLayout(layout);

    connect(_savebutton, &QAbstractButton::clicked, this, &SettingsWidget::saveSettings);
    connect(_resetbutton, &QAbstractButton::clicked, this, &SettingsWidget::resetSettings);
    connect(_restartbutton, &QAbstractButton::clicked, this, &SettingsWidget::onRestart);
    connect(_languageselect, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SettingsWidget::enableSave);
    connect(_skinselect, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SettingsWidget::selectSkin);
    connect(_importselect, &QAbstractButton::clicked, this, &SettingsWidget::selectImportPath);
    connect(_importenabled, &QCheckBox::toggled, this, &SettingsWidget::toggleImportEnabled);
    connect(_importtype, &QCheckBox::toggled, this, &SettingsWidget::toggleImportType);
    connect(_deployselect, &QAbstractButton::clicked, this, &SettingsWidget::selectPath);
    connect(_deploytype, &QCheckBox::toggled, this, &SettingsWidget::toggleDeployType);
    connect(_bgmautoplay, &QCheckBox::toggled, this, &SettingsWidget::toggleBGMAutoplay);
    connect(_folderletter, &QCheckBox::toggled, this, &SettingsWidget::toggleFolderLetter);
    connect(_autosaveenabled, &QCheckBox::toggled, this, &SettingsWidget::toggleAutosave);
    connect(_autosaveinterval, QOverload<int>::of(&QSpinBox::valueChanged), this, &SettingsWidget::enableSave);
    connect(_autosavelist, &QAbstractItemView::clicked, this, &SettingsWidget::autosaveItemSelected);
    connect(_autosaverestore, &QAbstractButton::clicked, this, &SettingsWidget::restoreAutosave);
    connect(_autosavedelete, &QAbstractButton::clicked, this, &SettingsWidget::deleteAutosave);
    connect(_branding, &QCheckBox::toggled, this, &SettingsWidget::toggleBranding);

    delete folder;
}

SettingsWidget::~SettingsWidget() {
    delete _languageselect;
    delete _skinselect;
    delete _tempimportdir;
    delete _importtype;
    delete _importenabled;
    delete _importselect;
    delete _importlabel;
    delete _deploytype;
    delete _deploylabel;
    delete _deployselect;
    delete _bgmautoplay;
    delete _autosaveenabled;
    delete _autosaveinterval;
    _autosavelist->setModel(0);
    delete _autosavemodel;
    delete _autosavelist;
    delete _autosaverestore;
    delete _autosavedelete;
    delete _savebutton;
    delete _resetbutton;
    delete _restartbutton;
    delete _branding;
}

QPixmap SettingsWidget::restartIcon() {
    Restart *restart = new Restart(QColor(Color::MAIN));
    QPixmap icon = restart->icon();
    delete restart;
    return icon;
}

QHBoxLayout *SettingsWidget::restartLayout(QLabel *label) {
    QHBoxLayout *layout = new QHBoxLayout();
    QLabel *icon = new QLabel();
    icon->setPixmap(restartIcon());
    icon->setToolTip(tr("Requires restart to take effect."));
    layout->addWidget(icon);
    layout->addWidget(label, 1);
    return layout;
}

void SettingsWidget::enableSave(int) {
    _savebutton->setEnabled(true);
    _savebutton->setText(tr("Save"));
    _resetbutton->setEnabled(true);
}

void SettingsWidget::onRestart() {
    emit restart();
}

void SettingsWidget::saveSettings() {
    QSettings settings;
    if (LOCALE[_languageselect->currentIndex()] != settings.value("currentLocale").toString()) {
        _restartbutton->show();
    } else {
        _restartbutton->hide();
    }
    settings.setValue("locale", LOCALE[_languageselect->currentIndex()]);
    SourceFile::recentlyModified = SourceFile::deploypath != _deploypath;
    SourceFile::deploypath = _deploypath;
    settings.setValue("import/path", _importpath == _defaultimportpath ? "" : _importpath);
    settings.setValue("import/folder", _isfolderimport);
    settings.setValue("bgmautoplay", _isbgmautoplay);
    settings.setValue("skin", SKIN[_skinselect->currentIndex()]);
    emit bgmAutoplayChanged(_isbgmautoplay);
    settings.setValue("folderletter", _showfolderletter);
    int interval = 0;
    if (_autosaveenabled->isChecked() && _autosaveinterval->value() > 0) {
        settings.setValue("autosave", _autosaveinterval->value());
        interval = _autosaveinterval->value();
    } else {
        settings.remove("autosave");
    }
    emit autosaveChanged(interval);
    settings.setValue("branding", _isbranding);
    _savebutton->setText(tr("Saved"));
    _savebutton->setEnabled(false);
    _resetbutton->setEnabled(false);
}

void SettingsWidget::resetSettings() {
    QSettings settings;
    if (SourceFile::deploypath.isEmpty()) {
        resetPath();
    } else {
        _deploypath = SourceFile::deploypath;
        QFileInfo deploypath = QFileInfo(_deploypath);
        _deploytype->setChecked(deploypath.suffix() != "zip");
        _deploylabel->setText(_deploypath);
    }
    _isimportenabled = settings.contains("import/path") && !settings.value("import/path").toString().isEmpty();
    _importenabled->setChecked(_isimportenabled);
    _importselect->setEnabled(_isimportenabled);
    if (!_isimportenabled) {
        resetImportPath();
    } else {
        _importpath = settings.value("import/path").toString();
        _importlabel->setText(_importpath);
    }
    _isfolderimport = settings.contains("import/folder") ? settings.value("import/folder").toBool() : false;
    _importtype->setChecked(_isfolderimport);
    int index = LOCALE.indexOf(settings.value("locale").toString());
    _languageselect->setCurrentIndex(index);
    if (settings.value("currentLocale").toString() == settings.value("locale").toString()) {
        _restartbutton->hide();
    } else {
        _restartbutton->show();
    }
    index = SKIN.indexOf(settings.value("skin").toString());
    _skinselect->setCurrentIndex(index);
    _isbgmautoplay = settings.contains("bgmautoplay") ? settings.value("bgmautoplay").toBool() : false;
    _bgmautoplay->setChecked(_isbgmautoplay);
    _showfolderletter = settings.contains("folderletter") ? settings.value("folderletter").toBool() : true;
    _folderletter->setChecked(_showfolderletter);
    _autosaveenabled->setChecked(settings.contains("autosave"));
    _autosaveinterval->setValue(_autosaveenabled->isChecked() ? settings.value("autosave").toInt() : 0);
    _isbranding = settings.contains("branding") ? settings.value("branding").toBool() : false;
    _branding->setChecked(_isbranding);
    _savebutton->setEnabled(false);
    _resetbutton->setEnabled(false);
    emit skinChanged(settings.value("skin").toString());
}

void SettingsWidget::toggleDeployType(bool enabled) {
    _defaultpath = enabled ? tr("(No folder selected)", "Tooltip that shows up when no destination has been selected for the deploy folder.") : tr("(No file selected)", "Tooltip that shows up when no destination has been selected for the deploy path.");
    QFileInfo deploypath = QFileInfo(SourceFile::deploypath);
    if (SourceFile::deploypath.isEmpty() || (deploypath.isDir() && !enabled) || (deploypath.suffix() == "zip" && enabled)) {
        resetPath();
        _deploypath = "";
    } else {
        _deploylabel->setText(SourceFile::deploypath);
    }
    emit deployTypeChanged(enabled);
}

void SettingsWidget::toggleImportType(bool enabled) {
    _isfolderimport = enabled;
    enableSave(0);
}

void SettingsWidget::toggleImportEnabled(bool enabled) {
    _isimportenabled = enabled;
    resetImportPath();
    _importselect->setEnabled(enabled);
    enableSave(0);
}

void SettingsWidget::selectImportPath() {
    _importpath = QFileDialog::getExistingDirectory(this, tr("Select import location", "Title for file dialog to select import folder"));
    if (!_importpath.isEmpty()) {
        _importlabel->setText(_importpath);
    }
    enableSave(0);
}

void SettingsWidget::selectPath() {
    if (_deploytype->isChecked()) {
        _deploypath = QFileDialog::getExistingDirectory(this, tr("Select deploy folder", "Title for file dialog to select deploy folder"));
        if (!_deploypath.isEmpty()) {
            QDir dir = QDir(_deploypath);
            QStringList filelist = dir.entryList(QDir::Files | QDir::NoDotAndDotDot | QDir::NoDot | QDir::NoDotDot);
            if (filelist.contains("info.smdh", Qt::CaseInsensitive) || filelist.contains("body_lz.bin", Qt::CaseInsensitive) || filelist.contains("bgm.bcstm", Qt::CaseInsensitive) || filelist.contains("preview.png", Qt::CaseInsensitive)) {
                    QMessageBox msgBox(QMessageBox::Warning, tr("Theme Files Already Exist", "Title for message box warning users deploy folder already contains theme files"), tr("The folder you have chosen for theme deploy already contains theme files. Please note that these files will be overwritten. If this is not what you want to happen, please pick a different deploy folder."), QFlag(0), this);
                    msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
                    msgBox.exec();
                    }
            _deploylabel->setText(_deploypath);
        }
    } else {
        QFileDialog *dialog = new QFileDialog();
        dialog->setAcceptMode(QFileDialog::AcceptSave);
        dialog->setNameFilters(QStringList() << tr("Zip archive (*.zip)"));
        dialog->setWindowTitle(tr("Select deploy path", "Title for file dialog to select deploy path"));
        dialog->setDefaultSuffix("zip");

        if (dialog->exec()) {
            _deploypath = dialog->selectedFiles().first();
            _deploylabel->setText(_deploypath);
        }
    }
    enableSave(0);
}

void SettingsWidget::selectSkin(int index) {
    emit skinChanged(SKIN[index]);
    enableSave(0);
}

void SettingsWidget::resetImportPath() {
    _importpath = _defaultimportpath;
    _importlabel->setText(_defaultimportpath);
}

void SettingsWidget::resetPath() {
    _deploylabel->setText(_defaultpath);
}

void SettingsWidget::updatePath() {
    if (SourceFile::deploypath.isEmpty()) {
        resetPath();
    } else {
        _deploylabel->setText(SourceFile::deploypath);
        QFileInfo deploypath = QFileInfo(SourceFile::deploypath);
        _deploytype->setChecked(deploypath.suffix() != "zip");
        if (_deploytype->isChecked() && deploypath.isDir()) {
            QDir().mkpath(SourceFile::deploypath);
        }
        _deploypath = SourceFile::deploypath;
    }
}

void SettingsWidget::toggleBGMAutoplay(bool enabled) {
    _isbgmautoplay = enabled;
    enableSave(0);
}

void SettingsWidget::toggleFolderLetter(bool enabled) {
    _showfolderletter = enabled;
    emit folderLetterChanged(enabled);
    enableSave(0);
}

void SettingsWidget::toggleAutosave(bool enabled) {
    _autosaveinterval->setEnabled(enabled);
    _autosavelist->setEnabled(enabled);
    _autosavelist->clearSelection();
    _autosaverestore->setEnabled(false);
    _autosavedelete->setEnabled(false);
    enableSave(0);
}

QStringList SettingsWidget::findAutosaveFiles() {
    QDir cachePath = QDir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    cachePath.setNameFilters(QStringList() << "*.rc.autosave");
    cachePath.setFilter(QDir::Files);
    QStringList results = cachePath.entryList();
    // Current autosave file doesn't count
    QSettings settings;
    if (settings.contains("autosaveFilename")) {
        int index = results.indexOf(settings.value("autosaveFilename").toString());
        if (index >= 0) {
            results.removeAt(index);
        }
    }
    return results;
}

void SettingsWidget::restoreAutosave() {
    emit autosaveRestored(_autosavefilepath);
}

void SettingsWidget::deleteAutosave() {
    QFile(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/" + _autosavefilepath).remove();
    _autosavemodel->setStringList(findAutosaveFiles());
}

void SettingsWidget::autosaveItemSelected(const QModelIndex index) {
    _autosavefilepath = index.data().toString();
    bool enabled = !_autosavefilepath.isEmpty();
    _autosaverestore->setEnabled(enabled);
    _autosavedelete->setEnabled(enabled);
}

void SettingsWidget::autosaveDeleted() {
    _autosavemodel->setStringList(findAutosaveFiles());
    _autosavelist->clearSelection();
    _autosaverestore->setEnabled(false);
    _autosavedelete->setEnabled(false);
}

void SettingsWidget::toggleBranding(bool enabled) {
    _isbranding = enabled;
    enableSave(0);
}
