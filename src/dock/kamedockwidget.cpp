// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QVBoxLayout>
#include <QScrollArea>
#include <QScrollBar>
#include <QMouseEvent>

#include "kamedockwidget.h"

KameDockWidget::KameDockWidget(QWidget *contents, QString title, QWidget *parent) : QDockWidget(title, parent) {
    setAutoFillBackground(true);

    setAllowedAreas(Qt::AllDockWidgetAreas);
    _contentFrame = new QFrame(this);

    _sizegrip = new QSizeGrip(_contentFrame);
    QSizeGrip *sizeGrip = new QSizeGrip(_contentFrame);

    _contents = contents;

    QScrollArea *scrollarea = new QScrollArea();

    QVBoxLayout *layout = new QVBoxLayout(_contentFrame);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    scrollarea->setWidget(_contentFrame);
    scrollarea->setWidgetResizable(true);
    scrollarea->setCornerWidget(sizeGrip);

    layout->addWidget(_contents);
    layout->addWidget(_sizegrip, 0, Qt::AlignRight | Qt::AlignBottom);

    _overlay = new OverlayWidget(_contents);
    _overlay->hide();

    _contentFrame->setLayout(layout);

    setWidget(scrollarea);

    connect(this, &QDockWidget::dockLocationChanged, this, &KameDockWidget::onDockLocationChanged);
    connect(scrollarea->horizontalScrollBar(), &QAbstractSlider::rangeChanged, this, &KameDockWidget::onScrollBarRangeChanged);
    connect(scrollarea->verticalScrollBar(), &QAbstractSlider::rangeChanged, this, &KameDockWidget::onScrollBarRangeChanged);
    connect(scrollarea->horizontalScrollBar(), &QAbstractSlider::valueChanged, this, &KameDockWidget::onScrollBarValueChanged);
    connect(scrollarea->verticalScrollBar(), &QAbstractSlider::valueChanged, this, &KameDockWidget::onScrollBarValueChanged);
}

KameDockWidget::~KameDockWidget() {
    delete _sizegrip;
    delete _overlay;
    delete _contents;
    delete _contentFrame;
}

void KameDockWidget::setEnabled(bool value) {
    QWidget::setEnabled(value);
    if (value) {
        _overlay->hide();
    } else {
        _overlay->show();
    }
}

void KameDockWidget::onDockLocationChanged(Qt::DockWidgetArea) {
    updateSizeGrip();
}

void KameDockWidget::onScrollBarRangeChanged(int, int) {
    updateSizeGrip();
}

void KameDockWidget::onScrollBarValueChanged(int) {
    updateSizeGrip();
}

void KameDockWidget::resizeEvent(QResizeEvent *) {
    updateSizeGrip();
}

void KameDockWidget::updateSizeGrip() {
    QScrollArea *scrollarea = static_cast<QScrollArea*>(widget());
    int rangex = scrollarea->horizontalScrollBar()->maximum() - scrollarea->horizontalScrollBar()->minimum();
    int rangey = scrollarea->verticalScrollBar()->maximum() - scrollarea->verticalScrollBar()->minimum();

    if (rangex > 0 || rangey > 0) {
        _sizegrip->setStyleSheet("background-color: transparent;");
    } else {
        _sizegrip->setStyleSheet("width: 16px; height: 16px;");
    }
}

void KameDockWidget::mouseMoveEvent(QMouseEvent *event) {
    event->accept();
}
