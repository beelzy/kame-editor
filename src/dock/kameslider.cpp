// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPainter>

#include "kameslider.h"
#include "../color.h"

KameSlider::KameSlider(QWidget *parent) : QWidget(parent) {
    _totalrange = 10000;
    _stepsize = 10;
    _units = "";
    _shift = 0;
    _scaleFactor = 1;
    QVBoxLayout *vlayout = new QVBoxLayout();
    QHBoxLayout *hlayout = new QHBoxLayout();
    QHBoxLayout *titlelayout = new QHBoxLayout();

    _slider = new QSlider(Qt::Horizontal); 
    _slider->setMaximum(_totalrange);
    _slider->setTickInterval(_totalrange / _stepsize);
    _slider->setTickPosition(QSlider::TicksBelow);

    QFont small = QFont("Nimbus Sans", 6);
    QFont font = QFont("Nimbus Sans", 10);
    _maxlabel = new QLabel("10000");
    _maxlabel->setFont(small);
    _minlabel = new QLabel("0");
    _minlabel->setFont(small);
    _currentlabel = new QLabel("0");
    _currentlabel->setFont(font);
    _titlelabel = new QLabel(":");
    _titlelabel->setFont(font);

    hlayout->setContentsMargins(7, 0, 7, 0);
    hlayout->addWidget(_minlabel, 1);
    hlayout->addWidget(_maxlabel, 1, Qt::AlignRight);

    titlelayout->addWidget(_titlelabel, 0);
    titlelayout->addWidget(_currentlabel, 0, Qt::AlignLeft);
    titlelayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));

    vlayout->setContentsMargins(7, 0, 7, 0);
    vlayout->addLayout(titlelayout, 0);
    vlayout->addWidget(_slider);
    vlayout->addLayout(hlayout, 1);

    setLayout(vlayout);

    updateSlider();
    connect(_slider, &QAbstractSlider::valueChanged, this, &KameSlider::onValueChanged);

    setMinimumHeight(55);
}

KameSlider::~KameSlider() {
    delete _titlelabel;
    delete _minlabel;
    delete _currentlabel;
    delete _maxlabel;
    delete _slider;
}

void KameSlider::setTitle(QString value) {
    _titlelabel->setText(value + ":");
}

void KameSlider::setUnits(QString units) {
    _units = units;
    updateSlider();
}

void KameSlider::setRange(float min, float max) {
    float range = max - min;
    _scaleFactor = range / _totalrange;
    _shift = min;

    _minlabel->setText(QString::number(min));
    _maxlabel->setText(QString::number(max));
}

void KameSlider::setStep(float value) {
    float range = _scaleFactor * _totalrange;
    _totalrange = value * range;
    _scaleFactor = range / _totalrange;
    _stepsize = _totalrange / value;
    _slider->setMaximum(_totalrange);
}

float KameSlider::value() {
    return _slider->value() / float(_totalrange);
}

void KameSlider::onValueChanged(int value) {
    updateSlider();
    emit valueChanged(value);
}

void KameSlider::setValue(float value) {
    _slider->setValue(value * float(_totalrange));
}

void KameSlider::updateSlider() {
    _currentlabel->setText(QString::number(_slider->value() * _scaleFactor + _shift) + _units);
}

void KameSlider::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    p.setBrush(Color::MAIN);
    p.setPen(QColor(Qt::transparent));
    float scale = _slider->geometry().height() / 20.0;
    p.drawRect(14, _slider->geometry().y() + _slider->geometry().height() - 9 * scale, 1, 8);
    float range = _slider->geometry().x() + _slider->geometry().width() - 8 - 14;

    for (int i = 1; i < _stepsize; i++) {
        p.drawRect(14 + range * i / _stepsize, _slider->geometry().y() + _slider->geometry().height() - 9 * scale, 1, 4);
    }

    p.drawRect(_slider->geometry().x() + _slider->geometry().width() - 8, _slider->geometry().y() + _slider->geometry().height() - 9 * scale, 1, 8);
}
