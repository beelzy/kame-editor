// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef IMAGEPICKER_H
#define IMAGEPICKER_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QDateTime>

#include "thumbnailwidget.h"
#include "accordionwidget.h"
#include "infodialog.h"
#include "../kametools.h"

class ImagePicker : public QWidget {
    Q_OBJECT
    public:
        explicit ImagePicker(QWidget *parent = 0);
        ~ImagePicker();

        void setAccordionWidget(AccordionWidget *widget);
        void setTitle(QString value);
        void setTooltip(QString value);
        void setInfoDialog(QString value, QString title = "", int minWidth = 256);
        void setDimensions(QSize size);
        void setImage(QString path, bool isUser = true);
        void setClearable(bool value);
        void setDither(bool value);
        bool fileChanged();
        void reload();
        QString filepath();
        void reset();

        public slots:
            void clearImage();

    signals:
        void stateChanged(bool value);
        void imageDithered();

    protected slots:
        void onSelectClicked();
        void onUserDithered(bool, QString);
        void onDithered(bool, QString);

    protected:
        void _setImage(QString path);
        void setFileData(QString path);
        bool isImageValid(QString path, bool isUser = true);
        void _onDithered(bool, QString);

    private:
        ThumbnailWidget *_thumbnail;
        QLabel *_title;
        QLabel *_caption;
        QPushButton *_select;
        QLabel *_fileinput;
        InfoIcon *_infoicon;
        QLabel *_warning;
        QString _filepath;
        QPushButton *_reset;
        InfoDialog *_infodialog;
        bool _dither;
        KameTools *_kametools;

        QString _rgb565warning;
        QDateTime _lastChanged;
        int _filesize;
};

#endif // IMAGEPICKER_H
