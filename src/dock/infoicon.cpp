// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QMouseEvent>

#include "infoicon.h"
#include "../color.h"

InfoIcon::InfoIcon(QWidget *parent) : QAbstractButton(parent) {
    setFixedSize(16, 16);
    setCursor(Qt::PointingHandCursor);
}

void InfoIcon::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    QPen pen = QPen(Color::LIGHT);
    pen.setWidth(1.5);
    pen.setCosmetic(true);
    p.setPen(pen);
    p.drawEllipse(1, 1, 14, 14);

    p.setPen(QColor(Qt::transparent));
    p.setBrush(Color::LIGHT);
    p.drawEllipse(7, 3, 2, 2);
    p.drawRect(7, 7, 2, 6);
}
