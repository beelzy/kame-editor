// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QDockWidget>
#include <QMainWindow>
#include <QMouseEvent>

#include "consolebutton.h"

ConsoleButton::ConsoleButton(QWidget * parent) : QAbstractButton(parent) {
    _skinid = "3ds";
    setBase("buttonbase");
    setCheckable(true);
    setChecked(true);
    setCursor(Qt::PointingHandCursor);
}

void ConsoleButton::setBasePath(QString base, QString down) {
    _normal.load(base);
    _pressed.load(down);
    resize(_normal.size());
    setFixedSize(_normal.size());
    updateIcon();
}

void ConsoleButton::setBase(QString base) {
    _base = base;
    updateSkin();
}

void ConsoleButton::setSkinId(QString value) {
    _skinid = value;
    updateSkin();
}

void ConsoleButton::updateSkin() {
    setBasePath(":/resources/skins/" + _skinid + "/" + _base + ".png", ":/resources/skins/" + _skinid + "/" + _base + "-down.png");
}

void ConsoleButton::setIcon(QString path) {
    _icon.load(path);

    updateIcon();
}

void ConsoleButton::updateIcon() {
    if (!_icon.isNull()) {
        QImage off(_normal.width(), _normal.height(), QImage::Format_ARGB32_Premultiplied);
        off.fill(QColor("#BDBDBD"));

        QImage on(_normal.width(), _normal.height(), QImage::Format_ARGB32_Premultiplied);
        on.fill(QColor(Qt::white));

        _iconoff = _icon.toImage().scaled(_normal.width(), _normal.height(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        _iconon = _icon.toImage().scaled(_normal.width(), _normal.height(), Qt::KeepAspectRatio, Qt::SmoothTransformation);

        QPainter p;

        p.begin(&_iconoff);
        p.setCompositionMode(QPainter::CompositionMode_SourceIn);
        p.drawImage(0, 0, off);
        p.end();

        p.begin(&_iconon);
        p.setCompositionMode(QPainter::CompositionMode_SourceIn);
        p.drawImage(0, 0, on);
        p.end();
    }
}

void ConsoleButton::updateChecked(bool isVisible) {
    QDockWidget *dockWidget = dynamic_cast<QDockWidget*>(QObject::sender());
    QMainWindow *main = dynamic_cast<QMainWindow*>(dockWidget->parentWidget());
    bool isAvailable = isVisible;
    if (main != nullptr && !main->tabifiedDockWidgets(dockWidget).isEmpty()) {
        isAvailable = true;
    }
    bool oldState = blockSignals(true);
    setChecked(isAvailable);
    blockSignals(oldState);
}

void ConsoleButton::paintEvent(QPaintEvent *) {
    QPainter p(this);

    QImage icon = isChecked() || !isCheckable() ? _iconon : _iconoff;

    float posy = 0;
    float iconX = (width() - icon.width()) / 2.0;
    float iconY = (height() - icon.height()) / 2.0;

    if (isDown()) {
        p.drawPixmap(0, 0, width(), height(), _pressed);
        posy++;
    } else {
        p.drawPixmap(0, 0, width(), height(), _normal);
    }

    p.drawImage(iconX, iconY + posy, icon);
}

void ConsoleButton::mouseMoveEvent(QMouseEvent *event) {
    event->accept();
}
