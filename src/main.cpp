// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>

#include "kamewidget.h"
#include "color.h"

int main(int argc, char *argv[]) {
    // Unfortunately, it appears Mesa or Intel integrated graphics or some combination of the two haven't been playing nicely with OpenGL multisampling since Qt 5.12, so we make it possible to disable msaa by running it with an extra argument.
    bool msaa = true;
    for (int i = 0; i < argc; i++) {
        if (QString(argv[i]) == "--no-msaa") {
            msaa = false;
        }
    }
    QSurfaceFormat fmt;
    fmt.setDepthBufferSize(24);

    fmt.setSwapInterval(1);
    if (msaa) {
        fmt.setSamples(8);
    }
    fmt.setVersion(3, 3);
    fmt.setProfile(QSurfaceFormat::CoreProfile);
    fmt.setAlphaBufferSize(8);
    QSurfaceFormat::setDefaultFormat(fmt);


    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication a(argc, argv);
    QCoreApplication::setApplicationName("kame-editor");
    QCoreApplication::setOrganizationName("beelzy");

    QString locale = QLocale::system().name();
    QSettings settings;
    if (settings.contains("locale")) {
        locale = settings.value("locale").toString();
    }

    QTranslator translator;
    translator.load("kameeditor_" + locale, ":/translations/");
    a.installTranslator(&translator);

    QTranslator baseTranslator;
    baseTranslator.load("qtbase_" + locale, ":/translations/");
    a.installTranslator(&baseTranslator);

    qputenv("PATH", qgetenv("PATH") + ":" + QCoreApplication::applicationDirPath().toLocal8Bit());

    QIcon appIcon;
    appIcon.addFile(":/resources/icons/icon-64x64.png");
    a.setWindowIcon(appIcon);

    QPalette pal = QPalette();
    pal.setColor(QPalette::Window, Color::DARK);
    pal.setColor(QPalette::Light, Color::DARK);
    pal.setColor(QPalette::WindowText, Color::MAIN);
    pal.setColor(QPalette::Base, Color::BLACK);
    pal.setColor(QPalette::Text, Color::LIGHT);
    // Most likely not needed, breaks on < Qt 5.12.
    //pal.setColor(QPalette::PlaceholderText, Color::MAIN);
    pal.setColor(QPalette::Button, Color::DARK);
    pal.setColor(QPalette::ButtonText, Color::MAIN);
    pal.setColor(QPalette::BrightText, Color::LIGHT);
    pal.setColor(QPalette::Highlight, QColor("#00A800"));
    pal.setColor(QPalette::Link, Color::MAIN);

    a.setPalette(pal);

    QFont font = QFont("Nimbus Sans", 10);
    a.setFont(font);

    QString styles = "QScrollArea {background-color: transparent;}"
            "QScrollArea > QWidget > QWidget {background-color: transparent;}"
            "QScrollArea > QWidget > QScrollBar {background-color: " + Color::BLACK.name() + ";}"
            "QScrollBar {background: " + Color::BLACK.name() + "; border-radius: 7px;}"
            "QScrollBar:vertical {margin: 14px 0 14px 0; width: 15px;}"
            "QScrollBar:horizontal {margin: 0 14px 0 14px; height: 15px;}"
            "QScrollBar::handle {background: " + Color::DARK.name() + "; border: 1px solid " + Color::MAIN.name() + "; border-radius: 7px;}"
            "QScrollBar::handle:vertical {min-height: 14px;}"
            "QScrollBar::handle:horizontal {min-width: 14px;}"
            "QScrollBar::add-line, QScrollBar::sub-line {background: " + Color::DARK.name() + "; border-radius: 7px; border: 1px solid " + Color::MAIN.name() + ";}"
            "QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {height: 14px;}"
            "QScrollBar::add-line:horizontal, QScrollBar::sub-line:horizontal {width: 14px;}"
            "QScrollBar::add-line:vertical {subcontrol-position: bottom; subcontrol-origin: margin;}"
            "QScrollBar::sub-line:vertical {subcontrol-position: top; subcontrol-origin: margin;}"
            "QScrollBar::add-line:horizontal {subcontrol-position: right; subcontrol-origin: margin;}"
            "QScrollBar::sub-line:horizontal {subcontrol-position: left; subcontrol-origin: margin;}"
            "QScrollBar::left-arrow, QScrollBar::right-arrow, QScrollBar::up-arrow, QScrollBar::down-arrow {background: transparent; width: 5px; height: 5px;}"
            "QScrollBar::down-arrow {image: url(:resources/dock/arrowdown.png);}"
            "QScrollBar::up-arrow {image: url(:resources/dock/arrowup.png);}"
            "QScrollBar::left-arrow {image: url(:resources/dock/arrowleft.png);}"
            "QScrollBar::right-arrow {image: url(:resources/dock/arrowright.png);}"
            "QComboBox {color: " + Color::MAIN.name() + "; border: 1px solid " + Color::MAIN.name() + "; border-radius: 3px; background-color: " + Color::DARK.name() + "; padding: 1px 3px; selection-background-color: " + Color::MAIN.name() + "; selection-color: " + Color::LIGHT.name() + ";}"
            "QComboBox::down-arrow {image: url(:/resources/dock/arrowdown.png);}"
            "QComboBox::drop-down:button {border-left: 1px solid " + Color::MAIN.name() + "; outline: none;}"
            "QComboBox QAbstractItemView {border: 1px solid " + Color::MAIN.name() + "; background-color: " + Color::DARK.name() + "; outline: none;}"
            "KameAccordion {outline: none; border: none;}"
            "ClickableFrame {background: " + Color::MAIN.name() + "; outline: none; border: none;}"
            "ClickableFrame QLabel {color: " + Color::LIGHT.name() + ";}"
            "KameDockWidget ClickableFrame QPushButton {border: none; outline: none; margin: 0; padding: 0;}"
            "AudioPicker ClickableFrame {background: none;}"
            "AudioPicker ClickableFrame QLabel {color: " + Color::MAIN.name() + ";}"
            "AudioPicker ContentPane .QFrame {background-color: " + Color::BLACK.name() + ";}"
            "QFrame, ContentPane {outline: none; border: none;}"
            "KameDockWidget {"
            "titlebar-normal-icon: url(:/resources/dock/floatbutton.png);"
            "titlebar-close-icon: url(:/resources/dock/closebutton.png);"
            "color: " + Color::MAIN.name() + ";}"
            "KameDockWidget::title {background: " + Color::BLACK.name() + "; padding-top: 3px;}"
            "KameDockWidget::close-button {background: transparent; border: none;}"
            "KameDockWidget::close-button:hover, KameDockWidget::float-button:hover {"
            "background: transparent; border: 1px solid " + Color::MAIN.name() + "; border-radius: 3px;}"
            "ClickableFrame:hover {background-color: " + Color::BLACK.name() + ";}"
            "QTabBar::tab {background-color: " + Color::DARK.name() + "; border: 1px solid " + Color::MAIN.name() + "; border-bottom: " + Color::DARK.name() + "; outline: none; padding: 5px 10px; border-top-left-radius: 6px; border-top-right-radius: 6px;}"
            "QTabBar::tab:hover {border: 1px solid " + Color::LIGHT.name() + "; border-bottom: " + Color::DARK.name() + "; color: " + Color::LIGHT.name() + ";}"
            "QTabBar::tab:selected {background-color: " + Color::MAIN.name() + "; color: " + Color::LIGHT.name() + "; border: 1px solid " + Color::MAIN.name() + ";}"
            "QTabBar::tab:!selected {margin-top: 3px;}"
            "KameSlider > QSlider::groove {border: none; background: transparent; height: 2px; color: " + Color::MAIN.name() + ";}"
            "KameSlider > QSlider::sub-page, KameSlider > QSlider::add-page {"
            "margin-left: 7px; margin-right: 7px; background: " + Color::MAIN.name() + "; height: 3px;"
            "}"
            "KameSlider > QSlider::handle {background: " + Color::DARK.name() + "; margin-top: -7px; margin-bottom: -7px; width: 14px; height: 16px; border-radius: 8px; border: 1px solid " + Color::MAIN.name() + ";}"
            "KameSlider > QSlider::handle:hover {border: 1px solid " + Color::LIGHT.name() + ";}"
            "ConsoleMenu > QPushButton {border: none; outline: none;}"
            "QLineEdit, QDoubleSpinBox, QSpinBox {color: " + Color::MAIN.name() + "; selection-background-color: " + Color::MAIN.name() + ";}"
            "QLineEdit:focus, QDoubleSpinBox:focus, QSpinBox:focus {border: 1px solid " + Color::MAIN.name() + "; border-radius: 3px;}"
            "QDoubleSpinBox::up-button, QSpinBox::up-button, QDoubleSpinBox::down-button, QSpinBox::down-button {background: " + Color::DARK.name() + "; outline: none;  border: none; border-image: none;}"
            "QDoubleSpinBox::up-button, QSpinBox::up-button {margin-top: -1px;}"
            "QDoubleSpinBox::down-button, QSpinBox::down-button {margin-bottom: -1px;}"
            "QDoubleSpinBox::up-button:disabled, QSpinBox::up-button:disabled, QDoubleSpinBox::up-button:off, QSpinBox::up-button:off, QDoubleSpinBox::down-button:disabled, QSpinBox::down-button:disabled, QDoubleSpinBox::down-button:off, QSpinBox::down-button:off {background: #20252A;}"
            "QDoubleSpinBox::up-button:pressed, QSpinBox::up-button:pressed, QDoubleSpinBox::down-button:pressed, QSpinBox::down-button:pressed {background: #22272A;}"
            "QDoubleSpinBox::up-arrow, QSpinBox::up-arrow {image: url(:/resources/dock/arrowup.png);}"
            "QDoubleSpinBox::down-arrow, QSpinBox::down-arrow {image: url(:/resources/dock/arrowdown.png);}"
            "QSpinBox:disabled {background: " + Color::DARK.name() + "; color:#33513F;}"
            "QCheckBox {color: " + Color::MAIN.name() + ";}"
            "QListView::item {color: " + Color::MAIN.name() + "; padding: 3px 0 3px 0;}"
            "QListView::item:active {color: " + Color::MAIN.name() + "; background: " + Color::BLACK.name() + ";}"
            "QListView::item:selected, QListView::item:selected:active, QListView::item:selected:alternate {background: " + Color::MAIN.name() + "; color: " + Color::LIGHT.name() + ";}"
            "QListView::item:alternate {background: #1D2225;}"
            "QListView::item:disabled, QListView::item:disabled:alternate {background: " + Color::DARK.name() + "; color: #33513F;}"
            "QTableView::item {color: " + Color::MAIN.name() + "; padding: 3px 0 3px 0;}"
            "QTableView::item:active {color: " + Color::MAIN.name() + "; background: " + Color::BLACK.name() + ";}"
            "QTableView::item:selected, QTableView::item:selected:active, QTableView::item:selected:alternate {background: " + Color::MAIN.name() + "; color: " + Color::LIGHT.name() + ";}"
            "QTableView::item:alternate {background: #1D2225;}"
            "QTableView::item:disabled, QtableView::item:disabled:alternate {background: " + Color::DARK.name() + "; color: #33513F;}"
            "QTableView#readonly::item:disabled {color: " + Color::MAIN.name() + "; background: " + Color::BLACK.name() + ";}"
            "QTableView#readonly::item:disabled:alternate {background: #1D2225;}"
            "QTableView::focus {border: none;}"
            "QTableView QHeaderView::section {background: " + Color::DARK.name() + "; padding-top: 3px; bottom: 3px; border: 1px solid " + Color::MAIN.name() + ";}"
            "QDialog QWidget {selection-background-color: " + Color::MAIN.name() + ";}"
            "KameDockWidget QPushButton, QDialog QPushButton {background-color: " + Color::DARK.name() + "; border: 1px solid " + Color::MAIN.name() + "; border-radius:3px; padding: 5px;}"
            "KameDockWidget QPushButton:hover, QDialog QPushButton:hover {background-color:#20252A;}"
            "KameDockWidget QPushButton:pressed, QDialog QPushButton:pressed {background-color: " + Color::MAIN.name() + "; color: " + Color::LIGHT.name() + ";}"
            "KameDockWidget QPushButton:disabled, QDialog QPushButton:disabled {border-color:#33513F; color:#33513F;}"
            "ConsolePad QPushButton {background: none; border: none; outline: none;}"
            "QDockWidget QScrollArea {background: " + Color::DARK.name() + ";}"
            "QWidget {color: " + Color::MAIN.name() + ";}"
            "QToolTip {padding-top: 3px; background: " + Color::MAIN.name() + ";}";

    a.setStyleSheet(styles);

    KameWidget k(NULL);
    k.show();

    a.setStyleSheet(styles);

    QObject::connect(&a, &QGuiApplication::applicationStateChanged, &k, &KameWidget::handleFocus);

    return k.isValid ? a.exec() : 1;
}
