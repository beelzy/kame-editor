// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QMouseEvent>
#include <QSettings>
#include <QPainterPath>

#include "consolejoystick.h"

ConsoleJoystick::ConsoleJoystick(QWidget * parent) : QAbstractButton(parent) {
    _skinid = "3ds";
    updateSkin();
    setCheckable(true);
    QSettings settings;
    bool checked = settings.contains("animations") ? settings.value("animations").toBool() : true;
    setChecked(checked);
    setCursor(Qt::PointingHandCursor);
}

void ConsoleJoystick::setSkinId(QString value) {
    _skinid = value;
    updateSkin();
}

void ConsoleJoystick::updateSkin() {
    _base.load(":/resources/skins/" + _skinid + "/joystick.png");
    QSize size = QSize(_base.width() + 8, _base.height());
    resize(size);
    setFixedSize(size);
}

void ConsoleJoystick::mouseMoveEvent(QMouseEvent *event) {
    event->accept();
}

void ConsoleJoystick::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    float offsetX = isChecked() ? 0 : 8;
    if (isDown()) {
        p.drawPixmap(offsetX, 0, _base.width(), height(), _base);
        p.translate(offsetX, 0);
    } else {
        p.drawPixmap(4, 0, _base.width(), height(), _base);
        p.translate(4, 0);
    }

    p.setPen(QColor("#40000000"));
    p.setBrush(QColor(Qt::transparent));
    if (isChecked()) {
        int offsetX = (_base.width() - 13) / 2;
        int offsetY = (_base.height() - 15) / 2;
        p.drawRect(offsetX, offsetY, 5, 15);
        p.drawRect(offsetX + 8, offsetY, 5, 15);
    } else {
        int offsetX = (_base.width() - 10) / 2;
        int offsetY = (_base.height() - 14) / 2;
        QPainterPath path;
        path.moveTo(offsetX, offsetY);
        path.lineTo(offsetX + 10, offsetY + 7);
        path.lineTo(offsetX, offsetY + 14);
        path.lineTo(offsetX, offsetY);
        p.drawPath(path);
    }
}
