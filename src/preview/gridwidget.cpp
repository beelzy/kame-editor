// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include <QPainter>
#include <QGridLayout>
#include <QMouseEvent>
#include <QGraphicsDropShadowEffect>
#include <QSettings>

#include "gridwidget.h"
#include "emptyicon.h"
#include "preview.h"
#include "../sourcefile.h"
#include "../color.h"
#include "../dock/icons/logo.h"

const QColor GridWidget::DEFAULT_MAIN = QColor("#DFDBD7");
const QColor GridWidget::DEFAULT_SHADOW = QColor("#51000000");

const QColor GridWidget::DEFAULT_FILE_DARK = QColor("#C0BEBA");
const QColor GridWidget::DEFAULT_FILE_MAIN = QColor("#EEEFF1");
const QColor GridWidget::DEFAULT_FILE_LIGHT = QColor("#FBFBFB");
const QColor GridWidget::DEFAULT_FILE_SHADOW = QColor("#55000000");

QPointF GridWidget::OFFSET[6] = {QPointF(27, 11), QPointF(27, 3), QPointF(22, 6), QPointF(17, 6), QPointF(14, 6), QPointF(18, 2.5)};
float GridWidget::FOLDER_OFFSET[6] = {38, 38, 20, 13, 7, 2};
int GridWidget::SPACING[6] = {14, 14, 6, 6, 4, 4};
int GridWidget::VISIBLE_ITEMS[6] = {3, 3, 5, 7, 9, 10};

QPointF GridWidget::ICON_SIZE[6] = {QPointF(70, 69), QPointF(70, 69), QPointF(48, 47), QPointF(34, 34), QPointF(28, 28), QPointF(24, 24)};
float GridWidget::EMPTY_ICON_SIZE = 1.0 / 3;
int GridWidget::INNER_ICON_SIZE[6] = {48, 48, 38, 28, 24, 20};
QPointF GridWidget::INNER_ICON_POS[6] = {QPointF(11, 10), QPointF(11, 10), QPointF(5, 5), QPointF(3, 3), QPointF(2, 2), QPointF(2, 2)};
// outer inner
QPointF GridWidget::ICON_RADIUS[6] = {QPointF(12, 4), QPointF(12, 4), QPointF(7, 3), QPointF(5, 2), QPointF(3, 2), QPointF(3, 2)};

GridWidget::GridWidget(QWidget *parent, bool isFolder) : QFrame(parent) {
    _isFolder = isFolder;
    _mouseallowed = true;
    _zoomlevel = _isFolder ? 1 : 0;
    _screenIndex = 0;
    _isTexture = false;
    _audioEnabled = true;

    QGridLayout *layout = new QGridLayout;
    layout->setContentsMargins(0, 0, 0, 0);

    GridIcon *gridicon = new GridIcon(this);
    GridIcon *gridicon2 = new GridIcon(this);
    GridIcon *gridicon3 = new GridIcon(this);

    if (!_isFolder) {
        gridicon->setGameText("Kame Editor");
        gridicon->setDemoText(tr("Try interacting with this preview. Use the mouse to click and drag on various elements.", "Demo text that appears in the top screen when the first item in the bottom screen is selected."));

        QPixmap logo = QPixmap(GridWidget::INNER_ICON_SIZE[0], GridWidget::INNER_ICON_SIZE[0]);
        logo.fill(QColor(Qt::transparent));

        QPainter p;

        p.begin(&logo);
        p.setRenderHints(QPainter::Antialiasing);
        QLinearGradient gradient = QLinearGradient(QPointF(0, 0), QPointF(0, logo.height()));
        gradient.setColorAt(0, Color::DARK);
        gradient.setColorAt(1, Color::DARK.lighter(180));
        p.fillRect(0, 0, logo.width(), logo.height(), QBrush(gradient));
        p.setBrush(Color::MAIN);
        QPen pen = QPen(Color::LIGHT);
        pen.setWidth(3);
        p.setPen(pen);
        p.translate(19, -1);
        p.rotate(30);
        Logo *logopainter = new Logo();
        p.drawPath(logopainter->base(32));
        pen.setWidth(1);
        p.setPen(pen);
        p.setBrush(QColor(Qt::transparent));
        p.drawPath(logopainter->lattice(32));
        p.end();

        gridicon->setIcon(logo);

        gridicon2->setGameText(tr("Turtle Game", "Title of the second item in the bottom screen"));
        gridicon2->setIcon(QPixmap(":/resources/preview/game-icon.png"));
        gridicon2->setDemoText(tr("Some buttons in this preview don't do anything, but you can still click on them to see their pressed states.", "Demo text that belongs to the Turtle Game item."));

        gridicon3->setGameText(tr("Warning", "Title of the third item in the bottom screen."));
        gridicon3->setDemoText(tr("This preview may not be 100% accurate. Sorry for any disappointment this may cause.", "Demo text that belongs to the Warning item."));

        QPixmap warning = QPixmap(GridWidget::INNER_ICON_SIZE[0], GridWidget::INNER_ICON_SIZE[0]);
        warning.fill(QColor(Qt::transparent));

        p.begin(&warning);
        p.setRenderHints(QPainter::Antialiasing);
        gradient = QLinearGradient(QPointF(0, 0), QPointF(0, warning.height()));
        gradient.setColorAt(0, Color::MAIN);
        gradient.setColorAt(1, Color::MAIN.lighter(150));
        p.fillRect(0, 0, warning.width(), warning.height(), QBrush(gradient));
        p.setBrush(QColor("#FFB541"));
        pen = QPen(Color::DARK);
        pen.setWidth(3);
        pen.setJoinStyle(Qt::RoundJoin);
        p.setPen(pen);
        QPainterPath triangle;
        triangle.moveTo(24, (48 - 16 * sqrt(3)) / 2.0);
        triangle.lineTo(48 - 8, 48 - (48 - 16 * sqrt(3)) / 2.0);
        triangle.lineTo(8, 48 - (48 - 16 * sqrt(3)) / 2.0);
        triangle.lineTo(24, (48 - 16 * sqrt(3)) / 2.0);
        p.drawPath(triangle);

        p.setBrush(Color::DARK);
        QFont font("Nimbus Sans");
        font.setPixelSize(19);
        font.setWeight(QFont::Bold);
        p.setFont(font);
        p.drawText(20.5, 34, "!");
        p.end();

        gridicon3->setIcon(warning);

        delete logopainter;
    }

    _items.append(gridicon);
    _items.append(gridicon2);
    _items.append(gridicon3);

    layout->addWidget(gridicon, 0, 0);
    layout->addWidget(gridicon2, 0, 1);
    layout->addWidget(gridicon3, 0, 2);

    int itemCount = 237;

    QSettings settings;
    bool showletter = settings.contains("folderletter") ? settings.value("folderletter").toBool() : true;

    if (!_isFolder) {
        FolderIcon *foldericon = new FolderIcon(this);
        foldericon->showLetter(showletter);

        FolderIcon *foldericon2 = new FolderIcon(this);
        foldericon2->showLetter(showletter);
        foldericon2->setIndex(2);
        foldericon2->setState(FolderIcon::State::OPEN);

        _items.append(foldericon);
        _items.append(foldericon2);

        layout->addWidget(foldericon, 0, 3);
        layout->addWidget(foldericon2, 0, 4);

        itemCount = 235;
    }

    for(int i = 0; i < itemCount; i++) {
        EmptyIcon *emptyicon = new EmptyIcon(this);
        _items.append(emptyicon);

        layout->addWidget(emptyicon, 0, i + ITEM_COUNT - itemCount);
    }

    _scrollpos = 0;

    setLayout(layout);

    updateColors(true);

    updateZoomLevel();

    _animation = new QPropertyAnimation(this, "pos");
    _animation->setDuration(200);
}

GridWidget::~GridWidget() {
    while (!_items.isEmpty()) {
        delete _items.takeFirst();
    }
    delete _animation;
}

void GridWidget::updateZoomLevel() {
    int rows = _isFolder ? _zoomlevel : _zoomlevel + 1;
    int cols = ITEM_COUNT / (rows);

    float progress = float(_scrollpos) / width();

    float newheight = _zoomlevel == 0 ? GridWidget::ICON_1X_HEIGHT : (rows) * (SPACING[_zoomlevel] + GridWidget::ICON_SIZE[_zoomlevel].y());
    float newwidth = (ITEM_COUNT / rows) * (SPACING[_zoomlevel] + GridWidget::ICON_SIZE[_zoomlevel].x()) + 2 * getOffset() + SPACING[_zoomlevel];

    int offsetY = _isFolder ? 8 : 33;
    int shadowHeight = _isFolder ? 12 : 6;
    int bgheight = _isFolder ? BG_FOLDER_HEIGHT : BG_HEIGHT;
    int marginY = _isFolder ? FOLDER_OFFSET[_zoomlevel] - SPACING[_zoomlevel] / 2.0 : OFFSET[_zoomlevel].y();
    setGeometry(_scrollpos, offsetY, newwidth, bgheight + shadowHeight * 2);

    layout()->setContentsMargins(getOffset(), marginY + shadowHeight, getOffset(), bgheight + shadowHeight - newheight - marginY);
    layout()->setSpacing(0);

    for (int i = 0; i < cols; i++) {
        for (int j = 0; j < rows; j++) {
           QWidget *item = _items[i * rows + j];
           int iconheight = _zoomlevel == 0 ? GridWidget::ICON_1X_HEIGHT : GridWidget::ICON_SIZE[_zoomlevel].y();
           item->setFixedSize(GridWidget::SPACING[_zoomlevel] + GridWidget::ICON_SIZE[_zoomlevel].x(), iconheight + GridWidget::SPACING[_zoomlevel]);
           static_cast<QGridLayout*>(layout())->addWidget(item, j, i);
        }
    }

    _scrollpos = snapPosX(progress * newwidth);

    _audioEnabled = false;
    move(_scrollpos, pos().y());
    _audioEnabled = true;
    displayArrows();

    emit zoomLevelChanged(_zoomlevel, layout()->contentsMargins());
    emit scrollPositionChanged(_scrollpos);
}

void GridWidget::zoomOut() {
    int minZoomLevel = _isFolder ? 1: 0;
    setZoomLevel(std::max(minZoomLevel, _zoomlevel - 1));
}

void GridWidget::zoomIn() {
    setZoomLevel(std::min(5, _zoomlevel + 1));
}

void GridWidget::setZoomLevel(int zoom) {
    _zoomlevel = zoom;

    updateZoomLevel();
}

int GridWidget::zoomLevel() {
    return _zoomlevel;
}

void GridWidget::setMainColor(QColor color) {
    _maincolor = color;
    update();
}

void GridWidget::setShadowColor(QColor color) {
    _shadowcolor = color;
    _shadowcolor.setAlpha(color.alpha() * 0x51 / 255.0);
    update();
}

void GridWidget::setIconLightColor(QColor color) {
    for (int i = 0; i < _items.size(); i++) {
        EmptyIcon *item = dynamic_cast<EmptyIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setLightColor(color);
        }
    }
    update();
}

void GridWidget::setIconDarkColor(QColor color) {
    for (int i = 0; i < _items.size(); i++) {
        EmptyIcon *item = dynamic_cast<EmptyIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setDarkColor(color);
        }
    }
    update();
}

void GridWidget::onFolderTextureChanged(FolderIcon::State state, QPixmap image) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        if (item != nullptr) {
            switch(state) {
                case FolderIcon::State::OPEN:
                    item->setFolderOpen(image);
                    break;
                case FolderIcon::State::CLOSE:
                    item->setFolderClose(image);
                    break;
            }
        }
    }
}

void GridWidget::onFileTextureChanged(GridIcon::Size size, QPixmap image) {
    for (int i = 0; i < _items.size(); i++) {
        GridIcon *item = dynamic_cast<GridIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setTexture(size, image);
        }
    }
}

void GridWidget::onFolderLetterChanged(bool enabled) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        if (item != nullptr) {
            item->showLetter(enabled);
        }
    }
}

void GridWidget::setFolderDarkColor(QColor color) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setFolderDarkColor(color);
        }
    }
}

void GridWidget::setFolderMainColor(QColor color) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setFolderMainColor(color);
        }
    }
}

void GridWidget::setFolderLightColor(QColor color) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setFolderLightColor(color);
        }
    }
}

void GridWidget::setFolderShadowColor(QColor color) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setFolderShadowColor(color);
        }
    }
}

void GridWidget::setFileDarkColor(QColor color) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        GridIcon *griditem = dynamic_cast<GridIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setFileDarkColor(color);
        } else if (griditem != nullptr) {
            griditem->setDarkColor(color);
        }
    }
}

void GridWidget::setFileMainColor(QColor color) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        GridIcon *griditem = dynamic_cast<GridIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setFileMainColor(color);
        } else if (griditem != nullptr) {
            griditem->setMainColor(color);
        }
    }
}

void GridWidget::setFileLightColor(QColor color) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        GridIcon *griditem = dynamic_cast<GridIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setFileLightColor(color);
        } else if (griditem != nullptr) {
            griditem->setLightColor(color);
        }
    }
}

void GridWidget::setFileShadowColor(QColor color) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        GridIcon *griditem = dynamic_cast<GridIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setFileShadowColor(color);
        } else if (griditem != nullptr) {
            griditem->setShadowColor(color);
        }
    }
}

void GridWidget::updateFolderColors(bool defaultValue) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        if (item != nullptr) {
            item->updateFolderColors(defaultValue);
        }
    }
}

void GridWidget::updateFileColors(bool defaultValue) {
    for (int i = 0; i < _items.size(); i++) {
        FolderIcon *item = dynamic_cast<FolderIcon*>(_items.at(i));
        GridIcon *griditem = dynamic_cast<GridIcon*>(_items.at(i));
        if (item != nullptr) {
            item->updateFileColors(defaultValue);
        } else if (griditem != nullptr) {
            griditem->updateColors(defaultValue);
        }
    }
}

void GridWidget::updateColors(bool defaultValue) {
    if (defaultValue) {
        _maincolor = DEFAULT_MAIN;
        _shadowcolor = DEFAULT_SHADOW;
        setIconDarkColor(EmptyIcon::DEFAULT_DARK_COLOR);
        setIconLightColor(EmptyIcon::DEFAULT_LIGHT_COLOR);
    } else {
        _maincolor = SourceFile::folderviewcolors["main"];
        QColor shadowcolor = SourceFile::folderviewcolors["shadow"];
        shadowcolor.setAlpha(shadowcolor.alpha() * 0x51 / 255.0);
        _shadowcolor = shadowcolor;
        setIconDarkColor(SourceFile::folderviewcolors["dark"]);
        setIconLightColor(SourceFile::folderviewcolors["light"]);
    }
    update();
}

int GridWidget::screenIndex() {
    return _screenIndex;
}

void GridWidget::setScreenIndex(int value) {
    _screenIndex = value;

    updateIconSelection();
}

void GridWidget::updateIconSelection() {
    int gridunit = ICON_SIZE[_zoomlevel].x() + SPACING[_zoomlevel];
    int offset = -_scrollpos / gridunit;
    int rows = _isFolder ? _zoomlevel : _zoomlevel + 1;

    emit iconChanged(static_cast<QWidget*>(children()[offset * rows + _screenIndex]));
}

bool GridWidget::isFolder() {
    return _isFolder;
}

void GridWidget::setTextureMode(bool value) {
    _isTexture = value;

    for (int i = 0; i < _items.size(); i++) {
        EmptyIcon *item = dynamic_cast<EmptyIcon*>(_items.at(i));
        if (item != nullptr) {
            item->setTextureMode(value);
        }
    }
}

float GridWidget::getOffset() {
    return OFFSET[_zoomlevel].x() + SPACING[_zoomlevel] / 2.0;
}

float GridWidget::getMaxScroll() {
    int rows = _isFolder ? _zoomlevel : _zoomlevel + 1;
    return -(ITEM_COUNT / rows - VISIBLE_ITEMS[_zoomlevel]) * (SPACING[_zoomlevel] + GridWidget::ICON_SIZE[_zoomlevel].x());
}

void GridWidget::displayArrows() {
    if (_scrollpos >= 0) {
        emit updateArrows(false, true);
    } else if (_scrollpos <= getMaxScroll()) {
        emit updateArrows(true, false);
    } else {
        emit updateArrows(true, true);
    }
    emit updateOpenClose(true);
}

void GridWidget::paintEvent(QPaintEvent *) {
    if (!_isTexture) {
        QPainter p(this);
        p.setRenderHints(QPainter::Antialiasing);

        int posX = _isFolder ? 12 : 6;
        int bgheight = _isFolder ? BG_FOLDER_HEIGHT : BG_HEIGHT;
        QRectF rect = QRectF(11, posX, width() - 22 - SPACING[_zoomlevel], bgheight);

        float blurradius = _isFolder ? 12.0f : 4.0f;
        float radius = _isFolder ? 8.0f : 10.0f;
        Preview::drawRoundedRectGlow(p, _shadowcolor, rect, radius, blurradius);

        QPainterPath path;
        path.addRoundedRect(rect, radius, radius);

        p.setPen(_maincolor.lighter(110));
        p.setBrush(_maincolor);
        p.drawPath(path);
    }
}

int GridWidget::snapPosX(int x, bool flatten) {
    int zoomlevel = _zoomlevel;
    if (flatten) {
        zoomlevel = 0;
    }
    int posx = round(x / (SPACING[zoomlevel] + GridWidget::ICON_SIZE[zoomlevel].x())) * (SPACING[zoomlevel] + GridWidget::ICON_SIZE[zoomlevel].x());
    return std::min(0.0f, std::max(float(posx), getMaxScroll()));
}

void GridWidget::scroll(int direction) {
    if (_animation->state() != QAbstractAnimation::Running) {
        _animation->setStartValue(QPointF(_scrollpos, pos().y()));
        _scrollpos = std::min(0.0f, std::max(float(_scrollpos + direction * VISIBLE_ITEMS[_zoomlevel] * (SPACING[_zoomlevel] + GridWidget::ICON_SIZE[_zoomlevel].x())), getMaxScroll()));

        _animation->setEndValue(QPointF(_scrollpos, pos().y()));
        _animation->start();

        updateIconSelection();
        displayArrows();        

        emit pageScroll(_animation->startValue().value<QPointF>(), _animation->endValue().value<QPointF>());
    }
}

void GridWidget::scrollLeft() {
    scroll(1);
}

void GridWidget::scrollRight() {
    scroll(-1);
}

void GridWidget::moveEvent(QMoveEvent *event) {
    if (_audioEnabled) {
        int snap = snapPosX(pos().x(), true);
        int oldX = event->oldPos().x();
        int newX = event->pos().x();
        int gridspace = SPACING[0] + GridWidget::ICON_SIZE[0].x();
        if ((newX >= snap && oldX < snap) || (newX <= snap && oldX > snap)) {
            int frame = abs(snap / gridspace) % 3;
            if (newX < oldX) {
                frame = (frame + 2) % 3;
            }
            switch(frame) {
                case 0:
                    emit playFrame0();
                    break;
                case 1:
                    emit playFrame1();
                    break;
                case 2:
                    emit playFrame2();
                    break;
            }
        }
    }
}

void GridWidget::mousePressEvent(QMouseEvent *event) {
    if (_animation->state() != QAbstractAnimation::Running) {
        _mousestart = event->pos().x();
        _globalmousex = event->globalPos().x();
        emit updateArrows(false, false);
        emit updateOpenClose(false);
        emit playCursor();
    } else {
        _mouseallowed = false;
    }
}

void GridWidget::mouseMoveEvent(QMouseEvent *event) {
    if (_mouseallowed) {
        float posX = std::min(0.0f, std::max(float(pos().x() + event->pos().x() - _mousestart), getMaxScroll()));
        move(posX, pos().y());

        emit scrollPositionChanged(posX);
    }
}

void GridWidget::mouseReleaseEvent(QMouseEvent *event) {
    if (_mouseallowed) {
        int posx = pos().x() + event->pos().x() - _mousestart;
        _scrollpos = snapPosX(posx);
        move(_scrollpos, pos().y());
        displayArrows();

        emit scrollPositionChanged(_scrollpos);

        if (_globalmousex == event->globalPos().x() && childAt(event->pos()) != nullptr) {
            QWidget *icon = childAt(event->pos());

            QPointF pos = icon->pos();
            bool inRange = pos.x() >= std::abs(_scrollpos) && pos.x() <= abs(_scrollpos) + GridWidget::VISIBLE_ITEMS[_zoomlevel] * (GridWidget::ICON_SIZE[_zoomlevel].x() + GridWidget::SPACING[_zoomlevel]);

            if (inRange) {
                int rows = _isFolder ? _zoomlevel : _zoomlevel + 1;
                int offset = -_scrollpos / (ICON_SIZE[_zoomlevel].x() + SPACING[_zoomlevel]);
                _screenIndex = std::max(0, layout()->indexOf(icon) - offset * rows);

                emit iconClicked(pos, _scrollpos);
                emit iconChanged(icon);
            }
        } else {
            updateIconSelection();
        }
    }
    _mouseallowed = true;
}
