// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GRIDICON_H
#define GRIDICON_H

#include <QWidget>
#include <QMouseEvent>

class GridIcon : public QWidget {
    public:
        explicit GridIcon(QWidget *parent = 0);

        QString gametext();
        void setGameText(QString value);

        QString demotext();
        void setDemoText(QString value);

        enum class Size {
            LARGE,
            SMALL
        };

        void setTexture(GridIcon::Size size, QPixmap pixmap);
        void setIcon(QPixmap pixmap);

        void setDarkColor(QColor value);
        void setMainColor(QColor value);
        void setLightColor(QColor value);
        void setShadowColor(QColor value);
        void updateColors(bool defaultValue);

    protected:
        void paintEvent(QPaintEvent *event);
        virtual void mousePressEvent(QMouseEvent *event);

    private:
        QString _gametext;
        QString _demotext;
        QPixmap _largetexture;
        QPixmap _smalltexture;
        QPixmap _icontexture;

        QColor _dark;
        QColor _main;
        QColor _light;
        QColor _shadow;
};

#endif // GRIDICON_H
