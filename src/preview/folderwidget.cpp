// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>

#include "folderwidget.h"

FolderWidget::FolderWidget(QWidget *parent) : QFrame(parent) {
    _grid = new GridWidget(this, true);
    _backbutton = new FolderBackButton(this);

    _backbutton->setGeometry(25, 0, 68, 20);

    connect(_backbutton, &QAbstractButton::clicked, this, &FolderWidget::onBackPressed);
}

FolderWidget::~FolderWidget() {
    delete _grid;
    delete _backbutton;
}

GridWidget *FolderWidget::grid() {
    return _grid;
}

FolderBackButton *FolderWidget::backbutton() {
    return _backbutton;
}

void FolderWidget::onBackPressed() {
    hide();
    emit closeFolder();
}

void FolderWidget::paintEvent(QPaintEvent *) {

}
