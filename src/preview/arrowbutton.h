// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ARROWBUTTON_H
#define ARROWBUTTON_H

#include <QAbstractButton>

class ArrowButton : public QAbstractButton {
    Q_OBJECT

    public:
        explicit ArrowButton(QWidget *parent = 0);

        static const QColor DEFAULT_DARK_COLOR;
        static const QColor DEFAULT_MAIN_COLOR;
        static const QColor DEFAULT_LIGHT_COLOR;

        static const QColor DEFAULT_BORDER_COLOR;
        static const QColor DEFAULT_UNPRESSED_COLOR;
        static const QColor DEFAULT_PRESSED_COLOR;

        enum class Direction {
            LEFT,
            RIGHT
        };

        QRect boundingRect();

        ArrowButton::Direction direction();
        void setDirection(ArrowButton::Direction direction);

    signals:
        void pressedTimer();

    public slots:
        void updateVisibility(bool showLeft, bool showRight);
        void setDarkColor(QColor value);
        void setMainColor(QColor value);
        void setLightColor(QColor value);
        void setBorderColor(QColor value);
        void setUnpressedColor(QColor value);
        void setPressedColor(QColor value);
        void updateBaseColors(bool defaultValue);
        void updateArrowColors(bool defaultValue);

    protected:
        void paintEvent(QPaintEvent *event);
        void timerEvent(QTimerEvent *event);

    private:
        ArrowButton::Direction _direction;
        int _timerid;
        void startDragTimer();
        void stopDragTimer();

        QColor _dark;
        QColor _main;
        QColor _light;

        QColor _border;
        QColor _unpressed;
        QColor _pressed;
};

#endif // ARROWBUTTON_H
