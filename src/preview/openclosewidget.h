// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef OPENCLOSEWIDGET_H
#define OPENCLOSEWIDGET_H

#include <QFrame>

#include "openclosebutton.h"

class OpenCloseWidget : public QFrame {
    Q_OBJECT

    public:
        explicit OpenCloseWidget(QWidget *parent = 0);
        ~OpenCloseWidget();

        void showFolder();
        void showGrid();
        void reset();

    signals:
        void openFolder();
        void playFolder();
        void playLaunch();
        void playClose();

    public slots:
        void onCursorChange(QWidget *icon);
        void flexButtonClicked();
        void closeButtonClicked();
        void setDarkColor(QColor value);
        void setMainColor(QColor value);
        void setLightColor(QColor value);
        void setTextShadowColor(QColor value);
        void setTextMainColor(QColor value);
        void setTextSelectedColor(QColor value);
        void setTextShadowPos(float value);
        void setCloseDarkColor(QColor value);
        void setCloseMainColor(QColor value);
        void setCloseLightColor(QColor value);
        void setCloseTextShadowColor(QColor value);
        void setCloseTextMainColor(QColor value);
        void setCloseTextSelectedColor(QColor value);
        void setCloseTextShadowPos(float value);
        void updateColors(bool defaultValue);

    private:
        OpenCloseButton *_flexbutton;
        OpenCloseButton *_leftbutton;
        OpenCloseButton *_closebutton;
        bool _isFolder;
        bool _isGrid;
};

#endif // OPENCLOSEWIDGET_H
