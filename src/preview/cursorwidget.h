// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CURSORWIDGET_H
#define CURSORWIDGET_H

#include <QAbstractButton>
#include <QParallelAnimationGroup>

class CursorWidget : public QAbstractButton {
    Q_OBJECT
    Q_PROPERTY(float scale READ scale WRITE setScale)
    Q_PROPERTY(QColor color READ color WRITE setColor)
    Q_PROPERTY(QColor outline READ outline WRITE setOutline)

    public:
        explicit CursorWidget(QWidget *parent = 0);
        ~CursorWidget();

        static const QColor DEFAULT_COLOR;
        static const QColor DEFAULT_GLOW;
        static const QColor DEFAULT_OUTLINE;
        static const QColor DEFAULT_LIGHT;

        float scale();
        void setScale(float value);
        QColor color();
        void setColor(QColor value);
        QColor outline();
        void setOutline(QColor value);
        void setOrigin(QPointF origin);
        QPointF origin();
        void setZoom(int zoom);

    public slots:
        void setGlowColor(QColor value);
        void setMainColor(QColor value);
        void setDarkColor(QColor value);
        void setLightColor(QColor value);
        void updateColors(bool defaultvalue);

        void setAnimated(bool value);

    protected:
        void paintEvent(QPaintEvent *event);
        void updateAnimation();
        void checkUpdate();

    private:
        void updateAnimationPosition();
        QParallelAnimationGroup *_animation;
        float _scale;
        int _zoom;
        QPointF _origin;
        QColor _color;
        QColor _outline;
        QColor _glow;
        QColor _main;
        QColor _light;
        QColor _dark;
        static float MAX_SCALE;
        static int DURATION;
        bool _animated;
};

#endif // CURSORWIDGET_H
