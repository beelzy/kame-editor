// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GAMETEXTWIDGET_H
#define GAMETEXTWIDGET_H

#include <QFrame>

class GameTextWidget : public QFrame {
    Q_OBJECT

    public:
        explicit GameTextWidget(QWidget *parent = 0);

        static const QColor DEFAULT_MAIN_COLOR;
        static const QColor DEFAULT_TEXTMAIN_COLOR;

        enum class Position {
            LEFT,
            MIDDLE,
            RIGHT
        };

        QString text();
        void setText(QString value);

        Position position();
        void setPosition(Position value);

    public slots:
        void onIconChanged(QWidget *icon);
        void onZoomLevelChanged(int zoom, QMargins margins);
        void setMainColor(QColor value);
        void setTextMainColor(QColor value);
        void updateColors(int state);

    protected:
        void paintEvent(QPaintEvent *event);

    private:
        QString _text;
        Position _position;
        bool _showGameText;
        bool _displayDisabled;

        QColor _main;
        QColor _textmain;
};

#endif // GAMETEXTWIDGET_H
