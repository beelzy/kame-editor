// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include <QGraphicsPixmapItem>
#include <QGraphicsDropShadowEffect>
#include <QDebug>

#include "bottomview.h"
#include "gridwidget.h"
#include "preview.h"
#include "../sourcefile.h"

class Texture : public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
    Q_PROPERTY(QPointF pos READ pos WRITE setPos)
public:
    Texture(const QPixmap &pix) : QObject(), QGraphicsPixmapItem(pix) {
        setCacheMode(DeviceCoordinateCache);
    }
};

int BottomView::PAGE_SIZE[6] = {1, 1, 2, 3, 3, 3};
const QColor BottomView::DEFAULT_MAIN = QColor("#DFDFE7");
const QColor BottomView::DEFAULT_DARK = QColor("#D0D0D9");

BottomView::BottomView(QWidget *parent) : QGraphicsView(parent) {
    _scrollpos = 0;
    _zoomlevel = 0;
    _maincolor = DEFAULT_MAIN;
    _darkcolor = DEFAULT_DARK;
    _scene = new QGraphicsScene(parent);
    _scene->setSceneRect(0, 0, Preview::BOTTOM_SCREEN_WIDTH, Preview::BOTTOM_SCREEN_HEIGHT);

    setScene(_scene);

    setStyleSheet("border: 0");

    // set plain color
    setBackgroundBrush(QBrush(_maincolor, Qt::SolidPattern));

    QPixmap iconimage(":/resources/preview/bottom-icons.png");
    Texture *icontexture = new Texture(iconimage);

    _scene->addItem(icontexture);
    icontexture->setPos(62, 0);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setEnabled(false);

    _animation = new QPropertyAnimation(this, "scrollpos");
    _animation->setDuration(200);
}

BottomView::DrawType BottomView::drawtype() {
    return static_cast<DrawType>(SourceFile::bottomdraw);
}

void BottomView::updateDrawType() {
    DrawType drawtype = static_cast<DrawType>(SourceFile::bottomdraw);
    if (drawtype == DrawType::NONE) {
        updateMainColor();
        updateDarkColor();
    }
    if (drawtype == DrawType::TEXTURE) {
        setBackgroundBrush(QBrush(QColor(Qt::black), Qt::SolidPattern));
    }
    update();
}

void BottomView::updateFrameType() {
    onScrollPositionChange(_scrollpos);
}

void BottomView::updateMainColor() {
    _maincolor = static_cast<DrawType>(SourceFile::bottomdraw) == DrawType::NONE ? DEFAULT_MAIN : SourceFile::bottomoutercolors["main"];
    _maincolor.setAlpha(255);
    setBackgroundBrush(QBrush(_maincolor, Qt::SolidPattern));
}

void BottomView::setMainColor(QColor color) {
    SourceFile::bottomoutercolors["main"] = color;
    updateMainColor();
}

void BottomView::updateDarkColor() {
    _darkcolor = static_cast<DrawType>(SourceFile::bottomdraw) == DrawType::NONE ? DEFAULT_DARK : SourceFile::bottomoutercolors["dark"];
    update();
}

void BottomView::setDarkColor(QColor color) {
    SourceFile::bottomoutercolors["dark"] = color;
    updateDarkColor();
}

int BottomView::scrollpos() {
    return _scrollpos;
}

void BottomView::setScrollPos(int value) {
    _scrollpos = value;
    _scene->update();
}

int BottomView::calculatePageScroll() {
    int gridsize = PAGE_SIZE[_zoomlevel] * (GridWidget::ICON_SIZE[_zoomlevel].x() + GridWidget::SPACING[_zoomlevel]);
     int index = int(floor(float(_scrollpos + gridsize / 2.0) / gridsize));
     FrameType frametype = static_cast<FrameType>(SourceFile::bottomframe);
     if (frametype == FrameType::PAGESCROLL) {
         return (index % 3) * Preview::BOTTOM_SCREEN_WIDTH;
     }

     if (frametype == FrameType::BOUNCESCROLL) {
         int bounceIndex = index % 4;
         bounceIndex = bounceIndex < -2 ? -1 : bounceIndex;
         return bounceIndex * Preview::BOTTOM_SCREEN_WIDTH;
     }

     return _scrollpos;
}

void BottomView::onScrollPositionChange(int pos) {
    if (static_cast<DrawType>(SourceFile::bottomdraw) == DrawType::TEXTURE) {
        _scrollpos = static_cast<FrameType>(SourceFile::bottomframe) == FrameType::SLOWSCROLL ? pos * Preview::SLOWSCROLL_DAMPING : pos;
        _scene->update();
    }
}

void BottomView::onPageScroll(QPointF start, QPointF end) {
    FrameType frametype = static_cast<FrameType>(SourceFile::bottomframe);
    int startPos = frametype == FrameType::SLOWSCROLL ? int(start.x()) * Preview::SLOWSCROLL_DAMPING : int(start.x());
    int endPos = frametype == FrameType::SLOWSCROLL ? int(end.x()) * Preview::SLOWSCROLL_DAMPING : int(end.x());

    _animation->setStartValue(startPos);
    _animation->setEndValue(endPos);

    _animation->start();
}

void BottomView::onZoomLevelChanged(int zoom, QMargins) {
    _zoomlevel = zoom;
}

void BottomView::setTexture(QPixmap image) {
    _texture = image.copy(0, 0, 1008, 256);
}

void BottomView::drawBackground(QPainter *p, const QRectF &rect) {
    if (static_cast<DrawType>(SourceFile::bottomdraw) == DrawType::TEXTURE) {
        QGraphicsView::drawBackground(p, rect);
        int posX = _scrollpos % 1008;
        FrameType frametype = static_cast<FrameType>(SourceFile::bottomframe);
        if (frametype == FrameType::PAGESCROLL || frametype == FrameType::BOUNCESCROLL)
            posX = calculatePageScroll();
        if (frametype == FrameType::SINGLE) {
            posX = 0;
        }
        p->drawTiledPixmap(posX, 0, 1008 * 2, 240, _texture);
    } else {
        QGraphicsView::drawBackground(p, rect);
        QColor color = _darkcolor;
        float step = 40;
        p->fillRect(0, 237, Preview::BOTTOM_SCREEN_WIDTH, 1, color);
        for (int i = 0; i < 6; i++) {
            color.setAlpha(255 - i * step);
            p->fillRect(0, 237 - i * 4, Preview::BOTTOM_SCREEN_WIDTH, 1, color);
        }
    }
}

BottomView::~BottomView() {
    delete _scene;
    delete _animation;
}

#include "bottomview.moc"
