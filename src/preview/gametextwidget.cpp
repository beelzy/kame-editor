// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QGraphicsDropShadowEffect>
#include <QDebug>

#include "gametextwidget.h"
#include "gridwidget.h"
#include "gridicon.h"
#include "foldericon.h"
#include "preview.h"
#include "../sourcefile.h"

const QColor GameTextWidget::DEFAULT_MAIN_COLOR = QColor("#F8F5F1");
const QColor GameTextWidget::DEFAULT_TEXTMAIN_COLOR = QColor("#5A5653");

GameTextWidget::GameTextWidget(QWidget *parent) : QFrame(parent) {
    setAttribute(Qt::WA_TransparentForMouseEvents);
    _showGameText = true;
    _text = "TEST";
    _position = Position::LEFT;
    _displayDisabled = false;
    updateColors(true);
}

QString GameTextWidget::text() {
    return _text;
}

void GameTextWidget::setText(QString value) {
    _text = value;
}

GameTextWidget::Position GameTextWidget::position() {
    return _position;
}

void GameTextWidget::setPosition(Position value) {
    _position = value;
}

void GameTextWidget::onZoomLevelChanged(int zoom, QMargins) {
    _showGameText = zoom == 0;
    if (_showGameText && !_displayDisabled && !_text.isEmpty()) {
        update();
        show();
    } else {
        hide();
    }
}

void GameTextWidget::onIconChanged(QWidget *icon) {
    GridWidget *grid = static_cast<GridWidget*>(QObject::sender());

    bool isGridIcon = dynamic_cast<GridIcon*>(icon) != nullptr;
    bool isFolderIcon = dynamic_cast<FolderIcon*>(icon) != nullptr;

    if (isGridIcon || isFolderIcon) {

        switch(grid->screenIndex()) {
            case 0:
                _position = Position::LEFT;
                break;
            case 1:
                _position = Position::MIDDLE;
                break;
            case 2:
                _position = Position::RIGHT;
                break;
            default:
                break;
        }

        if (isGridIcon) {
            _text = dynamic_cast<GridIcon*>(icon)->gametext();
        } else {
            _text = QString::number(dynamic_cast<FolderIcon*>(icon)->index()) + " " + tr("(New)", "Appears in the game text tooltip when the grid zoom level is at its highest and a folder is selected.");
        }
        update();
        if (_showGameText && !_displayDisabled) {
            show();
        }
    } else {
        _text = "";
        hide();
    } 
}

void GameTextWidget::setMainColor(QColor value) {
    _main = value;
    update();
}

void GameTextWidget::setTextMainColor(QColor value) {
    _textmain = value;
    update();
}

void GameTextWidget::updateColors(int state) {
    if (state == Qt::PartiallyChecked) {
        _main = DEFAULT_MAIN_COLOR;
        _textmain = DEFAULT_TEXTMAIN_COLOR;
    } else {
        _main = SourceFile::gametextcolors["main"];
        _textmain = SourceFile::gametextcolors["textmain"];
    }
    _displayDisabled = state == Qt::Unchecked;
    setVisible(!_displayDisabled && _showGameText);
    update();
}

void GameTextWidget::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    QPainterPath path;

    int offsetX = 24;
    int tailX = 75;

    switch(_position) {
        case Position::LEFT:
            break;
        case Position::MIDDLE:
            offsetX = 32;
            tailX += GridWidget::ICON_SIZE[0].x() + GridWidget::SPACING[0];
            break;
        case Position::RIGHT:
            offsetX = 40;
            tailX += 2 * (GridWidget::ICON_SIZE[0].x() + GridWidget::SPACING[0]);
            break;
    }

    QColor shadow1 = QColor("#20000000");
    QColor shadow2 = QColor("#00000000");

    Preview::drawRoundedRectShadow(p, shadow1, QRect(offsetX, 0, 264, 70), QPointF(14, 14), 4);

    QLinearGradient gradient = QLinearGradient(QPointF(tailX, 0), QPointF(tailX + 10, 0));
    gradient.setColorAt(0, shadow2);
    gradient.setColorAt(2.0/5, shadow1);
    gradient.setColorAt(3.0/5, shadow1);
    gradient.setColorAt(1, shadow2);
    p.fillRect(tailX, 65, 10, 12, QBrush(gradient));

    path.addEllipse(tailX - 3, 73, 16, 16);

    QRadialGradient radial = QRadialGradient(QPointF(tailX + 5, 81), 8, QPointF(tailX + 5, 81));

    radial.setColorAt(0, shadow1);
    radial.setColorAt(0.5, shadow1);
    radial.setColorAt(1, shadow2);

    p.setPen(QColor(Qt::transparent));
    p.setBrush(QBrush(radial));
    p.drawPath(path);

    path = QPainterPath();

    path.addRoundedRect(offsetX + 4, 3, 256, 62, 10, 10);
    path.addRect(tailX + 4, 65, 2, 12);
    path.addEllipse(tailX + 1, 77, 8, 8);

    p.setBrush(_main);
    p.drawPath(path);

    QFont font = QFont("Nimbus Sans");
    font.setPixelSize(15);

    p.setFont(font);

    p.setPen(_textmain);
    p.drawText(QRect(offsetX + 4, 3, 256, 62), Qt::AlignCenter, _text);
}
