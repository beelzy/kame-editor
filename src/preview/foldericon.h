// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FOLDERICON_H
#define FOLDERICON_H

#include <QWidget>
#include <QMouseEvent>

class FolderIcon : public QWidget {
    public:
        explicit FolderIcon(QWidget *parent = 0);

        static int FOLDER_SIZE[6];
        static int FOLDER_OFFSET[6];
        static int FOLDER_FONTSIZE[6];

        static const QColor DEFAULT_FOLDER_DARK;
        static const QColor DEFAULT_FOLDER_MAIN;
        static const QColor DEFAULT_FOLDER_LIGHT;
        static const QColor DEFAULT_FOLDER_SHADOW;

        enum class State {
            CLOSE,
            OPEN
        };

        State state();
        void setState(State value);

        int index();
        void setIndex(int value);

        void setFolderOpen(QPixmap image);
        void setFolderClose(QPixmap image);
        void showLetter(bool enabled);

    public slots:
        void setFolderDarkColor(QColor value);
        void setFolderMainColor(QColor value);
        void setFolderLightColor(QColor value);
        void setFolderShadowColor(QColor value);
        void updateFolderColors(bool defaultValue);

        void setFileDarkColor(QColor value);
        void setFileMainColor(QColor value);
        void setFileLightColor(QColor value);
        void setFileShadowColor(QColor value);
        void updateFileColors(bool defaultValue);

    protected:
        void paintEvent(QPaintEvent *event);
        virtual void mousePressEvent(QMouseEvent *event);

    private:
        int _index;
        QPixmap _closetexture;
        QPixmap _opentexture;
        State _state;
        bool _showletter;

        QColor _folderdark;
        QColor _foldermain;
        QColor _folderlight;
        QColor _foldershadow;

        QColor _filedark;
        QColor _filemain;
        QColor _filelight;
        QColor _fileshadow;
};

#endif // FOLDERICON_H
