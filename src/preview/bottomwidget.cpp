// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>

#include "bottomwidget.h"
#include "preview.h"

BottomWidget::BottomWidget(QWidget *parent) : QFrame(parent) {
    _fakebutton = new FakeButton(this);
    _zoomwidget = new ZoomWidget(this);

    _fakebutton->setGeometry(0, 0, 60, 45);
    _zoomwidget->setGeometry(Preview::BOTTOM_SCREEN_WIDTH - 52, 0, 61, 45);

    _gridwidget = new GridWidget(this);

    _gametext = new GameTextWidget(this);

    _gametext->setGeometry(-4, 46, Preview::BOTTOM_SCREEN_WIDTH, 90);

    _folderwidget = new FolderWidget(this);

    _folderwidget->setGeometry(0, 45, Preview::BOTTOM_SCREEN_WIDTH, 174);
    _folderwidget->hide();

    _openclosewidget = new OpenCloseWidget(this);

    _openclosewidget->setGeometry(0, 214, Preview::BOTTOM_SCREEN_WIDTH, 26);

    _gridcursor = new CursorOverlay(this);

    _gridcursor->setGeometry(0, 33, Preview::BOTTOM_SCREEN_WIDTH, GridWidget::BG_HEIGHT + 12);

    _leftarrow = new ArrowButton(this);
    _leftarrow->setGeometry(0, (Preview::BOTTOM_SCREEN_HEIGHT - 70.0) / 2 + 4, _leftarrow->boundingRect().width(), _leftarrow->boundingRect().height());
    _leftarrow->hide();

    _rightarrow = new ArrowButton(this);
    _rightarrow->setDirection(ArrowButton::Direction::RIGHT);
    _rightarrow->setGeometry(Preview::BOTTOM_SCREEN_WIDTH - _rightarrow->boundingRect().width() / 4, (Preview::BOTTOM_SCREEN_HEIGHT - 70.0) / 2 + 4, _rightarrow->boundingRect().width(), _rightarrow->boundingRect().height());

    _zoomwidget->updateZoomLevel(_gridwidget->zoomLevel());

    connectGrid();

    connect(_gridwidget, &GridWidget::iconChanged, _gametext, &GameTextWidget::onIconChanged);
    connect(_gridwidget, &GridWidget::zoomLevelChanged, _gametext, &GameTextWidget::onZoomLevelChanged);

    connect(_openclosewidget, &OpenCloseWidget::openFolder, this, &BottomWidget::onFolderOpen);
    connect(_folderwidget, &FolderWidget::closeFolder, this, &BottomWidget::onFolderClose);
}

BottomWidget::~BottomWidget() {
    delete _fakebutton;
    delete _zoomwidget;
    delete _openclosewidget;
    delete _leftarrow;
    delete _rightarrow;
    delete _gridwidget;
    delete _folderwidget;
    delete _gridcursor;
    delete _gametext;
}

CursorWidget *BottomWidget::cursor() {
    return _gridcursor->cursor();
}

void BottomWidget::onFolderOpen() {
    _folderwidget->show();
    _zoomwidget->setMinZoomLevel(1);
    _gridwidget->setEnabled(false);
    _gridcursor->setFolderMode(true);
    _gridcursor->resetCursorPosition();
    connectFolder();
    disconnectGrid();

    _folderwidget->grid()->updateZoomLevel();
    emit folderStateChanged(true);
}

void BottomWidget::onFolderClose() {
    _zoomwidget->setMinZoomLevel(0);
    _gridwidget->setEnabled(true);
    _gridcursor->setFolderMode(false);
    disconnectFolder();
    connectGrid();

    _gridcursor->restoreCursorPosition();
    _gridwidget->updateZoomLevel();
    emit folderStateChanged(false);
}

void BottomWidget::setArrowDarkColor(QColor value) {
    _leftarrow->setDarkColor(value);
    _rightarrow->setDarkColor(value);
}

void BottomWidget::setArrowMainColor(QColor value) {
    _leftarrow->setMainColor(value);
    _rightarrow->setMainColor(value);
}

void BottomWidget::setArrowLightColor(QColor value) {
    _leftarrow->setLightColor(value);
    _rightarrow->setLightColor(value);
}

void BottomWidget::setArrowBorderColor(QColor value) {
    _leftarrow->setBorderColor(value);
    _rightarrow->setBorderColor(value);
}

void BottomWidget::setArrowUnpressedColor(QColor value) {
    _leftarrow->setUnpressedColor(value);
    _rightarrow->setUnpressedColor(value);
}

void BottomWidget::setArrowPressedColor(QColor value) {
    _leftarrow->setPressedColor(value);
    _rightarrow->setPressedColor(value);
}

void BottomWidget::updateArrowBaseColors(bool defaultValue) {
    _leftarrow->updateBaseColors(defaultValue);
    _rightarrow->updateBaseColors(defaultValue);
}

void BottomWidget::updateArrowColors(bool defaultValue) {
    _leftarrow->updateArrowColors(defaultValue);
    _rightarrow->updateArrowColors(defaultValue);
}

void BottomWidget::setFakeButtonDarkColor(QColor value) {
    _zoomwidget->setDarkColor(value);
}

void BottomWidget::setFakeButtonMainColor(QColor value) {
    _zoomwidget->setMainColor(value);
    _fakebutton->setMainColor(value);
}

void BottomWidget::setFakeButtonLightColor(QColor value) {
    _zoomwidget->setLightColor(value);
    _fakebutton->setLightColor(value);
}

void BottomWidget::setFakeButtonIconMainColor(QColor value) {
    _zoomwidget->setIconMainColor(value);
    _fakebutton->setIconMainColor(value);
}

void BottomWidget::setFakeButtonIconLightColor(QColor value) {
    _zoomwidget->setIconLightColor(value);
    _fakebutton->setIconLightColor(value);
}

void BottomWidget::updateFakeButtonColors(bool defaultValue) {
    _zoomwidget->updateColors(defaultValue);
    _fakebutton->updateColors(defaultValue);
}

GridWidget * BottomWidget::gridwidget() {
    return _gridwidget;
}

GridWidget * BottomWidget::foldergrid() {
    return _folderwidget->grid();
}

FolderBackButton * BottomWidget::folderbackbutton() {
    return _folderwidget->backbutton();
}

OpenCloseWidget * BottomWidget::openclosewidget() {
    return _openclosewidget;
}

GameTextWidget * BottomWidget::gametextwidget() {
    return _gametext;
}

void BottomWidget::mousePressEvent(QMouseEvent *event) {
    event->accept();
}

void BottomWidget::mouseMoveEvent(QMouseEvent *event) {
    event->accept();
}

void BottomWidget::connectGrid() {
    connect(_leftarrow, &QAbstractButton::clicked, _gridwidget, &GridWidget::scrollLeft);
    connect(_leftarrow, &ArrowButton::pressedTimer, _gridwidget, &GridWidget::scrollLeft);
    connect(_rightarrow, &QAbstractButton::clicked, _gridwidget, &GridWidget::scrollRight);
    connect(_rightarrow, &ArrowButton::pressedTimer, _gridwidget, &GridWidget::scrollRight);
    connect(_gridwidget, &GridWidget::updateArrows, _leftarrow, &ArrowButton::updateVisibility);
    connect(_gridwidget, &GridWidget::updateArrows, _rightarrow, &ArrowButton::updateVisibility);
    connect(_gridwidget, &GridWidget::updateOpenClose, _openclosewidget, &QWidget::setVisible);

    connect(_zoomwidget, &ZoomWidget::zoomInClicked, _gridwidget, &GridWidget::zoomIn);
    connect(_zoomwidget, &ZoomWidget::zoomOutClicked, _gridwidget, &GridWidget::zoomOut);
    connect(_gridwidget, &GridWidget::zoomLevelChanged, _zoomwidget, &ZoomWidget::updateZoomLevel);
    connect(_gridwidget, &GridWidget::zoomLevelChanged, _gridcursor, &CursorOverlay::changeZoom);
    connect(_gridwidget, &GridWidget::iconClicked, _gridcursor, &CursorOverlay::updatePosition);
    connect(_gridwidget, &GridWidget::iconChanged, _openclosewidget, &OpenCloseWidget::onCursorChange);
}

void BottomWidget::disconnectGrid() {
    disconnect(_leftarrow, &QAbstractButton::clicked, _gridwidget, &GridWidget::scrollLeft);
    disconnect(_leftarrow, &ArrowButton::pressedTimer, _gridwidget, &GridWidget::scrollLeft);
    disconnect(_rightarrow, &QAbstractButton::clicked, _gridwidget, &GridWidget::scrollRight);
    disconnect(_rightarrow, &ArrowButton::pressedTimer, _gridwidget, &GridWidget::scrollRight);
    disconnect(_gridwidget, &GridWidget::updateArrows, _leftarrow, &ArrowButton::updateVisibility);
    disconnect(_gridwidget, &GridWidget::updateArrows, _rightarrow, &ArrowButton::updateVisibility);
    disconnect(_gridwidget, &GridWidget::updateOpenClose, _openclosewidget, &QWidget::setVisible);

    disconnect(_zoomwidget, &ZoomWidget::zoomInClicked, _gridwidget, &GridWidget::zoomIn);
    disconnect(_zoomwidget, &ZoomWidget::zoomOutClicked, _gridwidget, &GridWidget::zoomOut);
    disconnect(_gridwidget, &GridWidget::zoomLevelChanged, _zoomwidget, &ZoomWidget::updateZoomLevel);
    disconnect(_gridwidget, &GridWidget::zoomLevelChanged, _gridcursor, &CursorOverlay::changeZoom);
    disconnect(_gridwidget, &GridWidget::iconClicked, _gridcursor, &CursorOverlay::updatePosition);
    disconnect(_gridwidget, &GridWidget::iconChanged, _openclosewidget, &OpenCloseWidget::onCursorChange);
}

void BottomWidget::connectFolder() {
    connect(_leftarrow, &QAbstractButton::clicked, _folderwidget->grid(), &GridWidget::scrollLeft);
    connect(_leftarrow, &ArrowButton::pressedTimer, _folderwidget->grid(), &GridWidget::scrollLeft);
    connect(_rightarrow, &QAbstractButton::clicked, _folderwidget->grid(), &GridWidget::scrollRight);
    connect(_rightarrow, &ArrowButton::pressedTimer, _folderwidget->grid(), &GridWidget::scrollRight);
    connect(_folderwidget->grid(), &GridWidget::updateArrows, _leftarrow, &ArrowButton::updateVisibility);
    connect(_folderwidget->grid(), &GridWidget::updateArrows, _rightarrow, &ArrowButton::updateVisibility);
    connect(_folderwidget->grid(), &GridWidget::updateOpenClose, _openclosewidget, &QWidget::setVisible);

    connect(_zoomwidget, &ZoomWidget::zoomInClicked, _folderwidget->grid(), &GridWidget::zoomIn);
    connect(_zoomwidget, &ZoomWidget::zoomOutClicked, _folderwidget->grid(), &GridWidget::zoomOut);
    connect(_folderwidget->grid(), &GridWidget::zoomLevelChanged, _zoomwidget, &ZoomWidget::updateZoomLevel);
    connect(_folderwidget->grid(), &GridWidget::zoomLevelChanged, _gridcursor, &CursorOverlay::changeZoom);
    connect(_folderwidget->grid(), &GridWidget::iconClicked, _gridcursor, &CursorOverlay::updatePosition);
    connect(_folderwidget->grid(), &GridWidget::iconChanged, _openclosewidget, &OpenCloseWidget::onCursorChange);
}

void BottomWidget::disconnectFolder() {
    disconnect(_leftarrow, &QAbstractButton::clicked, _folderwidget->grid(), &GridWidget::scrollLeft);
    disconnect(_leftarrow, &ArrowButton::pressedTimer, _folderwidget->grid(), &GridWidget::scrollLeft);
    disconnect(_rightarrow, &QAbstractButton::clicked, _folderwidget->grid(), &GridWidget::scrollRight);
    disconnect(_rightarrow, &ArrowButton::pressedTimer, _folderwidget->grid(), &GridWidget::scrollRight);
    disconnect(_folderwidget->grid(), &GridWidget::updateArrows, _leftarrow, &ArrowButton::updateVisibility);
    disconnect(_folderwidget->grid(), &GridWidget::updateArrows, _rightarrow, &ArrowButton::updateVisibility);
    disconnect(_folderwidget->grid(), &GridWidget::updateOpenClose, _openclosewidget, &QWidget::setVisible);

    disconnect(_zoomwidget, &ZoomWidget::zoomInClicked, _folderwidget->grid(), &GridWidget::zoomIn);
    disconnect(_zoomwidget, &ZoomWidget::zoomOutClicked, _folderwidget->grid(), &GridWidget::zoomOut);
    disconnect(_folderwidget->grid(), &GridWidget::zoomLevelChanged, _zoomwidget, &ZoomWidget::updateZoomLevel);
    disconnect(_folderwidget->grid(), &GridWidget::zoomLevelChanged, _gridcursor, &CursorOverlay::changeZoom);
    disconnect(_folderwidget->grid(), &GridWidget::iconClicked, _gridcursor, &CursorOverlay::updatePosition);
    disconnect(_folderwidget->grid(), &GridWidget::iconChanged, _openclosewidget, &OpenCloseWidget::onCursorChange);
}
