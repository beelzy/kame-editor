// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QGraphicsDropShadowEffect>
#include <QPainterPath>

#include "fakebutton.h"
#include "preview.h"
#include "../sourcefile.h"

const QColor FakeButton::DEFAULT_DARK_COLOR = QColor("#C4C4C4");
const QColor FakeButton::DEFAULT_MAIN_COLOR = QColor("#E9E9EA");
const QColor FakeButton::DEFAULT_LIGHT_COLOR = QColor(Qt::white);
const QColor FakeButton::DEFAULT_ICONMAIN_COLOR = QColor("#849EC6");
const QColor FakeButton::DEFAULT_ICONLIGHT_COLOR = QColor("#95B0D3");

FakeButton::FakeButton(QWidget *parent) : QAbstractButton(parent) {
    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect();
    shadow->setColor(QColor("#33000000"));
    shadow->setBlurRadius(6);
    shadow->setOffset(QPointF(0, 1));
    setGraphicsEffect(shadow);

    updateColors(true);
}

void FakeButton::setMainColor(QColor value) {
    _main = value;
    update();
}

void FakeButton::setLightColor(QColor value) {
    _light = value;
    update();
}

void FakeButton::setIconMainColor(QColor value) {
    _iconmain = value;
    update();
}

void FakeButton::setIconLightColor(QColor value) {
    _iconlight = value;
    update();
}

void FakeButton::updateColors(bool defaultValue) {
    if (defaultValue) {
        _main = DEFAULT_MAIN_COLOR;
        _light = DEFAULT_LIGHT_COLOR;
        _iconmain = DEFAULT_ICONMAIN_COLOR;
        _iconlight = DEFAULT_ICONLIGHT_COLOR;
    } else {
        _main = SourceFile::bottomcornercolors.value("main");
        _light = SourceFile::bottomcornercolors.value("light");
        _iconmain = SourceFile::bottomcornercolors.value("iconmain");
        _iconlight = SourceFile::bottomcornercolors.value("iconlight");
    }
    update();
}

void FakeButton::paintEvent(QPaintEvent *) {
    QPainter p(this);
    p.setRenderHints(QPainter::Antialiasing);

    QPen pen = QPen(_main);
    pen.setWidthF(2.0);
    pen.setCosmetic(false);

    QPainterPath path;
    path.addRoundedRect(-9, -9, 60, 40, 8, 8);
    p.setPen(pen);
    p.drawPath(path);

    path = QPainterPath();

    path.addRoundedRect(-8, -8, 58, 38, 8, 8);

    pen.setColor(_light);
    pen.setWidthF(2.0);

    p.setPen(pen);
    p.drawPath(path);

    QColor adjusted = _main;
    adjusted.setAlphaF(0.8);
    QLinearGradient gradient = QLinearGradient(QPointF(0, 0), QPointF(0, 38));
    gradient.setColorAt(0, Preview::colormix(_main, _light));
    gradient.setColorAt(1, adjusted);
    p.setClipRect(QRect(0, 0, 58, 38));
    p.setBrush(gradient);
    p.setPen(QPen(Qt::transparent));

    p.drawPath(path);

    if (isDown()) {
        Preview::drawRoundedRectShadow(p, _iconlight, QRect(2, 2, 49, 28), 4.0, 6.0);
    }

    path = QPainterPath();

    path.moveTo(13, 15);
    path.lineTo(23.5, 5);
    path.lineTo(32, 13);
    path.lineTo(30, 15);
    path.cubicTo(29, 16, 29, 21, 27.5, 22);
    path.lineTo(25, 24.5);
    path.lineTo(18, 24.5);
    path.quadTo(16, 24.5, 16, 22);
    path.lineTo(16, 16);

    path.moveTo(20.8, 16.5);
    path.lineTo(26, 16.5);
    path.lineTo(26, 20);
    path.lineTo(20.8, 20);
    path.moveTo(27, 25);
    path.lineTo(29.5, 22);
    path.cubicTo(30, 20, 30, 17, 32, 15.5);
    path.lineTo(35, 14);
    path.quadTo(36, 14, 37, 15);
    path.lineTo(34, 19);
    path.lineTo(36, 20);
    path.lineTo(39, 16.8);
    path.cubicTo(39, 17, 40.5, 17, 40, 18);
    //path.lineTo(41, 17);
    path.quadTo(41, 17, 39.5, 22);
    path.lineTo(39.5, 22);
    path.cubicTo(38, 23, 34, 26, 34, 24);
    path.lineTo(30, 27.8);
    path.quadTo(26.5, 27.5, 27, 25);

    if (isDown()) {
        p.setBrush(_main);
    } else {
        p.translate(0, 1);
        p.setPen(QColor(Qt::transparent));
        p.setBrush(_light);
        p.drawPath(path);
        p.translate(0, -1);
        gradient = QLinearGradient(QPointF(0, 0), QPointF(0, 26));
        gradient.setColorAt(0, _iconlight);
        gradient.setColorAt(1, _iconmain);

        p.setBrush(gradient);
    }

    p.setPen(QColor(Qt::transparent));
    p.drawPath(path);
}
