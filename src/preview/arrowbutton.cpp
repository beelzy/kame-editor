// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include <QPainter>
#include <QPainterPath>
#include <QGraphicsDropShadowEffect>
#include <QRadialGradient>
#include <QMap>

#include "arrowbutton.h"
#include "../sourcefile.h"

const QColor ArrowButton::DEFAULT_DARK_COLOR = QColor("#B3B3B3");
const QColor ArrowButton::DEFAULT_MAIN_COLOR = QColor("#D9D8D8");
const QColor ArrowButton::DEFAULT_LIGHT_COLOR = QColor("#F4F4F4");

const QColor ArrowButton::DEFAULT_BORDER_COLOR = QColor("#9CB9B4");
const QColor ArrowButton::DEFAULT_UNPRESSED_COLOR = QColor("#9FC0BA");
const QColor ArrowButton::DEFAULT_PRESSED_COLOR = QColor("#BEE2DB");

ArrowButton::ArrowButton(QWidget *parent) : QAbstractButton(parent) {
    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect();
    shadow->setColor(QColor("#33000000"));
    shadow->setBlurRadius(24);
    shadow->setOffset(QPointF(0, 1));
    setGraphicsEffect(shadow);

    updateBaseColors(true);
    updateArrowColors(true);

    setDirection(ArrowButton::Direction::LEFT);

    connect(this, &QAbstractButton::pressed, this, &ArrowButton::startDragTimer);
    connect(this, &QAbstractButton::released, this, &ArrowButton::stopDragTimer);
}

ArrowButton::Direction ArrowButton::direction() {
    return _direction;
}

void ArrowButton::setDirection(ArrowButton::Direction direction) {
    _direction = direction;

    float posX = -1;
    float radius = 70.0 / sqrt(3);
    float sign = 1;
    float offsetY = -6;
    if (_direction == ArrowButton::Direction::LEFT) {
        posX = -radius * 1.5;
        sign = -1;
        offsetY = 0;
    }

    QRegion clickableMask(QRect(posX - sign, offsetY, radius * 2, radius * 2), QRegion::Ellipse);

    setMask(clickableMask);
    update();
}

QRect ArrowButton::boundingRect() {
    float radius = 70.0 / sqrt(3);
    return QRect(0, 0, radius * 2, radius * 2);
}

void ArrowButton::startDragTimer() {
    _timerid = startTimer(400);
}

void ArrowButton::stopDragTimer() {
    killTimer(_timerid);
}

void ArrowButton::timerEvent(QTimerEvent *) {
    emit pressedTimer();
}

void ArrowButton::updateVisibility(bool showLeft, bool showRight) {
    if (_direction == ArrowButton::Direction::LEFT) {
        if (showLeft) {
            show();
        } else {
            hide();
        }
    } else {
        if (showRight) {
            show();
        } else {
            hide();
        }
    }
}

void ArrowButton::setDarkColor(QColor value) {
    _dark = value;
    update();
}

void ArrowButton::setMainColor(QColor value) {
    _main = value;
    update();
}

void ArrowButton::setLightColor(QColor value) {
    _light = value;
    update();
}

void ArrowButton::setBorderColor(QColor value) {
    _border = value;
    update();
}

void ArrowButton::setUnpressedColor(QColor value) {
    _unpressed = value;
    update();
}

void ArrowButton::setPressedColor(QColor value) {
    _pressed = value;
    update();
    setDown(true);
}

void ArrowButton::updateBaseColors(bool defaultValue) {
    if (defaultValue) {
        _dark = DEFAULT_DARK_COLOR;
        _main = DEFAULT_MAIN_COLOR;
        _light = DEFAULT_LIGHT_COLOR;
    } else {
        _dark = SourceFile::arrowbuttonbasecolors["dark"];
        _main = SourceFile::arrowbuttonbasecolors["main"];
        _light = SourceFile::arrowbuttonbasecolors["light"];
    }
    update();
}

void ArrowButton::updateArrowColors(bool defaultValue) {
    if (defaultValue) {
        _border = DEFAULT_BORDER_COLOR;
        _unpressed = DEFAULT_UNPRESSED_COLOR;
        _pressed = DEFAULT_PRESSED_COLOR;
    } else {
        _border = SourceFile::arrowbuttonarrowcolors["border"];
        _unpressed = SourceFile::arrowbuttonarrowcolors["unpressed"];
        _pressed = SourceFile::arrowbuttonarrowcolors["pressed"];
    }
    setDown(false);
    update();
}

void ArrowButton::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    float offsetX = 0;
    float offsetY = _direction == ArrowButton::Direction::LEFT ? 0 : -6;

    if (isDown()) {
        offsetX = _direction == ArrowButton::Direction::LEFT ? -3 : 3;
    }

    float radius = 70.0 / sqrt(3);
    float center = radius;

    int focus = 32;

    if (_direction == ArrowButton::Direction::LEFT) {
        center = -radius / 2;
        focus = -28;
    }

    QRadialGradient gradient = QRadialGradient(QPointF(center + offsetX, radius + offsetY), radius + 10, QPointF(center + offsetX + focus, radius + offsetY));
    QColor adjustedMain = _main;
    adjustedMain.setAlpha(0xDD);
    QColor adjustedLight = _light;
    adjustedLight.setAlpha(0xDD);
    gradient.setColorAt(0, adjustedMain);
    gradient.setColorAt(0.8, adjustedMain);
    gradient.setColorAt(0.9, adjustedLight);

    p.setPen(QColor(Qt::transparent));
    p.setBrush(gradient);

    float posX = 0;
    float startAngle = 120 * 16;
    focus = 16;

    if (_direction == ArrowButton::Direction::LEFT) {
        posX = -radius * 1.5;
        startAngle = -60 * 16;
        focus = 4;
    }

    p.drawChord(QRect(posX + offsetX, offsetY, radius * 2, radius * 2), startAngle, 120 * 16);

    gradient = QRadialGradient(QPointF(center + offsetX, radius + offsetY), radius, QPointF(offsetX + focus, radius + offsetY + 24));
    QColor adjustedDark = _dark;
    adjustedDark.setAlpha(0x50);
    QColor transparent = _dark;
    transparent.setAlpha(0);
    gradient.setColorAt(0, adjustedDark);
    gradient.setColorAt(0.7, transparent);
    p.setBrush(gradient);
    p.drawChord(QRect(posX + offsetX, offsetY, radius * 2, radius * 2), startAngle, 120 * 16);

    QPainterPath path;

    path.moveTo(4, 2 + radius);
    path.lineTo(10, 2 + radius);
    path.lineTo(10, 7 + radius);
    path.lineTo(17, 0 + radius);
    path.lineTo(10, -7 + radius);
    path.lineTo(10, -2 + radius);
    path.lineTo(4, -2 + radius);
    path.quadTo(3, -2 + radius, 3, radius);
    path.quadTo(3, 2 + radius, 4, 2 + radius);

    if (_direction == ArrowButton::Direction::LEFT) {
        p.scale(-1, 1);
        p.translate(-offsetX - radius / 2, offsetY);
    } else {
        p.translate(offsetX, offsetY);
    }

    QColor bordercolor = _border;
    bordercolor.setAlpha(50);

    p.setPen(bordercolor);
    p.setBrush(_unpressed);

    if (isDown()) {
        p.setPen(_border);
        p.setBrush(_pressed);
    }
    p.drawPath(path);
}
