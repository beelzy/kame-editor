// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QDebug>
#include <QtEndian>
#include <QRegularExpression>
#include <QFileInfo>

#include "../preview/adpcmplayer.h"
#include <portaudio.h>

/*
 * AdpcmPlayer takes output from vgmstream and plays and loops the pcm output from it. It makes a
 * call to vgmstream to obtain metadata for display and to get the looping parameters, then it makes
 * another vgmstream call to get actual PCM data. Then we open a portaudio stream when the user wants to
 * play and read data into a portaudio callback. The PCM data also includes a WAV/RIFF header, so we use
 * that instead to get the correct parameters for portaudio. The loop values will be converted for the
 * playback so they stop and start at the right places.
 */

AdpcmPlayer::AdpcmPlayer(QWidget *parent) : QWidget(parent) {
    _vgmstream = nullptr;
    _vgmstreamFound = true;
    _mediaStream = nullptr;
    _stream = nullptr;
    _streamOpened = false;
    _loopstart = 0;
    _loopend = -1;
    _loopenabled = true;
    _wavstart = 0;
    _position = 0;
    _playing = false;
    _wavscale = 1;
}

AdpcmPlayer::~AdpcmPlayer() {
    if (_mediaStream != nullptr) {
        _mediaStream->close();
        delete _mediaStream;
    }
    if (_stream != nullptr) {
        Pa_CloseStream(_stream);
    }
    if (_vgmstream != nullptr) {
        delete _vgmstream;
    }
}

void AdpcmPlayer::loadFile(QString filepath) {
    _playing = false;
    if (_mediaStream != nullptr) {
        _mediaStream->close();
        Pa_CloseStream(_stream);
        _streamOpened = false;
        delete _mediaStream;
        _mediaStream = nullptr;
    }
    _filepath = filepath;
    if (_vgmstreamFound) {
        getLoop();
    }
}

QString AdpcmPlayer::filepath() {
    return _filepath;
}

int AdpcmPlayer::samples() {
    return _numsamples;
}

void AdpcmPlayer::setLooping(bool value) {
    _loopenabled = value;
}

void AdpcmPlayer::setVgmstreamAvailable(bool value) {
    _vgmstreamFound = value;
}

bool AdpcmPlayer::isPlaying() {
    return Pa_IsStreamActive(_stream) == 1 && _playing;
}

QString AdpcmPlayer::errors() {
    return _errors;
}

void AdpcmPlayer::play() {
    if (!_filepath.isEmpty()) {
        _playing = true;
        if (!_streamOpened) {
            openStream();
        } else {
            Pa_StartStream(_stream);
        }
    }
}

void AdpcmPlayer::pause() {
    Pa_StopStream(_stream);
}

void AdpcmPlayer::reset() {
    Pa_AbortStream(_stream);
    if (_mediaStream != nullptr && _mediaStream->isOpen()) {
        _mediaStream->seek(_wavstart);
    }
    _position = 0;
    emit positionChanged(_position);
    _playing = false;
}

void AdpcmPlayer::unload() {
    reset();

    _filepath = "";
    if (_mediaStream != nullptr) {
        _mediaStream->close();
        Pa_CloseStream(_stream);
        _streamOpened = false;
        delete _mediaStream;
        _mediaStream = nullptr;
    }
    _description = "";
}

long long AdpcmPlayer::toMSecs(long long duration) {
    return duration * 1000 * 1000 * _wavscale / _samplerate / _samplerate;
}

void AdpcmPlayer::getLoop() {
    _errors = "";
    if (_vgmstream != nullptr) {
        _vgmstream->terminate();
        _vgmstream->close();
        _vgmstream->deleteLater();
    }
    _vgmstream = new QProcess();
    connect(_vgmstream, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AdpcmPlayer::metadataCallback);
    _vgmstream->start("vgmstream-cli", QStringList() << "-m" << _filepath);
    _vgmstream->waitForFinished(-1);
}

void AdpcmPlayer::metadataCallback(int exitcode, QProcess::ExitStatus exitstatus) {
    disconnect(_vgmstream, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AdpcmPlayer::metadataCallback);
    if (exitcode == 0 && exitstatus == QProcess::NormalExit) {
        _loopstart = 0;
        _loopend = -1;
        _description = _vgmstream->readAllStandardOutput();
        QRegularExpression regex("sample rate: ([0-9]+) Hz");
        QRegularExpressionMatch match = regex.match(_description);
        _samplerate = match.captured(1).toInt();
        QRegularExpression totalRx("stream total samples: ([0-9]+) \\(");
        QRegularExpressionMatch totalRxMatch = totalRx.match(_description);
        _numsamples = totalRxMatch.captured(1).toLongLong();
        if (_description.contains("loop start:")) {
            QRegularExpression startRx("loop start: ([0-9]+) samples");
            QRegularExpressionMatch startRxMatch = startRx.match(_description);
            _loopstart = startRxMatch.captured(1).toLongLong();
        }
        if (_description.contains("loop end:")) {
            QRegularExpression endRx("loop end: ([0-9]+) samples");
            QRegularExpressionMatch endRxMatch = endRx.match(_description);
            _loopend = endRxMatch.captured(1).toLongLong();
        } else if (_loopstart > 0) {
            _loopend = _numsamples;
        }
        QFileInfo fileInfo = QFileInfo(_filepath);
        QLocale locale = this->locale();
        _description += "file size: " + locale.formattedDataSize(fileInfo.size(), 2, QLocale::DataSizeTraditionalFormat) + "\n";

        _vgmstream->close();
        connect(_vgmstream, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AdpcmPlayer::vgmstreamCallback);
        _vgmstream->start("vgmstream-cli", QStringList() << "-P" << "-l" << "1" << "-F" << _filepath);
        _vgmstream->waitForFinished(-1);
    } else {
        _description = _vgmstream->readAllStandardError() + "\n" +  _vgmstream->readAllStandardOutput();
        _vgmstream->close();
        emit fileError();
    }
}

void AdpcmPlayer::vgmstreamCallback(int exitcode, QProcess::ExitStatus statuscode) {
    if (!_vgmstream) {
        return;
    }
    disconnect(_vgmstream, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &AdpcmPlayer::vgmstreamCallback);
    if (exitcode == 0 && statuscode == QProcess::NormalExit) {
        QByteArray audioData(_vgmstream->readAllStandardOutput());
        _vgmstream->close();
        if (_mediaStream != nullptr) {
            delete _mediaStream;
        }
        _mediaStream = new QBuffer();
        _mediaStream->setData(audioData);
        _mediaStream->open(QIODevice::ReadOnly);

        //Iterate through PCM header and grab information
        //vgmstream returns output back as a PCM wav file
        _mediaStream->read(4);
        qFromLittleEndian<quint32>(_mediaStream->read(sizeof(uint32_t)));
        _mediaStream->read(4);
        //if we can't read these in the header, it's not a valid file
        _wavnumchannels = 0;
        _wavbitspersample = 0;
        while(!_mediaStream->atEnd()) {
            QByteArray chunkName = _mediaStream->read(4);
            int chunkLen = qFromLittleEndian<quint32>(_mediaStream->read(sizeof(uint32_t)));
            //qDebug() << "read chunk" << chunkName << chunkLen;
            if(chunkName == "fmt ") {
                readHeader(chunkLen);
            }
            else if(chunkName == "data") {
                //qDebug() << "length" << double(chunkLen) / _wavsamplerate / _wavnumchannels / (_wavbitspersample / 8);
                break; // start playing now
            } else {
                // skip chunk
                _mediaStream->read(chunkLen);
            }
        }
        _wavstart = _mediaStream->pos();

        _wavscale = _numsamples * (_samplerate / 1000.0) / _mediaStream->size();

        //qDebug () << "file info" << _loopstart << _loopend << _samplerate << _wavnumchannels << _wavbitspersample << _wavstart << _wavscale;

        _position = 0;
        emit positionChanged(_position);

        emit fileLoaded();
    } else {
        _vgmstream->close();
    }
    _vgmstream->deleteLater();
    _vgmstream = nullptr;
}

void AdpcmPlayer::openStream() {
    PaStreamParameters outputParameters;
    outputParameters.device = Pa_GetDefaultOutputDevice();
    outputParameters.channelCount = _wavnumchannels;
    outputParameters.sampleFormat = _sampleFormat;
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultHighOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    //qDebug() << "pa device info" << Pa_GetDeviceCount() << Pa_GetDeviceInfo(outputParameters.device)->name;
    //qDebug() << "opening stream..." << _stream << outputParameters.sampleFormat << outputParameters.channelCount << (outputParameters.device == paNoDevice) << outputParameters.suggestedLatency << _wavsamplerate;
    PaError ret = Pa_OpenStream(&_stream, NULL, &outputParameters, _wavsamplerate, paFramesPerBufferUnspecified, 0, &AdpcmPlayer::paStreamCallback_map, this);
    if (ret != paNoError) {
        _errors = QString("error opening stream ").append(QString::number(ret)).append(" ").append(Pa_GetErrorText(ret));
        qDebug() << _errors;
        if (_stream) {
            Pa_CloseStream(_stream);
        }
        emit streamError();
        return;
    }
    _errors = "";
    _streamOpened = true;
    Pa_StartStream(_stream);
}

void AdpcmPlayer::readHeader(int size) {
    int fmttag = qFromLittleEndian<quint16>(_mediaStream->read(sizeof(uint16_t)));
    _wavnumchannels = qFromLittleEndian<quint16>(_mediaStream->read(sizeof(uint16_t)));
    _wavsamplerate = qFromLittleEndian<quint32>(_mediaStream->read(sizeof(uint32_t)));
    qFromLittleEndian<quint32>(_mediaStream->read(sizeof(uint32_t)));
    qFromLittleEndian<quint16>(_mediaStream->read(sizeof(uint16_t)));

    _wavbitspersample = qFromLittleEndian<quint16>(_mediaStream->read(sizeof(uint16_t)));
    if (fmttag == 1) {
        switch(_wavbitspersample) {
            case 8:
                _sampleFormat = paInt8;
                break;
            case 16:
                _sampleFormat = paInt16;
                break;
            case 32:
                _sampleFormat = paInt32;
        }
    } else {
        _sampleFormat = paFloat32;
    }
    if(size > 16) {
        uint16_t extendedSize = qFromLittleEndian<quint16>(_mediaStream->read(sizeof(uint16_t)));
        _mediaStream->read(extendedSize);
    }
}

int AdpcmPlayer::paStreamCallback_map(const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData) {
    if(auto self = reinterpret_cast<AdpcmPlayer*>(userData)) {
        return self->paStreamCallback(inputBuffer, outputBuffer, framesPerBuffer, timeInfo, statusFlags);
    }
    return 0;
}

int AdpcmPlayer::paStreamCallback(const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags) {
    int bytespersample = _wavbitspersample / 8;
    unsigned long original = framesPerBuffer;

    // Read data from stream
    qint64 numRead = _mediaStream->read((char*)outputBuffer, framesPerBuffer * _wavnumchannels * bytespersample);
    outputBuffer = (uint8_t*)outputBuffer + numRead * _wavnumchannels * bytespersample;
    // Ensure correct number of bytes are read
    framesPerBuffer -= numRead / bytespersample / _wavnumchannels;

    if (_playing) {
        _position += numRead;
    }
    if (framesPerBuffer > 0) {
        //qDebug() << "fill remaining buffer" << framesPerBuffer << "was" << original << "is" << numRead / bytespersample / _wavnumchannels << original - numRead / bytespersample / _wavnumchannels;
        //memset(outputBuffer, 0, framesPerBuffer * _wavnumchannels * bytespersample);
        emit positionChanged(_position);
        if (_loopstart < _loopend && _loopenabled) {
            _position = _loopstart * _samplerate / 1000 / _wavscale;
            _position = _position + ((bytespersample - (_position % bytespersample)) % bytespersample);
            //qDebug() << "loop" << _wavstart << _position;
            _mediaStream->seek(_wavstart + _position);
            emit positionChanged(_position);
            return paContinue;
        } else {
            emit mediaEnd();
            return paComplete;
        }
    }

    if (_playing) {
        emit positionChanged(_position);
        return paContinue;
    } else {
        _position = 0;
        emit positionChanged(0);
        return paComplete;
    }
}

qint64 AdpcmPlayer::total() {
    return _mediaStream->size();
}

QString AdpcmPlayer::description() {
    return _description;
}
