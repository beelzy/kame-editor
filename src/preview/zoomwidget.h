// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ZOOMWIDGET_H
#define ZOOMWIDGET_H

#include <QWidget>

#include "zoominbutton.h"
#include "zoomoutbutton.h"

class ZoomWidget : public QWidget {
    Q_OBJECT

    public:
        explicit ZoomWidget(QWidget *parent = 0);
        ~ZoomWidget();

        void setMinZoomLevel(int value);

    signals:
        void zoomInClicked();
        void zoomOutClicked();

    public slots:
        void updateZoomLevel(int zoom);
        void setDarkColor(QColor value);
        void setMainColor(QColor value);
        void setLightColor(QColor value);
        void setIconMainColor(QColor value);
        void setIconLightColor(QColor value);
        void updateColors(bool defaultValue);

    protected:
        void paintEvent(QPaintEvent *event);

    protected slots:
        void slot_zoomInClicked();
        void slot_zoomOutClicked();

    private:
        ZoomInButton *_zoominbutton;
        ZoomOutButton *_zoomoutbutton;
        int _minzoomlevel;

        QColor _dark;
        QColor _main;
        QColor _light;
};

#endif // ZOOMWIDGET_H
