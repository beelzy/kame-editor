// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QVariant>

#include "openclosebutton.h"
#include "preview.h"
#include "../sourcefile.h"

const QColor OpenCloseButton::DEFAULT_DARK_COLOR = QColor("#5D5852");
const QColor OpenCloseButton::DEFAULT_MAIN_COLOR = QColor("#DFDBD7");
const QColor OpenCloseButton::DEFAULT_LIGHT_COLOR = QColor("#FCFBF7");

const QColor OpenCloseButton::DEFAULT_TEXT_SHADOW = QColor("#FCFBF7");
const QColor OpenCloseButton::DEFAULT_TEXT_MAIN = QColor("#4F4C48");
const QColor OpenCloseButton::DEFAULT_TEXT_SELECTED = QColor("#FCFBF7");

const float OpenCloseButton::DEFAULT_TEXT_SHADOW_POS = -1;

const QColor OpenCloseButton::DEFAULT_CLOSE_DARK_COLOR = QColor("#302B25");
const QColor OpenCloseButton::DEFAULT_CLOSE_MAIN_COLOR = QColor("#56514B");
const QColor OpenCloseButton::DEFAULT_CLOSE_LIGHT_COLOR = QColor("#87827C");

const QColor OpenCloseButton::DEFAULT_CLOSE_TEXT_SHADOW = QColor("#302B25");
const QColor OpenCloseButton::DEFAULT_CLOSE_TEXT_MAIN = QColor("#FFFEFC");
const QColor OpenCloseButton::DEFAULT_CLOSE_TEXT_SELECTED = QColor("#C3C1BE");

const float OpenCloseButton::DEFAULT_CLOSE_TEXT_SHADOW_POS = 1;

OpenCloseButton::OpenCloseButton(QWidget *parent) : QAbstractButton(parent) {
    _mode = OpenCloseButton::Mode::SINGLE;
    _isclose = false;

    updateColors(true);
}

OpenCloseButton::Mode OpenCloseButton::mode() {
    return _mode;
}

void OpenCloseButton::setMode(OpenCloseButton::Mode mode) {
    _mode = mode;
}

QString OpenCloseButton::text() {
    return _text;
}

void OpenCloseButton::setText(QString value) {
    _text = value;

    update();
}

bool OpenCloseButton::isClose() {
    return _isclose;
}

void OpenCloseButton::setIsClose(bool value) {
    _isclose = value;
    updateColors(true);
}

void OpenCloseButton::setDarkColor(QColor value) {
    _dark = value;
    update();
}

void OpenCloseButton::setMainColor(QColor value) {
    _main = value;
    update();
}

void OpenCloseButton::setLightColor(QColor value) {
    _light = value;
    update();
}

void OpenCloseButton::setTextShadowColor(QColor value) {
    _textshadow = value;
    update();
}

void OpenCloseButton::setTextMainColor(QColor value) {
    _textmain = value;
    update();
}

void OpenCloseButton::setTextSelectedColor(QColor value) {
    _textselected = value;
    update();
}

void OpenCloseButton::setTextShadowPos(float value) {
    _textshadowpos = value;
    update();
}

void OpenCloseButton::updateColors(bool defaultValue) {
    if (defaultValue) {
        if (_isclose) {
            _dark = DEFAULT_CLOSE_DARK_COLOR;
            _main = DEFAULT_CLOSE_MAIN_COLOR;
            _light = DEFAULT_CLOSE_LIGHT_COLOR;
            _textshadow = DEFAULT_CLOSE_TEXT_SHADOW;
            _textmain = DEFAULT_CLOSE_TEXT_MAIN;
            _textselected = DEFAULT_CLOSE_TEXT_SELECTED;
            _textshadowpos = DEFAULT_CLOSE_TEXT_SHADOW_POS;
        } else {
            _dark = DEFAULT_DARK_COLOR;
            _main = DEFAULT_MAIN_COLOR;
            _light = DEFAULT_LIGHT_COLOR;
            _textshadow = DEFAULT_TEXT_SHADOW;
            _textmain = DEFAULT_TEXT_MAIN;
            _textselected = DEFAULT_TEXT_SELECTED;
            _textshadowpos = DEFAULT_TEXT_SHADOW_POS;
        }
    } else {
        if (_isclose) {
            _dark = qvariant_cast<QColor>(SourceFile::closecolors["dark"]);
            _main = qvariant_cast<QColor>(SourceFile::closecolors["main"]);
            _light = qvariant_cast<QColor>(SourceFile::closecolors["light"]);
            _textshadow = qvariant_cast<QColor>(SourceFile::closecolors["textshadow"]);
            _textmain = qvariant_cast<QColor>(SourceFile::closecolors["textmain"]);
            _textselected = qvariant_cast<QColor>(SourceFile::closecolors["textselected"]);
            _textshadowpos = SourceFile::closecolors["textshadowpos"].toFloat();
        } else {
            _dark = qvariant_cast<QColor>(SourceFile::opencolors["dark"]);
            _main = qvariant_cast<QColor>(SourceFile::opencolors["main"]);
            _light = qvariant_cast<QColor>(SourceFile::opencolors["light"]);
            _textshadow = qvariant_cast<QColor>(SourceFile::opencolors["textshadow"]);
            _textmain = qvariant_cast<QColor>(SourceFile::opencolors["textmain"]);
            _textselected = qvariant_cast<QColor>(SourceFile::opencolors["textselected"]);
            _textshadowpos = SourceFile::opencolors["textshadowpos"].toFloat();
        }
    }
    update();
}

void OpenCloseButton::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    QPainterPath path;

    int buttonWidth = Preview::BOTTOM_SCREEN_WIDTH;

    path.addRoundedRect(0, 1, buttonWidth, 36, 10, 10);

    if (_mode == OpenCloseButton::Mode::LEFT) {
        QPainterPath clip;
        clip.addRect(106, 1, Preview::BOTTOM_SCREEN_WIDTH, 36);
        path = path.subtracted(clip);
        buttonWidth = 106;
    } else if (_mode == OpenCloseButton::Mode::RIGHT) {
        QPainterPath clip;
        clip.addRect(0, 1, 106, 36);
        path.translate(-106, 0);
        buttonWidth -= 106;
    }

    QLinearGradient gradient;

    if (isDown()) {
        gradient = QLinearGradient(QPointF(0, 0), QPointF(0, 25));
        gradient.setColorAt(0, _dark);
        gradient.setColorAt(1, _main);
        p.setBrush(gradient);
        p.setPen(_light);
        p.drawPath(path); 
    } else {
        p.setBrush(_light);
        p.setPen(Preview::colormix(_dark, _main));
        p.drawPath(path);

        int shadingWidth = buttonWidth;
        if (_mode == OpenCloseButton::Mode::LEFT) {
            p.setClipRect(QRect(0, 0, 105, 26));
            shadingWidth = Preview::BOTTOM_SCREEN_WIDTH;
        } 
        QPointF radius = QPointF(10.0, 10.0);
        if (_mode == OpenCloseButton::Mode::RIGHT) {
            radius.setX(0.0);
        }
        Preview::drawRoundedRectShadow(p, _main, QRect(0, 2, shadingWidth, 35), radius, 3.0);

        QColor adjusted = _light;
        adjusted.setAlphaF(0.3);
        QColor transparent = _light;
        transparent.setAlpha(0);
        gradient = QLinearGradient(QPointF(0, 2), QPointF(0, 24));
        gradient.setColorAt(0, adjusted);
        gradient.setColorAt(1, transparent);

        p.setClipPath(path);
        p.fillRect(0, 2, buttonWidth, 22, QBrush(gradient));

        gradient = QLinearGradient(QPointF(0, 16), QPointF(0, 25));
        adjusted = _dark;
        adjusted.setAlphaF(0.3);
        transparent = adjusted;
        transparent.setAlpha(0);
        gradient.setColorAt(0, transparent);
        gradient.setColorAt(1, adjusted);

        p.fillRect(0, 16, buttonWidth, 10, QBrush(gradient));
        p.setClipping(false);
    }

    QFont font = QFont("Nimbus Sans");
    font.setPixelSize(17);
    font.setLetterSpacing(QFont::AbsoluteSpacing, 1);

    p.setFont(font);

    int shift = _isclose ? 11 + 15 + 3 : 6;

    QFontMetrics metrics = QFontMetrics(font);
    qreal scale = std::min(1.0, (buttonWidth - shift - 12) * 1.0 / metrics.horizontalAdvance(_text));
    QRect rect = QRect(shift, 3, buttonWidth - shift - 12, 26);

    p.save();
    p.translate(rect.center());
    p.scale(scale, scale);
    p.translate(QPoint(-rect.center().x() / scale, -rect.center().y()));

    if (isDown()) {
        p.setPen(_textselected);
    } else {
        p.setPen(_textshadow);
        p.drawText(QRect(shift, 3 - _textshadowpos, (buttonWidth - shift) * 1 / scale, 22), Qt::AlignCenter, _text);

        p.setPen(_textmain);
    }

    p.drawText(QRect(shift, 3, (buttonWidth - shift) * 1.0 / scale, 22), Qt::AlignCenter, _text);
    p.restore();

    if (_isclose) {
        path = QPainterPath();

        path.addRoundedRect(12, 7, 15, 15, 3, 3);

        QPainterPath slash;
        slash.moveTo(15, 11);
        slash.lineTo(23, 18);
        slash.lineTo(24, 17);
        slash.lineTo(16, 10);

        QPainterPath backslash;
        backslash.moveTo(23, 10);
        backslash.lineTo(15, 17);
        backslash.lineTo(16, 18);
        backslash.lineTo(24, 11);

        QPainterPath cross = slash.united(backslash);

        path = path.subtracted(cross);

        p.setPen(QColor(Qt::transparent));
        if (isDown()) {
            p.setBrush(_textselected);
        } else {
            path.translate(0, -_textshadowpos);
            p.setBrush(_textshadow);
            p.drawPath(path);

            path.translate(0, _textshadowpos);

            p.setBrush(_textmain);
        }
        p.drawPath(path);
    }
}
