// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ZOOMINBUTTON_H
#define ZOOMINBUTTON_H

#include <QAbstractButton>

class ZoomInButton : public QAbstractButton {
    Q_OBJECT

    public:
        explicit ZoomInButton(QWidget *parent = 0);

    public slots:
        void setMainColor(QColor value);
        void setLightColor(QColor value);
        void setIconMainColor(QColor value);
        void setIconLightColor(QColor value);

    protected:
        void paintEvent(QPaintEvent *event);

        QColor _main;
        QColor _light;
        QColor _iconmain;
        QColor _iconlight;
};

#endif // ZOOMINBUTTON_H
