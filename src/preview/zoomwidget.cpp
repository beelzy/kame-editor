// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QGraphicsDropShadowEffect>

#include "zoomwidget.h"
#include "fakebutton.h"
#include "preview.h"
#include "../sourcefile.h"

ZoomWidget::ZoomWidget(QWidget *parent) : QWidget(parent) {
    _minzoomlevel = 0;
    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect();
    shadow->setColor(QColor("#33000000"));
    shadow->setBlurRadius(6);
    shadow->setOffset(QPointF(0, 1));
    setGraphicsEffect(shadow);

    _zoominbutton = new ZoomInButton(this);
    _zoominbutton->setGeometry(26, 0, 26, 32);

    _zoomoutbutton = new ZoomOutButton(this);
    _zoomoutbutton->setGeometry(1, 0, 24, 32);

    updateColors(true);

    connect(_zoominbutton, &QAbstractButton::clicked, this, &ZoomWidget::slot_zoomInClicked);
    connect(_zoomoutbutton, &QAbstractButton::clicked, this, &ZoomWidget::slot_zoomOutClicked);
}

ZoomWidget::~ZoomWidget() {
    delete _zoominbutton;
    delete _zoomoutbutton;
}

void ZoomWidget::setMinZoomLevel(int value) {
    _minzoomlevel = value;
}

void ZoomWidget::slot_zoomInClicked() {
    emit zoomInClicked();
}

void ZoomWidget::slot_zoomOutClicked() {
    emit zoomOutClicked();
}

void ZoomWidget::updateZoomLevel(int zoom) {
    _zoominbutton->setEnabled(true);
    _zoomoutbutton->setEnabled(true);
    if (zoom == _minzoomlevel) {
        _zoomoutbutton->setEnabled(false);
    } else if (zoom == 5) {
        _zoominbutton->setEnabled(false);
    }
}

void ZoomWidget::setDarkColor(QColor value) {
    _dark = value;
    update();
}

void ZoomWidget::setMainColor(QColor value) {
    _main = value;
    _zoominbutton->setMainColor(value);
    _zoomoutbutton->setMainColor(value);
    update();
}

void ZoomWidget::setLightColor(QColor value) {
    _light = value;
    _zoominbutton->setLightColor(value);
    _zoomoutbutton->setLightColor(value);
    update();
}

void ZoomWidget::setIconMainColor(QColor value) {
    _zoominbutton->setIconMainColor(value);
    _zoomoutbutton->setIconMainColor(value);
}

void ZoomWidget::setIconLightColor(QColor value) {
    _zoominbutton->setIconLightColor(value);
    _zoomoutbutton->setIconLightColor(value);
}

void ZoomWidget::updateColors(bool defaultValue) {
    if (defaultValue) {
        _dark = FakeButton::DEFAULT_DARK_COLOR;
        _main = FakeButton::DEFAULT_MAIN_COLOR;
        _light = FakeButton::DEFAULT_LIGHT_COLOR;
        _zoominbutton->setMainColor(FakeButton::DEFAULT_MAIN_COLOR);
        _zoominbutton->setLightColor(FakeButton::DEFAULT_LIGHT_COLOR);
        _zoominbutton->setIconMainColor(FakeButton::DEFAULT_ICONMAIN_COLOR);
        _zoominbutton->setIconLightColor(FakeButton::DEFAULT_ICONLIGHT_COLOR);
        _zoomoutbutton->setMainColor(FakeButton::DEFAULT_MAIN_COLOR);
        _zoomoutbutton->setLightColor(FakeButton::DEFAULT_LIGHT_COLOR);
        _zoomoutbutton->setIconMainColor(FakeButton::DEFAULT_ICONMAIN_COLOR);
        _zoomoutbutton->setIconLightColor(FakeButton::DEFAULT_ICONLIGHT_COLOR);
    } else {
        _dark = SourceFile::bottomcornercolors["dark"];
        _main = SourceFile::bottomcornercolors["main"];
        _light = SourceFile::bottomcornercolors["light"];
        _zoominbutton->setMainColor(_main);
        _zoominbutton->setLightColor(_light);
        _zoominbutton->setIconMainColor(SourceFile::bottomcornercolors["iconmain"]);
        _zoominbutton->setIconLightColor(SourceFile::bottomcornercolors["iconlight"]);
        _zoomoutbutton->setMainColor(_main);
        _zoomoutbutton->setLightColor(_light);
        _zoomoutbutton->setIconMainColor(SourceFile::bottomcornercolors["iconmain"]);
        _zoomoutbutton->setIconLightColor(SourceFile::bottomcornercolors["iconlight"]);
    }
    update();
}

void ZoomWidget::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    QPen pen = QPen(_main);
    pen.setWidthF(2.0);
    pen.setCosmetic(false);

    QPainterPath path;
    path.addRoundedRect(1, -9, 61, 40, 8, 8);
    p.setPen(pen);
    p.drawPath(path);

    path = QPainterPath();

    path.addRoundedRect(2, -8, 58, 38, 8, 8);

    pen.setWidthF(2.0);
    pen.setColor(_light);
    p.setPen(pen);
    p.drawPath(path);

    QColor adjusted = _main;
    adjusted.setAlphaF(0.8);
    QLinearGradient gradient = QLinearGradient(QPointF(0, 0), QPointF(0, 38));
    gradient.setColorAt(0, Preview::colormix(_main, _light));
    gradient.setColorAt(1, adjusted);
    p.setClipRect(QRect(2, 0, 58, 38));
    p.setBrush(gradient);
    p.setPen(QPen(Qt::transparent));

    p.drawPath(path);

    QColor transparent = _dark;
    transparent.setAlpha(0);
    QColor divider = _dark;
    divider.setAlpha(128);
    gradient = QLinearGradient(QPointF(0, 3), QPointF(0, 27));
    gradient.setColorAt(0, transparent);
    gradient.setColorAt(5.0 / 24, divider);
    gradient.setColorAt(19.0 / 24, divider);
    gradient.setColorAt(1, transparent);

    p.fillRect(25, 3, 1, 24, QBrush(gradient));

    transparent = _light;
    transparent.setAlpha(0);
    gradient.setColorAt(0, transparent);
    gradient.setColorAt(5.0 / 24, _light);
    gradient.setColorAt(19.0 / 24, _light);
    gradient.setColorAt(1, transparent);

    p.fillRect(26, 3, 1, 24, QBrush(gradient));
}
