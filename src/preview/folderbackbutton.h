// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FOLDERBACKBUTTON_H
#define FOLDERBACKBUTTON_H

#include <QAbstractButton>

class FolderBackButton : public QAbstractButton {
    Q_OBJECT

    public:
        explicit FolderBackButton(QWidget *parent = 0);

        static const QColor DEFAULT_DARK_COLOR;
        static const QColor DEFAULT_MAIN_COLOR;
        static const QColor DEFAULT_LIGHT_COLOR;

        static const QColor DEFAULT_TEXT_SHADOW;
        static const QColor DEFAULT_TEXT_MAIN;
        static const QColor DEFAULT_TEXT_SELECTED;

        static const float DEFAULT_TEXT_SHADOW_POS;

        void setDarkColor(QColor value);
        void setMainColor(QColor value);
        void setLightColor(QColor value);
        void setTextShadowColor(QColor value);
        void setTextMainColor(QColor value);
        void setTextSelectedColor(QColor value);
        void setTextShadowPos(float value);
        void updateColors(bool defaultValue);

    protected:
        void paintEvent(QPaintEvent *event);

    private:
        QColor _dark;
        QColor _main;
        QColor _light;
        QColor _textshadow;
        QColor _textmain;
        QColor _textselected;
        float _textshadowpos;
};

#endif // FOLDERBACKBUTTON_H
