// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef EMPTYICON_H
#define EMPTYICON_H

#include <QWidget>
#include <QMouseEvent>

class EmptyIcon : public QWidget {
    public:
        explicit EmptyIcon(QWidget *parent = 0);

        static const QColor DEFAULT_LIGHT_COLOR;
        static const QColor DEFAULT_DARK_COLOR;

        void setLightColor(QColor color);
        void setDarkColor(QColor color);
        void setTextureMode(bool value);

    protected:
        void mousePressEvent(QMouseEvent *event);
        void paintEvent(QPaintEvent *event);

    private:
        QColor _lightcolor;
        QColor _darkcolor;
        bool _isTexture;
};

#endif  // EMPTYICON_H
