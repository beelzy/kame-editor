// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#define GL_GLEXT_PROTOTYPES
#include <QOpenGLFunctions>
#include <QOpenGLContext>
#include <QTimer>
#include <QDateTime>
#include <QPainter>
#include <QPainterPath>

#include "topview.h"
#include "gridicon.h"
#include "gridwidget.h"
#include "preview.h"
#include "foldericon.h"
#include "objloader.h"
#include "../sourcefile.h"

// 400 / 64
const GLfloat TopView::TILE_X = 6.25;
// 240 / 64
const GLfloat TopView::TILE_Y = 3.75;
// 6 / 512
const GLfloat TopView::TEXTURE_X = 0.01171875;
// 240 / 256
const GLfloat TopView::TEXTURE_Y = 0.9375;
const QString TopView::DEFAULT_TEXTURE = ":/resources/defaults/topExt.png";
const QColor TopView::DEFAULT_COLOR = QColor("#D0D0DC");

const QColor TopView::DEFAULT_TEXTMAIN_COLOR = QColor("#484C4F");
const QColor TopView::DEFAULT_MAIN_COLOR = QColor("#DCDCE2");
const QColor TopView::DEFAULT_SHADOW_COLOR = QColor("#484C4F");

const QColor TopView::DEFAULT_DEMO_MAIN_COLOR = QColor("#F4F4F4");
const QColor TopView::DEFAULT_DEMO_TEXTMAIN_COLOR = QColor("#4D4D4D");

TopView::TopView(QWidget *parent) : QOpenGLWidget(parent) {
    makeCurrent();
    _vbo.destroy();
    _tvbo.destroy();
    _fvbo.destroy();
    _filevbo.destroy();
    doneCurrent();

    _tileshader = nullptr;
    _textureshader = nullptr;
    _demoscrollpos = DEMO_OFFSET;
    _scrollpos = 0;
    _scrollenabled = false;

    _alpha = 1.0;
    _gradientalpha = 1.0;
    _gradientbrightness = 1.0;
    _alphaext = 1.0;
    _drawtype = DrawType::NONE;

    _animated = true;

    _texturepath = DEFAULT_TEXTURE;
    _textureextpath = _texturepath;

    _textureimage = QImage(DEFAULT_TEXTURE).mirrored();
    _textureextimage = QImage(DEFAULT_TEXTURE).mirrored();

    ObjLoader *loader = new ObjLoader();
    _foldervertices = loader->load(":/resources/meshes/folder.obj");

    _filevertices = loader->load(":/resources/meshes/file.obj");

    _showFolder = false;

    _battery = new QImage(":/resources/preview/battery.png");

    _demotimer = new QTimer(this);
    connect(_demotimer, SIGNAL(timeout()), this, SLOT(enableScrolling()));
    _demotimer->setInterval(2000);

    _scrollanimation = new QPropertyAnimation(this, "scrollpos");
    _scrollanimation->setDuration(200);

    _timer = new QTimer(this);
}

TopView::~TopView() {
    cleanup();

    delete _battery;
    delete _demotimer;
    delete _scrollanimation;
}

QSize TopView::sizeHint() const {
    return QSize(Preview::TOP_SCREEN_WIDTH, Preview::TOP_SCREEN_HEIGHT);
}

void TopView::initializeGL() {
    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &TopView::cleanup);

    initializeOpenGLFunctions();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearColor(235.0 / 255.0, 235.0 / 255.0, 240.0 / 255.0, 1);

    _color = DEFAULT_COLOR;
    updateColors(true);
    updateFolderColors(true);
    updateFileColors(true);
    updateDemoColors(true);

    _tileshader = new QOpenGLShaderProgram;
    _tileshader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/resources/shaders/topVertexShader.vsh");
    // Solid color texture is the default
    _tileshader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/resources/shaders/tileFragmentShader.fsh");
    _tileshader->link();

    _textureshader = new QOpenGLShaderProgram;
    _textureshader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/resources/shaders/topVertexShader.vsh");
    _textureshader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/resources/shaders/textureFragmentShader.fsh");
    _textureshader->link();

    _foldershader = new QOpenGLShaderProgram;
    _foldershader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/resources/shaders/modelVertexShader.vsh");
    _foldershader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/resources/shaders/modelFragmentShader.fsh");
    _foldershader->link();

    GLfloat w = 1.0f;
    GLfloat h = GLfloat(Preview::TOP_SCREEN_HEIGHT) / GLfloat(Preview::TOP_SCREEN_WIDTH);

    _vertices << -0.5 * w << 0.5 * h << -2
        << -0.5 * w << -0.5 * h << -2
        << 0.5 * w << -0.5 * h << -2
        << 0.5 * w << -0.5 * h << -2
        << 0.5 * w << 0.5 * h << -2
        << -0.5 * w << 0.5 * h << -2;

    _tileshader->bind();

    _colorid = _tileshader->uniformLocation("color");
    _timeid = _tileshader->uniformLocation("time");

    _tileshader->release();

    _textureshader->bind();

    _texturetimeid = _textureshader->uniformLocation("time");

    _textureshader->release();

    _foldershader->bind();

    _foldermainid = _foldershader->uniformLocation("maincolor");
    _folderdarkid = _foldershader->uniformLocation("dark");
    _folderlightid = _foldershader->uniformLocation("light");

    _foldershader->release();

    _vbo.create();
    _tvbo.create();
    _fvbo.create();
    _filevbo.create();
    _vao.create();
    _fvao.create();
    _filevao.create();

    _fvbo.bind();
    _fvbo.allocate(_foldervertices.constData(), _foldervertices.size() * 3 * sizeof(GLfloat));

    QOpenGLVertexArrayObject::Binder fvaoBinder(&_fvao);

    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glBindBuffer(GL_ARRAY_BUFFER, _fvbo.bufferId());
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);

    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));

    _fvbo.release();

    _filevbo.bind();
    _filevbo.allocate(_filevertices.constData(), _filevertices.size() * 3 * sizeof(GLfloat));

    QOpenGLVertexArrayObject::Binder filevaoBinder(&_filevao);

    f->glBindBuffer(GL_ARRAY_BUFFER, _filevbo.bufferId());
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);

    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));

    _filevbo.release();

    connect(_timer, SIGNAL(timeout()), this, SLOT(update()));
    _timer->setInterval(75);

    updateDrawType();

    // Tiling animation and texture
    _texture = new QOpenGLTexture(_textureimage);
    _textureext = new QOpenGLTexture(_textureextimage);
}

void TopView::setDrawType() {
    _drawtype = static_cast<DrawType>(SourceFile::topdraw);
    if (_drawtype == DrawType::NONE) {
        setTexture(DEFAULT_TEXTURE);
        setTextureExt(DEFAULT_TEXTURE);
        _color = DEFAULT_COLOR;
        _alpha = 1.0;
        _gradientbrightness = 1.0;
        _alphaext = 1.0;
        _gradientalpha = 1.0;
    }

    updateDrawType();
}

void TopView::setAnimated(bool value) {
    if (_animated != value) {
        _animated = value;
        if (!value) {
            _timeractivated = _timer->isActive();
            _timer->stop();
        } else if (_timeractivated) {
            _timer->start();
        }
    }
}

bool TopView::needsUpdate() {
    return (_drawtype == DrawType::TEXTURE && !_timer->isActive()) || !_animated;
}

void TopView::checkUpdate() {
    if (needsUpdate()) {
        update();
    }
}

void TopView::updateDrawType() {
    GLfloat offsetX = TopView::TILE_X;
    GLfloat offsetY = TopView::TILE_Y;
    GLfloat startX = 0;

    if (_drawtype == DrawType::TEXTURE) {
        if (static_cast<FrameType>(SourceFile::topframe) == FrameType::SINGLE) {
            offsetX = 406.0/512;
            startX = TopView::TEXTURE_X;
        } else {
            offsetX = 400.0/1008;
        }
        offsetY = TopView::TEXTURE_Y;
    }

    _uv = QVector<QVector2D>();
    _uv << QVector2D(startX, offsetY) << QVector2D(startX, 0) << QVector2D(offsetX, 0) << QVector2D(offsetX, 0) << QVector2D(offsetX, offsetY) << QVector2D(startX, offsetY);

    _vbo.bind();
    _vbo.allocate(_vertices.constData(), 18 * sizeof(GLfloat));

    _tvbo.bind();
    _tvbo.allocate(_uv.constData(), 12 * sizeof(GLfloat));

    QOpenGLVertexArrayObject::Binder vaoBinder(&_vao);

    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glEnableVertexAttribArray(0);
    f->glBindBuffer(GL_ARRAY_BUFFER, _vbo.bufferId());
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    f->glEnableVertexAttribArray(1);
    f->glBindBuffer(GL_ARRAY_BUFFER, _tvbo.bufferId());
    f->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    _vbo.release();
    _tvbo.release();

    if (_animated) {
        if (_drawtype == DrawType::TEXTURE) {
            if (!_scrollenabled) {
                _timer->stop();
            }
            update();
        } else {
            _timer->start();
        }
    } else {
        if (_drawtype == DrawType::TEXTURE && !_scrollenabled) {
            _timeractivated = false;
        } else {
            _timeractivated = true;
        }
    }
}

void TopView::setFrameType() {
    _textureimage = QImage(_texturepath).mirrored();
    if (static_cast<FrameType>(SourceFile::topframe) != FrameType::SINGLE) {
        _textureimage = _textureimage.copy(QRect(0, 0, 1008, 256));
    }
    delete _texture;
    _texture = new QOpenGLTexture(_textureimage);

    checkUpdate();
}

void TopView::setTexture(QString path) {
    QImage image = QImage(QString(path)).mirrored();
    if (!image.isNull() && !path.isEmpty()) {
        if (static_cast<FrameType>(SourceFile::topframe) != FrameType::SINGLE && _drawtype == DrawType::TEXTURE) {
            _textureimage = image.copy(QRect(0, 0, 1008, 256));
        } else {
            _textureimage = image;
        }
        delete _texture;
        _texture = new QOpenGLTexture(_textureimage);
    } else if (_drawtype != DrawType::TEXTURE) {
        _textureimage = QImage(DEFAULT_TEXTURE).mirrored();
        delete _texture;
        _texture = new QOpenGLTexture(_textureimage);
    }

    checkUpdate();
}

void TopView::setTextureExt(QString path) {
    QImage image = QImage(QString(path)).mirrored();
    if (!image.isNull() && !path.isEmpty()) {
        _textureextimage = image;
        delete _textureext;
        _textureext = new QOpenGLTexture(_textureextimage);
    } else if (_drawtype != DrawType::TEXTURE) {
        _textureextimage = QImage(DEFAULT_TEXTURE).mirrored();
        delete _textureext;
        _textureext = new QOpenGLTexture(_textureextimage);
    }

    checkUpdate();
}

void TopView::setColor(QColor color) {
    _color = color;
    update();
}

void TopView::setAlpha(float value) {
    _alpha = value;
    update();
}

void TopView::setGradientAlpha(float value) {
    _gradientalpha = value;
    update();
}

void TopView::setGradientBrightness(float value) {
    _gradientbrightness = value;
    update();
}

void TopView::setAlphaExt(float value) {
    _alphaext = value;
    update();
}

void TopView::setMainColor(QColor value) {
    _main = value;
    update();
}

void TopView::setShadowColor(QColor value) {
    _shadow = value;
    update();
}

void TopView::setTextMainColor(QColor value) {
    _textcolor = value;
    update();
}

void TopView::updateColors(bool defaultValue) {
    if (defaultValue) {
        _main = DEFAULT_MAIN_COLOR;
        _shadow = DEFAULT_SHADOW_COLOR;
        _textcolor = DEFAULT_TEXTMAIN_COLOR;
    } else {
        _main = SourceFile::topcornercolors["main"];
        _shadow = SourceFile::topcornercolors["shadow"];
        _textcolor = SourceFile::topcornercolors["textmain"];
    }
    update();
}

void TopView::setFolderMainColor(QColor value) {
    _foldermain = value;
    checkUpdate();
}

void TopView::setFolderDarkColor(QColor value) {
    _folderdark = value;
    checkUpdate();
}

void TopView::setFolderLightColor(QColor value) {
    _folderlight = Preview::colormix(_foldermain, value);
    checkUpdate();
}

void TopView::updateFolderColors(bool defaultValue) {
    if (defaultValue) {
        _foldermain = FolderIcon::DEFAULT_FOLDER_MAIN;
        _folderlight = Preview::colormix(FolderIcon::DEFAULT_FOLDER_MAIN, FolderIcon::DEFAULT_FOLDER_LIGHT);
        _folderdark = FolderIcon::DEFAULT_FOLDER_DARK;
    } else {
        _foldermain = SourceFile::foldercolors["main"];
        _folderlight = Preview::colormix(SourceFile::foldercolors["light"], _foldermain);
        _folderdark = SourceFile::foldercolors["dark"];
    }
}

void TopView::setFileColor(QColor value) {
    _filecolor = value;
    checkUpdate();
}

void TopView::updateFileColors(bool defaultValue) {
    if (defaultValue) {
        _filecolor = GridWidget::DEFAULT_FILE_MAIN;
    } else {
        _filecolor = SourceFile::filecolors["main"];
    }
}

void TopView::setDemoMainColor(QColor value) {
    _demomain = value;
    checkUpdate();
}

void TopView::setDemoTextMainColor(QColor value) {
    _demotextmain = value;
    checkUpdate();
}

void TopView::updateDemoColors(bool defaultValue) {
    if (defaultValue) {
        _demomain = DEFAULT_DEMO_MAIN_COLOR;
        _demotextmain = DEFAULT_DEMO_TEXTMAIN_COLOR;
    } else {
        _demomain = SourceFile::demotextcolors["main"];
        _demotextmain = SourceFile::demotextcolors["textmain"];
    }
    checkUpdate();
}

float TopView::scrollpos() {
    return float(_scrollpos);
}

void TopView::setScrollPos(float value) {
    _scrollpos = GLfloat(value);
    checkUpdate();
}

void TopView::resizeGL(int, int height) {
    if (height == 0) {
        height = 1;
    }

    GLfloat h = GLfloat(Preview::TOP_SCREEN_HEIGHT) / GLfloat(Preview::TOP_SCREEN_WIDTH);

    _pMatrix.setToIdentity();
    _pMatrix.ortho(-0.5f, 0.5f, -0.5f * h, 0.5f * h, -3.0f, 1000.0f);

    _matrix3D.setToIdentity();
    _matrix3D.perspective(30.0, (float) Preview::TOP_SCREEN_WIDTH / (float) Preview::TOP_SCREEN_HEIGHT, 0.001, 10);

    glViewport(0, 0, Preview::TOP_SCREEN_WIDTH, Preview::TOP_SCREEN_HEIGHT);
}

void TopView::paintGL() {
    QOpenGLFunctions glFuncs(QOpenGLContext::currentContext());
    glFuncs.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glFuncs.glEnable(GL_CULL_FACE);
    glFuncs.glEnable(GL_BLEND);
    glFuncs.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glFuncs.glClearColor(0, 0, 0, 1);
    glFuncs.glColorMask(1, 1, 1, 0);

    QMatrix4x4 mMatrix;
    QMatrix4x4 vMatrix;
    QMatrix4x4 folderVMatrix;
    QMatrix4x4 folderMMatrix;

    QMatrix4x4 cameraTransformation;
    float rotation = 0;
    float bounce = 1;
    if (_animated) {
        QDateTime date = QDateTime::currentDateTime();
        date.setTimeSpec(Qt::OffsetFromUTC);
        rotation = -fmod((date.time().second() + date.time().msec() / 1000.0) * 60, 360);
        bounce = (sin(2 * ((date.time().minute() % 6) * 60 + date.time().second() + date.time().msec() / 1000.0)) + 1) / 2;
    }

    QVector3D cameraPosition = cameraTransformation * QVector3D(0, 0, 2.5);
    QVector3D cameraUpDirection = cameraTransformation * QVector3D(0, 1, 0);

    folderVMatrix.lookAt(cameraPosition, QVector3D(0, 0, 0), cameraUpDirection);

    folderMMatrix.rotate(rotation, 0, 1, 0);
    folderMMatrix.scale(0.09);
    folderMMatrix.translate(0, bounce - 1, 0);

    QOpenGLVertexArrayObject::Binder vaoBinder(&_vao);

    if (_drawtype == DrawType::TEXTURE) {
        _textureshader->bind();

        _textureshader->setUniformValue("mvpMatrix", _pMatrix *vMatrix * mMatrix);

        _texture->bind();

        GLfloat posX = _scrollpos;
        if (static_cast<FrameType>(SourceFile::topframe) == FrameType::SINGLE) {
            posX = 0;
        }
        _textureshader->setUniformValue(_texturetimeid, posX);

        glFuncs.glDrawArrays(GL_TRIANGLES, 0, 6);
        _textureshader->release();
    } else {
        _tileshader->bind();

        _tileshader->setUniformValue("mvpMatrix", _pMatrix * vMatrix * mMatrix);

        _tileshader->setUniformValue(_colorid, _color);
        _tileshader->setUniformValue("gradientbrightness", _gradientbrightness);
        _tileshader->setUniformValue("gradientalpha", _gradientalpha);
        _tileshader->setUniformValue("alpha", _alpha);
        _tileshader->setUniformValue("alphaext", _alphaext);

        if (!needsUpdate()) {
            QDateTime date = QDateTime::currentDateTime();
            GLfloat timeDelta = ((date.time().minute() % 4) * 60 + date.time().second()) * 1000 + date.time().msec();
            _tileshader->setUniformValue(_timeid, timeDelta);
        }

        glFuncs.glActiveTexture(GL_TEXTURE0);
        _texture->bind();
        _tileshader->setUniformValue("image", 0);

        glFuncs.glActiveTexture(GL_TEXTURE1);
        _textureext->bind();
        _tileshader->setUniformValue("ext", 1);

        glFuncs.glDrawArrays(GL_TRIANGLES, 0, 6);

        _tileshader->release();
        
        glFuncs.glActiveTexture(GL_TEXTURE0);
    }

    _vao.release();

    if (_showFolder) {
        glFuncs.glEnable(GL_DEPTH_TEST);
        QOpenGLVertexArrayObject::Binder fvaoBinder(&_fvao);

        _foldershader->bind();
        _foldershader->setUniformValue("mvpMatrix", _matrix3D * folderVMatrix * folderMMatrix);
        _foldershader->setUniformValue("mMatrix", folderMMatrix);
        _foldershader->setUniformValue(_foldermainid, _foldermain);
        _foldershader->setUniformValue(_folderdarkid, _folderdark);
        _foldershader->setUniformValue(_folderlightid, _folderlight);

        glDrawArrays(GL_TRIANGLES, 0, _foldervertices.size());

        _fvao.release();
        QOpenGLVertexArrayObject::Binder filevaoBinder(&_filevao);

        _foldershader->setUniformValue(_foldermainid, _filecolor);
        _foldershader->setUniformValue(_folderdarkid, _filecolor);
        _foldershader->setUniformValue(_folderlightid, _filecolor);

        glDrawArrays(GL_TRIANGLES, 0, _filevertices.size());

        _foldershader->release();

        _filevao.release();
    }

    glFuncs.glDisable(GL_CULL_FACE);
    glFuncs.glDisable(GL_DEPTH_TEST);
    glFuncs.glDisable(GL_BLEND);

    QPainter p(this);
    drawForeground(&p);
    p.end();
}

void TopView::drawButtons(QPainter *p) {
    QPainterPath path;

    int buttonOffset = Preview::TOP_SCREEN_HEIGHT - 23;

    // Draw button faces
    path.moveTo(0, 23 + buttonOffset);
    path.lineTo(0, buttonOffset);
    path.lineTo(74, buttonOffset);
    path.quadTo(80, buttonOffset, 82, 10 + buttonOffset);
    path.lineTo(86, 23 + buttonOffset);
    path.moveTo(200, 23 + buttonOffset);
    path.moveTo(314, 23 + buttonOffset);
    path.lineTo(318, 10 + buttonOffset);
    path.quadTo(320, buttonOffset, 326, buttonOffset);
    path.lineTo(400, buttonOffset);
    path.lineTo(400, 23 + buttonOffset);
    path.moveTo(0, 23 + buttonOffset);

    QColor adjustedshadow = _shadow;
    adjustedshadow.setAlpha(0x33);
    QColor adjustedmain = _main;
    adjustedmain.setAlpha(0xCC);
    p->setPen(adjustedshadow);
    p->setBrush(adjustedmain);
    p->drawPath(path);

    int cameraOffsetY = buttonOffset + 6;
    int cameraOffsetX = 15;

    // Draw corner button icons
    path = QPainterPath();

    // Draw the left icon
    path.moveTo(9 + cameraOffsetX, 12 + cameraOffsetY);
    path.lineTo(2 + cameraOffsetX, 12 + cameraOffsetY);
    path.quadTo(cameraOffsetX, 12 + cameraOffsetY, cameraOffsetX, 10 + cameraOffsetY);
    path.lineTo(cameraOffsetX, 2 + cameraOffsetY);
    path.quadTo(cameraOffsetX, cameraOffsetY, 2 + cameraOffsetX, cameraOffsetY);
    path.lineTo(15 + cameraOffsetX, cameraOffsetY);
    path.quadTo(17 + cameraOffsetX, cameraOffsetY, 17 + cameraOffsetX, 2 + cameraOffsetY);

    QPen pen;
    pen.setColor(_textcolor);
    pen.setWidthF(1.8);

    p->setPen(pen);
    p->setBrush(QColor(Qt::transparent));
    p->drawPath(path);

    path = QPainterPath();

    path.addRoundedRect(14 + cameraOffsetX, 4 + cameraOffsetY, 6, 4, 3, 3);

    p->setPen(QColor(Qt::transparent));
    p->setBrush(_textcolor);
    p->drawPath(path);

    path = QPainterPath();

    QPainterPath camera;

    camera.addRoundedRect(11 + cameraOffsetX, 6 + cameraOffsetY, 12, 8, 1.5, 1.5);

    QPainterPath cameraHole;
    cameraHole.addEllipse(14.75 + cameraOffsetX, 7.75 + cameraOffsetY, 4.5, 4.5);

    path = camera.subtracted(cameraHole);

    p->drawPath(path);

    path = QPainterPath();

    path.addEllipse(36 + cameraOffsetX, cameraOffsetY - 1, 14, 14);

    QFont font = QFont("Nimbus Sans");
    font.setPixelSize(11);

    path.addText(39.3 + cameraOffsetX, 10.5 + cameraOffsetY, font, "Y");

    pen.setWidthF(1.6);

    p->setBrush(QColor(Qt::transparent));
    p->setPen(pen);
    p->drawPath(path);

    // Draw the right icon
    path = QPainterPath();

    cameraOffsetY -= 1;
    cameraOffsetX = 326;

    path.addRoundedRect(4 + cameraOffsetX, cameraOffsetY, 8, 4, 3, 3);

    p->setBrush(_textcolor);
    p->setPen(QColor(Qt::transparent));
    p->drawPath(path);

    camera = QPainterPath();
    camera.addRoundedRect(cameraOffsetX, 2 + cameraOffsetY, 16, 12, 1.5, 1.5);

    cameraHole = QPainterPath();
    cameraHole.addEllipse(4.75 + cameraOffsetX, 4.75 + cameraOffsetY, 6.5, 6.5);

    path = camera.subtracted(cameraHole);

    p->drawPath(path);

    path = QPainterPath();

    path.moveTo(22.5 + cameraOffsetX, 14 + cameraOffsetY);
    path.quadTo(23.5 + cameraOffsetX, cameraOffsetY + 0.5, 24.5 + cameraOffsetX, cameraOffsetY + 0.5);
    path.lineTo(36.5 + cameraOffsetX, cameraOffsetY + 0.5);
    path.quadTo(37.5 + cameraOffsetX, 3.5 + cameraOffsetY, 35.5 + cameraOffsetX, 14 + cameraOffsetY);
    path.lineTo(22.5 + cameraOffsetX, 14 + cameraOffsetY);

    path.moveTo(69 + cameraOffsetX, 14 + cameraOffsetY);
    path.quadTo(68 + cameraOffsetX, cameraOffsetY + 0.5, 67 + cameraOffsetX, cameraOffsetY + 0.5);
    path.lineTo(55 + cameraOffsetX, cameraOffsetY + 0.5);
    path.quadTo(54 + cameraOffsetX, 3.5 + cameraOffsetY, 56 + cameraOffsetX, 14 + cameraOffsetY);
    path.lineTo(69 + cameraOffsetX, 14 + cameraOffsetY);
    
    font.setPixelSize(11);
    path.addText(26.5 + cameraOffsetX, 11.5 + cameraOffsetY, font, "L");
    path.addText(58 + cameraOffsetX, 11.5 + cameraOffsetY, font, "R");

    p->setBrush(QColor(Qt::transparent));
    p->setPen(pen);
    p->drawPath(path);

    //Plus sign because the font isn't quite right
    path = QPainterPath();
    path.moveTo(42 + cameraOffsetX, 7 + cameraOffsetY);
    path.lineTo(50 + cameraOffsetX, 7 + cameraOffsetY);
    path.moveTo(46 + cameraOffsetX, 3 + cameraOffsetY);
    path.lineTo(46 + cameraOffsetX, 11 + cameraOffsetY);

    pen.setWidthF(2.0);
    p->setPen(pen);
    p->drawPath(path);
}

void TopView::drawStatus(QPainter *p) {
    p->drawImage(368, -6, *_battery);

    QPainterPath path;

    // Draw wifi icons
    path.addRect(3, 7, 3, 11);
    path.addRect(8, 12, 5, 6);
    path.addRect(13, 7, 5, 11);
    path.addRect(18, 2, 5, 16);
    path.addEllipse(1.5, 1, 6, 6);

    p->setBrush(QColor("#28B6F2"));
    p->setPen(QColor(Qt::white));
    p->drawPath(path);

    path = QPainterPath();

    QRect internetRect = QRect(26, 1, 108, 17);
    path.addRoundedRect(internetRect, 4, 4);

    QLinearGradient gradient(QPointF(0, 2), QPointF(0, 19));
    gradient.setColorAt(0, QColor("#32B2EF"));
    gradient.setColorAt(0.4, QColor("#54C2F6"));
    gradient.setColorAt(0.41, QColor("#2BAFED"));
    gradient.setColorAt(1, QColor("#37B4F0"));

    p->setBrush(gradient);
    p->drawPath(path);

    QFont font = QFont("Nimbus Sans");
    font.setPixelSize(12.5);
    font.setLetterSpacing(QFont::AbsoluteSpacing, 1);

    p->setBrush(QColor(Qt::transparent));
    p->setFont(font);
    QRect textRect = internetRect;
    textRect.setY(1);
    p->drawText(textRect, Qt::AlignCenter, tr("Internet", "Internet status text in the top screen."));

    // Draw coin icon
    gradient = QLinearGradient(QPointF(0, 2), QPointF(0, 20));
    gradient.setColorAt(0, QColor("#B4AD03"));
    gradient.setColorAt(1, QColor("#AA7900"));

    p->setBrush(gradient);
    p->drawEllipse(145, 2, 17, 17);

    p->setBrush(QColor(Qt::transparent));
    p->drawEllipse(147, 4, 13, 13);

    QPainterPath frame = QPainterPath();

    frame.addRoundedRect(149.5, 6.5, 8, 8, 1, 1);

    QPainterPath screens = QPainterPath();
    screens.addRect(151.5, 7.5, 4, 2);
    screens.addRect(151.75, 11.5, 3.5, 2);

    path = frame.subtracted(screens);

    p->setBrush(QColor(Qt::white));
    p->setPen(QColor(Qt::transparent));
    p->drawPath(path);

    // Draw text
    font.setPixelSize(16);
    //font.setLetterSpacing(QFont::AbsoluteSpacing, 1);

    path = QPainterPath();
    path.addText(166, 16, font, "300");

    QLocale locale;
    QString dateTime = locale.toString(QDateTime::currentDateTime(), "d/MM (ddd) HH:mm");

    QFontMetrics dateMetrics(font);

    int dateWidth = dateMetrics.horizontalAdvance(dateTime);

    path.addText(365 - dateWidth, 16, font, dateTime);

    QPen pen((QColor(Qt::white)));
    pen.setWidth(2);
    QBrush brush(QColor("#343434"));

    p->strokePath(path, pen);
    p->fillPath(path, brush);
}

void TopView::drawText(QPainter *p) {
    QLinearGradient gradient = QLinearGradient(QPointF(0, Preview::TOP_SCREEN_HEIGHT - 20), QPointF(0, Preview::TOP_SCREEN_HEIGHT));

    QColor transparent = _demomain;
    transparent.setAlpha(0);
    gradient.setColorAt(0, transparent);
    gradient.setColorAt(1, _demomain);

    p->fillRect(0, Preview::TOP_SCREEN_HEIGHT - 20, Preview::TOP_SCREEN_WIDTH, 20, QBrush(gradient));

    QFont font = QFont("Nimbus Sans");
    font.setPixelSize(16);
    QFontMetrics metrics(font);

    int scrollWidth = metrics.horizontalAdvance(_text);

    p->setFont(font);

    p->setPen(_demotextmain);
    if (scrollWidth > Preview::TOP_SCREEN_WIDTH) {
        scrollWidth += DEMO_OFFSET;
        if (_animated && _scrollenabled)
            _demoscrollpos -= 3;
        _demoscrollpos = _demoscrollpos % scrollWidth;
        p->drawText(QRect(scrollWidth + _demoscrollpos, Preview::TOP_SCREEN_HEIGHT - 20, scrollWidth, 20), Qt::AlignVCenter, _text);
    } else {
        _demoscrollpos = 0;
    }

    p->drawText(QRect(_demoscrollpos, Preview::TOP_SCREEN_HEIGHT - 20, scrollWidth, 20), Qt::AlignVCenter, _text);
}

void TopView::drawFolderLabel(QPainter *p) {
    QColor shadow1 = QColor("#20000000");
    Preview::drawRoundedRectShadow(*p, shadow1, QRect(88, 163, 232, 54), QPointF(14, 14), 4);

    QPainterPath path;

    path.addRoundedRect(92, 166, 224, 46, 10, 10);

    p->setBrush(QColor(Qt::white));
    p->setPen(QColor(Qt::transparent));

    p->drawPath(path);

    QFont font = QFont("Nimbus Sans");
    font.setPixelSize(13);

    p->setFont(font);
    p->setPen(QColor(Qt::black));
    p->setBrush(QColor(Qt::black));
    p->drawText(QRect(92, 166, 224, 46), Qt::AlignCenter, _foldertitle);
}

void TopView::drawForeground(QPainter *p) {
    p->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    if (_text.isEmpty()) {
        drawButtons(p);
    } else {
        drawText(p);
    }
    if (_showFolder) {
        drawFolderLabel(p);
    }
    drawStatus(p);
}

void TopView::onIconChange(QWidget *icon) {
    GridIcon *gridIcon = dynamic_cast<GridIcon*>(icon);
    FolderIcon *folderIcon = dynamic_cast<FolderIcon*>(icon);
    _demoscrollpos = DEMO_OFFSET;
    _scrollenabled = false;
    _showFolder = false;

    if (gridIcon != nullptr) {
        _demotimer->start();
        _text = gridIcon->demotext();
        if (!needsUpdate()) {
            update();
        }
    } else {
        if (folderIcon != nullptr) {
            _showFolder = true;
            _foldertitle = QString::number(folderIcon->index()) + " " + tr("(New)", "Appears in the folder tooltip in the top screen when a folder is selected.");
        }
        _text = "";
        if (_drawtype == DrawType::TEXTURE && _scrollenabled) {
            _timer->stop();
        }
    }
    checkUpdate();
}

void TopView::onFolderStateChanged(bool open) {
    _showFolder = !open;
    checkUpdate();
}

void TopView::onScrollPositionChange(int pos) {
    _scrollpos = static_cast<FrameType>(SourceFile::topframe) == FrameType::SLOWSCROLL ? GLfloat(pos) * Preview::SLOWSCROLL_DAMPING : GLfloat(pos);
    checkUpdate();
}

void TopView::onPageScroll(QPointF start, QPointF end) {
    float startX = start.x();
    float endX = end.x();

    if (static_cast<FrameType>(SourceFile::topframe) == FrameType::SLOWSCROLL) {
        startX *= Preview::SLOWSCROLL_DAMPING;
        endX *= Preview::SLOWSCROLL_DAMPING;
    }
    _scrollanimation->setStartValue(startX);
    _scrollanimation->setEndValue(endX);
    _scrollanimation->start();
}

void TopView::enableScrolling() {
    _demotimer->stop();
    _scrollenabled = true;
    if (_drawtype == DrawType::TEXTURE) {
        _timer->start();
    }
}

void TopView::cleanup() {
    if (_tileshader == nullptr || _textureshader == nullptr) {
        return;
    }

    if (_foldershader == nullptr) {
        return;
    }

    makeCurrent();
    _vbo.destroy();
    _tvbo.destroy();
    _fvbo.destroy();
    _filevbo.destroy();
    delete _timer;
    delete _texture;
    delete _textureext;
    delete _tileshader;
    delete _textureshader;
    delete _foldershader;
    _tileshader = 0;
    _textureshader = 0;
    _foldershader = 0;
    doneCurrent();
}
