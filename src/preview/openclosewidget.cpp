// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include "openclosewidget.h"
#include "emptyicon.h"
#include "gridwidget.h"
#include "gridicon.h"
#include "preview.h"

OpenCloseWidget::OpenCloseWidget(QWidget *parent) : QFrame(parent) {
    _isFolder = false;
    _isGrid = false;
    _flexbutton = new OpenCloseButton(this);
    _leftbutton = new OpenCloseButton(this);
    _closebutton = new OpenCloseButton(this);

    _leftbutton->setGeometry(0, 0, 106, 26);
    _leftbutton->setMode(OpenCloseButton::Mode::LEFT);

    _closebutton->setGeometry(0, 0, 106, 26);
    _closebutton->setText(tr("Close", "Bottom screen close button"));
    _closebutton->setIsClose(true);
    _closebutton->setMode(OpenCloseButton::Mode::LEFT);

    showGrid();

    connect(_flexbutton, &QAbstractButton::clicked, this, &OpenCloseWidget::flexButtonClicked);
    connect(_closebutton, &QAbstractButton::clicked, this, &OpenCloseWidget::closeButtonClicked);
}

void OpenCloseWidget::flexButtonClicked() {
    if (_isFolder) {
        emit openFolder();
    } else if (_isGrid) {
        emit playLaunch();
    } else {
        emit playFolder();
    }
}

void OpenCloseWidget::closeButtonClicked() {
    emit playClose();
}

void OpenCloseWidget::showFolder() {
    _isFolder = true;
    _flexbutton->setGeometry(106, 0, Preview::BOTTOM_SCREEN_WIDTH, 26);
    _flexbutton->setText(tr("Open", "Bottom screen open button (when a folder is selected)"));
    _flexbutton->setMode(OpenCloseButton::Mode::RIGHT);

    _leftbutton->setText(tr("Settings", "Bottom screen open button (on the left when a folder is selected)"));

    _leftbutton->show();

    _closebutton->hide();
}

void OpenCloseWidget::showGrid() {
    _isGrid = true;
    _flexbutton->setGeometry(106, 0, Preview::BOTTOM_SCREEN_WIDTH, 26);
    _flexbutton->setText(tr("Start", "Bottom screen open button"));
    _flexbutton->setMode(OpenCloseButton::Mode::RIGHT);

    _leftbutton->hide();

    _closebutton->show();
}

void OpenCloseWidget::reset() {
    _flexbutton->setGeometry(0, 0, Preview::BOTTOM_SCREEN_WIDTH, 26);
    _flexbutton->setText(tr("Interactive Preview", "Bottom screen open/close button (when nothing is selected)"));
    _flexbutton->setMode(OpenCloseButton::Mode::SINGLE);

    _leftbutton->hide();

    _closebutton->hide();
}

void OpenCloseWidget::setDarkColor(QColor value) {
    _leftbutton->setDarkColor(value);
    _flexbutton->setDarkColor(value);
}

void OpenCloseWidget::setMainColor(QColor value) {
    _leftbutton->setMainColor(value);
    _flexbutton->setMainColor(value);
}

void OpenCloseWidget::setLightColor(QColor value) {
    _leftbutton->setLightColor(value);
    _flexbutton->setLightColor(value);
}

void OpenCloseWidget::setTextShadowColor(QColor value) {
    _leftbutton->setTextShadowColor(value);
    _flexbutton->setTextShadowColor(value);
}

void OpenCloseWidget::setTextMainColor(QColor value) {
    _leftbutton->setTextMainColor(value);
    _flexbutton->setTextMainColor(value);
}

void OpenCloseWidget::setTextSelectedColor(QColor value) {
    _leftbutton->setTextSelectedColor(value);
    _flexbutton->setTextSelectedColor(value);
    _leftbutton->setDown(true);
    _flexbutton->setDown(true);
}

void OpenCloseWidget::setTextShadowPos(float value) {
    float formattedValue = 20 * value / 10000.0 - 10;
    _leftbutton->setTextShadowPos(formattedValue);
    _flexbutton->setTextShadowPos(formattedValue);
}

void OpenCloseWidget::setCloseDarkColor(QColor value) {
    _closebutton->setDarkColor(value);
}

void OpenCloseWidget::setCloseMainColor(QColor value) {
    _closebutton->setMainColor(value);
}

void OpenCloseWidget::setCloseLightColor(QColor value) {
    _closebutton->setLightColor(value);
}

void OpenCloseWidget::setCloseTextShadowColor(QColor value) {
    _closebutton->setTextShadowColor(value);
}

void OpenCloseWidget::setCloseTextMainColor(QColor value) {
    _closebutton->setTextMainColor(value);
}

void OpenCloseWidget::setCloseTextSelectedColor(QColor value) {
    _closebutton->setTextSelectedColor(value);
    _closebutton->setDown(true);
}

void OpenCloseWidget::setCloseTextShadowPos(float value) {
    float formattedValue = 20 * value / 10000.0 - 10;
    _closebutton->setTextShadowPos(formattedValue);
}

void OpenCloseWidget::updateColors(bool defaultValue) {
    _leftbutton->updateColors(defaultValue);
    _flexbutton->updateColors(defaultValue);
    _closebutton->updateColors(defaultValue);
    _leftbutton->setDown(false);
    _flexbutton->setDown(false);
    _closebutton->setDown(false);
}

void OpenCloseWidget::onCursorChange(QWidget *icon) {
    _isFolder = false;
    _isGrid = false;
    show();
    GridWidget *grid = dynamic_cast<GridWidget*>(QObject::sender());
    if (dynamic_cast<EmptyIcon*>(icon) != nullptr) {
        reset();
        if (grid->isFolder()) {
            hide();
        }
    } else if (dynamic_cast<GridIcon*>(icon) != nullptr)
        showGrid();
    else
        showFolder();
}

OpenCloseWidget::~OpenCloseWidget() {
    delete _flexbutton;
    delete _leftbutton;
    delete _closebutton;
}
