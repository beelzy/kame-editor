// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef BOTTOMWIDGET_H
#define BOTTOMWIDGET_H

#include <QFrame>
#include <QMouseEvent>

#include "fakebutton.h"
#include "zoomwidget.h"
#include "openclosewidget.h"
#include "arrowbutton.h"
#include "gridwidget.h"
#include "folderwidget.h"
#include "cursoroverlay.h"
#include "gametextwidget.h"

class BottomWidget : public QFrame {
    Q_OBJECT

    public:
        explicit BottomWidget(QWidget *parent = 0);
        ~BottomWidget();

        GridWidget *gridwidget();
        GridWidget *foldergrid();
        FolderBackButton *folderbackbutton();
        CursorWidget *cursor();
        OpenCloseWidget *openclosewidget();
        GameTextWidget *gametextwidget();

    signals:
        void folderStateChanged(bool open);

    public slots:
        void onFolderOpen();
        void onFolderClose();

        void setArrowDarkColor(QColor value);
        void setArrowMainColor(QColor value);
        void setArrowLightColor(QColor value);

        void setArrowBorderColor(QColor value);
        void setArrowUnpressedColor(QColor value);
        void setArrowPressedColor(QColor value);

        void updateArrowBaseColors(bool defaultValue);
        void updateArrowColors(bool defaultValue);

        void setFakeButtonDarkColor(QColor value);
        void setFakeButtonMainColor(QColor value);
        void setFakeButtonLightColor(QColor value);
        void setFakeButtonIconMainColor(QColor value);
        void setFakeButtonIconLightColor(QColor value);
        void updateFakeButtonColors(bool defaultValue);

    protected:
        virtual void mousePressEvent(QMouseEvent *event);
        virtual void mouseMoveEvent(QMouseEvent *event);

        void connectGrid();
        void disconnectGrid();
        void connectFolder();
        void disconnectFolder();

    private:
        FakeButton *_fakebutton;
        ZoomWidget *_zoomwidget;
        OpenCloseWidget *_openclosewidget;
        ArrowButton *_leftarrow;
        ArrowButton *_rightarrow;
        GridWidget *_gridwidget;
        FolderWidget *_folderwidget;
        CursorOverlay *_gridcursor;
        GameTextWidget *_gametext;
};

#endif // BOTTOMWIDGET_H
