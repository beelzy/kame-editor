// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QVariant>

#include "folderbackbutton.h"
#include "preview.h"
#include "../sourcefile.h"

const QColor FolderBackButton::DEFAULT_DARK_COLOR = QColor("#2C2823");
const QColor FolderBackButton::DEFAULT_MAIN_COLOR = QColor("#524D47");
const QColor FolderBackButton::DEFAULT_LIGHT_COLOR = QColor("#8C8781");

const QColor FolderBackButton::DEFAULT_TEXT_SHADOW = QColor("#2C2823");
const QColor FolderBackButton::DEFAULT_TEXT_MAIN = QColor(Qt::white);
const QColor FolderBackButton::DEFAULT_TEXT_SELECTED = QColor("#E3E3E3");

const float FolderBackButton::DEFAULT_TEXT_SHADOW_POS = 1;

FolderBackButton::FolderBackButton(QWidget *parent) : QAbstractButton(parent) {
    updateColors(true);
}

void FolderBackButton::setDarkColor(QColor value) {
    _dark = value;
    update();
}

void FolderBackButton::setMainColor(QColor value) {
    _main = value;
    update();
}

void FolderBackButton::setLightColor(QColor value) {
    _light = value;
    update();
}

void FolderBackButton::setTextShadowColor(QColor value) {
    _textshadow = value;
    update();
}

void FolderBackButton::setTextMainColor(QColor value) {
    _textmain = value;
    update();
}

void FolderBackButton::setTextSelectedColor(QColor value) {
    _textselected = value;
    update();
    setDown(true);
}

void FolderBackButton::setTextShadowPos(float value) {
    float formattedValue = 20 * value / 10000.0 - 10;
    _textshadowpos = formattedValue;
    update();
}

void FolderBackButton::updateColors(bool defaultValue) {
    if (defaultValue) {
        _dark = DEFAULT_DARK_COLOR;
        _main = DEFAULT_MAIN_COLOR;
        _light = DEFAULT_LIGHT_COLOR;
        _textshadow = DEFAULT_TEXT_SHADOW;
        _textmain = DEFAULT_TEXT_MAIN;
        _textselected = DEFAULT_TEXT_SELECTED;
        _textshadowpos = DEFAULT_TEXT_SHADOW_POS;
    } else {
        _dark = qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["dark"]);
        _main = qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["main"]);
        _light = qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["light"]);
        _textshadow = qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["textshadow"]);
        _textmain = qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["textmain"]);
        _textselected = qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["textselected"]);
        _textshadowpos = SourceFile::folderbackbuttoncolors["textshadowpos"].toFloat();
    }
    setDown(false);
    update();
}

void FolderBackButton::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    QPainterPath path;

    path.addRoundedRect(0, 0, 68, 30, 10, 10);

    QColor color1(Preview::colormix(_main, _light));
    QColor color2(_main);
    p.setBrush(_light);

    if (isDown()) {
        p.setBrush(_dark);
        color1 = _dark;
        color2 = Preview::colormix(_dark, _main);
    }

    p.setPen(QColor(Qt::transparent));

    p.drawPath(path);
    Preview::drawRoundedRectShadow(p, color1, QRect(0, 2, 68, 30), 10, 4);
    Preview::drawRoundedRectShadow(p, color2, QRect(-6, 6, 80, 30), 10, 6);

    if (!isDown()) {
        p.setBrush(QColor(Qt::transparent));
        p.setPen(_dark);
        p.drawPath(path);
    }

    path = QPainterPath();

    path.moveTo(26, 8);
    path.lineTo(33, 2);
    path.lineTo(33, 6);
    path.arcTo(29, 6, 13, 12, 90, -180);
    path.lineTo(33, 18);
    path.lineTo(33, 15);
    path.arcTo(31, 9, 8, 6, -90, 180);
    path.lineTo(33, 9);
    path.lineTo(33, 13);
    path.lineTo(26, 8);

    if (!isDown()) {
        p.translate(0, -_textshadowpos);
        p.setPen(QColor(Qt::transparent));
        p.setBrush(_textshadow);
        p.drawPath(path);
        p.setBrush(_textmain);
        p.translate(0, _textshadowpos);
    } else {
        p.setBrush(_textselected);
        p.translate(0, 2);
    }
    p.drawPath(path);
}
