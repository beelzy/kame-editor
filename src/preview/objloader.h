// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef OBJLOADER_H
#define OBJLOADER_H

#include <QObject>

#include "../tiny_obj_loader.h"

class ObjLoader : public QObject {
    public:
        ObjLoader(QObject *parent = 0);
        QVector<QVector3D> load(QString filepath);

        struct vec3 {
            float v[3];
            vec3() {
                v[0] = 0.0f;
                v[1] = 0.0f;
                v[2] = 0.0f;
            }
        };

        static void calcNormal(float N[3], float v0[3], float v1[3], float v2[3]);
    protected:
        bool hasSmoothingGroup(const tinyobj::shape_t &shape);
        void computeSmoothingNormals(const tinyobj::attrib_t &attrib, const tinyobj::shape_t &shape, std::map<int, vec3>& smoothVertexNormals);
        void normalizeVector(vec3 &v);
};

#endif // OBJLOADER_H
