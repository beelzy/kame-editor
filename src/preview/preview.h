// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef PREVIEW_H
#define PREVIEW_H

#include <QPainter>
#include <QPainterPath>

class Preview {
    public:
        static const int TOP_SCREEN_WIDTH = 400;
        static const int TOP_SCREEN_HEIGHT = 240;
        static const int BOTTOM_SCREEN_WIDTH = 320;
        static const int BOTTOM_SCREEN_HEIGHT = 240;
        static constexpr float SLOWSCROLL_DAMPING = 0.8;

        // Use this for better shadow performance
        static void drawRoundedRectGlow(QPainter &p, QColor color, QRectF rect, float radius, float blurradius) {
            QPainterPath path;
            path.moveTo(rect.x() + radius, rect.y() - blurradius);
            path.arcTo(rect.x() - blurradius, rect.y() - blurradius, 2 * (radius + blurradius), 2 * (radius + blurradius), 90, 90);
            path.lineTo(rect.x(), rect.y() + radius);
            path.arcTo(rect.x(), rect.y(), radius * 2, radius * 2, 180, -90);
            path.lineTo(rect.x() + radius, rect.y() - blurradius);

            QRadialGradient corner = QRadialGradient(QPointF(rect.x() + radius, rect.y() + radius), radius + blurradius, QPointF(rect.x() + radius, rect.y() + radius));

            QColor color1 = color;
            color1.setAlpha(0);
            corner.setColorAt(0, color);
            corner.setColorAt(radius / (radius + blurradius), color);
            corner.setColorAt(1, color1);

            p.setPen(QColor(Qt::transparent));
            p.setBrush(corner);
            p.drawPath(path);

            path = QPainterPath();

            path.moveTo(rect.x() + rect.width() - radius, rect.y() - blurradius);
            path.arcTo(rect.x() + rect.width() - 2 * radius - blurradius, rect.y() - blurradius, 2 * (radius + blurradius), 2 * (radius + blurradius), 90, -90);
            path.lineTo(rect.x() + rect.width(), rect.y() + radius);
            path.arcTo(rect.x() + rect.width() - 2 * radius, rect.y(), radius * 2, radius * 2, 0, 90);
            path.lineTo(rect.x() + rect.width() - radius, rect.y() - blurradius);

            corner.setCenter(QPointF(rect.x() + rect.width() - radius, rect.y() + radius));
            corner.setFocalPoint(corner.center());

            p.setBrush(corner);
            p.drawPath(path);

            path = QPainterPath();

            path.moveTo(rect.x() - blurradius, rect.y() + rect.height() - radius);
            path.arcTo(rect.x() - blurradius, rect.y() + rect.height() - 2 * radius - blurradius, 2 * (radius + blurradius), 2 * (radius + blurradius), 180, 90);
            path.lineTo(rect.x() + radius, rect.y() + rect.height());
            path.arcTo(rect.x(), rect.y() + rect.height() - 2 * radius, 2 * radius, 2 * radius, -90, -90);
            path.lineTo(rect.x() - blurradius, rect.y() + rect.height() - radius);

            corner.setCenter(QPointF(rect.x() + radius, rect.y() + rect.height() - radius));
            corner.setFocalPoint(corner.center());

            p.setBrush(corner);
            p.drawPath(path);

            path = QPainterPath();

            path.moveTo(rect.x() + rect.width() - radius, rect.y() + rect.height() + blurradius);
            path.arcTo(rect.x() + rect.width() - 2 * radius - blurradius, rect.y() + rect.height() - 2 * radius - blurradius, 2 * (radius + blurradius) , 2 * (radius + blurradius), -90, 90);
            path.lineTo(rect.x() + rect.width(), rect.y() + rect.height() - radius);
            path.arcTo(rect.x() + rect.width() - 2 * radius, rect.y() + rect.height() - 2 * radius, 2 * radius, 2 * radius, 0, -90);
            path.lineTo(rect.x() + rect.width() - radius, rect.y() + rect.height() + blurradius);

            corner.setCenter(QPointF(rect.x() + rect.width() - radius, rect.y() + rect.height() - radius));
            corner.setFocalPoint(corner.center());

            p.setBrush(corner);
            p.drawPath(path);

            QLinearGradient gradient = QLinearGradient(QPointF(rect.x() - blurradius, 0), QPointF(rect.x(), 0));

            gradient.setColorAt(0, color1);
            gradient.setColorAt(1, color);

            p.fillRect(rect.x() - blurradius, rect.y() + radius, blurradius, rect.height() - 2 * radius, QBrush(gradient));

            gradient.setFinalStop(QPointF(rect.x() + rect.width(), 0));
            gradient.setStart(QPointF(rect.x() + rect.width() + blurradius, 0));

            p.fillRect(rect.x() + rect.width(), rect.y() + radius, blurradius, rect.height() - 2 * radius, QBrush(gradient));

            gradient.setStart(QPointF(0, rect.y() - blurradius));
            gradient.setFinalStop(QPointF(0, rect.y()));

            p.fillRect(rect.x() + radius, rect.y() - blurradius, rect.width() - 2 * radius, blurradius, QBrush(gradient));

            gradient.setFinalStop(QPointF(0, rect.y() + rect.height()));
            gradient.setStart(QPointF(0, rect.y() + rect.height() + blurradius));

            p.fillRect(rect.x() + radius, rect.y() + rect.height(), rect.width() - 2 * radius, blurradius, QBrush(gradient));
        }

        // radius: left, right
        static void drawRoundedRectShadow(QPainter &p, QColor color, QRect rect, QPointF radius, float blurradius) {

                QColor color1 = color;
                color1.setAlpha(0);
                QColor color2 = color;

                QPointF maxradius = QPointF(
                        std::max(float(radius.x()), blurradius),
                        std::max(float(radius.y()), blurradius)
                        );

                QLinearGradient gradient = QLinearGradient(QPointF(0, rect.y()), QPointF(0, rect.y() + rect.height()));
                gradient.setColorAt(0, color1);
                gradient.setColorAt(blurradius / rect.height(), color2);
                gradient.setColorAt((rect.height() - blurradius) / rect.height(), color2);
                gradient.setColorAt(1, color1);

                p.fillRect(rect.x() + maxradius.x(), rect.y(), rect.width() - maxradius.x() - maxradius.y(), rect.height(), QBrush(gradient));

                gradient = QLinearGradient(QPointF(rect.x(), 0), QPointF(rect.x() + maxradius.x(), 0));
                gradient.setColorAt(0, color1);
                gradient.setColorAt(blurradius / maxradius.x(), color2);
                gradient.setColorAt(1, color2);

                p.fillRect(rect.x(), rect.y() + maxradius.x(), maxradius.x(), rect.height() - 2 * maxradius.x(), QBrush(gradient));

                gradient = QLinearGradient(QPointF(rect.x() + rect.width() - maxradius.y(), 0), QPointF(rect.x() + rect.width(), 0));
                gradient.setColorAt(0, color2);
                gradient.setColorAt((maxradius.y() - blurradius) / maxradius.y(), color2);
                gradient.setColorAt(1, color1);

                p.fillRect(rect.x() + rect.width() - maxradius.y(), rect.y() + maxradius.y(), maxradius.y(), rect.height() - maxradius.y() * 2, QBrush(gradient));

                /*
                QRectF cornerradius = QRectF(
                        std::max(maxradius.x(), maxradius.width()),
                        std::max(maxradius.x(), maxradius.height()),
                        std::max(maxradius.y(), maxradius.width()),
                        std::max(maxradius.y(), maxradius.height())
                        );
                    */

            QRadialGradient cornergradient = QRadialGradient(QPointF(rect.x() + maxradius.x(), rect.y() + maxradius.x()), maxradius.x(), QPointF(rect.x() + maxradius.x(), rect.y() + maxradius.x()));
            cornergradient.setColorAt(0, color2);
            cornergradient.setColorAt(std::max(float(radius.x() - blurradius), 0.0f) / maxradius.x(), color2);
            cornergradient.setColorAt(1, color1);

            p.fillRect(rect.x(), rect.y(), maxradius.x(), maxradius.x(), QBrush(cornergradient));

            cornergradient = QRadialGradient(QPointF(rect.x() + rect.width() - maxradius.y(), rect.y() + maxradius.y()), maxradius.y(), QPointF(rect.x() + rect.width() - maxradius.y(), rect.y() + maxradius.y()));
            cornergradient.setColorAt(0, color2);
            cornergradient.setColorAt(std::max(float(radius.y() - blurradius), 0.0f) / maxradius.y(), color2);
            cornergradient.setColorAt(1, color1);

            p.fillRect(rect.x() + rect.width() - maxradius.y(), rect.y(), maxradius.y(), maxradius.y(), QBrush(cornergradient));

            cornergradient = QRadialGradient(QPointF(rect.x() + maxradius.x(), rect.y() + rect.height() - maxradius.x()), maxradius.x(), QPointF(rect.x() + maxradius.x(), rect.y() + rect.height() - maxradius.x()));
            cornergradient.setColorAt(0, color2);
            cornergradient.setColorAt(std::max(float(radius.x() - blurradius), 0.0f) / maxradius.x(), color2);
            cornergradient.setColorAt(1, color1);

            p.fillRect(rect.x(), rect.y() + rect.height() - maxradius.x(), maxradius.x(), maxradius.x(), QBrush(cornergradient));

            cornergradient = QRadialGradient(QPointF(rect.x() + rect.width() - maxradius.y(), rect.y() + rect.height() - maxradius.y()), maxradius.y(), QPointF(rect.x() + rect.width() - maxradius.y(), rect.y() + rect.height() - maxradius.y()));
            cornergradient.setColorAt(0, color2);
            cornergradient.setColorAt(std::max(float(radius.y() - blurradius), 0.0f) / maxradius.y(), color2);
            cornergradient.setColorAt(1, color1);

            p.fillRect(rect.x() + rect.width() - maxradius.y(), rect.y() + rect.height() - maxradius.y(), maxradius.y(), maxradius.y(), QBrush(cornergradient));
        }

        // radius: up, down
        static void drawFolderRect(QPainter &p, QColor color, QRect rect, QPointF radius, float blurradius) {

            QColor color1 = color;
            color1.setAlpha(0);
            QColor color2 = color;

            QPointF maxradius = QPointF(
                    std::max(float(radius.x()), blurradius),
                    std::max(float(radius.y()), blurradius)
                    );

            QLinearGradient gradient = QLinearGradient(QPointF(rect.x(), 0), QPointF(rect.x() + rect.width(), 0));
            gradient.setColorAt(0, color1);
            gradient.setColorAt(blurradius / rect.width(), color2);
            gradient.setColorAt((rect.width() - blurradius) / rect.width(), color2);
            gradient.setColorAt(1, color1);

            p.fillRect(rect.x(), rect.y() + maxradius.x(), rect.width(), rect.height() - maxradius.x() - maxradius.y(), QBrush(gradient));

            gradient = QLinearGradient(QPointF(0, rect.y()), QPointF(0, rect.y() + maxradius.x()));
            gradient.setColorAt(0, color1);
            gradient.setColorAt(blurradius / maxradius.x(), color2);
            gradient.setColorAt(1, color2);

            p.fillRect(rect.x() + maxradius.x(), rect.y(), rect.width() - 2 * maxradius.x(), maxradius.x(), QBrush(gradient));

            gradient = QLinearGradient(QPointF(0, rect.y() + rect.height() - maxradius.y()), QPointF(0, rect.y() + rect.height()));
            gradient.setColorAt(0, color2);
            gradient.setColorAt((maxradius.y() - blurradius) / maxradius.y(), color2);
            gradient.setColorAt(1, color1);

            p.fillRect(rect.x() + maxradius.y(), rect.y() + rect.height() - maxradius.y(), rect.width() - maxradius.y() * 2, maxradius.y(), QBrush(gradient));

            QRadialGradient cornergradient = QRadialGradient(QPointF(rect.x() + maxradius.x(), rect.y() + maxradius.x()), maxradius.x(), QPointF(rect.x() + maxradius.x(), rect.y() + maxradius.x()));
            cornergradient.setColorAt(0, color2);
            cornergradient.setColorAt(std::max(float(radius.x() - blurradius), 0.0f) / maxradius.x(), color2);
            cornergradient.setColorAt(1, color1);

            p.fillRect(rect.x(), rect.y(), maxradius.x(), maxradius.x(), QBrush(cornergradient));

            cornergradient = QRadialGradient(QPointF(rect.x() + rect.width() - maxradius.x(), rect.y() + maxradius.x()), maxradius.x(), QPointF(rect.x() + rect.width() - maxradius.x(), rect.y() + maxradius.x()));
            cornergradient.setColorAt(0, color2);
            cornergradient.setColorAt(std::max(float(radius.x() - blurradius), 0.0f) / maxradius.x(), color2);
            cornergradient.setColorAt(1, color1);

            p.fillRect(rect.x() + rect.width() - maxradius.x(), rect.y(), maxradius.x(), maxradius.x(), QBrush(cornergradient));

            cornergradient = QRadialGradient(QPointF(rect.x() + maxradius.y(), rect.y() + rect.height() - maxradius.y()), maxradius.y(), QPointF(rect.x() + maxradius.y(), rect.y() + rect.height() - maxradius.y()));
            cornergradient.setColorAt(0, color2);
            cornergradient.setColorAt(std::max(float(radius.y() - blurradius), 0.0f) / maxradius.y(), color2);
            cornergradient.setColorAt(1, color1);

            p.fillRect(rect.x(), rect.y() + rect.height() - maxradius.y(), maxradius.y(), maxradius.y(), QBrush(cornergradient));

            cornergradient = QRadialGradient(QPointF(rect.x() + rect.width() - maxradius.y(), rect.y() + rect.height() - maxradius.y()), maxradius.y(), QPointF(rect.x() + rect.width() - maxradius.y(), rect.y() + rect.height() - maxradius.y()));
            cornergradient.setColorAt(0, color2);
            cornergradient.setColorAt(std::max(float(radius.y() - blurradius), 0.0f) / maxradius.y(), color2);
            cornergradient.setColorAt(1, color1);

            p.fillRect(rect.x() + rect.width() - maxradius.y(), rect.y() + rect.height() - maxradius.y(), maxradius.y(), maxradius.y(), QBrush(cornergradient));
        }

        static void drawRoundedRectShadow(QPainter &p, QColor color, QRect rect, float radius, float blurradius) {
            drawRoundedRectShadow(p, color, rect, QPointF(radius, radius), blurradius);
        }

        static QColor colormix(QColor color1, QColor color2, float fraction = 0.5) {
            QColor mixed;
            mixed.setRed((color2.red() - color1.red()) * fraction + color1.red());
            mixed.setGreen((color2.green() - color1.green()) * fraction + color1.green());
            mixed.setBlue((color2.blue() - color1.blue()) * fraction + color1.blue());
            mixed.setAlpha((color2.alpha() - color1.alpha()) * fraction + color1.alpha());
            return mixed;
        }
};

#endif // PREVIEW_H
