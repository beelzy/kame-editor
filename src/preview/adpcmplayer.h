// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ADPCMPLAYER_H
#define ADPCMPLAYER_H

#include <QWidget>
#include <QProcess>
#include <QBuffer>
#include <portaudio.h>

class AdpcmPlayer : public QWidget {
    Q_OBJECT

    public:
        explicit AdpcmPlayer(QWidget *parent = 0);
        ~AdpcmPlayer();

        void loadFile(QString filepath);
        qint64 total();
        QString description();
        QString filepath();
        bool isPlaying();
        int samples();
        void setLooping(bool value);
        void setVgmstreamAvailable(bool value);
        long long toMSecs(long long duration);
        QString errors();

        typedef struct
        {
            short *buffer;
            int bufferSampleCount;
            int playbackIndex;
        }
        paTestData;

    signals:
        void fileLoaded();
        void fileError();
        void streamError();
        void positionChanged(qint64 position);
        void durationChanged(qint64 duration);
        void mediaEnd();

    public slots:
        void play();
        void pause();
        void reset();
        void unload();

    protected:
        void getLoop();
        static int paStreamCallback_map(const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData);
        int paStreamCallback(const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags);
        void readHeader(int size);
        void openStream();

    protected slots:
        void vgmstreamCallback(int exitcode, QProcess::ExitStatus statuscode);
        void metadataCallback(int exitcode, QProcess::ExitStatus statuscode);

    private:
        QString _filepath;
        QString _description;
        long long _loopstart;
        long long _loopend;
        int _numsamples;
        int _samplerate;
        int _wavsamplerate;
        int _wavnumchannels;
        int _wavbitspersample;
        int _position;
        PaSampleFormat _sampleFormat;
        bool _loopenabled;
        bool _vgmstreamFound;
        bool _streamOpened;
        QBuffer *_mediaStream;
        PaStream *_stream;
        int _wavstart;
        double _wavscale;
        bool _playing;
        QString _errors;

        QProcess *_vgmstream;
};

#endif // ADPCMPLAYER_H
