// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QPainterPath>

#include "emptyicon.h"
#include "gridwidget.h"

const QColor EmptyIcon::DEFAULT_LIGHT_COLOR = QColor(Qt::white);
const QColor EmptyIcon::DEFAULT_DARK_COLOR = QColor(Qt::black);

EmptyIcon::EmptyIcon(QWidget *parent) : QWidget(parent) {
    _isTexture = false;
    int zoomlevel = static_cast<GridWidget*>(parent)->zoomLevel();
    int iconheight = zoomlevel == 0 ? GridWidget::ICON_1X_HEIGHT : GridWidget::ICON_SIZE[zoomlevel].y();
    setFixedSize(GridWidget::SPACING[zoomlevel] + GridWidget::ICON_SIZE[zoomlevel].x(), iconheight + GridWidget::SPACING[zoomlevel]);
    _lightcolor = DEFAULT_LIGHT_COLOR;
    _darkcolor = DEFAULT_DARK_COLOR;
}

void EmptyIcon::setLightColor(QColor color) {
    _lightcolor = color;
}

void EmptyIcon::setDarkColor(QColor color) {
    _darkcolor = color;
}

void EmptyIcon::setTextureMode(bool value) {
    _isTexture = value;
}

void EmptyIcon::mousePressEvent(QMouseEvent *event) {
    event->ignore();
}

void EmptyIcon::paintEvent(QPaintEvent *) {
    int zoomlevel = static_cast<GridWidget*>(parentWidget())->zoomLevel();
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    int spacing = GridWidget::SPACING[zoomlevel] / 2.0;
    int offsety = spacing;

    if (zoomlevel == 0) {
        offsety = GridWidget::ICON_1X_HEIGHT - GridWidget::ICON_SIZE[zoomlevel].y();
    }

    // Debugging only
    /*
    p.setPen(QColor("#FF0000"));
    p.drawRect(spacing, offsety, GridWidget::ICON_SIZE[zoomlevel].x(), GridWidget::ICON_SIZE[zoomlevel].y());
    */

    QPainterPath path;

    float emptyIconSize = GridWidget::ICON_SIZE[zoomlevel].x() * GridWidget::EMPTY_ICON_SIZE;
    int posx = (GridWidget::ICON_SIZE[zoomlevel].x() - emptyIconSize) / 2;
    int posy = (GridWidget::ICON_SIZE[zoomlevel].y() - emptyIconSize) / 2;
    float ratio = GridWidget::SPACING[zoomlevel] / GridWidget::SPACING[0];
    path.addRoundedRect(spacing + posx, offsety + posy, emptyIconSize, emptyIconSize, std::max(4 * ratio, 2.0f), std::max(4 * ratio, 2.0f));


    if (_isTexture) {
        p.setBrush(QColor("#20FFFFFF"));
        p.setPen(QColor(Qt::transparent));
    } else {
        QLinearGradient gradient = QLinearGradient(QPointF(0, offsety + posy), QPointF(0, offsety + posy + emptyIconSize));

        QColor color1 = _darkcolor.isValid() ? _darkcolor : QColor(Qt::black);
        QColor color2 = color1;
        color1.setAlpha(3);
        color2.setAlpha(32);
        QColor color3 = _lightcolor.isValid() ? _lightcolor : QColor(Qt::black);
        QColor color4 = color3;
        color3.setAlpha(32);
        color4.setAlpha(56);

        gradient.setColorAt(0, color2);
        gradient.setColorAt(0.2, color1);
        gradient.setColorAt(0.8, color1);
        gradient.setColorAt(1, color3);

        p.setBrush(gradient);
        p.setPen(color4);
    }
    p.drawPath(path);
}
