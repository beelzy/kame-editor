// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>

#include "cursoroverlay.h"
#include "gridwidget.h"

CursorOverlay::CursorOverlay(QWidget *parent) : QFrame(parent) {
    setAttribute(Qt::WA_TransparentForMouseEvents);
    _cursor = new CursorWidget(this);
    _isFolder = false;
    resetCursorPosition();
    _zoomlevel = 0;
    _gridPos = QPointF();
}

CursorOverlay::~CursorOverlay() {
    delete _cursor;
}

CursorWidget *CursorOverlay::cursor() {
    return _cursor;
}

void CursorOverlay::setFolderMode(bool value) {
    _isFolder = value;
}

void CursorOverlay::changeZoom(int zoom, QMargins bounds) {
    _zoomlevel = zoom;
    _grid = bounds;

    QSize gridunit = QSize(GridWidget::ICON_SIZE[zoom].x() + GridWidget::SPACING[zoom], GridWidget::ICON_SIZE[zoom].y() + GridWidget::SPACING[zoom]);
    int rows = _isFolder ? zoom : zoom + 1;

    int indexX = round(float(_cursor->origin().x() - _grid.left()) / gridunit.width());
    indexX = indexX % GridWidget::VISIBLE_ITEMS[zoom];
    int indexY = round(float(_cursor->origin().y() - _grid.top()) / gridunit.height());
    int row = zoom == 0 ? std::max(0, indexY - 1) : indexY;

    int nearestX = _grid.left() + (GridWidget::SPACING[zoom] / 2.0) + indexX * gridunit.width();
    int nearestY = zoom == 0 ? _grid.top() + GridWidget::ICON_1X_HEIGHT - GridWidget::ICON_SIZE[zoom].y() : _grid.top() + (GridWidget::SPACING[zoom] / 2.0) + std::min(indexY, rows - 1) * gridunit.height();

    nearestY += _isFolder ? 19 : 0;

    _cursor->setZoom(zoom);
    QPointF newpos = QPointF(nearestX, nearestY);
    _cursor->setOrigin(newpos);

    if (!_isFolder) {
        _gridPos = newpos;
    }

    GridWidget *grid = dynamic_cast<GridWidget*>(QObject::sender());
    grid->setScreenIndex(indexX * rows + row);
}

void CursorOverlay::updatePosition(QPointF pos, int scrollpos) {
    int posX = pos.x() - std::abs(scrollpos) + (GridWidget::SPACING[_zoomlevel] / 2.0);
    int posY = _zoomlevel == 0 ? _grid.top() + GridWidget::ICON_1X_HEIGHT - GridWidget::ICON_SIZE[_zoomlevel].y() : pos.y() + (GridWidget::SPACING[_zoomlevel] / 2.0);
    posY += _isFolder ? 19 : 0;
    QPointF newpos = QPointF(posX, posY);
    _cursor->setOrigin(newpos);
    if (!_isFolder) {
        _gridPos = newpos;
    }
}

void CursorOverlay::resetCursorPosition() {
    if (_isFolder) {
        _cursor->setOrigin(QPointF(41, 56));
        _grid = QMargins(34, 43, 34, 40);
    } else {
        _cursor->setOrigin(QPointF(41, 96));
        _grid = QMargins(34, 17, 34, 19);
    }
}

void CursorOverlay::restoreCursorPosition() {
    _cursor->setOrigin(_gridPos);
}
