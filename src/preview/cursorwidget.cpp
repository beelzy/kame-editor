// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QPainterPath>
#include <QGraphicsDropShadowEffect>
#include <QPropertyAnimation>

#include "cursorwidget.h"
#include "gridwidget.h"
#include "../sourcefile.h"

float CursorWidget::MAX_SCALE = 1.05;
int CursorWidget::DURATION = 800;
const QColor CursorWidget::DEFAULT_COLOR = QColor("#32E79D");
const QColor CursorWidget::DEFAULT_GLOW = QColor("#4AF8C2");
const QColor CursorWidget::DEFAULT_OUTLINE = QColor("#2DBC7C");
const QColor CursorWidget::DEFAULT_LIGHT = QColor("#48F7C0");

CursorWidget::CursorWidget(QWidget *parent) : QAbstractButton(parent) {
    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect();
    shadow->setColor(DEFAULT_GLOW);
    shadow->setBlurRadius(6);
    shadow->setOffset(QPointF(0, 0));
    setGraphicsEffect(shadow);

    _scale = 1.0;
    _animated = true;

    QPropertyAnimation *scale = new QPropertyAnimation(this, "size");
    scale->setEasingCurve(QEasingCurve::InQuad);
    scale->setDuration(DURATION);
    scale->setLoopCount(-1);
    scale->setStartValue(QSize(80, 80));
    scale->setKeyValueAt(0.5, QSize(80 * MAX_SCALE, 80 * MAX_SCALE));
    scale->setEndValue(QSize(80, 80));

    QPropertyAnimation *color = new QPropertyAnimation(this, "color");
    color->setDuration(DURATION);
    color->setLoopCount(-1);

    color->setStartValue(DEFAULT_GLOW);
    color->setKeyValueAt(0.5, DEFAULT_COLOR);
    color->setEndValue(DEFAULT_GLOW);

    QPropertyAnimation *position = new QPropertyAnimation(this, "pos");
    position->setEasingCurve(QEasingCurve::InQuad);
    position->setDuration(DURATION);
    position->setLoopCount(-1);

    float offset = (80 * MAX_SCALE - 80) / 2;

    position->setStartValue(QPointF(_origin.x() - 4.0, _origin.y() - 4.0));
    position->setKeyValueAt(0.5, QPointF(_origin.x() - 4.0 - offset, _origin.y() - 4.0 - offset));
    position->setEndValue(QPointF(_origin.x() - 4.0, _origin.y() - 4.0));

    QPropertyAnimation *outlinecolor = new QPropertyAnimation(this, "outline");
    outlinecolor->setDuration(DURATION);
    outlinecolor->setLoopCount(-1);

    outlinecolor->setStartValue(DEFAULT_GLOW);
    outlinecolor->setKeyValueAt(0.5, DEFAULT_OUTLINE);
    outlinecolor->setEndValue(DEFAULT_GLOW);

    _animation = new QParallelAnimationGroup;
    _animation->addAnimation(scale);
    _animation->addAnimation(color);
    _animation->addAnimation(position);
    _animation->addAnimation(outlinecolor);

    _animation->start();

    _zoom = 0;

    updateColors(true);
}

CursorWidget::~CursorWidget() {
    //_animation->deleteLater();
}

void CursorWidget::setAnimated(bool value) {
    _animated = value;
    if (value) {
        _animation->start();
    } else {
        _animation->stop();
    }
}

void CursorWidget::checkUpdate() {
    if (!_animated) {
        update();
    }
}

void CursorWidget::updateAnimation() {
    QPropertyAnimation *main = static_cast<QPropertyAnimation*>(_animation->animationAt(1));
    main->setStartValue(_glow);
    main->setKeyValueAt(0.5, _main);
    main->setEndValue(_glow);

    QPropertyAnimation *outline = static_cast<QPropertyAnimation*>(_animation->animationAt(3));
    outline->setStartValue(_glow);
    outline->setKeyValueAt(0.5, _dark);
    outline->setEndValue(_glow);

    if (!_animated) {
        _color = qvariant_cast<QColor>(main->currentValue());
        _outline = qvariant_cast<QColor>(outline->currentValue());
    }
    checkUpdate();
}

void CursorWidget::updateColors(bool defaultvalue) {
    if (defaultvalue) {
        _main = DEFAULT_COLOR;
        _glow = DEFAULT_GLOW;
        _dark = DEFAULT_OUTLINE;
        _light = DEFAULT_LIGHT;
    } else {
        _main = SourceFile::cursorcolors["main"];
        _glow = SourceFile::cursorcolors["glow"];
        _dark = SourceFile::cursorcolors["dark"];
        _light = SourceFile::cursorcolors["light"];
    }
    static_cast<QGraphicsDropShadowEffect*>(graphicsEffect())->setColor(_glow);

    updateAnimation();
}

void CursorWidget::setScale(float value) {
    _scale = value;
}

float CursorWidget::scale() {
    return _scale;
}

void CursorWidget::setColor(QColor value) {
    _color = value;
}

QColor CursorWidget::color() {
    return _color;
}

void CursorWidget::setOutline(QColor value) {
    _outline = value;
}

QColor CursorWidget::outline() {
    return _outline;
}

void CursorWidget::setGlowColor(QColor value) {
    _glow = value;
    static_cast<QGraphicsDropShadowEffect*>(graphicsEffect())->setColor(_glow);
    updateAnimation();
}

void CursorWidget::setMainColor(QColor value) {
    _main = value;
    updateAnimation();
}

void CursorWidget::setDarkColor(QColor value) {
    _dark = value;
    updateAnimation();
}

void CursorWidget::setLightColor(QColor value) {
    _light = value;
    checkUpdate();
}

void CursorWidget::setOrigin(QPointF origin) {
    _origin = origin;

    setGeometry(origin.x(), origin.y(), width(), height());
    updateAnimationPosition();
    checkUpdate();
}

QPointF CursorWidget::origin() {
    return _origin;
}

void CursorWidget::updateAnimationPosition() {
    float zoomsize = 80 * float(GridWidget::ICON_SIZE[_zoom].x()) / GridWidget::ICON_SIZE[0].x();
    float zoomscale = float(GridWidget::ICON_SIZE[_zoom].x()) / GridWidget::ICON_SIZE[0].x();
    float offset = (zoomsize * MAX_SCALE - zoomsize) / 2;

    QPropertyAnimation *position = static_cast<QPropertyAnimation*>(_animation->animationAt(2));
    position->setStartValue(QPointF(_origin.x() - 4.0 * zoomscale, _origin.y() - 4.0 * zoomscale));
    position->setKeyValueAt(0.5, QPointF(_origin.x() - 4.0 * zoomscale - offset, _origin.y() - 4.0 * zoomscale - offset));
    position->setEndValue(QPointF(_origin.x() - 4.0 * zoomscale, _origin.y() - 4.0 * zoomscale));

    if (!_animated) {
        move(qvariant_cast<QPoint>(position->currentValue()));
    }
}

void CursorWidget::setZoom(int zoom) {
    _zoom = zoom;

    updateAnimationPosition();

    float zoomscale = float(GridWidget::ICON_SIZE[_zoom].x()) / GridWidget::ICON_SIZE[0].x();

    QPropertyAnimation *scale = static_cast<QPropertyAnimation*>(_animation->animationAt(0));
    scale->setStartValue(QSize(zoomscale * 80, zoomscale * 80));
    scale->setKeyValueAt(0.5, QSize(zoomscale * 80 * MAX_SCALE, zoomscale * 80 * MAX_SCALE));
    scale->setEndValue(QSize(zoomscale * 80, zoomscale * 80));

    if (!_animated) {
        resize(qvariant_cast<QSize>(scale->currentValue()));
    }

    checkUpdate();
}

void CursorWidget::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    _scale = size().width() / 80.0;

    QPainterPath path;

    path.addRoundedRect(0, 0, 80 * _scale, 80 * _scale, 10 * _scale, 10 * _scale);
    path.addRoundedRect(8 * _scale, 8 * _scale, 64 * _scale, 64 * _scale, 4 * _scale, 4 * _scale);

    QPainterPath diamond;
    diamond.moveTo(39 * _scale, -18 * _scale);
    diamond.lineTo(97 * _scale, 40 * _scale);
    diamond.lineTo(40 * _scale, 97 * _scale);
    diamond.lineTo(-18 * _scale, 40 * _scale);
    diamond.lineTo(39 * _scale, -18 * _scale);

    path = path.subtracted(diamond);

    //float zoomscale = float(GridWidget::ICON_SIZE[_zoom].x()) / GridWidget::ICON_SIZE[0].x();

    //p.scale(zoomscale, zoomscale);
    p.setBrush(QColor(Qt::transparent));
    p.setPen(_outline);
    p.fillPath(path, QBrush(_light));

    QColor color1 = _color;
    color1.setAlpha(0);

    QRadialGradient corner = QRadialGradient(QPointF(14 * _scale, 14 * _scale), 16 * _scale, QPointF(14 * _scale, 14 * _scale));
    corner.setColorAt(0, _color);
    corner.setColorAt(0.7, _color);
    corner.setColorAt(0.9, color1);
    corner.setColorAt(1, color1);

    p.fillPath(path, QBrush(corner));

    corner.setCenter(66 * _scale, 14 * _scale);
    corner.setFocalPoint(66 * _scale, 14 * _scale);

    p.fillPath(path, QBrush(corner));

    corner.setCenter(14 * _scale, 66 * _scale);
    corner.setFocalPoint(14 * _scale, 66 * _scale);

    p.fillPath(path, QBrush(corner));

    corner.setCenter(66 * _scale, 66 * _scale);
    corner.setFocalPoint(66 * _scale, 66 * _scale);

    p.fillPath(path, QBrush(corner));

    p.drawPath(path);
}
