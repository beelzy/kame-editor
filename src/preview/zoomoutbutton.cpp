// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>

#include "zoomoutbutton.h"
#include "preview.h"
#include "fakebutton.h"

ZoomOutButton::ZoomOutButton(QWidget *parent) : QAbstractButton(parent) {

}

void ZoomOutButton::setMainColor(QColor value) {
    _main = value;
    update();
}

void ZoomOutButton::setLightColor(QColor value) {
    _light = value;
    update();
}

void ZoomOutButton::setIconMainColor(QColor value) {
    _iconmain = value;
    update();
}

void ZoomOutButton::setIconLightColor(QColor value) {
    _iconlight = value;
    update();
}

void ZoomOutButton::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    if (isDown() && isEnabled()) {
        Preview::drawRoundedRectShadow(p, _iconlight, QRect(0, 2, 24, 28), 4.0, 6.0);
    }

    QPainterPath path;

    path.addRoundedRect(6, 10, 12, 12, 3, 3);

    QLinearGradient gradient = QLinearGradient(QPointF(0, 0), QPointF(0, 26));

    QPixmap pixmap = QPixmap(18, 23);
    pixmap.fill(QColor(Qt::transparent));

    QPainter logo;
    logo.begin(&pixmap);
    logo.setRenderHints(QPainter::Antialiasing);
    logo.setPen(QColor(Qt::transparent));
    if (isEnabled() && isDown()) {
        logo.setBrush(_main);
    } else {
        logo.translate(0, 1);
        logo.setBrush(_light);
        logo.drawPath(path);
        logo.translate(0, -1);
        gradient.setColorAt(0, _iconlight);
        gradient.setColorAt(1, _iconmain);

        logo.setBrush(gradient);
    }
    logo.drawPath(path);
    logo.end();

    if (!isEnabled()) {
        p.setOpacity(0x33 / 255.0);
    }
    p.drawPixmap(0, 0, pixmap);
}
