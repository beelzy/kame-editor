// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include <QPainter>

#include "foldericon.h"
#include "gridwidget.h"
#include "preview.h"
#include "../sourcefile.h"

int FolderIcon::FOLDER_SIZE[6] = {72, 72, 50, 38, 30, 26};
int FolderIcon::FOLDER_OFFSET[6] = {6, 6, 3, 1, 2, 0};
int FolderIcon::FOLDER_FONTSIZE[6] = {27, 27, 20, 16, 13, 13};

const QColor FolderIcon::DEFAULT_FOLDER_DARK = QColor("#4EAFCD");
const QColor FolderIcon::DEFAULT_FOLDER_MAIN = QColor("#6BCDE8");
const QColor FolderIcon::DEFAULT_FOLDER_LIGHT = QColor("#AFEAF9");
const QColor FolderIcon::DEFAULT_FOLDER_SHADOW = QColor("#55000000");

FolderIcon::FolderIcon(QWidget *parent) : QWidget(parent) {
    _index = 1;
    _state = State::CLOSE;

    updateFolderColors(true);
    _foldershadow = DEFAULT_FOLDER_SHADOW;

    updateFileColors(true);
    
    int zoomlevel = static_cast<GridWidget*>(parent)->zoomLevel();
    int iconheight = zoomlevel == 0 ? GridWidget::ICON_1X_HEIGHT : GridWidget::ICON_SIZE[zoomlevel].y();
    setFixedSize(GridWidget::SPACING[zoomlevel] + GridWidget::ICON_SIZE[zoomlevel].x(), iconheight + GridWidget::SPACING[zoomlevel]);
}

FolderIcon::State FolderIcon::state() {
    return _state;
}

void FolderIcon::setState(State value) {
    _state = value;
}

int FolderIcon::index() {
    return _index;
}

void FolderIcon::setIndex(int value) {
    _index = value;
}

void FolderIcon::setFolderOpen(QPixmap image) {
    _opentexture = image.isNull() ? QPixmap() : image;
    update();
}

void FolderIcon::setFolderClose(QPixmap image) {
    _closetexture = image.isNull() ? QPixmap() : image;
    update();
}

void FolderIcon::showLetter(bool enabled) {
    _showletter = enabled;
    update();
}

void FolderIcon::setFolderDarkColor(QColor value) {
    _folderdark = value;
    update();
}

void FolderIcon::setFolderMainColor(QColor value) {
    _foldermain = value;
    update();
}

void FolderIcon::setFolderLightColor(QColor value) {
    _folderlight = value;
    update();
}

void FolderIcon::setFolderShadowColor(QColor value) {
    _foldershadow = value;
    update();
}

void FolderIcon::setFileDarkColor(QColor value) {
    _filedark = value;
    update();
}

void FolderIcon::setFileMainColor(QColor value) {
    _filemain = value;
    update();
}

void FolderIcon::setFileLightColor(QColor value) {
    _filelight = value;
    update();
}

void FolderIcon::setFileShadowColor(QColor value) {
    _fileshadow = value;
    update();
}

void FolderIcon::updateFolderColors(bool defaultValue) {
    if (defaultValue) {
        _folderdark = DEFAULT_FOLDER_DARK;
        _foldermain = DEFAULT_FOLDER_MAIN;
        _folderlight = DEFAULT_FOLDER_LIGHT;
        // unused?
        //_foldershadow = DEFAULT_FOLDER_SHADOW;
    } else {
        _folderdark = SourceFile::foldercolors["dark"];
        _foldermain = SourceFile::foldercolors["main"];
        _folderlight = SourceFile::foldercolors["light"];
        // unused?
        //_foldershadow = SourceFile::foldercolors["shadow"];
    }
    update();
}

void FolderIcon::updateFileColors(bool defaultValue) {
    if (defaultValue) {
        _filedark = GridWidget::DEFAULT_FILE_DARK;
        _filemain = GridWidget::DEFAULT_FILE_MAIN;
        _filelight = GridWidget::DEFAULT_FILE_LIGHT;
        _fileshadow = GridWidget::DEFAULT_FILE_SHADOW;
    } else {
        _filedark = SourceFile::filecolors["dark"];
        _filemain = SourceFile::filecolors["main"];
        _filelight = SourceFile::filecolors["light"];
        _fileshadow = SourceFile::filecolors["shadow"];
    }
    update();
}

void FolderIcon::mousePressEvent(QMouseEvent *event) {
    event->ignore();
}

void FolderIcon::paintEvent(QPaintEvent *) {
    int zoomlevel = static_cast<GridWidget*>(parentWidget())->zoomLevel();
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);

    int spacing = (GridWidget::SPACING[zoomlevel] + GridWidget::ICON_SIZE[zoomlevel].x() - FOLDER_SIZE[zoomlevel] ) / 2.0;
    float scale = float(FOLDER_SIZE[zoomlevel]) / FOLDER_SIZE[0];

    int folderopen = 0;
    int folderOffsetX = 1;
    QPixmap foldertexture = _state == State::OPEN ? _opentexture : _closetexture;
    if (_state == State::OPEN) {
        folderopen = 1;
        folderOffsetX = 4;
    }
    int offsety = (GridWidget::SPACING[zoomlevel] / 2.0) + FOLDER_OFFSET[zoomlevel] + folderopen;

    if (zoomlevel == 0) {
        offsety = GridWidget::ICON_1X_HEIGHT - GridWidget::ICON_SIZE[zoomlevel].y() + FOLDER_OFFSET[zoomlevel] + folderopen;
    }

    QColor main = Preview::colormix(_foldermain, _folderlight);

    Preview::drawRoundedRectShadow(p, DEFAULT_FOLDER_SHADOW, QRect(spacing - spacing / 2.0, offsety + 1,  FOLDER_SIZE[zoomlevel] + spacing, 60 * scale + spacing), 3 * scale, GridWidget::SPACING[zoomlevel] / 2.0);

    QPainterPath path;

    path.moveTo(3 * scale + spacing, offsety);
    path.arcTo(spacing, offsety, 6 * scale, 6 * scale, 90, 90);

    if (_state == State::OPEN) {
        path.lineTo(spacing, 19 * scale + offsety); 
        path.lineTo(-2 * scale + spacing, 19 * scale + offsety);
        path.arcTo(-4 * scale + spacing, 19 * scale + offsety, 4 * scale, 4 * scale, 90, 90);
        path.lineTo(1 * scale + spacing, 59 * scale + offsety);
        path.arcTo(1 * scale + spacing, 56 * scale + offsety, 6 * scale, 6 * scale, 180, 90);
        path.lineTo(69 * scale + spacing, 62 * scale + offsety);
        path.arcTo(66 * scale + spacing, 56 * scale + offsety, 6 * scale, 6 * scale, -90, 90);
        path.lineTo(76 * scale + spacing, 21 * scale + offsety);
        path.arcTo(72 * scale + spacing, 19 * scale + offsety, 4 * scale, 4 * scale, 0, 90);
        path.lineTo(72 * scale + spacing, 19 * scale + offsety);
    } else {
        path.lineTo(spacing, 59 * scale + offsety);
        path.arcTo(spacing, 56 * scale + offsety, 6 * scale, 6 * scale, 180, 90);
        path.lineTo(69 * scale + spacing, 62 * scale + offsety);
        path.arcTo(66 * scale + spacing, 56 * scale + offsety, 6 * scale, 6 * scale, -90, 90);
    }

    path.lineTo(72 * scale + spacing, 8 * scale + offsety);
    path.arcTo(64 * scale + spacing, 4 * scale + offsety, 8 * scale, 8 * scale, 0, 90);
    path.lineTo(30 * scale + spacing, 4 * scale + offsety);
    path.lineTo(28 * scale + spacing, offsety);
    path.lineTo(3 * scale + spacing, offsety);

    // Draw folder background
    p.setClipPath(path);
    if (foldertexture.isNull()) {
        p.setPen(_folderdark);
        QLinearGradient gradient = QLinearGradient(QPointF(spacing + 68 * scale, 0), QPointF(spacing + 72 * scale, 0));
        gradient.setColorAt(0, _foldermain);
        gradient.setColorAt(1, _folderdark);
        p.setBrush(gradient);
        p.drawPath(path);

        Preview::drawRoundedRectShadow(p, main.darker(110), QRect(spacing, offsety, 32 * scale, 40 * scale), 6 * scale, 4 * scale);
        Preview::drawRoundedRectShadow(p, main.darker(110), QRect(spacing + 32 * scale, 4 * scale + offsety, 40 * scale, 40 * scale), 6 * scale, 4 * scale);

        if (_state == State::OPEN) {
            gradient = QLinearGradient(QPointF(0, 14 * scale + offsety), QPointF(0, 19* scale + offsety));
            gradient.setColorAt(0, QColor(Qt::transparent));
            gradient.setColorAt(1, _folderdark);
            p.setBrush(gradient);
            p.drawPath(path);
        }
    } else {
        p.fillRect(spacing, offsety, 74 * scale, 64 * scale, QColor(Qt::black));
        p.drawPixmap(QRectF(spacing, offsety, 74 * scale, 64 * scale), foldertexture, QRectF(folderOffsetX, 1, 72, 62));

    }
    p.setClipping(false);

    // Draw the file
    QPainterPath file;
    file.moveTo(8 * scale + spacing, 20 * scale + offsety);
    file.arcTo(7 * scale + spacing, 11 * scale + offsety, 22 * scale, 22 * scale, 200, -90);
    file.lineTo(44 * scale + spacing, -1.5 * scale + offsety);
    file.arcTo(36 * scale + spacing, -2 * scale + offsety, 22 * scale, 22 * scale, 110, -90);
    file.lineTo(64 * scale + spacing, 20 * scale + offsety);
    file.lineTo(8 * scale + spacing, 20 * scale + offsety);

    p.setPen(_filemain);
    p.setBrush(_filelight);
    p.drawPath(file);

    p.setClipPath(file);

    int posx = zoomlevel == 0 ? 35.5 : 7;
    int posy = zoomlevel == 0 ? 12 : 17;
    float gradientpos = _state == State::OPEN ? 1 : 0.52;

    QPixmap pixmap(ceil(52 * scale), ceil(32 * scale));
    pixmap.fill(QColor(Qt::transparent));

    QPainter highlightp;
    highlightp.begin(&pixmap);
    highlightp.setRenderHints(QPainter::Antialiasing);
    Preview::drawRoundedRectShadow(highlightp, _filemain, QRect(0, 0, ceil(52 * scale), ceil(32 * scale)), ceil(10 * scale), ceil(8 * scale));
    highlightp.end();

    p.rotate(-23);
    p.drawPixmap(spacing - posx * scale, offsety + posy * scale, pixmap);
    p.rotate(23);

    QLinearGradient gradient = QLinearGradient(QPointF(0, -1.5 * scale + offsety), QPointF(0, 20 * scale + offsety));
    gradient.setColorAt(0, QColor(Qt::transparent));
    gradient.setColorAt(gradientpos, _filemain);

    p.setPen(QColor(Qt::transparent));
    p.setBrush(gradient);
    p.drawPath(file);

    gradient = QLinearGradient(QPointF(8 * scale + spacing, 20 * scale + offsety), QPointF(44 * scale, -1.5 * scale + offsety));

    QColor dark = Preview::colormix(_filelight, _filedark);
    dark.setAlphaF(0.5);
    gradient.setColorAt(1, QColor(Qt::transparent));
    gradient.setColorAt(0, dark);

    p.setBrush(gradient);
    p.drawPath(file);

    if (_state == State::OPEN) {
        gradient = QLinearGradient(QPointF(0, 15 * scale + offsety), QPointF(0, 20 * scale + offsety));
        gradient.setColorAt(0, QColor(Qt::transparent));
        gradient.setColorAt(1, _fileshadow);

        p.setBrush(gradient);
        p.drawPath(file);
    }

    p.setClipping(false);

    // Draw folder foreground
    if (foldertexture.isNull()) {
        p.setPen(QColor(Qt::transparent));
        QPainterPath subtract;
        if (_state == State::OPEN) {
            subtract.addRect(-4 * scale + spacing, offsety, 80 * scale, 19 * scale);
            p.setBrush(main);
            p.setPen(_folderdark);
        } else {
            subtract.addRect(spacing, offsety, 72 * scale, 8 * scale);
            QLinearGradient gradient = QLinearGradient(QPointF(0, 8 * scale + offsety), QPointF(0, 16 * scale + offsety));
            gradient.setColorAt(0, _foldermain);
            gradient.setColorAt(1, _folderdark);
            p.setBrush(gradient);
        }
        QPainterPath foreground = path.subtracted(subtract);
        p.drawPath(foreground);
        p.setClipPath(foreground);

        if (_state == State::OPEN) {
            p.setPen(QColor(Qt::transparent));
            QLinearGradient gradient = QLinearGradient(QPointF(0, 52 * scale + offsety), QPointF(0, 62 * scale + offsety));
            gradient.setColorAt(0, QColor(Qt::transparent));
            gradient.setColorAt(2.0 / 3.0, _foldermain);
            gradient.setColorAt(1, _folderdark);
            p.setBrush(gradient);
            p.drawRect(-4 * scale + spacing, 52 * scale + offsety, 80 * scale, 10 * scale);
            Preview::drawRoundedRectShadow(p, _folderlight, QRect(spacing - 3 * scale, 18 * scale + offsety, 79 * scale, 6 * scale), 2 * scale, 3 * scale);
            p.setPen(_folderdark);
            p.setBrush(QColor(Qt::transparent));
            p.drawPath(foreground);
            QPen pen;
            pen.setColor(_foldermain);
            pen.setWidth(4);
            p.setPen(pen);
            p.drawLine(spacing, 18 * scale + offsety, spacing + 72 * scale, 18 * scale + offsety);
        } else {
            Preview::drawFolderRect(p, _foldermain, QRect(spacing - 4 * scale, 8 * scale + offsety, round(80 * scale), round(55 * scale)), QPoint(2 * scale, 8 * scale), round(6 * scale));
            Preview::drawFolderRect(p, _folderlight, QRect(spacing, 4 * scale + offsety, round(72 * scale), round(12 * scale)), QPoint(2 * scale, 8 * scale), round(6 * scale));
            Preview::drawFolderRect(p, main, QRect(spacing, 8 * scale + offsety, round(72 * scale), round(52 * scale)), QPoint(2 * scale, 8 * scale), round(6 * scale));
        }
    } else {
        p.setClipPath(path);
        if (_state == State::OPEN) {
            p.fillRect(-4 * scale + spacing, 19 * scale + offsety, 80 * scale, 43 * scale, QColor(Qt::black));
            p.drawPixmap(QRectF(-4 * scale + spacing, 19 * scale + offsety, 80 * scale, 43 * scale), _opentexture, QRectF(1, 20, 80, 43));
        } else {
            p.fillRect(spacing, 8 * scale + offsety, 72 * scale, 54 * scale, QColor(Qt::black));
            p.drawPixmap(QRectF(spacing, 8 * scale + offsety, 72 * scale, 54 * scale), _closetexture, QRectF(1, 9, 72, 54));
        }
    }

    // Draw first letter on folder
    if (_showletter) {
        QFont font = QFont("Nimbus Sans");
        font.setPixelSize(FOLDER_FONTSIZE[zoomlevel]);
        QFontMetrics metrics = QFontMetrics(font);

        QPen pen(QColor("#FBFBFB"));
        pen.setWidth(std::max(2.0f, ceilf(4 * scale)));
        QBrush brush(QColor("#5D676A"));

        QString letter = QString::number(_index);

        path = QPainterPath();
        if (_state == State::CLOSE) {
            path.addText(spacing + (72 * scale - metrics.horizontalAdvance(letter)) / 2.0, 8 * scale + offsety + (54 * scale + metrics.height() / 2.0) / 2.0, font, letter);
        } else {
            pen.setColor(QColor("#C0C3C7"));
            brush.setColor(QColor("#535D66"));
            path.addText(-4 * scale + spacing + (80 * scale - metrics.horizontalAdvance(letter)) / 2.0, (1 / 0.75) * (19 * scale + offsety + (43 * scale + metrics.height() / 2.0) / 2.0), font, letter);
            p.scale(1.0, 0.75);
        }

        p.strokePath(path, pen);
        p.fillPath(path, brush);
    }
}
