// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef BOTTOMVIEW_H
#define BOTTOMVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPropertyAnimation>

class BottomView : public QGraphicsView {
    Q_OBJECT
    Q_PROPERTY(int scrollpos READ scrollpos WRITE setScrollPos);

    static const QColor DEFAULT_MAIN;
    static const QColor DEFAULT_DARK;

    public:
        explicit BottomView(QWidget *parent = 0);
        ~BottomView();

        enum class DrawType {
            NONE,
            COLOR,
            TEXTURE
        };

        enum class FrameType {
            SINGLE,
            FASTSCROLL,
            SLOWSCROLL,
            PAGESCROLL,
            BOUNCESCROLL
        };

        DrawType drawtype();
        void updateDrawType();

        void updateFrameType();

        int scrollpos();
        void setScrollPos(int value);

        void setTexture(QPixmap image);

        static int PAGE_SIZE[6];

    public slots:
        void onScrollPositionChange(int pos);
        void onPageScroll(QPointF start, QPointF end);
        void onZoomLevelChanged(int zoom, QMargins bounds);
        void updateMainColor();
        void updateDarkColor();
        void setMainColor(QColor color);
        void setDarkColor(QColor color);

    protected:
        void drawBackground(QPainter *p, const QRectF &rect);

    private:
        QGraphicsScene *_scene;
        QPixmap _texture;
        QPropertyAnimation *_animation;
        int calculatePageScroll();
        int _scrollpos;
        int _zoomlevel;
        QColor _maincolor;
        QColor _darkcolor;
};

#endif // BOTTOMVIEW_H
