// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QFile>
#include <QDebug>
#include <QVector3D>

#include "objloader.h"
#define TINYOBJLOADER_IMPLEMENTATION
#include "../tiny_obj_loader.h"

ObjLoader::ObjLoader(QObject *parent) : QObject(parent) {

}

QVector<QVector3D> ObjLoader::load(QString filepath) {
    QFile folderFile(filepath);
    folderFile.open(QIODevice::ReadOnly);
    QByteArray folderData(folderFile.readAll());

    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn;
    std::string err;
    std::stringstream data;
    data << folderData.toStdString();
    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, &data, NULL);

    QVector<QVector3D> vertices;

    if (!err.empty()) {
        qDebug() << QString::fromStdString(err);
        return vertices;
    } else if (ret) {
        //qDebug() << "obj loaded" << ret << shapes[0].mesh.material_ids.size() << materials.size() << attrib.vertices.size() << attrib.texcoords.size();
        float bmin[3];
        float bmax[3];
        bmin[0] = bmin[1] = bmin[2] = std::numeric_limits<float>::max();
        bmax[0] = bmax[1] = bmin[2] = -std::numeric_limits<float>::max();

        std::map<int, vec3> smoothVertexNormals;
        if (hasSmoothingGroup(shapes[0]) > 0) {
            computeSmoothingNormals(attrib, shapes[0], smoothVertexNormals);
        }

        for (int f = 0; f < shapes[0].mesh.indices.size() / 3; f++) {
            tinyobj::index_t idx0 = shapes[0].mesh.indices[3 * f + 0];
            tinyobj::index_t idx1 = shapes[0].mesh.indices[3 * f + 1];
            tinyobj::index_t idx2 = shapes[0].mesh.indices[3 * f + 2];

            float v[3][3];
            for (int k = 0; k < 3; k++) {
                int f0 = idx0.vertex_index;
                int f1 = idx1.vertex_index;
                int f2 = idx2.vertex_index;

                v[0][k] = attrib.vertices[3 * f0 + k];
                v[1][k] = attrib.vertices[3 * f1 + k];
                v[2][k] = attrib.vertices[3 * f2 + k];
                bmin[k] = std::min(v[0][k], bmin[k]);
                bmin[k] = std::min(v[1][k], bmin[k]);
                bmin[k] = std::min(v[2][k], bmin[k]);
                bmax[k] = std::max(v[0][k], bmax[k]);
                bmax[k] = std::max(v[1][k], bmax[k]);
                bmax[k] = std::max(v[2][k], bmax[k]);
            }

            float n[3][3];
            bool invalid_normal_index = false;
            if (attrib.normals.size() > 0) {
                int nf0 = idx0.normal_index;
                int nf1 = idx1.normal_index;
                int nf2 = idx2.normal_index;

                if (nf0 < 0 || nf1 < 0 || nf2 < 0) {
                    invalid_normal_index = true;
                } else {
                    for (int k = 0; k < 3; k++) {
                        n[0][k] = attrib.normals[3 * nf0 + k];
                        n[1][k] = attrib.normals[3 * nf1 + k];
                        n[2][k] = attrib.normals[3 * nf2 + k];
                    }
                }
            } else {
                invalid_normal_index = true;
            }

            if (invalid_normal_index && !smoothVertexNormals.empty()) {
                int f0 = idx0.vertex_index;
                int f1 = idx1.vertex_index;
                int f2 = idx2.vertex_index;

                if (f0 >= 0 && f1 >= 0 && f2 >= 0) {
                    n[0][0] = smoothVertexNormals[f0].v[0];
                    n[0][1] = smoothVertexNormals[f0].v[1];
                    n[0][2] = smoothVertexNormals[f0].v[2];

                    n[1][0] = smoothVertexNormals[f1].v[0];
                    n[1][1] = smoothVertexNormals[f1].v[1];
                    n[1][2] = smoothVertexNormals[f1].v[2];

                    n[2][0] = smoothVertexNormals[f2].v[0];
                    n[2][1] = smoothVertexNormals[f2].v[1];
                    n[2][2] = smoothVertexNormals[f2].v[2];

                    invalid_normal_index = false;
                }
            }

            if (invalid_normal_index) {
                calcNormal(n[0], v[0], v[1], v[2]);
                n[1][0] = n[0][0];
                n[1][1] = n[0][1];
                n[1][2] = n[0][2];
                n[2][0] = n[0][0];
                n[2][1] = n[0][1];
                n[2][2] = n[0][2];
            }
            for (int k = 0; k < 3; k++) {
                vertices.push_back(QVector3D(v[k][0], v[k][1], v[k][2]));
                vertices.push_back(QVector3D(n[k][0], n[k][1], n[k][2]));
            }
        }
        return vertices;
    }
} 

void ObjLoader::calcNormal(float N[3], float v0[3], float v1[3], float v2[3]) {
    float v10[3];
    v10[0] = v1[0] - v0[0];
    v10[1] = v1[1] - v0[1];
    v10[2] = v1[2] - v0[2];

    float v20[3];
    v20[0] = v2[0] - v0[0];
    v20[1] = v2[1] - v0[1];
    v20[2] = v2[2] - v0[2];

    N[0] = v20[1] * v10[2] - v20[2] * v10[1];
    N[1] = v20[2] * v10[0] - v20[0] * v10[2];
    N[2] = v20[0] * v10[1] - v20[1] * v10[0];

    float len2 = N[0] * N[0] + N[1] * N[1] + N[2] * N[2];
    if (len2 > 0.0f) {
        float len = sqrtf(len2);

        N[0] /= len;
        N[1] /= len;
        N[2] /= len;
    }
}

bool ObjLoader::hasSmoothingGroup(const tinyobj::shape_t &shape) {
    for (size_t i = 0; i < shape.mesh.smoothing_group_ids.size(); i++) {
        if (shape.mesh.smoothing_group_ids[i] > 0) {
            return true;
        }
    }
    return false;
}

void ObjLoader::computeSmoothingNormals(const tinyobj::attrib_t &attrib, const tinyobj::shape_t &shape, std::map<int, vec3> &smoothVertexNormals) {
    smoothVertexNormals.clear();
    std::map<int, vec3>::iterator iter;

    for (size_t f = 0; f < shape.mesh.indices.size() / 3; f++) {
        tinyobj::index_t idx0 = shape.mesh.indices[3 * f + 0];
        tinyobj::index_t idx1 = shape.mesh.indices[3 * f + 1];
        tinyobj::index_t idx2 = shape.mesh.indices[3 * f + 2];

        int vi[3];
        float v[3][3];

        for (int k = 0; k < 3; k++) {
            vi[0] = idx0.vertex_index;
            vi[1] = idx1.vertex_index;
            vi[2] = idx2.vertex_index;

            v[0][k] = attrib.vertices[3 * vi[0] + k];
            v[1][k] = attrib.vertices[3 * vi[1] + k];
            v[2][k] = attrib.vertices[3 * vi[2] + k];
        }

        float normal[3];
        calcNormal(normal, v[0], v[1], v[2]);

        for (size_t i = 0; i < 3; ++i) {
            iter = smoothVertexNormals.find(vi[i]);
            if (iter != smoothVertexNormals.end()) {
                iter->second.v[0] += normal[0];
                iter->second.v[1] += normal[1];
                iter->second.v[2] += normal[2];
            } else {
                smoothVertexNormals[vi[i]].v[0] = normal[0];
                smoothVertexNormals[vi[i]].v[1] = normal[1];
                smoothVertexNormals[vi[i]].v[2] = normal[2];
            }
        }
    }

    for (iter = smoothVertexNormals.begin(); iter != smoothVertexNormals.end(); iter++) {
        normalizeVector(iter->second);
    }
}

void ObjLoader::normalizeVector(vec3 &v) {
    float len2 = v.v[0] * v.v[0] + v.v[1] * v.v[1] + v.v[2] * v.v[2];
    if (len2 > 0.0f) {
        float len = sqrtf(len2);

        v.v[0] /= len;
        v.v[1] /= len;
        v.v[2] /= len;
    }
}
