// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GRIDWIDGET_H
#define GRIDWIDGET_H

#include <QFrame>
#include <QPropertyAnimation>

#include "foldericon.h"
#include "gridicon.h"

class GridWidget : public QFrame {
    Q_OBJECT

    public:
        explicit GridWidget(QWidget *parent = 0, bool isFolder = false);
        ~GridWidget();

        static const QColor DEFAULT_MAIN;
        static const QColor DEFAULT_SHADOW;

        static const QColor DEFAULT_FILE_DARK;
        static const QColor DEFAULT_FILE_MAIN;
        static const QColor DEFAULT_FILE_LIGHT;
        static const QColor DEFAULT_FILE_SHADOW;

        static const int BG_HEIGHT = 172;
        static const int BG_FOLDER_HEIGHT = 142;

        static QPointF OFFSET[6];
        static float FOLDER_OFFSET[6];
        static int SPACING[6];
        static int VISIBLE_ITEMS[6];
        static const int ITEM_COUNT = 240;

        static QPointF ICON_SIZE[6];
        static const int ICON_1X_HEIGHT = 148;
        static float EMPTY_ICON_SIZE;
        static int INNER_ICON_SIZE[6];
        static QPointF INNER_ICON_POS[6];
        static QPointF ICON_RADIUS[6];

        void setZoomLevel(int zoom);
        int zoomLevel();

        int screenIndex();
        void setScreenIndex(int value);

        void updateZoomLevel();
        void updateIconSelection();

        bool isFolder();
        void setTextureMode(bool value);

    signals:
        void updateArrows(bool showLeft, bool showRight);
        void updateOpenClose(bool show);
        void zoomLevelChanged(int zoom, QMargins bounds);
        void scrollPositionChanged(int pos);
        void pageScroll(QPointF start, QPointF end);
        void iconClicked(QPointF pos, int scrollpos);
        void iconChanged(QWidget* icon);
        void playCursor();
        void playFrame0();
        void playFrame1();
        void playFrame2();

    public slots:
        void scrollLeft();
        void scrollRight();
        void zoomIn();
        void zoomOut();
        void setMainColor(QColor color);
        void setShadowColor(QColor color);
        void setIconLightColor(QColor color);
        void setIconDarkColor(QColor color);
        void onFolderTextureChanged(FolderIcon::State state, QPixmap image);
        void onFileTextureChanged(GridIcon::Size size, QPixmap image);
        void onFolderLetterChanged(bool enabled);
        void setFolderDarkColor(QColor color);
        void setFolderMainColor(QColor color);
        void setFolderLightColor(QColor color);
        void setFolderShadowColor(QColor color);
        void setFileDarkColor(QColor color);
        void setFileMainColor(QColor color);
        void setFileLightColor(QColor color);
        void setFileShadowColor(QColor color);
        void updateFolderColors(bool defaultValue);
        void updateFileColors(bool defaultValue);
        void updateColors(bool defaultValue);

    protected:
        virtual void mousePressEvent(QMouseEvent *event);
        virtual void mouseMoveEvent(QMouseEvent *event);
        virtual void mouseReleaseEvent(QMouseEvent *event);
        void moveEvent(QMoveEvent *event);
        void paintEvent(QPaintEvent *event);
        void displayArrows();
        int snapPosX(int x, bool flatten=false);

    private:
        QList<QWidget *> _items;
        QPropertyAnimation *_animation;
        int _scrollpos;
        int _mousestart;
        int _globalmousex;
        int _screenIndex;
        // 0 = largest - 5 = smallest
        int _zoomlevel;
        float getOffset();
        float getMaxScroll();
        void scroll(int direction);
        bool _mouseallowed;
        bool _isFolder;
        bool _isTexture;
        bool _audioEnabled;

        QColor _maincolor;
        QColor _shadowcolor;
};

#endif // GRIDWIDGET_H
