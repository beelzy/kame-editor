// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include <QPainter>

#include "gridicon.h"
#include "gridwidget.h"
#include "preview.h"
#include "../sourcefile.h"
#include "../color.h"

GridIcon::GridIcon(QWidget *parent) : QWidget(parent) {
    int zoomlevel = static_cast<GridWidget*>(parent)->zoomLevel();
    int iconheight = zoomlevel == 0 ? GridWidget::ICON_1X_HEIGHT : GridWidget::ICON_SIZE[zoomlevel].y();

    updateColors(true);

    setFixedSize(GridWidget::SPACING[zoomlevel] + GridWidget::ICON_SIZE[zoomlevel].x(), iconheight + GridWidget::SPACING[zoomlevel]);
}

QString GridIcon::gametext() {
    return _gametext;
}

void GridIcon::setGameText(QString value) {
    _gametext = value;
}

QString GridIcon::demotext() {
    return _demotext;
}

void GridIcon::setDemoText(QString value) {
    _demotext = value;
}

void GridIcon::setIcon(QPixmap pixmap) {
    _icontexture = pixmap;
}

void GridIcon::setDarkColor(QColor value) {
    _dark = value;
    update();
}

void GridIcon::setMainColor(QColor value) {
    _main = value;
    update();
}

void GridIcon::setLightColor(QColor value) {
    _light = value;
    update();
}

void GridIcon::setShadowColor(QColor value) {
    _shadow = value;
    update();
}

void GridIcon::updateColors(bool defaultValue) {
    if (defaultValue) {
        _dark = GridWidget::DEFAULT_FILE_DARK;
        _main = GridWidget::DEFAULT_FILE_MAIN;
        _light = GridWidget::DEFAULT_FILE_LIGHT;
        _shadow = GridWidget::DEFAULT_FILE_SHADOW;
    } else {
        _dark = SourceFile::filecolors["dark"];
        _main = SourceFile::filecolors["main"];
        _light = SourceFile::filecolors["light"];
        _shadow = SourceFile::filecolors["shadow"];
    }
    update();
}

void GridIcon::setTexture(Size size, QPixmap pixmap) {
    QPainter p;
    switch(size) {
        case Size::LARGE:
            if (!pixmap.isNull()) {
                _largetexture = QPixmap(72, 72);
                _largetexture.fill(Qt::black);
                p.begin(&_largetexture);
                p.drawPixmap(QRectF(36, 0, 36, 72), pixmap, QRectF(0, 0, 36, 72));
                p.translate(36, 0);
                p.scale(-1, 1);
                p.drawPixmap(QRectF(0, 0, 36, 72), pixmap, QRectF(0, 0, 36, 72));
                p.end();
            } else {
                _largetexture = pixmap;
            }
            break;
        case Size::SMALL:
            if (!pixmap.isNull()) {
                _smalltexture = QPixmap(50, 50);
                _smalltexture.fill(Qt::black);
                p.begin(&_smalltexture);
                p.drawPixmap(QRectF(25, 0, 25, 50), pixmap, QRectF(0, 0, 25, 50));
                p.translate(25, 0);
                p.scale(-1, 1);
                p.drawPixmap(QRectF(0, 0, 25, 50), pixmap, QRectF(0, 0, 25, 50));
                p.end();
            } else {
                _smalltexture = pixmap;
            }
            break;
    }
    update();
}

void GridIcon::mousePressEvent(QMouseEvent *event) {
    event->ignore();
}

void GridIcon::paintEvent(QPaintEvent *) {
    int zoomlevel = static_cast<GridWidget*>(parentWidget())->zoomLevel();
    QPainter p(this);

    p.setRenderHints(QPainter::Antialiasing);

    int spacing = GridWidget::SPACING[zoomlevel] / 2.0;
    int offsety = spacing;

    if (zoomlevel == 0) {
        offsety = GridWidget::ICON_1X_HEIGHT - GridWidget::ICON_SIZE[zoomlevel].y();
    }

    p.setCompositionMode(QPainter::CompositionMode_Darken);
    Preview::drawRoundedRectShadow(p, _shadow, QRect(spacing - spacing / 2.0, offsety + 1 - spacing / 2, GridWidget::ICON_SIZE[zoomlevel].x() + spacing, GridWidget::ICON_SIZE[zoomlevel].y() + spacing), GridWidget::ICON_RADIUS[zoomlevel].x(), GridWidget::SPACING[zoomlevel] / 2.0);
    p.setCompositionMode(QPainter::CompositionMode_SourceOver);

    QPainterPath path;

    path.addRoundedRect(spacing, offsety, GridWidget::ICON_SIZE[zoomlevel].x(), GridWidget::ICON_SIZE[zoomlevel].y(), GridWidget::ICON_RADIUS[zoomlevel].x(), GridWidget::ICON_RADIUS[zoomlevel].x());

    p.setClipPath(path);
    if ((_largetexture.isNull() && zoomlevel < 2) || (_smalltexture.isNull() && zoomlevel > 1)) {
        float scale = GridWidget::ICON_RADIUS[zoomlevel].x() / float(GridWidget::ICON_RADIUS[0].x());

        p.setPen(_dark);
        p.setBrush(_dark);

        p.drawPath(path);

        Preview::drawRoundedRectShadow(p, _main, QRect(spacing - std::max(1.0f, 2 * scale), offsety - std::max(1.0f, 4 * scale), floor(GridWidget::ICON_SIZE[zoomlevel].x() + std::max(1.0f, 6 * scale)), floor(GridWidget::ICON_SIZE[zoomlevel].y() + std::max(1.0f, 4 * scale))), GridWidget::ICON_RADIUS[zoomlevel].x(), floor(GridWidget::ICON_RADIUS[zoomlevel].y() + std::max(1.0f, 2 * scale)));
        Preview::drawRoundedRectShadow(p, _light, QRect(spacing - scale, offsety - 4 * scale, ceil(GridWidget::ICON_SIZE[zoomlevel].x() + 2 * scale), GridWidget::ICON_SIZE[zoomlevel].y()), GridWidget::ICON_RADIUS[zoomlevel].x(), GridWidget::ICON_RADIUS[zoomlevel].x());
        if (zoomlevel < 3) {
            Preview::drawRoundedRectShadow(p, _main, QRect(GridWidget::INNER_ICON_POS[zoomlevel].x() + spacing - 10 * scale, GridWidget::INNER_ICON_POS[zoomlevel].y() + offsety - 10 * scale, floor(GridWidget::INNER_ICON_SIZE[zoomlevel] + 20 * scale), floor(GridWidget::INNER_ICON_SIZE[zoomlevel] + 20 * scale)), ceil(std::max(1.0f, 12 * scale)), ceil(std::max(1.0f, 8 * scale)));
        }

        QColor fade = _main;
        fade.setAlphaF(0.8);
        QLinearGradient gradient = QLinearGradient(QPointF(0, offsety + GridWidget::INNER_ICON_POS[zoomlevel].y()), QPointF(0, offsety + GridWidget::ICON_SIZE[zoomlevel].y() - 6 * scale));
        gradient.setColorAt(0, QColor(Qt::transparent));
        gradient.setColorAt(0.9, fade);
        gradient.setColorAt(1, QColor(Qt::transparent));

        p.setClipping(false);

        p.setPen(QColor(Qt::transparent));
        p.setBrush(gradient);
        p.drawPath(path);
    } else {
        float scale = 1;
        if (zoomlevel > 1) {
            scale = GridWidget::ICON_SIZE[zoomlevel].x() / 50.0;
            p.drawPixmap(QRectF(spacing, offsety, 50 * scale, 50 * scale), _smalltexture, QRectF(0, 0, 50, 50));
        } else {
            scale = GridWidget::ICON_SIZE[zoomlevel].x() / 72.0;
            p.drawPixmap(QRectF(spacing, offsety, 72 * scale, 72 * scale), _largetexture, QRectF(0, 0, 72, 72));
        }

        p.setClipping(false);
    }

    path = QPainterPath();

    path.addRoundedRect(GridWidget::INNER_ICON_POS[zoomlevel].x() + spacing, GridWidget::INNER_ICON_POS[zoomlevel].y() + offsety, GridWidget::INNER_ICON_SIZE[zoomlevel], GridWidget::INNER_ICON_SIZE[zoomlevel], GridWidget::ICON_RADIUS[zoomlevel].y(), GridWidget::ICON_RADIUS[zoomlevel].y());

    if (_icontexture.isNull()) {
        p.setPen(QColor(Qt::transparent));
        p.setBrush(Color::MAIN);

        p.drawPath(path);
    } else {
        p.setClipPath(path);
        p.drawPixmap(GridWidget::INNER_ICON_POS[zoomlevel].x() + spacing, GridWidget::INNER_ICON_POS[zoomlevel].y() + offsety, _icontexture.scaled(GridWidget::INNER_ICON_SIZE[zoomlevel], GridWidget::INNER_ICON_SIZE[zoomlevel]));
    }
}
