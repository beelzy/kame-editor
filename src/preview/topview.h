// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef TOPVIEW_H
#define TOPVIEW_H

#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QPropertyAnimation>

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram);
QT_FORWARD_DECLARE_CLASS(QOpenGLTexture)

class TopView : public QOpenGLWidget, protected QOpenGLFunctions {
    Q_OBJECT
    Q_PROPERTY(float scrollpos READ scrollpos WRITE setScrollPos);

    static const GLfloat TILE_X;
    static const GLfloat TILE_Y;

    static const GLfloat TEXTURE_X;
    static const GLfloat TEXTURE_Y;

    static const QColor DEFAULT_COLOR;
    static const QColor DEFAULT_TEXTMAIN_COLOR;
    static const QColor DEFAULT_MAIN_COLOR;
    static const QColor DEFAULT_SHADOW_COLOR;

    static const QColor DEFAULT_DEMO_MAIN_COLOR;
    static const QColor DEFAULT_DEMO_TEXTMAIN_COLOR;

    public:
        TopView(QWidget *parent = 0);
        ~TopView();
        QSize sizeHint() const;

        static const int DEMO_OFFSET = 10;
        static const QString DEFAULT_TEXTURE;

        enum class DrawType {
            NONE,
            COLOR,
            COLORTEXTURE,
            TEXTURE
        };

        enum class FrameType {
            SINGLE,
            FASTSCROLL,
            SLOWSCROLL
        };

        void setDrawType();
        void setTexture(QString path);
        void setTextureExt(QString path);
        void setAlpha(float value);
        void setGradientAlpha(float value);
        void setGradientBrightness(float value);
        void setAlphaExt(float value);

        void setFrameType();

        float scrollpos();
        void setScrollPos(float value);

    public slots:
        void onIconChange(QWidget *icon);
        void onFolderStateChanged(bool open);
        void onScrollPositionChange(int pos);
        void onPageScroll(QPointF start, QPointF end);
        void enableScrolling();
        void setColor(QColor color);

        void setMainColor(QColor value);
        void setShadowColor(QColor value);
        void setTextMainColor(QColor value);
        void updateColors(bool defaultValue);

        void setFolderMainColor(QColor value);
        void setFolderDarkColor(QColor value);
        void setFolderLightColor(QColor value);
        void updateFolderColors(bool defaultValue);

        void setFileColor(QColor value);
        void updateFileColors(bool defaultValue);

        void setDemoMainColor(QColor value);
        void setDemoTextMainColor(QColor value);
        void updateDemoColors(bool defaultValue);

        void setAnimated(bool value);

    protected:
        void initializeGL();
        void resizeGL(int width, int height);
        void paintGL();
        void cleanup();
        void drawForeground(QPainter *p);
        void updateDrawType();
        void checkUpdate();
        bool needsUpdate();

    private:
        void drawButtons(QPainter *p);
        void drawStatus(QPainter *p);
        void drawText(QPainter *p);
        void drawFolderLabel(QPainter *p);
        QMatrix4x4 _pMatrix;
        QMatrix4x4 _matrix3D;
        QOpenGLShaderProgram *_tileshader;
        QOpenGLShaderProgram *_textureshader;
        QOpenGLShaderProgram *_foldershader;
        QOpenGLVertexArrayObject _vao;
        QOpenGLVertexArrayObject _fvao;
        QOpenGLVertexArrayObject _filevao;
        QOpenGLBuffer _vbo;
        QOpenGLBuffer _tvbo;
        QOpenGLBuffer _fvbo;
        QOpenGLBuffer _filevbo;
        QOpenGLTexture *_texture;
        QOpenGLTexture *_textureext;
        QImage *_battery;
        QImage _textureimage;
        QImage _textureextimage;
        QString _texturepath;
        QString _textureextpath;
        QVector<GLfloat> _vertices;
        QVector<QVector3D> _foldervertices;
        QVector<QVector3D> _filevertices;
        QVector<QVector2D> _uv;
        QColor _color;
        QColor _textcolor;
        QColor _main;
        QColor _shadow;
        QColor _demomain;
        QColor _demotextmain;
        QColor _foldermain;
        QColor _folderdark;
        QColor _folderlight;
        QColor _filecolor;
        QTimer *_timer;
        QTimer *_demotimer;
        int _colorid;
        int _foldermainid;
        int _folderdarkid;
        int _folderlightid;
        int _timeid;
        int _texturetimeid;
        QString _text;
        QString _foldertitle;
        bool _scrollenabled;
        GLfloat _scrollpos;
        GLfloat _gradientbrightness;
        GLfloat _gradientalpha;
        GLfloat _alpha;
        GLfloat _alphaext;
        QPropertyAnimation *_scrollanimation;
        int _demoscrollpos;
        bool _animated;
        bool _timeractivated;
        bool _showFolder;

        DrawType _drawtype;
};

#endif // TOPVIEW_H
