// This file is part of Kame Editor.
// Copyright (C) 2021 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef KAMETOOLS_H
#define KAMETOOLS_H

#include <QObject>
#include <QProcess>

class KameTools : public QObject {
    Q_OBJECT

    public:
        explicit KameTools(QObject *parent = 0);
        ~KameTools();
        static int LOSSY_VERSION[];
        static int MIN_VERSION[];

        void checkAvailable();
        void extractTheme(QString input, QString output, QString resources);
        void makeTheme(QString input, QString output, bool isFolder);
        //Qt5 does provide a converter for RGB16 images, but cannot do so in a near lossless way like we do with our algorithm.
        void convertImageRGB565(QString input, QString output);

    signals:
        void checkCompleted(bool);
        void themeExtracted(bool, QString output);
        void themeDeployed(bool, QString output);
        void imageRGB565Converted(bool, QString output);

    protected slots:
        void onCheckComplete(int, QProcess::ExitStatus);
        void onCheckError(QProcess::ProcessError);
        void onThemeExtracted(int, QProcess::ExitStatus);
        void onThemeDeployed(int, QProcess::ExitStatus);
        void onImageRGB565Converted(int, QProcess::ExitStatus);

    protected:
        void cleanupCheck();

    private:
        QProcess * _process;
};

#endif // KAMETOOLS_H
