// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QPainter>
#include <QDockWidget>
#include <QMainWindow>
#include <QMouseEvent>
#include <QSettings>
#include <QPainterPath>

#include "consolepad.h"

ConsolePad::ConsolePad(QWidget * parent) : QWidget(parent) {
    _container = new QFrame(this);

    _up = new QPushButton(_container);
    _up->setCheckable(true);
    _up->setToolTip(tr("About Kame Editor", "Tooltip for D-Pad up on the left side of the console."));
    _up->setCursor(Qt::PointingHandCursor);

    _down = new QPushButton(_container);
    _down->setCheckable(true);
    _down->setToolTip(tr("Settings", "Tooltip for D-Pad down on the left side of the console"));
    _down->setCursor(Qt::PointingHandCursor);

    _left = new QPushButton(_container);
    _left->setCheckable(true);
    _left->setToolTip(tr("Toggle Widget Lock", "Tooltip for D-Pad left on the left side of the console"));
    _left->setCursor(Qt::PointingHandCursor);

    _right = new QPushButton(_container);
    _right->setCheckable(true);
    _right->setToolTip(tr("Help", "Tooltip for displaying help overlay"));
    _right->setCursor(Qt::PointingHandCursor);

    _skinid = "3ds";
    updateSkin();
}

ConsolePad::~ConsolePad() {
    delete _up;
    delete _down;
    delete _left;
    delete _right;
    delete _container;
}

void ConsolePad::setSkinId(QString value) {
    _skinid = value;
    updateSkin();
}

void ConsolePad::updateSkin() {
    _base.load(":/resources/skins/" + _skinid + "/pad.png");
    _baseup.load(":/resources/skins/" + _skinid + "/pad-up.png");
    _basedown.load(":/resources/skins/" + _skinid + "/pad-down.png");
    _baseleft.load(":/resources/skins/" + _skinid + "/pad-left.png");
    _baseright.load(":/resources/skins/" + _skinid + "/pad-right.png");

    resize(_base.size());
    setFixedSize(_base.size());

    _container->resize(_base.size());
    _container->setFixedSize(_base.size());

    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);

    QIcon icon = makeStripe(QRect(skin.value("dpad/width").toInt() / 2, skin.value("dpad/stripe.offset").toInt(), skin.value("dpad/width").toInt(), skin.value("dpad/height").toInt()), true);
    QSize iconsize = icon.availableSizes()[0];

    _up->resize(iconsize);
    _up->setFixedSize(iconsize);

    _up->setGeometry((_base.width() - skin.value("dpad/width").toInt()) / 2, 0, _up->width(), _up->height());
    _up->setIcon(icon);
    _up->setIconSize(iconsize);

    icon = makeStripe(QRect(skin.value("dpad/width").toInt() / 2, skin.value("dpad/height").toInt() - skin.value("dpad/stripe.offset").toInt() - skin.value("dpad/stripe.height").toInt(), skin.value("dpad/width").toInt(), skin.value("dpad/height").toInt()), true);

    _down->resize(iconsize);

    _down->setGeometry((_base.width() - skin.value("dpad/width").toInt()) / 2, _base.height() - skin.value("dpad/height").toInt(), _down->width(), _down->height());
    _down->setIcon(icon);
    _down->setIconSize(iconsize);

    icon = makeStripe(QRect(skin.value("dpad/stripe.offset").toInt(), skin.value("dpad/width").toInt() / 2, skin.value("dpad/height").toInt(), skin.value("dpad/width").toInt()), false);
    iconsize = icon.availableSizes()[0];

    _left->resize(iconsize);

    _left->setGeometry(0, (_base.height() - skin.value("dpad/width").toInt()) / 2, _left->width(), _left->height());
    _left->setIcon(icon);
    _left->setIconSize(iconsize);

    icon = makeStripe(QRect(skin.value("dpad/height").toInt() - skin.value("dpad/stripe.height").toInt() - skin.value("dpad/stripe.offset").toInt(), skin.value("dpad/width").toInt() / 2, skin.value("dpad/height").toInt(), skin.value("dpad/width").toInt()), false);

    _right->resize(iconsize);

    _right->setGeometry(_base.width() - skin.value("dpad/height").toInt(), (_base.height() - skin.value("dpad/width").toInt()) / 2, _right->width(), _right->height());
    _right->setIcon(icon);
    _right->setIconSize(iconsize);
}

QPushButton *ConsolePad::up() {
    return _up;
}

QPushButton *ConsolePad::down() {
    return _down;
}

QPushButton *ConsolePad::left() {
    return _left;
}

QPushButton *ConsolePad::right() {
    return _right;
}

QIcon ConsolePad::makeStripe(QRect rect, bool isVertical) {
    QPixmap on = QPixmap(rect.width(), rect.height());
    on.fill(QColor(Qt::transparent));

    QPixmap off = QPixmap(rect.width(), rect.height());
    off.fill(QColor(Qt::transparent));

    QPainter p;
    p.begin(&on);
    p.setRenderHints(QPainter::Antialiasing);

    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);
    QPainterPath path;
    path.moveTo(rect.x(), rect.y());
    if (isVertical) {
        path.lineTo(rect.x(), rect.y() + skin.value("dpad/stripe.height").toInt());
    } else {
        path.lineTo(rect.x() + skin.value("dpad/stripe.height").toInt(), rect.y());
    }

    QPen pen = QPen(QColor(Qt::white));
    pen.setWidth(2);
    pen.setCosmetic(true);
    pen.setCapStyle(Qt::RoundCap);
    p.setPen(pen);
    p.drawPath(path);
    p.end();

    p.begin(&off);
    p.setRenderHints(QPainter::Antialiasing);

    pen.setColor(QColor("#BDBDBD"));
    p.setPen(pen);
    p.drawPath(path);
    p.end();

    QIcon icon(on);
    icon.addPixmap(off, QIcon::Normal, QIcon::Off);
    icon.addPixmap(on, QIcon::Normal, QIcon::On);

    return icon;
}

void ConsolePad::updateCheckedUp(bool isVisible) {
    QDockWidget *dockWidget = dynamic_cast<QDockWidget*>(QObject::sender());
    QMainWindow *main = dynamic_cast<QMainWindow*>(dockWidget->parentWidget());
    bool isAvailable = updateChecked(isVisible, dockWidget, main);
    bool oldState = _up->blockSignals(true);
    _up->setChecked(isAvailable);
    _up->blockSignals(oldState);
}

void ConsolePad::updateCheckedDown(bool isVisible) {
    QDockWidget *dockWidget = dynamic_cast<QDockWidget*>(QObject::sender());
    QMainWindow *main = dynamic_cast<QMainWindow*>(dockWidget->parentWidget());
    bool isAvailable = updateChecked(isVisible, dockWidget, main);
    bool oldState = _down->blockSignals(true);
    _down->setChecked(isAvailable);
    _down->blockSignals(oldState);
}

void ConsolePad::updateCheckedLeft(bool isVisible) {
    QDockWidget *dockWidget = dynamic_cast<QDockWidget*>(QObject::sender());
    QMainWindow *main = dynamic_cast<QMainWindow*>(dockWidget->parentWidget());
    bool isAvailable = updateChecked(isVisible, dockWidget, main);
    bool oldState = _left->blockSignals(true);
    _left->setChecked(isAvailable);
    _left->blockSignals(oldState);
}

void ConsolePad::updateCheckedRight(bool isVisible) {
    QWidget *widget = dynamic_cast<QWidget*>(QObject::sender());
    bool isAvailable = widget->isVisible();
    QDockWidget *dockWidget = dynamic_cast<QDockWidget*>(widget);
    if (dockWidget != nullptr) {
        QMainWindow *main = dynamic_cast<QMainWindow*>(dockWidget->parentWidget());
        isAvailable = updateChecked(isVisible, dockWidget, main);
    }
    bool oldState = _right->blockSignals(true);
    _right->setChecked(isAvailable);
    _right->blockSignals(oldState);
}

bool ConsolePad::updateChecked(bool isVisible, QDockWidget *dockWidget, QMainWindow *main) {
    if (main != nullptr && !main->tabifiedDockWidgets(dockWidget).isEmpty()) {
        return true;
    }
    return isVisible;
}

void ConsolePad::mouseMoveEvent(QMouseEvent *event) {
    event->accept();
}

void ConsolePad::paintEvent(QPaintEvent *) {
    QPainter p(this);
    _container->move(0, 0);

    QPixmap button = _base;
    if (_up->isDown()) {
        button = _baseup;
        _container->move(0, -1);
    } else if (_down->isDown()) {
        button = _basedown;
        _container->move(0, 1);
    } else if (_left->isDown()) {
        button = _baseleft;
        _container->move(-1, 0);
    } else if (_right->isDown()) {
        button = _baseright;
        _container->move(1, 0);
    }
    p.drawPixmap(0, 0, width(), height(), button);
}
