// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SEMVER_H
#define SEMVER_H

class SemVer {
    public:
        static int semver_compare_version (int x[3], int y[3]) {
            int res;

            if ((res = binary_comparison(x[0], y[0])) == 0) {
                if ((res = binary_comparison(x[1], y[1])) == 0) {
                    return binary_comparison(x[2], y[2]);
                }
            }

            return res;
        }

        static int binary_comparison (int x, int y) {
            if (x == y) return 0;
            if (x > y) return 1;
            return -1;
        }
};

#endif // SEMVER_H
