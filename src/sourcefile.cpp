// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QMap>
#include <QVariant>
#include <QColor>
#include <QFileInfo>
#include <QDir>

#include "sourcefile.h"

namespace SourceFile {
    const QStringList LANGUAGES = QList<QString>() << ""
        << "japanese"
        << "english"
        << "french"
        << "german"
        << "italian"
        << "spanish"
        << "simplifiedchinese"
        << "korean"
        << "dutch"
        << "portuguese"
        << "russian"
        << "traditionalchinese";

    const QStringList RATINGS = QList<QString>() << "cero"
        << "esrb"
        << "usk"
        << "pegigen"
        << "pegiptr"
        << "pegibbfc"
        << "cob"
        << "grb"
        << "cgsrr";

    const QStringList REGION = QList<QString>() << "japan"
        << "northamerica"
        << "europe"
        << "australia"
        << "china"
        << "korea"
        << "taiwan";

    const QStringList FLAGS = QList<QString>() << "visible"
        << "autoboot"
        << "allow3d"
        << "requireeula"
        << "autosave"
        << "extendedbanner"
        << "ratingrequired"
        << "savedata"
        << "recordusage"
        << "nosavebackups"
        << "new3ds";

    const QStringList TOPDRAW = QList<QString>() << "none"
        << "color"
        << "colortexture"
        << "texture";

    const QStringList TOPFRAME = QList<QString>() << "single"
        << "fastscroll"
        << "slowscroll";

    const QStringList BOTTOMDRAW = QList<QString>() << "none"
        << "color"
        << "texture";

    const QStringList BOTTOMFRAME = QList<QString>() << "single"
        << "fastscroll"
        << "slowscroll"
        << "pagescroll"
        << "bouncescroll";

    bool recentlyModified = false;

    QString shorttitle[13] = {""};
    QString longtitle[13] = {""};
    QString publisher[13] = {""};

    QString smdhIconPath = "";
    QString smdhSmallIconPath = "";

    QMap<QString, QVariant> ratings[9] = {QMap<QString, QVariant>()};
    bool isRegionFree = false;
    QStringList regions = QList<QString>();

    QString matchmakerid = "";
    QString matchmakerbitid = "";

    QStringList flags = QList<QString>();
    QString eulaversion = "";
    float optimalbannerframe = 0.0;

    QString streetpassid = "";

    int topdraw = 0;
    int topframe = 0;
    int bottomdraw = 0;
    int bottomframe = 0;

    QStringList toptexture = QList<QString>() << "" << "" << "";
    QString topexttexture = "";

    QMap<QString, QVariant> topcolors = QMap<QString, QVariant>();

    QStringList bottomtexture = QList<QString>() << "" << "";

    QMap<QString, QColor> bottomoutercolors = QMap<QString, QColor>();
    QMap<QString, QColor> bottominnercolors = QMap<QString, QColor>();

    QStringList foldericons = QList<QString>() << "" << "";
    QStringList fileicons = QList<QString>() << "" << "";

    QMap<QString, QColor> cursorcolors = QMap<QString, QColor>();

    bool cursorcolorEnabled = false;

    QMap<QString, QColor> foldercolors = QMap<QString, QColor>();

    bool foldercolorEnabled = false;

    QMap<QString, QColor> filecolors = QMap<QString, QColor>();

    bool filecolorEnabled = false;

    QMap<QString, QVariant> opencolors = QMap<QString, QVariant>();
    QMap<QString, QVariant> closecolors = QMap<QString, QVariant>();

    bool openclosecolorEnabled = false;

    QMap<QString, QVariant> folderbackbuttoncolors = QMap<QString, QVariant>();

    bool folderbackbuttoncolorEnabled = false;

    QMap<QString, QColor> arrowbuttonbasecolors = QMap<QString, QColor>();

    bool arrowbuttonbasecolorEnabled = false;

    QMap<QString, QColor> arrowbuttonarrowcolors = QMap<QString, QColor>();

    bool arrowbuttonarrowcolorEnabled = false;

    QMap<QString, QColor> bottomcornercolors = QMap<QString, QColor>();

    bool bottomcornercolorEnabled = false;

    QMap<QString, QColor> topcornercolors = QMap<QString, QColor>();

    bool topcornercolorEnabled = false;

    QMap<QString, QColor> folderviewcolors = QMap<QString, QColor>();

    bool folderviewcolorEnabled = false;

    QMap<QString, QColor> gametextcolors = QMap<QString, QColor>();

    bool gametextcolorEnabled = false;
    bool gametextcolorHidden = false;

    QMap<QString, QColor> demotextcolors = QMap<QString, QColor>();

    bool demotextcolorEnabled = false;

    QString bgm = "";

    QMap<QString, QString> sfxes = QMap<QString, QString>();

    QString deploypath = "";

    QString colorToHex(QColor color, QColor::NameFormat format) {
        QString output = color.name().replace(0, 1, "0x");
        return format == QColor::HexArgb ? output + QString::number(color.alpha(), 16) : output;
    }

    QColor hexToColor(QString hex) {
        QColor color = QColor(hex.replace(0, 2, "#"));
        if (hex.length() > 7) {
            int value = color.alpha();
            color.setAlpha(color.blue());
            int value2 = color.red();
            color.setRed(value);
            value = color.green();
            color.setGreen(value2);
            color.setBlue(value);
        }
        return color;
    }

    QString rgb565path(QString path, QString ext) {
        QFileInfo file(path);
        if (file.exists() && !file.completeSuffix().endsWith("rgb565." + ext)) {
            QFileInfo ditherpath(file.dir().path() + "/" + file.completeBaseName() + ".rgb565." + ext);
            return ditherpath.exists() ? ditherpath.canonicalFilePath() : path;
        }
        return path;
    }

    QMap<QString, QColor> toQColorMap(QMap<QString, QVariant> map) {
        QMap<QString, QColor> colormap;
        for (auto key : map.keys()) {
            if (key != "textshadowpos") {
                colormap[key] = map[key].value<QColor>();
            }
        }
        return colormap;
    }

    void toQVariantMap(QMap<QString, QColor> map, QMap<QString, QVariant> *variant) {
        for (auto key : map.keys()) {
            (*variant)[key] = map[key];
        }
    }

    void reset() {
        for (int i = 0; i < 13; i++) {
            shorttitle[i] = "";
            longtitle[i] = "";
            publisher[i] = "";
        }

        smdhIconPath = "";
        smdhSmallIconPath = "";

        for (int i = 0; i < 9; i++) {
            QMap<QString, QVariant> rating;
            rating["age"] = 0;
            rating["active"] = false;
            rating["pending"] = false;
            rating["noage"] = false;
            ratings[i] = rating;
        }
        isRegionFree = false;
        regions = QList<QString>();

        matchmakerid = "";
        matchmakerbitid = "";

        flags = QList<QString>();
        eulaversion = "";
        optimalbannerframe = 0.0;

        streetpassid = "";

        topdraw = 0;
        topframe = 0;
        bottomdraw = 0;
        bottomframe = 0;

        toptexture = QList<QString>() << "" << "" << "";
        topexttexture = "";

        topcolors = QMap<QString, QVariant>();

        bottomtexture = QList<QString>() << "" << "";

        bottomoutercolors = QMap<QString, QColor>();
        bottominnercolors = QMap<QString, QColor>();

        foldericons = QList<QString>() << "" << "";
        fileicons = QList<QString>() << "" << "";

        cursorcolorEnabled = false;

        cursorcolors = QMap<QString, QColor>();
        cursorcolors["dark"] = QColor();
        cursorcolors["main"] = QColor();
        cursorcolors["light"] = QColor();
        cursorcolors["glow"] = QColor();

        foldercolorEnabled = false;

        foldercolors = QMap<QString, QColor>();
        foldercolors["dark"] = QColor();
        foldercolors["main"] = QColor();
        foldercolors["light"] = QColor();
        foldercolors["shadow"] = QColor();

        filecolorEnabled = false;

        filecolors = QMap<QString, QColor>();
        filecolors["dark"] = QColor();
        filecolors["main"] = QColor();
        filecolors["light"] = QColor();
        filecolors["shadow"] = QColor();

        openclosecolorEnabled = false;

        opencolors = QMap<QString, QVariant>();
        opencolors["textshadowpos"] = 0;
        opencolors["dark"] = QColor();
        opencolors["main"] = QColor();
        opencolors["light"] = QColor();
        opencolors["glow"] = QColor();
        opencolors["shadow"] = QColor();
        opencolors["textshadow"] = QColor();
        opencolors["textmain"] = QColor();
        opencolors["textselected"] = QColor();

        closecolors = QMap<QString, QVariant>();
        closecolors["textshadowpos"] = 0;
        closecolors["dark"] = QColor();
        closecolors["main"] = QColor();
        closecolors["light"] = QColor();
        closecolors["glow"] = QColor();
        closecolors["shadow"] = QColor();
        closecolors["textshadow"] = QColor();
        closecolors["textmain"] = QColor();
        closecolors["textselected"] = QColor();

        folderbackbuttoncolorEnabled = false;

        folderbackbuttoncolors = QMap<QString, QVariant>();
        folderbackbuttoncolors["textshadowpos"] = 0;
        folderbackbuttoncolors["dark"] = QColor();
        folderbackbuttoncolors["main"] = QColor();
        folderbackbuttoncolors["light"] = QColor();
        folderbackbuttoncolors["glow"] = QColor();
        folderbackbuttoncolors["shadow"] = QColor();
        folderbackbuttoncolors["textshadow"] = QColor();
        folderbackbuttoncolors["textmain"] = QColor();
        folderbackbuttoncolors["textselected"] = QColor();

        arrowbuttonbasecolorEnabled = false;

        arrowbuttonbasecolors = QMap<QString, QColor>();
        arrowbuttonbasecolors["dark"] = QColor();
        arrowbuttonbasecolors["main"] = QColor();
        arrowbuttonbasecolors["light"] = QColor();
        arrowbuttonbasecolors["shadow"] = QColor();

        arrowbuttonarrowcolorEnabled = false;

        arrowbuttonarrowcolors = QMap<QString, QColor>();
        arrowbuttonarrowcolors["border"] = QColor();
        arrowbuttonarrowcolors["unpressed"] = QColor();
        arrowbuttonarrowcolors["pressed"] = QColor();

        bottomcornercolorEnabled = false;

        bottomcornercolors = QMap<QString, QColor>();
        bottomcornercolors["dark"] = QColor();
        bottomcornercolors["main"] = QColor();
        bottomcornercolors["light"] = QColor();
        bottomcornercolors["shadow"] = QColor();
        bottomcornercolors["iconmain"] = QColor();
        bottomcornercolors["iconlight"] = QColor();
        bottomcornercolors["icontextmain"] = QColor();

        topcornercolorEnabled = false;

        topcornercolors = QMap<QString, QColor>();
        topcornercolors["main"] = QColor();
        topcornercolors["light"] = QColor();
        topcornercolors["shadow"] = QColor();
        topcornercolors["textmain"] = QColor();

        folderviewcolorEnabled = false;

        folderviewcolors = QMap<QString, QColor>();
        folderviewcolors["dark"] = QColor();
        folderviewcolors["main"] = QColor();
        folderviewcolors["light"] = QColor();
        folderviewcolors["shadow"] = QColor();

        gametextcolorEnabled = false;
        gametextcolorHidden = false;

        gametextcolors = QMap<QString, QColor>();
        gametextcolors["main"] = QColor();
        gametextcolors["light"] = QColor();
        gametextcolors["shadow"] = QColor();
        gametextcolors["textmain"] = QColor();

        demotextcolorEnabled = false;

        demotextcolors = QMap<QString, QColor>();
        demotextcolors["main"] = QColor();
        demotextcolors["textmain"] = QColor();

        bgm = "";

        sfxes = QMap<QString, QString>();
        sfxes["cursor"] = "";
        sfxes["launch"] = "";
        sfxes["folder"] = "";
        sfxes["close"] = "";
        sfxes["frame0"] = "";
        sfxes["frame1"] = "";
        sfxes["frame2"] = "";
        sfxes["resume"] = "";

        deploypath = "";
    }
}
