// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CONSOLEMENU_H
#define CONSOLEMENU_H

#include <QWidget>
#include <QPushButton>
#include <QSettings>
#include <QFileDialog>
#include <QProgressDialog>
#include <QTimer>
#include <memory>

#include "kametools.h"
#include "resourcesdialog.h"

class ConsoleMenu : public QWidget {
    Q_OBJECT

    public:
        explicit ConsoleMenu(QWidget *parent = 0);
        ~ConsoleMenu();

        enum class State {
            SOURCE,
            SOURCEMORE,
            DEPLOY
        };

        static QString DEFAULT_IMPORT_RC;
        static QStringList DEFAULT_SHORTTITLE;
        static QStringList DEFAULT_LONGTITLE;
        static QStringList DEFAULT_PUBLISHER;

        bool saveSourceFile(bool autosave=false, bool rgb565=false);
        void setSkinId(QString value);
        void updateSkin();
        void deleteAutosave();

    public slots:
        void checkSourceFile();
        void autosaveChanged(int interval);
        void restoreAutosave(QString path);
        void changeDeployType(bool enabled);

    signals:
        void resetSource();
        void sourceLoaded();
        void deployPathSelected();
        void autosaveDeleted();
        void kameToolsCheckComplete();

    protected:
        void updateSaveButton();
        void updateLoadButton();
        QPixmap disabledTexture(QString path);
        QPixmap warningTexture(QString path);
        void loadSourceFile();
        bool checkMissingResources(QString file);
        void addMissingResource(QString id, QString path, QString type, QString title);
        bool validateSourceFile();
        static std::unique_ptr<QFileDialog> getSaveDialog();
        static std::unique_ptr<QFileDialog> getLoadDialog();
        static std::unique_ptr<QFileDialog> getDeployDialog();
        static std::unique_ptr<QFileDialog> getImportDialog();
        void showLoadDialog();
        void resetSourceFile();
        void generateSuffix();
        void checkKameTools();
        void init();
        void drawOptionButton(QPushButton *button, QString icon, int index, bool warning=false);
        void drawHomeButton(QString icon);
        void mouseMoveEvent(QMouseEvent *event);
        void importRGB565Texture(QString key);
        QString importOutputPath(QString input);
        void resetImportOutputPath(QString input, bool recreate=true);

        QPixmap combineIcons(QPixmap button, QPixmap option);
        QPixmap overlayIcons(QPixmap button, QPixmap icon, int offsetY = 0);

    protected slots:
        void onLeftButtonClicked();
        void onMiddleButtonClicked();
        void onRightButtonClicked();
        void handleImport(bool success, QString output);
        void handleDeploy(bool success, QString output);
        void onKameToolsCheck(bool success);
        void processAutosave();

    private:
        QPushButton *_leftButton;
        QPushButton *_middleButton;
        QPushButton *_rightButton;

        State _state;

        QSettings *_sourcefile;
        QString _validateSaveMessage;
        QString _validateLoadMessage;

        QString _filepath;
        QString _deploypath;
        QString _importpath;

        KameTools *_kametools;
        QProgressDialog *_progress;

        QVector<missingResource> _missing;
        bool _isvalid;
        bool _kametoolsFound;
        bool _importEnabled;
        bool _noditherEnabled;
        bool _deploytype;
        int _kametoolsVersion[3];
        QString _skinid;
        QTimer *_autosavetimer;
        QString _suffix;
        QString _currentAutosavePath;
        QString _unsavedChangesTitle;
        QString _unsavedChanges;
        QString _warnLossy;
};

#endif // CONSOLEMENU_H
