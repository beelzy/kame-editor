// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SOURCEFILE_H
#define SOURCEFILE_H

#include <QObject>
#include <QMap>

namespace SourceFile {
    extern const QStringList LANGUAGES;
    extern const QStringList RATINGS;
    extern const QStringList REGION;
    extern const QStringList FLAGS;
    extern const QStringList TOPDRAW;
    extern const QStringList TOPFRAME;
    extern const QStringList BOTTOMDRAW;
    extern const QStringList BOTTOMFRAME;

    extern bool recentlyModified;

    extern QString shorttitle[13];
    extern QString longtitle[13];
    extern QString publisher[13];

    extern QString smdhIconPath;
    extern QString smdhSmallIconPath;

    extern QMap<QString, QVariant> ratings[9];
    extern bool isRegionFree;
    extern QStringList regions;

    extern QString matchmakerid;
    extern QString matchmakerbitid;

    extern QStringList flags;

    extern QString eulaversion;

    extern float optimalbannerframe;
    extern QString streetpassid;

    extern int topdraw;
    extern int topframe;
    extern int bottomdraw;
    extern int bottomframe;

    extern QStringList toptexture;
    extern QString topexttexture;
    extern QMap<QString, QVariant> topcolors;

    extern QStringList bottomtexture;
    extern QMap<QString, QColor> bottomoutercolors;
    extern QMap<QString, QColor> bottominnercolors;

    extern QStringList foldericons;
    extern QStringList fileicons;

    extern bool cursorcolorEnabled;
    extern QMap<QString, QColor> cursorcolors;

    extern bool foldercolorEnabled;
    extern QMap<QString, QColor> foldercolors;

    extern bool filecolorEnabled;
    extern QMap<QString, QColor> filecolors;

    extern bool openclosecolorEnabled;
    extern QMap<QString, QVariant>opencolors;
    extern QMap<QString, QVariant>closecolors;

    extern bool folderbackbuttoncolorEnabled;
    extern QMap<QString, QVariant>folderbackbuttoncolors;

    extern bool arrowbuttonbasecolorEnabled;
    extern QMap<QString, QColor>arrowbuttonbasecolors;

    extern bool arrowbuttonarrowcolorEnabled;
    extern QMap<QString, QColor>arrowbuttonarrowcolors;

    extern bool bottomcornercolorEnabled;
    extern QMap<QString, QColor>bottomcornercolors;

    extern bool topcornercolorEnabled;
    extern QMap<QString, QColor>topcornercolors;

    extern bool folderviewcolorEnabled;
    extern QMap<QString, QColor>folderviewcolors;

    extern bool gametextcolorEnabled;
    extern bool gametextcolorHidden;
    extern QMap<QString, QColor>gametextcolors;

    extern bool demotextcolorEnabled;
    extern QMap<QString, QColor>demotextcolors;

    extern QString bgm;
    extern QMap<QString, QString> sfxes;

    extern QString deploypath;

    extern QColor hexToColor(QString hex);
    extern QString colorToHex(QColor color, QColor::NameFormat format = QColor::HexRgb);
    extern QString rgb565path(QString path, QString ext="png");
    extern QMap<QString, QColor> toQColorMap(QMap<QString, QVariant>);
    extern void toQVariantMap(QMap<QString, QColor>, QMap<QString, QVariant>*);
    extern void reset();
}

#endif //SOURCEFILE_H
