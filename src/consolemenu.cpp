// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QHBoxLayout>
#include <QPainter>
#include <QMessageBox>
#include <QBuffer>
#include <QProgressBar>
#include <QStandardPaths>
#include <QDateTime>
#include <memory>
#include <QMap>
#include <QDebug>

#include "consolemenu.h"
#include "consolewidget.h"
#include "sourcefile.h"
#include "preview/topview.h"
#include "preview/bottomview.h"
#include "miniz.h"
#include "color.h"
#include "semver.h"

/*
    TRANSLATOR ConsoleMenu

    The three buttons at the bottom of the console. They change when you click on the middle source button. There are three modes:
    Source 1 - contains options for creating a new theme or loading a theme source file
    Source 2 - contains options for saving theme source files
    Deploy - contains options for deploying theme files
*/

QString ConsoleMenu::DEFAULT_IMPORT_RC = "theme.rc";

QStringList ConsoleMenu::DEFAULT_SHORTTITLE = {
    "Theme",
    "テーマ",
    "Theme"
};

QStringList ConsoleMenu::DEFAULT_LONGTITLE = {
    "Generic Theme",
    "汎用テーマ",
    "Generic Theme"
};

QStringList ConsoleMenu::DEFAULT_PUBLISHER = {
    "Kame Editor",
    "カメエディタ",
    "Kame Editor"
};

ConsoleMenu::ConsoleMenu(QWidget *parent) : QWidget(parent) {
    _unsavedChangesTitle = tr("Theme has unsaved changes", "Title for the dialog");
    _unsavedChanges = tr("The current theme has unsaved changes. Would you like to save them before loading or starting a new one?", "Asks the user whether or not they want to save the current theme before creating a new theme.");
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setContentsMargins(0, 0, 0, 0);

    _kametoolsFound = true;
    _importEnabled = false;
    _noditherEnabled = false;
    _warnLossy = "";
    _deploytype = false;

    _kametools = new KameTools();
    _progress = nullptr;

    setFixedSize(371, 20);

    _sourcefile = nullptr;
    _state = State::SOURCE;

    _leftButton = new QPushButton();
    _leftButton->setFlat(true);
    _leftButton->setCursor(Qt::PointingHandCursor);

    _middleButton = new QPushButton();
    _middleButton->setFlat(true);
    _middleButton->setCursor(Qt::PointingHandCursor);

    _rightButton = new QPushButton();
    _rightButton->setFlat(true);
    _rightButton->setCursor(Qt::PointingHandCursor);

    init();

    setLayout(layout);

    _skinid = "3ds";
    updateSkin();

    _suffix = QDateTime::currentDateTime().toString("dd-MM-yyyy_HH-mm-ss-zzz");

    _autosavetimer = new QTimer(this);

    connect(_autosavetimer, &QTimer::timeout, this, &ConsoleMenu::processAutosave);

    processAutosave();

    connect(_leftButton, &QAbstractButton::clicked, this, &ConsoleMenu::onLeftButtonClicked);
    connect(_middleButton, &QAbstractButton::clicked, this, &ConsoleMenu::onMiddleButtonClicked);
    connect(_rightButton, &QAbstractButton::clicked, this, &ConsoleMenu::onRightButtonClicked);

    checkKameTools();
    checkSourceFile();
}

ConsoleMenu::~ConsoleMenu() {
    delete _leftButton;
    delete _middleButton;
    delete _rightButton;

    if (_sourcefile != nullptr) {
        delete _sourcefile;
    }
    delete _kametools;
    if (_progress != nullptr) {
        delete _progress;
    }

    delete _autosavetimer;
}

void ConsoleMenu::setSkinId(QString value) {
    _skinid = value;
    updateSkin();
}

void ConsoleMenu::updateSkin() {
    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);

    int width = skin.value("menu/width", 0).toInt();
    int height = skin.value("menu/height", 0).toInt();
    setFixedSize(width, height);
    QString leftOption = _state == State::SOURCE ? "new" : _state == State::SOURCEMORE ? "save" : "load";
    QString rightOption = _state == State::SOURCE ? "load" : _state == State::DEPLOY ? "save" : "saveas";
    QString middleOption = _state == State::DEPLOY ? "deploy" : "source";

    if (skin.value("menu/type").toString() == "row") {
        _leftButton->setIconSize(QSize(width / 3, height));
        _leftButton->setIcon(QIcon(":/resources/skins/" + _skinid + "/button-" + leftOption + ".png"));

        _middleButton->setIconSize(QSize(width / 3, height));
        _middleButton->setIcon(QIcon(":/resources/skins/" + _skinid + "/button-" + middleOption +".png"));

        _rightButton->setIconSize(QSize(width / 3, height));
        if (_state != State::DEPLOY) {
            _rightButton->setIcon(QIcon(":/resources/skins/" + _skinid + "/button-" + rightOption + ".png"));
        }

        dynamic_cast<QHBoxLayout*>(layout())->addWidget(_leftButton, 1, Qt::AlignCenter);
        dynamic_cast<QHBoxLayout*>(layout())->addWidget(_middleButton, 1, Qt::AlignCenter);
        dynamic_cast<QHBoxLayout*>(layout())->addWidget(_rightButton, 1, Qt::AlignCenter);
    } else if (skin.value("menu/type").toString() == "detached") {
        QHBoxLayout *hlayout = dynamic_cast<QHBoxLayout*>(layout());
        hlayout->removeWidget(_leftButton);
        drawOptionButton(_leftButton, leftOption, 1, false);

        hlayout->removeWidget(_rightButton);
        if (_state != State::DEPLOY) {
            drawOptionButton(_rightButton, rightOption, 2);
        }

        hlayout->removeWidget(_middleButton);
        drawHomeButton(middleOption);
    }
    if (_state == State::DEPLOY) {
        updateSaveButton();
        updateLoadButton();
    }
}

void ConsoleMenu::deleteAutosave() {
    if (!_currentAutosavePath.isEmpty()) {
        QFile(_currentAutosavePath).remove();
        emit autosaveDeleted();
    }
}

void ConsoleMenu::init() {
    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);

    if (skin.value("menu/type").toString() == "row") {
        _leftButton->setIcon(QIcon(":/resources/skins/" + _skinid + "/button-new.png"));
        _middleButton->setIcon(QIcon(":/resources/skins/" + _skinid + "/button-source.png"));
        _rightButton->setIcon(QIcon(":/resources/skins/" + _skinid + "/button-load.png"));
    } else if (skin.value("menu/type").toString() == "detached") {
        drawOptionButton(_leftButton, "new", 1);
        drawOptionButton(_rightButton, "load", 2);

        drawHomeButton("source");
    }
    _leftButton->setToolTip(tr("Create New Theme", "Display a tooltip when hovering over the New button."));
    _middleButton->setToolTip(tr("Source Menu: Manage source files used by Kame Editor. - Click here to toggle more menus.", "Display a tooltip when hovering over the Source button."));
    _rightButton->setToolTip(tr("Load Theme Source", "Display a tooltip when hovering over the Load button."));

    _leftButton->setEnabled(true);
}

void ConsoleMenu::drawOptionButton(QPushButton *button, QString iconpath, int index, bool warning) {
    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);
    QPixmap optionsButton = QPixmap(":/resources/skins/" + _skinid + "/menubase.png");
    QPixmap optionsButtonDown = QPixmap(":/resources/skins/" + _skinid + "/menubase-down.png");
    int optionsHeight = skin.value("menu/options.height").toInt();
    QString path = ":/resources/skins/" + _skinid + "/button-" + iconpath + ".png";
    QPixmap pixmap = warning ? warningTexture(path) : QPixmap(path);
    button->setIconSize(QSize(optionsButton.width() + pixmap.width(), optionsHeight));
    QIcon icon(combineIcons(optionsButton, pixmap));
    icon.addPixmap(combineIcons(optionsButtonDown, pixmap), QIcon::Active);
    button->setIcon(icon);
    button->setGeometry(skin.value("menu/option" + QString::number(index) + ".x", 0).toInt(), skin.value("menu/option" + QString::number(index) + ".y", 0).toInt(), optionsButton.width() + pixmap.width(), optionsHeight);
}

void ConsoleMenu::drawHomeButton(QString iconpath) {
    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);
    QPixmap homeButton = QPixmap(":/resources/skins/" + _skinid + "/homebase.png");
    QPixmap homeButtonDown = QPixmap(":/resources/skins/" + _skinid + "/homebase-down.png");
    QPixmap homeIcon = QPixmap(":/resources/skins/" + _skinid + "/button-" + iconpath + ".png");
    QIcon icon(overlayIcons(homeButton, homeIcon));
    icon.addPixmap(overlayIcons(homeButtonDown, homeIcon, 1), QIcon::Active);

    _middleButton->setIconSize(QSize(skin.value("menu/home.width", 0).toInt(), skin.value("menu/home.height", 0).toInt()));
    _middleButton->setIcon(icon);
    _middleButton->setGeometry(skin.value("menu/home.x", 0).toInt(), skin.value("menu/home.y", 0).toInt(), skin.value("menu/home.width", 0).toInt(), skin.value("menu/home.height", 0).toInt());
}

void ConsoleMenu::onLeftButtonClicked() {
    _leftButton->clearFocus();
    switch(_state) {
        case State::SOURCE:
            if (SourceFile::recentlyModified) {
                QMessageBox::StandardButton reply;
                reply = QMessageBox::question(this, _unsavedChangesTitle,
                        _unsavedChanges,
                        QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
                if(reply == QMessageBox::Save) {
                    saveSourceFile();
                }
                if (reply == QMessageBox::Save || reply == QMessageBox::Discard) {
                    resetSourceFile();
                }
            } else {
                resetSourceFile();
            }
            break;
        case State::SOURCEMORE:
            saveSourceFile();
            break;
        case State::DEPLOY:
            if (_kametoolsFound && _importEnabled) {
                QSettings settings;
                bool isFolder = settings.contains("import/folder") && settings.value("import/folder").toBool();
                if (isFolder) {
                    _importpath = QFileDialog::getExistingDirectory(this, tr("Import Theme from Folder...", "Dialog when choosing theme folder for import."));
                } else {
                    auto _filedialog = getImportDialog();
                    if (_filedialog->exec()) {
                        _importpath = _filedialog->selectedFiles().first();
                    }
                }
                if (!_importpath.isEmpty()) {
                    connect(_kametools, &KameTools::themeExtracted, this, &ConsoleMenu::handleImport);
                    resetImportOutputPath(_importpath);
                    QString output = importOutputPath(_importpath);
                    _kametools->extractTheme(_importpath, output + "/" + ConsoleMenu::DEFAULT_IMPORT_RC, output);
                    QProgressBar *bar = new QProgressBar();
                    QPalette pal = bar->palette();
                    pal.setColor(QPalette::Highlight, Color::MAIN);
                    bar->setPalette(pal);
                    if (_progress != nullptr) {
                        delete _progress;
                    }
                    _progress = new QProgressDialog();
                    _progress->setWindowTitle(tr("Kame Editor", "Titlebar for deploying progress dialog."));
                    _progress->setLabelText(tr("Importing...", "Click on the load (import) button. Indicates the theme is being imported in a progress dialog."));
                    _progress->setBar(bar);
                    _progress->setWindowModality(Qt::WindowModal);
                    _progress->setRange(0, 0);
                    _progress->setMinimumDuration(0);
                    _progress->setCancelButton(0);
                    _progress->setValue(1);
                }
            } else {
                QMessageBox msgBox(QMessageBox::Warning, _importEnabled ? tr("kame-tools Version", "Displays a warning message when trying to click on the load button and kame-tools is < v1.3.1.") : tr("kame-tools Missing", "In the same warning message as the previous item. Displayed if kame-tools is not found in the PATH variable."), _validateLoadMessage, QFlag(0), this);
                msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
                msgBox.exec();
            }
            break;
        default:
            break;
    }
}

void ConsoleMenu::onMiddleButtonClicked() {
    _middleButton->clearFocus();
    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);
    switch(_state) {
        case State::SOURCE:
            if (skin.value("menu/type").toString() == "row") {
                _leftButton->setIcon(QIcon(":/resources/skins/" + _skinid + "/button-save.png"));
                _rightButton->setIcon(QIcon(":/resources/skins/" + _skinid + "/button-saveas.png"));
            } else {
                drawOptionButton(_leftButton, "save", 1);
                drawOptionButton(_rightButton, "saveas", 2);
            }
            _leftButton->setEnabled(true);
            _leftButton->setToolTip(tr("Save Source File", "Display a tooltip when hovering over the save button"));
            _rightButton->setToolTip(tr("Save Source File As...", "Display a tooltip when hovering over the save as button"));
            break;
        case State::SOURCEMORE:
            if (skin.value("menu/type").toString() == "row") {
                QString loadpath = ":/resources/skins/" + _skinid + "/button-load.png";
                QIcon icon = QIcon(loadpath);
                icon.addPixmap(disabledTexture(loadpath), QIcon::Disabled);
                _leftButton->setIcon(icon);
                _middleButton->setIcon(QIcon(":/resources/skins/" + _skinid + "/button-deploy.png"));
            } else {
                drawOptionButton(_leftButton, "load", 1, false);
                drawHomeButton("deploy");
            }
            _middleButton->setToolTip(tr("Deploy Menu: Manage theme files used on an actual 3DS. - Click here to toggle more menus.", "Display a tooltip when hovering over the middle button for toggling sections"));
            updateSaveButton();
            updateLoadButton();
            break;
        case State::DEPLOY:
        default:
            init();
            break;
    }

    _state = static_cast<State>((static_cast<int>(_state) + 1) % (static_cast<int>(State::DEPLOY) + 1));
}

void ConsoleMenu::onRightButtonClicked() {
    _rightButton->clearFocus();
    switch(_state) {
        case State::SOURCE:
            if (SourceFile::recentlyModified) {
                QMessageBox::StandardButton reply;
                reply = QMessageBox::question(this, _unsavedChangesTitle,
                        _unsavedChanges,
                        QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
                if(reply == QMessageBox::Save) {
                        saveSourceFile();
                }
                if (reply == QMessageBox::Discard || reply == QMessageBox::Save) {
                        showLoadDialog();
                }
            } else {
                showLoadDialog();
            }
            break;
        case State::SOURCEMORE: {
                auto _filedialog = getSaveDialog();
                if (_filedialog->exec()) {
                    _filepath = _filedialog->selectedFiles().first();
                    saveSourceFile();
                }
            }
            break;
        case State::DEPLOY:
            if (_kametoolsFound) {
                _deploypath = SourceFile::deploypath;
                if (_deploypath.isEmpty()) {
                    if (_deploytype) {
                        QString path = QFileDialog::getExistingDirectory(this, tr("Deploy Theme Folder...", "Dialogue title when choosing to deploy a theme to a folder."));
                        if (path.isEmpty()) {
                            return;
                        } else {
                            _deploypath = path;
                            QDir dir = QDir(_deploypath);
                            QStringList filelist = dir.entryList(QDir::Files | QDir::NoDotAndDotDot | QDir::NoDot | QDir::NoDotDot);
                            if (filelist.contains("info.smdh", Qt::CaseInsensitive) || filelist.contains("body_lz.bin", Qt::CaseInsensitive) || filelist.contains("bgm.bcstm", Qt::CaseInsensitive) || filelist.contains("preview.png", Qt::CaseInsensitive)) {
                                QMessageBox msgBox(QMessageBox::Warning, tr("Theme Files Already Exist", "Title for message box warning users deploy folder already contains theme files"), tr("The folder you have chosen for theme deploy already contains theme files. These files will be overwritten if you deploy your current theme. Continue with theme deploy anyways?"), QFlag(0), this);
                                msgBox.addButton(tr("Deploy"), QMessageBox::AcceptRole);
                                msgBox.addButton(tr("Cancel"), QMessageBox::RejectRole);
                                if (msgBox.exec() == QMessageBox::RejectRole) {
                                    return;
                                }
                            }

                            SourceFile::deploypath = _deploypath;
                            emit deployPathSelected();
                        }
                    } else {
                        auto _filedialog = getDeployDialog();
                        if (_filedialog->exec()) {
                            _deploypath = _filedialog->selectedFiles().first();
                            SourceFile::deploypath = _deploypath;
                            emit deployPathSelected();
                        } else {
                            return;
                        }
                    }
                }
                if (saveSourceFile(false, true)) {
                    connect(_kametools, &KameTools::themeDeployed, this, &ConsoleMenu::handleDeploy);
                    _kametools->makeTheme(SourceFile::rgb565path(_filepath, "rc"), _deploypath, _deploytype);
                    QProgressBar *bar = new QProgressBar();
                    QPalette pal = bar->palette();
                    pal.setColor(QPalette::Highlight, Color::MAIN);
                    bar->setPalette(pal);
                    if (_progress != nullptr) {
                        delete _progress;
                    }
                    _progress = new QProgressDialog();
                    _progress->setWindowTitle(tr("Kame Editor", "Titlebar for deploying progress dialog."));
                    _progress->setLabelText(tr("Deploying...", "Click on the deploy button. Indicates the theme is being deployed in a progress dialog."));
                    _progress->setBar(bar);
                    _progress->setWindowModality(Qt::WindowModal);
                    _progress->setRange(0, 0);
                    _progress->setMinimumDuration(0);
                    _progress->setCancelButton(0);
                    _progress->setValue(1);
                }
            } else {
                QMessageBox msgBox(QMessageBox::Warning, _kametoolsFound ? tr("Theme Required Fields Missing", "Displays a warning message when trying to click on the deploy button and some required fields are missing.") : tr("kame-tools Missing", "In the same warning message as the previous item. Displayed if kame-tools is not found in the PATH variable."), _validateSaveMessage, QFlag(0), this);
                msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
                msgBox.exec();
            }
            break;
    }
}

void ConsoleMenu::showLoadDialog() {
    auto _filedialog = getLoadDialog();
    if (_filedialog->exec()) {
        // Already handled by save/discard dialog
        deleteAutosave();
        QString filepath = _filedialog->selectedFiles().first();
        QString autosavePath = filepath + ".autosave";
        if (QFile(autosavePath).exists()) {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(this, tr("Theme has an autosave file", "Title for the autosave detected dialog"),
                    tr("The current theme has an autosave file. Would you like to load it? If you load it, the original file will still be intact, but the autosave file will disappear if you close it without saving it. The autosave file will be deleted if you choose not to open it.", "Asks the user whether or not they want to load the autosaved theme."),
                    QMessageBox::Open | QMessageBox::Discard | QMessageBox::Cancel);
            if(reply == QMessageBox::Open) {
                filepath = autosavePath;
            } else if (reply == QMessageBox::Discard) {
                _currentAutosavePath = autosavePath;
                deleteAutosave();
            } else if (reply == QMessageBox::Cancel) {
                return;
            }
        }
        if (checkMissingResources(filepath)) {
            _currentAutosavePath = autosavePath;
            _filepath = filepath;
            loadSourceFile();
            emit sourceLoaded();
        }
    }
}

void ConsoleMenu::handleImport(bool success, QString output) {
    disconnect(_kametools, &KameTools::themeExtracted, this, &ConsoleMenu::handleImport);
    QMessageBox msgBox(QMessageBox::NoIcon, tr("Theme Import Results", "Title of the message that appears after a theme finished importing"), tr("Theme imported.", "Message that appears when a theme finishes importing"), QFlag(0), this);
    _progress->cancel();
    if (!success) {
        msgBox.setText("Theme import failed.");
        resetImportOutputPath(_importpath, false);
    } else {
        deleteAutosave();
        _filepath = importOutputPath(_importpath) + "/" + ConsoleMenu::DEFAULT_IMPORT_RC;
        _currentAutosavePath = _filepath + ".autosave";
        delete _sourcefile;
        _sourcefile = new QSettings(_filepath, QSettings::IniFormat);
        if (_sourcefile->value("frames/top.type").toString() == "texture") {
            importRGB565Texture("textures/top.image");
        }
        importRGB565Texture("textures/bottom.image");
        importRGB565Texture("info/icon");
        importRGB565Texture("info/smallicon");
        _sourcefile->sync();
        loadSourceFile();
        //We don't want autosave in the temporary directory; it's of no use to anybody there
        QSettings settings;
        QDir tmp(settings.value("import/tmp").toString());
        if (!tmp.cleanPath(tmp.relativeFilePath(_filepath)).startsWith("..")) {
            _filepath = _currentAutosavePath;
            generateSuffix();
        }
        emit sourceLoaded();
    }
    _importpath = "";
    msgBox.setDetailedText(output);
    msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
    msgBox.exec();
}

void ConsoleMenu::handleDeploy(bool success, QString output) {
    disconnect(_kametools, &KameTools::themeDeployed, this, &ConsoleMenu::handleDeploy);
    QMessageBox msgBox(QMessageBox::NoIcon, tr("Theme Deploy Results", "Title of the message that appears after a theme finished deploying"), tr("Theme deployed.", "Message that appears when a theme finishes deploying"), QFlag(0), this);
    _progress->cancel();
    if (!success) {
        msgBox.setText("Theme failed to deploy.");
    } else {
        QPixmap preview = dynamic_cast<ConsoleWidget*>(parent())->capturePreview();
        QByteArray previewData;
        QBuffer buffer(&previewData);
        buffer.open(QIODevice::WriteOnly);
        preview.save(&buffer, "PNG");
        buffer.close();

        if (_deploytype) {
            QFile file(QDir(_deploypath).filePath("preview.png"));
            file.open(QIODevice::WriteOnly);
            file.write(previewData);
            file.close();
        } else {
            mz_zip_add_mem_to_archive_file_in_place(_deploypath.toLocal8Bit(), "preview.png", previewData.data(), previewData.size(), "no comment", (uint16_t)strlen("no comment"), MZ_BEST_COMPRESSION);
        }
    }
    QFile rgb565source(SourceFile::rgb565path(_filepath, "rc"));
    rgb565source.remove();

    QSettings settings;
    QDir tmp(settings.value("import/tmp").toString());

    if (!tmp.cleanPath(tmp.relativeFilePath(_filepath)).startsWith("..")) {
        _filepath = "";
        SourceFile::recentlyModified = true;
    }

    msgBox.setDetailedText(output);
    msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
    msgBox.exec();
}

void ConsoleMenu::importRGB565Texture(QString key) {
    QDir sourceDir = QFileInfo(_filepath).dir();
    if (_sourcefile->contains(key)) {
        QFileInfo texture(sourceDir, _sourcefile->value(key, "").toString());
        QFile file(texture.canonicalFilePath());
        QString newpath = texture.dir().path() + "/" + texture.completeBaseName() + ".rgb565.png";
        file.rename(newpath);
        _sourcefile->setValue(key, sourceDir.cleanPath(sourceDir.relativeFilePath(newpath)));
    }
}

QString ConsoleMenu::importOutputPath(QString input) {
    QFileInfo fileInfo(input);
    QSettings settings;
    QString outputDir = settings.contains("import/path") && !settings.value("import/path").toString().isEmpty() ? settings.value("import/path").toString() : settings.value("import/tmp").toString() + "/imports";
    return outputDir + "/" + fileInfo.baseName();
}

void ConsoleMenu::resetImportOutputPath(QString input, bool recreate) {
    QString outputDir = importOutputPath(input);
    QFileInfo fileInfo(input);
    QDir dir(outputDir);
    if (dir.exists() && dir.absolutePath() != fileInfo.absoluteFilePath()) {
        dir.removeRecursively();
    }
    if (recreate) {
        dir.mkpath(".");
    }
}

void ConsoleMenu::checkKameTools() {
    connect(_kametools, &KameTools::checkCompleted, this, &ConsoleMenu::onKameToolsCheck);
    _kametools->checkAvailable();
}

void ConsoleMenu::onKameToolsCheck(bool success) {
    disconnect(_kametools, &KameTools::checkCompleted, this, &ConsoleMenu::onKameToolsCheck);
    if (success) {
        QSettings settings;
        _kametoolsFound = true;
        _importEnabled = settings.value("kame-tools/import").toBool();
        _noditherEnabled = settings.value("kame-tools/nodither").toBool();

        int version[3] = {settings.value("kame-tools/version/major").toInt(), settings.value("kame-tools/version/minor").toInt(), settings.value("kame-tools/version/patch").toInt()};

        if (!_importEnabled) {
            _validateLoadMessage = tr("kame-tools v%1.%2.%3 currently installed. Importing theme archives requires kame-tools v%4 or later. Please upgrade kame-tools to use this functionality.", "Warn users about the required version of kame-tools for importing theme archives.").arg(version[0]).arg(version[1]).arg(version[2]).arg(QString::number(KameTools::MIN_VERSION[0]) + "." + QString::number(KameTools::MIN_VERSION[1]) + "." + QString::number(KameTools::MIN_VERSION[2]));
        }

        if (!_noditherEnabled) {
            _warnLossy = "<p>" + tr("kame-tools v%1.%2.%3 currently installed. Subsequent importing and deploying of themes will cause lossy behavior in texture images. Please upgrade kame-tools to at least v%4 to prevent this from happening.", "Warn users about newer version of kame-tools which prevents images from being saved in a lossy manner when using deploy options.").arg(version[0]).arg(version[1]).arg(version[2]).arg(QString::number(KameTools::LOSSY_VERSION[0]) + "." + QString::number(KameTools::LOSSY_VERSION[1]) + "." + QString::number(KameTools::LOSSY_VERSION[2])) + "</p>";
        }
        emit kameToolsCheckComplete();
    } else {
        _kametoolsFound = false;
        _validateSaveMessage = tr("kame-tools could not be found. You can still save theme source files, but you won't be able to deploy them and use them on actual 3DS hardware.", "Warn users when kame-tools is not available.");
        _validateLoadMessage = tr("kame-tools could not be found. You can still load theme source files, but you won't be able to import theme archives.", "Warn users when kame-tools is not available.");
    }
}

void ConsoleMenu::processAutosave() {
    QSettings settings;
    if (!settings.contains("autosave")) {
        _autosavetimer->stop();
    } else {
        _autosavetimer->start(settings.value("autosave").toInt() * 1000 * 60);
        saveSourceFile(true);
    }
}

void ConsoleMenu::addMissingResource(QString id, QString path, QString type, QString title) {
    if (!path.isEmpty() && !QFile(path).exists()) {
        missingResource resource;
        resource.title = title;
        resource.type = type;
        resource.id = id;
        _missing.append(resource);
    }
}

bool ConsoleMenu::checkMissingResources(QString file) {
    QSettings *sourcefile = new QSettings(file, QSettings::IniFormat);

    QDir sourceDir = QFileInfo(file).dir();

    _missing.clear();

    QString smdhIconPath = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("info/icon", "").toString()));
    QString smdhSmallIconPath = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("info/smallicon", "").toString()));

    addMissingResource("info/icon", smdhIconPath, "texture", tr("Theme Icon", "Title for missing theme icon resource"));
    addMissingResource("info/smallicon", smdhSmallIconPath, "texture", tr("Small Theme Icon", "Title for missing small theme icon resource"));

    QString toptexture = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("textures/top.image", "").toString()));
    QString bottomtexture = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("textures/bottom.image", "").toString()));

    addMissingResource("textures/top.image", toptexture, "texture", tr("Top Screen Texture", "Title for missing top screen texture"));
    addMissingResource("textures/bottom.image", bottomtexture, "texture", tr("Bottom Screen Texture", "Title for missing bottom screen texture"));

    QString folderclose = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("textures/folderclose.image", "").toString()));
    QString folderopen = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("textures/folderopen.image", "").toString()));

    QString filelarge = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("textures/filelarge.image", "").toString()));
    QString filesmall = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("textures/filesmall.image", "").toString()));

    addMissingResource("textures/folderclose.image", folderclose, "texture", tr("Closed Folder Texture", "Title for missing closed folder texture"));
    addMissingResource("textures/folderopen.image", folderopen, "texture", tr("Opened Folder Texture", "Title for missing opened folder texture"));

    addMissingResource("textures/filelarge.image", filelarge, "texture", tr("Large File Texture", "Title for missing large file texture"));
    addMissingResource("textures/filesmall.image", filesmall, "texture", tr("Small File Texture", "Title for missing small file texture"));

    //Load audio
    QString bgm = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("audio/bgm", "").toString()));

    addMissingResource("audio/bgm", bgm, "bgm", tr("Background Music", "Title for missing BGM"));

    QString cursor = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("audio/sfx.cursor", "").toString()));
    QString launch = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("audio/sfx.launch", "").toString()));
    QString folder = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("audio/sfx.folder", "").toString()));
    QString close = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("audio/sfx.close", "").toString()));
    QString frame0 = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("audio/sfx.frame0", "").toString()));
    QString frame1 = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("audio/sfx.frame1", "").toString()));
    QString frame2 = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("audio/sfx.frame2", "").toString()));
    QString resume = sourceDir.cleanPath(sourceDir.absoluteFilePath(sourcefile->value("audio/sfx.openlid", "").toString()));

    addMissingResource("audio/sfx.cursor", cursor, "sfx", tr("Cursor SFX", "Title for missing cursor SFX"));
    addMissingResource("audio/sfx.launch", launch, "sfx", tr("Launch SFX", "Title for missing launch SFX"));
    addMissingResource("audio/sfx.folder", folder, "sfx", tr("Folder SFX", "Title for missing folder SFX"));
    addMissingResource("audio/sfx.close", close, "sfx", tr("Close SFX", "Title for missing close SFX"));
    addMissingResource("audio/sfx.frame0", frame0, "sfx", tr("Frame 0 SFX", "Title for missing frame0 SFX"));
    addMissingResource("audio/sfx.frame1", frame1, "sfx", tr("Frame 1 SFX", "Title for missing frame1 SFX"));
    addMissingResource("audio/sfx.frame2", frame2, "sfx", tr("Frame 2 SFX", "Title for missing frame2 SFX"));
    addMissingResource("audio/sfx.openlid", resume, "sfx", tr("Open Lid SFX", "Title for missing open lid SFX"));

    if (!_missing.empty()) {
        ResourcesDialog *resourcesdialog = new ResourcesDialog();
        resourcesdialog->setList(_missing, sourcefile);
        if (resourcesdialog->exec()) {
            delete resourcesdialog;
            return true;
        } else {
            delete resourcesdialog;
            return false;
        }
    }
    return true;
}

void ConsoleMenu::generateSuffix() {
    _suffix = QDateTime::currentDateTime().toString("dd-MM-yyyy_HH-mm-ss-zzz");
}

void ConsoleMenu::resetSourceFile() {
    deleteAutosave();
    SourceFile::reset();
    emit resetSource();
    _filepath = "";
    checkSourceFile();
    generateSuffix();
    _currentAutosavePath = "";
}

void ConsoleMenu::loadSourceFile() {
    delete _sourcefile;
    _sourcefile = new QSettings(_filepath, QSettings::IniFormat);

    QDir sourceDir = QFileInfo(_filepath).dir();

    SourceFile::reset();

    // Load SMDH data
    for (int i = 0; i < SourceFile::LANGUAGES.size(); i++) {
        SourceFile::shorttitle[i] = _sourcefile->value("info/" + SourceFile::LANGUAGES[i] + "shorttitle", "").toString();
        SourceFile::longtitle[i] = _sourcefile->value("info/" + SourceFile::LANGUAGES[i] + "longtitle", "").toString();
        SourceFile::publisher[i] = _sourcefile->value("info/" + SourceFile::LANGUAGES[i] + "publisher", "").toString();
    }

    SourceFile::smdhIconPath = _sourcefile->value("info/icon").toString().isEmpty() ? "" : sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("info/icon", "").toString()));
    SourceFile::smdhSmallIconPath = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("info/smallicon", "").toString()));

    for (int i = 0; i < SourceFile::RATINGS.size(); i++) {
        int value = _sourcefile->value("info/" + SourceFile::RATINGS[i], 0).toInt();
        QMap<QString, QVariant> rating = QMap<QString, QVariant>();
        if (value >= 0x80) {
            rating["active"] = true;
            value -= 0x80;
        } else {
            rating["active"] = false;
        }
        if (value >= 0x40) {
            rating["pending"] = true;
            value -= 0x40;
        } else {
            rating["pending"] = false;
        }
        if (value >= 0x20) {
            rating["noage"] = true;
            value -= 0x20;
        } else {
            rating["noage"] = false;
        }
        rating["age"] = value;
        SourceFile::ratings[i] = rating;
    }

    QStringList regionlist = _sourcefile->value("info/regions", "").toString().split(',', Qt::SkipEmptyParts);
    if (regionlist.contains("regionfree")) {
        SourceFile::isRegionFree = true;
    } else {
        SourceFile::isRegionFree = false;
        SourceFile::regions = regionlist;
    }

    SourceFile::matchmakerid = _sourcefile->value("info/matchmakerid", "").toString();
    SourceFile::matchmakerbitid = _sourcefile->value("info/matchmakerbitid", "").toString();

    SourceFile::flags = _sourcefile->value("info/flags", "").toString().split(",", Qt::SkipEmptyParts);
    SourceFile::eulaversion = _sourcefile->value("info/eulaversion", "").toString();
    SourceFile::optimalbannerframe = _sourcefile->value("info/optimalbannerframe", "").toFloat();
    SourceFile::streetpassid = _sourcefile->value("info/streetpassid", "").toString();

    // Load textures and frame data
    SourceFile::topdraw = std::max(0, static_cast<int>(SourceFile::TOPDRAW.indexOf(_sourcefile->value("frames/top.type", "none").toString())));
    SourceFile::topframe = std::max(0, static_cast<int>(SourceFile::TOPFRAME.indexOf(_sourcefile->value("frames/top.frame", "single").toString())));
    SourceFile::bottomdraw = std::max(0, static_cast<int>(SourceFile::BOTTOMDRAW.indexOf(_sourcefile->value("frames/bottom.type", "none").toString())));
    SourceFile::bottomframe = std::max(0, static_cast<int>(SourceFile::BOTTOMFRAME.indexOf(_sourcefile->value("frames/bottom.frame", "single").toString())));

    TopView::DrawType topdrawtype = static_cast<TopView::DrawType>(SourceFile::topdraw);
    int index = 0;
    switch(topdrawtype) {
        case TopView::DrawType::COLOR:
        case TopView::DrawType::COLORTEXTURE:
            SourceFile::topcolors["color"] = SourceFile::hexToColor(_sourcefile->value("colors/topbgcolor", "0x").toString());
            SourceFile::topcolors["gradientalpha"] = _sourcefile->value("colors/topbgcolor.gradient", 0).toFloat() / 100.0;
            SourceFile::topcolors["alpha"] = _sourcefile->value("colors/topbgcolor.opacity", 0).toFloat() / 100.0;
            SourceFile::topcolors["alphaext"] = _sourcefile->value("colors/topbgcolor.altopacity", 0).toFloat() / 100.0;
            SourceFile::topcolors["gradientbrightness"] = _sourcefile->value("colors/topbgcolor.gradientcolor", 0).toFloat() / 100.0;

            if (topdrawtype == TopView::DrawType::COLORTEXTURE) {
                SourceFile::toptexture[2] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("textures/top.image", "").toString()));
                SourceFile::topexttexture = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("textures/topext.image", "").toString()));
            }
            break;
        case TopView::DrawType::TEXTURE:
            index = static_cast<TopView::FrameType>(SourceFile::topframe) == TopView::FrameType::SINGLE ? 0 : 1;
            SourceFile::toptexture[index] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("textures/top.image", "").toString()));
            break;
        case TopView::DrawType::NONE:
        default:
            break;
    }

    BottomView::DrawType bottomdrawtype = static_cast<BottomView::DrawType>(SourceFile::bottomdraw);
    switch(bottomdrawtype) {
        case BottomView::DrawType::COLOR:
            SourceFile::bottomoutercolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/bottombgoutercolor.dark", "0x").toString());
            SourceFile::bottomoutercolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/bottombgoutercolor.main", "0x").toString());
            SourceFile::bottomoutercolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/bottombgoutercolor.light", "0x").toString());
            SourceFile::bottominnercolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/bottombginnercolor.dark", "0x").toString());
            SourceFile::bottominnercolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/bottombginnercolor.main", "0x").toString());
            SourceFile::bottominnercolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/bottombginnercolor.light", "0x").toString());
            SourceFile::bottominnercolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/bottombginnercolor.shadow", "0x").toString());
            break;
        case BottomView::DrawType::TEXTURE:
            index = static_cast<BottomView::FrameType>(SourceFile::bottomframe) == BottomView::FrameType::SINGLE ? 0 : 1;
            SourceFile::bottomtexture[index] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("textures/bottom.image", "").toString()));
            break;
        case BottomView::DrawType::NONE:
        default:
            break;
    }

    SourceFile::foldericons[0] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("textures/folderclose.image", "").toString()));
    SourceFile::foldericons[1] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("textures/folderopen.image", "").toString()));

    SourceFile::fileicons[0] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("textures/filelarge.image", "").toString()));
    SourceFile::fileicons[1] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("textures/filesmall.image", "").toString()));

    // Load colors
    SourceFile::cursorcolorEnabled = !_sourcefile->contains("colors/cursorcolor.dark.hidden");
    QString hiddenString = SourceFile::cursorcolorEnabled ? "" : ".hidden";
    SourceFile::cursorcolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/cursorcolor.dark" + hiddenString, "0x").toString());
    SourceFile::cursorcolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/cursorcolor.main" + hiddenString, "0x").toString());
    SourceFile::cursorcolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/cursorcolor.light" + hiddenString, "0x").toString());
    SourceFile::cursorcolors["glow"] = SourceFile::hexToColor(_sourcefile->value("colors/cursorcolor.glow" + hiddenString, "0x").toString());

    SourceFile::foldercolorEnabled = !_sourcefile->contains("colors/foldercolor.dark.hidden");
    hiddenString = SourceFile::foldercolorEnabled ? "" : ".hidden";
    SourceFile::foldercolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/foldercolor.dark" + hiddenString, "0x").toString());
    SourceFile::foldercolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/foldercolor.main" + hiddenString, "0x").toString());
    SourceFile::foldercolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/foldercolor.light" + hiddenString, "0x").toString());
    SourceFile::foldercolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/foldercolor.shadow" + hiddenString, "0x").toString());

    SourceFile::filecolorEnabled = !_sourcefile->contains("colors/filecolor.dark.hidden");
    hiddenString = SourceFile::filecolorEnabled ? "" : ".hidden";
    SourceFile::filecolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/filecolor.dark" + hiddenString, "0x").toString());
    SourceFile::filecolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/filecolor.main" + hiddenString, "0x").toString());
    SourceFile::filecolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/filecolor.light" + hiddenString, "0x").toString());
    SourceFile::filecolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/filecolor.shadow" + hiddenString, "0x").toString());

    SourceFile::openclosecolorEnabled = !_sourcefile->contains("colors/open.dark.hidden");
    hiddenString = SourceFile::openclosecolorEnabled ? "" : ".hidden";
    SourceFile::opencolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/open.dark" + hiddenString, "0x").toString());
    SourceFile::opencolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/open.main" + hiddenString, "0x").toString());
    SourceFile::opencolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/open.light" + hiddenString, "0x").toString());
    SourceFile::opencolors["glow"] = SourceFile::hexToColor(_sourcefile->value("colors/open.glow" + hiddenString, "0x").toString());
    SourceFile::opencolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/open.shadow" + hiddenString, "0x").toString());

    SourceFile::opencolors["textshadow"] = SourceFile::hexToColor(_sourcefile->value("colors/open.textshadow" + hiddenString, "0x").toString());
    SourceFile::opencolors["textmain"] = SourceFile::hexToColor(_sourcefile->value("colors/open.textmain" + hiddenString, "0x").toString());
    SourceFile::opencolors["textselected"] = SourceFile::hexToColor(_sourcefile->value("colors/open.textselected" + hiddenString, "0x").toString());

    SourceFile::opencolors["textshadowpos"] = _sourcefile->value("colors/open.textshadowpos" + hiddenString, 0).toFloat();

    SourceFile::closecolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/close.dark" + hiddenString, "0x").toString());
    SourceFile::closecolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/close.main" + hiddenString, "0x").toString());
    SourceFile::closecolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/close.light" + hiddenString, "0x").toString());
    SourceFile::closecolors["glow"] = SourceFile::hexToColor(_sourcefile->value("colors/close.glow" + hiddenString, "0x").toString());
    SourceFile::closecolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/close.shadow" + hiddenString, "0x").toString());

    SourceFile::closecolors["textshadow"] = SourceFile::hexToColor(_sourcefile->value("colors/close.textshadow" + hiddenString, "0x").toString());
    SourceFile::closecolors["textmain"] = SourceFile::hexToColor(_sourcefile->value("colors/close.textmain" + hiddenString, "0x").toString());
    SourceFile::closecolors["textselected"] = SourceFile::hexToColor(_sourcefile->value("colors/close.textselected" + hiddenString, "0x").toString());

    SourceFile::closecolors["textshadowpos"] = _sourcefile->value("colors/close.textshadowpos" + hiddenString, 0).toFloat();

    SourceFile::folderbackbuttoncolorEnabled = !_sourcefile->contains("colors/folderarrowcolor.dark.hidden");
    hiddenString = SourceFile::folderbackbuttoncolorEnabled ? "" : ".hidden";
    SourceFile::folderbackbuttoncolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/folderarrowcolor.dark" + hiddenString, "0x").toString());
    SourceFile::folderbackbuttoncolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/folderarrowcolor.main" + hiddenString, "0x").toString());
    SourceFile::folderbackbuttoncolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/folderarrowcolor.light" + hiddenString, "0x").toString());
    SourceFile::folderbackbuttoncolors["glow"] = SourceFile::hexToColor(_sourcefile->value("colors/folderarrowcolor.glow" + hiddenString, "0x").toString());
    SourceFile::folderbackbuttoncolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/folderarrowcolor.shadow" + hiddenString, "0x").toString());

    SourceFile::folderbackbuttoncolors["textshadow"] = SourceFile::hexToColor(_sourcefile->value("colors/folderarrowcolor.textshadow" + hiddenString, "0x").toString());
    SourceFile::folderbackbuttoncolors["textmain"] = SourceFile::hexToColor(_sourcefile->value("colors/folderarrowcolor.textmain" + hiddenString, "0x").toString());
    SourceFile::folderbackbuttoncolors["textselected"] = SourceFile::hexToColor(_sourcefile->value("colors/folderarrowcolor.textselected" + hiddenString, "0x").toString());

    SourceFile::folderbackbuttoncolors["textshadowpos"] = _sourcefile->value("colors/folderarrowcolor.textshadowpos" + hiddenString, 0).toFloat();

    SourceFile::arrowbuttonbasecolorEnabled = !_sourcefile->contains("colors/arrowbuttoncolor.dark.hidden");
    hiddenString = SourceFile::arrowbuttonbasecolorEnabled ? "" : ".hidden";
    SourceFile::arrowbuttonbasecolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/arrowbuttoncolor.dark" + hiddenString, "0x").toString());
    SourceFile::arrowbuttonbasecolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/arrowbuttoncolor.main" + hiddenString, "0x").toString());
    SourceFile::arrowbuttonbasecolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/arrowbuttoncolor.light" + hiddenString, "0x").toString());
    SourceFile::arrowbuttonbasecolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/arrowbuttoncolor.shadow" + hiddenString, "0x").toString());

    SourceFile::arrowbuttonarrowcolorEnabled = !_sourcefile->contains("colors/arrowcolor.border.hidden");
    hiddenString = SourceFile::arrowbuttonarrowcolorEnabled ? "" : ".hidden";
    SourceFile::arrowbuttonarrowcolors["border"] = SourceFile::hexToColor(_sourcefile->value("colors/arrowcolor.border" + hiddenString, "0x").toString());
    SourceFile::arrowbuttonarrowcolors["unpressed"] = SourceFile::hexToColor(_sourcefile->value("colors/arrowcolor.unpressed" + hiddenString, "0x").toString());
    SourceFile::arrowbuttonarrowcolors["pressed"] = SourceFile::hexToColor(_sourcefile->value("colors/arrowcolor.pressed" + hiddenString, "0x").toString());

    SourceFile::bottomcornercolorEnabled = !_sourcefile->contains("colors/bottomcornerbuttoncolor.dark.hidden");
    hiddenString = SourceFile::bottomcornercolorEnabled ? "" : ".hidden";
    SourceFile::bottomcornercolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/bottomcornerbuttoncolor.dark" + hiddenString, "0x").toString());
    SourceFile::bottomcornercolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/bottomcornerbuttoncolor.main" + hiddenString, "0x").toString());
    SourceFile::bottomcornercolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/bottomcornerbuttoncolor.light" + hiddenString, "0x").toString());
    SourceFile::bottomcornercolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/bottomcornerbuttoncolor.shadow" + hiddenString, "0x").toString());
    SourceFile::bottomcornercolors["iconmain"] = SourceFile::hexToColor(_sourcefile->value("colors/bottomcornerbuttoncolor.iconmain" + hiddenString, "0x").toString());
    SourceFile::bottomcornercolors["iconlight"] = SourceFile::hexToColor(_sourcefile->value("colors/bottomcornerbuttoncolor.iconlight" + hiddenString, "0x").toString());
    SourceFile::bottomcornercolors["icontextmain"] = SourceFile::hexToColor(_sourcefile->value("colors/bottomcornerbuttoncolor.icontextmain" + hiddenString, "0x").toString());

    SourceFile::topcornercolorEnabled = !_sourcefile->contains("colors/topcornerbuttoncolor.main.hidden");
    hiddenString = SourceFile::topcornercolorEnabled ? "" : ".hidden";
    SourceFile::topcornercolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/topcornerbuttoncolor.main" + hiddenString, "0x").toString());
    SourceFile::topcornercolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/topcornerbuttoncolor.light" + hiddenString, "0x").toString());
    SourceFile::topcornercolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/topcornerbuttoncolor.shadow" + hiddenString, "0x").toString());
    SourceFile::topcornercolors["textmain"] = SourceFile::hexToColor(_sourcefile->value("colors/topcornerbuttoncolor.textmain" + hiddenString, "0x").toString());

    SourceFile::folderviewcolorEnabled = !_sourcefile->contains("colors/folderbgcolor.dark.hidden");
    hiddenString = SourceFile::folderviewcolorEnabled ? "" : ".hidden";
    SourceFile::folderviewcolors["dark"] = SourceFile::hexToColor(_sourcefile->value("colors/folderbgcolor.dark" + hiddenString, "0x").toString());
    SourceFile::folderviewcolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/folderbgcolor.main" + hiddenString, "0x").toString());
    SourceFile::folderviewcolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/folderbgcolor.light" + hiddenString, "0x").toString());
    SourceFile::folderviewcolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/folderbgcolor.shadow" + hiddenString, "0x").toString());

    SourceFile::gametextcolorEnabled = !_sourcefile->contains("colors/gametext.main.hidden");
    SourceFile::gametextcolorHidden = _sourcefile->value("flags/gametext.hidden", false).toBool();
    hiddenString = SourceFile::gametextcolorEnabled ? "" : ".hidden";
    SourceFile::gametextcolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/gametext.main" + hiddenString, "0x").toString());
    SourceFile::gametextcolors["light"] = SourceFile::hexToColor(_sourcefile->value("colors/gametext.light" + hiddenString, "0x").toString());
    SourceFile::gametextcolors["shadow"] = SourceFile::hexToColor(_sourcefile->value("colors/gametext.shadow" + hiddenString, "0x").toString());
    SourceFile::gametextcolors["textmain"] = SourceFile::hexToColor(_sourcefile->value("colors/gametext.textmain" + hiddenString, "0x").toString());

    SourceFile::demotextcolorEnabled = !_sourcefile->contains("colors/demotextcolor.main.hidden");
    hiddenString = SourceFile::demotextcolorEnabled ? "" : ".hidden";
    SourceFile::demotextcolors["main"] = SourceFile::hexToColor(_sourcefile->value("colors/demotextcolor.main" + hiddenString, "0x").toString());
    SourceFile::demotextcolors["textmain"] = SourceFile::hexToColor(_sourcefile->value("colors/demotextcolor.textmain" + hiddenString, "0x").toString());

    //Load audio
    SourceFile::bgm = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("audio/bgm", "").toString()));
    SourceFile::sfxes["cursor"] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("audio/sfx.cursor", "").toString()));
    SourceFile::sfxes["launch"] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("audio/sfx.launch", "").toString()));
    SourceFile::sfxes["folder"] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("audio/sfx.folder", "").toString()));
    SourceFile::sfxes["close"] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("audio/sfx.close", "").toString()));
    SourceFile::sfxes["frame0"] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("audio/sfx.frame0", "").toString()));
    SourceFile::sfxes["frame1"] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("audio/sfx.frame1", "").toString()));
    SourceFile::sfxes["frame2"] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("audio/sfx.frame2", "").toString()));
    SourceFile::sfxes["resume"] = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("audio/sfx.openlid", "").toString()));

    if (_sourcefile->contains("kame-editor/deploy")) {
        SourceFile::deploypath = sourceDir.cleanPath(sourceDir.absoluteFilePath(_sourcefile->value("kame-editor/deploy").toString()));
    }

    checkSourceFile();
}

bool ConsoleMenu::saveSourceFile(bool autosave, bool rgb565) {
    QString filepath = autosave ? _currentAutosavePath : _filepath;
    QSettings settings;
    QString tempPath = settings.value("import/tmp").toString() + "/deploy";
    QDir(tempPath).mkpath(".");
    if (_filepath.isEmpty() || _filepath == _currentAutosavePath) {
        if (autosave) {
            QString cachePath = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
            QDir().mkpath(cachePath);
            QString fileName = "untitled_" + _suffix + ".rc.autosave";
            filepath = QDir(cachePath + "/" + fileName).path();
            _currentAutosavePath = filepath;
            settings.setValue("autosaveFilename", fileName);
        } else if (rgb565 && _filepath.isEmpty()) {
            filepath = tempPath + "/" + _suffix + ".rc";
            _filepath = filepath;
        } else {
            auto _filedialog = getSaveDialog();
            if (_filedialog->exec()) {
                _filepath = _filedialog->selectedFiles().first();
                filepath = _filepath;
            }
        }
    }
    if (!filepath.isEmpty()) {
        QFileInfo fileInfo = QFileInfo(filepath);
        QDir sourceDir = fileInfo.dir();

        QString rgb565Icon;
        QString rgb565SmallIcon;
        QString rgb565TopTexture;
        QString rgb565BottomTexture;

        delete _sourcefile;
        _sourcefile = new QSettings(filepath, QSettings::IniFormat);
        _sourcefile->clear();
        _sourcefile->beginGroup("info");
        // Save SMDH info
        bool hasShortTitle = false;
        bool hasLongTitle = false;
        bool hasPublisher = false;
        for (int i = 0; i < SourceFile::LANGUAGES.size(); i++) {
            if (!SourceFile::shorttitle[i].isEmpty()) {
                _sourcefile->setValue(SourceFile::LANGUAGES[i] + "shorttitle", SourceFile::shorttitle[i].trimmed());
                hasShortTitle = true;
            }
            if (!SourceFile::longtitle[i].isEmpty()) {
                _sourcefile->setValue(SourceFile::LANGUAGES[i] + "longtitle", SourceFile::longtitle[i].trimmed());
                hasLongTitle = true;
            }
            if (!SourceFile::publisher[i].isEmpty()) {
                _sourcefile->setValue(SourceFile::LANGUAGES[i] + "publisher", SourceFile::publisher[i].trimmed());
                hasPublisher = true;
            }
        }
        if (!SourceFile::smdhIconPath.isEmpty()) {
            _sourcefile->setValue("icon", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::smdhIconPath)));
            if (rgb565) {
                rgb565Icon = sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::rgb565path(SourceFile::smdhIconPath)));
            }
        } else {
            QFile defaultIcon(":/defaults/smdh.rgb565.png");
            QString destPath = settings.value("import/tmp").toString() + "/deploy/smdh.rgb565.png";
            defaultIcon.copy(destPath);
            rgb565Icon = destPath;
        }

        // Optional SMDH data
        QString smallIconPath = sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::smdhSmallIconPath));
        if (!SourceFile::smdhSmallIconPath.isEmpty() && smallIconPath != "." && smallIconPath != "..") {
            _sourcefile->setValue("smallicon", smallIconPath);
            if (rgb565) {
                rgb565SmallIcon = sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::rgb565path(SourceFile::smdhSmallIconPath)));
            }
        }
        for (int i = 0; i < SourceFile::ratings->size(); i++) {
            QMap<QString, QVariant> rating = SourceFile::ratings[i];
            if (!rating.isEmpty()) {
                int value = rating["age"].toInt()
                    + (rating["noage"].toBool() ? 0x20 : 0)
                    + (rating["pending"].toBool() ? 0x40 : 0)
                    + (rating["active"].toBool() ? 0x80 : 0);
                if (value > 0) {
                    _sourcefile->setValue(SourceFile::RATINGS[i], value);
                }
            }
        }

        QString regions = "";
        if (SourceFile::isRegionFree) {
            regions = "regionfree";
        } else {
            regions = SourceFile::regions.join(",");
        }
        if (!regions.isEmpty()) {
            _sourcefile->setValue("regions", regions);
        }
        if (!SourceFile::flags.isEmpty()) {
            _sourcefile->setValue("flags", SourceFile::flags.join(","));
        }
        if (SourceFile::optimalbannerframe > 0.0) {
            _sourcefile->setValue("optimalbannerframe", QString::number(SourceFile::optimalbannerframe));
        }
        if (!SourceFile::matchmakerid.isEmpty()) {
            _sourcefile->setValue("matchmakerid", SourceFile::matchmakerid);
        }
        if (!SourceFile::matchmakerbitid.isEmpty()) {
            _sourcefile->setValue("matchmakerbitid", SourceFile::matchmakerbitid);
        }
        if (!SourceFile::eulaversion.isEmpty()) {
            _sourcefile->setValue("eulaversion", SourceFile::eulaversion);
        }
        if (!SourceFile::streetpassid.isEmpty()) {
            _sourcefile->setValue("streetpassid", SourceFile::streetpassid);
        }
        _sourcefile->endGroup();

        // Save frame and texture data
        _sourcefile->beginGroup("frames");
        _sourcefile->setValue("top.type", SourceFile::TOPDRAW[SourceFile::topdraw]);
        _sourcefile->setValue("top.frame", SourceFile::TOPFRAME[SourceFile::topframe]);
        _sourcefile->setValue("bottom.type", SourceFile::BOTTOMDRAW[SourceFile::bottomdraw]);
        _sourcefile->setValue("bottom.frame", SourceFile::BOTTOMFRAME[SourceFile::bottomframe]);
        _sourcefile->endGroup();

        int index = 0;
        TopView::DrawType topdrawtype = static_cast<TopView::DrawType>(SourceFile::topdraw);
        switch(topdrawtype) {
            case TopView::DrawType::COLOR:
            case TopView::DrawType::COLORTEXTURE:
                if (SourceFile::topcolors["color"].isValid()) {
                    _sourcefile->setValue("colors/topbgcolor", SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::topcolors["color"])));
                }
                _sourcefile->setValue("colors/topbgcolor.gradient", QString::number(SourceFile::topcolors["gradientalpha"].toFloat() * 100));
                _sourcefile->setValue("colors/topbgcolor.opacity", QString::number(SourceFile::topcolors["alpha"].toFloat() * 100));
                _sourcefile->setValue("colors/topbgcolor.altopacity", QString::number(SourceFile::topcolors["alphaext"].toFloat() * 100));
                _sourcefile->setValue("colors/topbgcolor.gradientcolor", QString::number(SourceFile::topcolors["gradientbrightness"].toFloat() * 100));
                if (topdrawtype == TopView::DrawType::COLORTEXTURE) {
                    _sourcefile->setValue("textures/top.image", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::toptexture[2])));
                    if (!SourceFile::topexttexture.isEmpty() && !QFileInfo(SourceFile::topexttexture).isDir()) {
                        _sourcefile->setValue("textures/topext.image", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::topexttexture)));
                    }
                }
                break;
            case TopView::DrawType::TEXTURE:
                index = std::min(SourceFile::topframe, 1);
                _sourcefile->setValue("textures/top.image", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::toptexture[index])));
                if (rgb565) {
                    rgb565TopTexture = sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::rgb565path(SourceFile::toptexture[index])));
                }
                break;
            case TopView::DrawType::NONE:
            default:
                break;
        }

        BottomView::DrawType bottomdrawtype = static_cast<BottomView::DrawType>(SourceFile::bottomdraw);
        switch(bottomdrawtype) {
            case BottomView::DrawType::COLOR:
                if (SourceFile::bottomoutercolors["dark"].isValid()) {
                    _sourcefile->setValue("colors/bottombgoutercolor.dark", SourceFile::colorToHex(SourceFile::bottomoutercolors["dark"], QColor::HexRgb));
                }
                if (SourceFile::bottomoutercolors["main"].isValid()) {
                    _sourcefile->setValue("colors/bottombgoutercolor.main", SourceFile::colorToHex(SourceFile::bottomoutercolors["main"], QColor::HexRgb));
                }
                if (SourceFile::bottomoutercolors["light"].isValid()) {
                    _sourcefile->setValue("colors/bottombgoutercolor.light", SourceFile::colorToHex(SourceFile::bottomoutercolors["light"], QColor::HexRgb));
                }
                if (SourceFile::bottominnercolors["dark"].isValid()) {
                    _sourcefile->setValue("colors/bottombginnercolor.dark", SourceFile::colorToHex(SourceFile::bottominnercolors["dark"], QColor::HexRgb));
                }
                if (SourceFile::bottominnercolors["main"].isValid()) {
                    _sourcefile->setValue("colors/bottombginnercolor.main", SourceFile::colorToHex(SourceFile::bottominnercolors["main"], QColor::HexRgb));
                }
                if (SourceFile::bottominnercolors["light"].isValid()) {
                    _sourcefile->setValue("colors/bottombginnercolor.light", SourceFile::colorToHex(SourceFile::bottominnercolors["light"], QColor::HexRgb));
                }
                if (SourceFile::bottominnercolors["shadow"].isValid()) {
                    _sourcefile->setValue("colors/bottombginnercolor.shadow", SourceFile::colorToHex(SourceFile::bottominnercolors["shadow"], QColor::HexArgb));
                }
                break;
            case BottomView::DrawType::TEXTURE:
                index = std::min(SourceFile::bottomframe, 1);
                _sourcefile->setValue("textures/bottom.image", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::bottomtexture[index])));
                if (rgb565) {
                    rgb565BottomTexture = sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::rgb565path(SourceFile::bottomtexture[index])));
                }
                break;
            case BottomView::DrawType::NONE:
            default:
                break;
        }

        _sourcefile->beginGroup("textures");
        if (!SourceFile::foldericons[0].isEmpty() && !QFileInfo(SourceFile::foldericons[0]).isDir()) {
            _sourcefile->setValue("folderclose.image", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::foldericons[0])));
        }
        if (!SourceFile::foldericons[1].isEmpty() && !QFileInfo(SourceFile::foldericons[1]).isDir()) {
            _sourcefile->setValue("folderopen.image", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::foldericons[1])));
        }

        if (!SourceFile::fileicons[0].isEmpty() && !QFileInfo(SourceFile::fileicons[0]).isDir()) {
            _sourcefile->setValue("filelarge.image", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::fileicons[0])));
        }
        if (!SourceFile::fileicons[1].isEmpty() && !QFileInfo(SourceFile::fileicons[1]).isDir()) {
            _sourcefile->setValue("filesmall.image", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::fileicons[1])));
        }
        _sourcefile->endGroup();

        // Save colors
        _sourcefile->beginGroup("colors");
        QString hiddenString = ".hidden";
        if (SourceFile::cursorcolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("cursorcolor.dark" + hiddenString, SourceFile::colorToHex(SourceFile::cursorcolors["dark"], QColor::HexRgb));
        _sourcefile->setValue("cursorcolor.main" + hiddenString, SourceFile::colorToHex(SourceFile::cursorcolors["main"], QColor::HexRgb));
        _sourcefile->setValue("cursorcolor.light" + hiddenString, SourceFile::colorToHex(SourceFile::cursorcolors["light"], QColor::HexRgb));
        _sourcefile->setValue("cursorcolor.glow" + hiddenString, SourceFile::colorToHex(SourceFile::cursorcolors["glow"], QColor::HexRgb));

        hiddenString = ".hidden";
        if (SourceFile::foldercolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("foldercolor.dark" + hiddenString, SourceFile::colorToHex(SourceFile::foldercolors["dark"], QColor::HexRgb));
        _sourcefile->setValue("foldercolor.main" + hiddenString, SourceFile::colorToHex(SourceFile::foldercolors["main"], QColor::HexRgb));
        _sourcefile->setValue("foldercolor.light" + hiddenString, SourceFile::colorToHex(SourceFile::foldercolors["light"], QColor::HexRgb));
        _sourcefile->setValue("foldercolor.shadow" + hiddenString, SourceFile::colorToHex(SourceFile::foldercolors["shadow"], QColor::HexRgb));

        hiddenString = ".hidden";
        if (SourceFile::filecolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("filecolor.dark" + hiddenString, SourceFile::colorToHex(SourceFile::filecolors["dark"], QColor::HexRgb));
        _sourcefile->setValue("filecolor.main" + hiddenString, SourceFile::colorToHex(SourceFile::filecolors["main"], QColor::HexRgb));
        _sourcefile->setValue("filecolor.light" + hiddenString, SourceFile::colorToHex(SourceFile::filecolors["light"], QColor::HexRgb));
        _sourcefile->setValue("filecolor.shadow" + hiddenString, SourceFile::colorToHex(SourceFile::filecolors["shadow"], QColor::HexArgb));

        hiddenString = ".hidden";
        if (SourceFile::openclosecolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("open.textshadowpos" + hiddenString, QString::number(SourceFile::opencolors["textshadowpos"].toFloat()));
        _sourcefile->setValue("open.dark" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::opencolors["dark"]), QColor::HexRgb));
        _sourcefile->setValue("open.main" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::opencolors["main"]), QColor::HexRgb));
        _sourcefile->setValue("open.light" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::opencolors["light"]), QColor::HexRgb));
        _sourcefile->setValue("open.glow" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::opencolors["glow"]), QColor::HexRgb));
        _sourcefile->setValue("open.shadow" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::opencolors["shadow"]), QColor::HexArgb));
        _sourcefile->setValue("open.textshadow" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::opencolors["textshadow"]), QColor::HexRgb));
        _sourcefile->setValue("open.textmain" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::opencolors["textmain"]), QColor::HexRgb));
        _sourcefile->setValue("open.textselected" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::opencolors["textselected"]), QColor::HexRgb));

        _sourcefile->setValue("close.textshadowpos" + hiddenString, QString::number(SourceFile::closecolors["textshadowpos"].toFloat()));
        _sourcefile->setValue("close.dark" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::closecolors["dark"]), QColor::HexRgb));
        _sourcefile->setValue("close.main" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::closecolors["main"]), QColor::HexRgb));
        _sourcefile->setValue("close.light" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::closecolors["light"]), QColor::HexRgb));
        _sourcefile->setValue("close.glow" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::closecolors["glow"]), QColor::HexRgb));
        _sourcefile->setValue("close.shadow" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::closecolors["shadow"]), QColor::HexArgb));
        _sourcefile->setValue("close.textshadow" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::closecolors["textshadow"]), QColor::HexRgb));
        _sourcefile->setValue("close.textmain" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::closecolors["textmain"]), QColor::HexRgb));
        _sourcefile->setValue("close.textselected" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::closecolors["textselected"]), QColor::HexRgb));

        hiddenString = ".hidden";
        if (SourceFile::folderbackbuttoncolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("folderarrowcolor.textshadowpos", QString::number(SourceFile::folderbackbuttoncolors["textshadowpos"].toFloat()));
        _sourcefile->setValue("folderarrowcolor.dark" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["dark"]), QColor::HexRgb));
        _sourcefile->setValue("folderarrowcolor.main" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["main"]), QColor::HexRgb));
        _sourcefile->setValue("folderarrowcolor.light" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["light"]), QColor::HexRgb));
        _sourcefile->setValue("folderarrowcolor.glow" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["glow"]), QColor::HexRgb));
        _sourcefile->setValue("folderarrowcolor.shadow" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["shadow"]), QColor::HexArgb));
        _sourcefile->setValue("folderarrowcolor.textshadow" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["textshadow"]), QColor::HexRgb));
        _sourcefile->setValue("folderarrowcolor.textmain" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["textmain"]), QColor::HexRgb));
        _sourcefile->setValue("folderarrowcolor.textselected" + hiddenString, SourceFile::colorToHex(qvariant_cast<QColor>(SourceFile::folderbackbuttoncolors["textselected"]), QColor::HexRgb));

        hiddenString = ".hidden";
        if (SourceFile::arrowbuttonbasecolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("arrowbuttoncolor.dark" + hiddenString, SourceFile::colorToHex(SourceFile::arrowbuttonbasecolors["dark"], QColor::HexRgb));
        _sourcefile->setValue("arrowbuttoncolor.main" + hiddenString, SourceFile::colorToHex(SourceFile::arrowbuttonbasecolors["main"], QColor::HexRgb));
        _sourcefile->setValue("arrowbuttoncolor.light" + hiddenString, SourceFile::colorToHex(SourceFile::arrowbuttonbasecolors["light"], QColor::HexRgb));
        _sourcefile->setValue("arrowbuttoncolor.shadow" + hiddenString, SourceFile::colorToHex(SourceFile::arrowbuttonbasecolors["shadow"], QColor::HexArgb));

        hiddenString = ".hidden";
        if (SourceFile::arrowbuttonarrowcolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("arrowcolor.border" + hiddenString, SourceFile::colorToHex(SourceFile::arrowbuttonarrowcolors["border"], QColor::HexRgb));
        _sourcefile->setValue("arrowcolor.unpressed" + hiddenString, SourceFile::colorToHex(SourceFile::arrowbuttonarrowcolors["unpressed"], QColor::HexRgb));
        _sourcefile->setValue("arrowcolor.pressed" + hiddenString, SourceFile::colorToHex(SourceFile::arrowbuttonarrowcolors["pressed"], QColor::HexRgb));

        hiddenString = ".hidden";
        if (SourceFile::bottomcornercolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("bottomcornerbuttoncolor.dark" + hiddenString, SourceFile::colorToHex(SourceFile::bottomcornercolors["dark"], QColor::HexRgb));
        _sourcefile->setValue("bottomcornerbuttoncolor.main" + hiddenString, SourceFile::colorToHex(SourceFile::bottomcornercolors["main"], QColor::HexRgb));
        _sourcefile->setValue("bottomcornerbuttoncolor.light" + hiddenString, SourceFile::colorToHex(SourceFile::bottomcornercolors["light"], QColor::HexRgb));
        _sourcefile->setValue("bottomcornerbuttoncolor.shadow" + hiddenString, SourceFile::colorToHex(SourceFile::bottomcornercolors["shadow"], QColor::HexRgb));
        _sourcefile->setValue("bottomcornerbuttoncolor.iconmain" + hiddenString, SourceFile::colorToHex(SourceFile::bottomcornercolors["iconmain"], QColor::HexRgb));
        _sourcefile->setValue("bottomcornerbuttoncolor.iconlight" + hiddenString, SourceFile::colorToHex(SourceFile::bottomcornercolors["iconlight"], QColor::HexRgb));
        _sourcefile->setValue("bottomcornerbuttoncolor.icontextmain" + hiddenString, SourceFile::colorToHex(SourceFile::bottomcornercolors["icontextmain"], QColor::HexRgb));

        hiddenString = ".hidden";
        if (SourceFile::topcornercolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("topcornerbuttoncolor.main" + hiddenString, SourceFile::colorToHex(SourceFile::topcornercolors["main"], QColor::HexRgb));
        _sourcefile->setValue("topcornerbuttoncolor.light" + hiddenString, SourceFile::colorToHex(SourceFile::topcornercolors["light"], QColor::HexRgb));
        _sourcefile->setValue("topcornerbuttoncolor.shadow" + hiddenString, SourceFile::colorToHex(SourceFile::topcornercolors["shadow"], QColor::HexRgb));
        _sourcefile->setValue("topcornerbuttoncolor.textmain" + hiddenString, SourceFile::colorToHex(SourceFile::topcornercolors["textmain"], QColor::HexRgb));

        hiddenString = ".hidden";
        if (SourceFile::folderviewcolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("folderbgcolor.dark" + hiddenString, SourceFile::colorToHex(SourceFile::folderviewcolors["dark"], QColor::HexRgb));
        _sourcefile->setValue("folderbgcolor.main" + hiddenString, SourceFile::colorToHex(SourceFile::folderviewcolors["main"], QColor::HexRgb));
        _sourcefile->setValue("folderbgcolor.light" + hiddenString, SourceFile::colorToHex(SourceFile::folderviewcolors["light"], QColor::HexRgb));
        _sourcefile->setValue("folderbgcolor.shadow" + hiddenString, SourceFile::colorToHex(SourceFile::folderviewcolors["shadow"], QColor::HexArgb));

        hiddenString = ".hidden";
        if (SourceFile::gametextcolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("gametext.main" + hiddenString, SourceFile::colorToHex(SourceFile::gametextcolors["main"], QColor::HexRgb));
        _sourcefile->setValue("gametext.light" + hiddenString, SourceFile::colorToHex(SourceFile::gametextcolors["light"], QColor::HexRgb));
        _sourcefile->setValue("gametext.shadow" + hiddenString, SourceFile::colorToHex(SourceFile::gametextcolors["shadow"], QColor::HexArgb));
        _sourcefile->setValue("gametext.textmain" + hiddenString, SourceFile::colorToHex(SourceFile::gametextcolors["textmain"], QColor::HexRgb));

        hiddenString = ".hidden";
        if (SourceFile::demotextcolorEnabled) {
            hiddenString = "";
        }
        _sourcefile->setValue("demotextcolor.main" + hiddenString, SourceFile::colorToHex(SourceFile::demotextcolors["main"], QColor::HexRgb));
        _sourcefile->setValue("demotextcolor.textmain" + hiddenString, SourceFile::colorToHex(SourceFile::demotextcolors["textmain"], QColor::HexRgb));
        _sourcefile->endGroup();

        if (SourceFile::gametextcolorHidden) {
            _sourcefile->setValue("flags/gametext.hidden", 1);
        }

        // Save audio

        _sourcefile->beginGroup("audio");
        if (!SourceFile::bgm.isEmpty() && !QFileInfo(SourceFile::bgm).isDir()) {
            _sourcefile->setValue("bgm", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::bgm)));
        }

        if (!SourceFile::sfxes["cursor"].isEmpty() && !QFileInfo(SourceFile::sfxes["cursor"]).isDir()) {
            _sourcefile->setValue("sfx.cursor", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::sfxes["cursor"])));
        }

        if (!SourceFile::sfxes["launch"].isEmpty() && !QFileInfo(SourceFile::sfxes["launch"]).isDir()) {
            _sourcefile->setValue("sfx.launch", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::sfxes["launch"])));
        }

        if (!SourceFile::sfxes["folder"].isEmpty() && !QFileInfo(SourceFile::sfxes["folder"]).isDir()) {
            _sourcefile->setValue("sfx.folder", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::sfxes["folder"])));
        }

        if (!SourceFile::sfxes["close"].isEmpty() && !QFileInfo(SourceFile::sfxes["close"]).isDir()) {
            _sourcefile->setValue("sfx.close", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::sfxes["close"])));
        }

        if (!SourceFile::sfxes["frame0"].isEmpty() && !QFileInfo(SourceFile::sfxes["frame0"]).isDir()) {
            _sourcefile->setValue("sfx.frame0", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::sfxes["frame0"])));
        }

        if (!SourceFile::sfxes["frame1"].isEmpty() && !QFileInfo(SourceFile::sfxes["frame1"]).isDir()) {
            _sourcefile->setValue("sfx.frame1", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::sfxes["frame1"])));
        }

        if (!SourceFile::sfxes["frame2"].isEmpty() && !QFileInfo(SourceFile::sfxes["frame2"]).isDir()) {
            _sourcefile->setValue("sfx.frame2", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::sfxes["frame2"])));
        }

        if (!SourceFile::sfxes["resume"].isEmpty() && !QFileInfo(SourceFile::sfxes["resume"]).isDir()) {
            _sourcefile->setValue("sfx.openlid", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::sfxes["resume"])));
        }
        _sourcefile->endGroup();

        _sourcefile->beginGroup("kame-editor");
        if (!SourceFile::deploypath.isEmpty()) {
            _sourcefile->setValue("deploy", sourceDir.cleanPath(sourceDir.relativeFilePath(SourceFile::deploypath)));
        }
        _sourcefile->sync();
        // No longer need the autosave; the original file is successfully saved.
        if (!autosave) {
            SourceFile::recentlyModified = false;
            deleteAutosave();
            _currentAutosavePath = _filepath + ".autosave";
        }

        if (rgb565) {
            QString rgb565path = fileInfo.dir().path() + "/" + fileInfo.completeBaseName() + ".rgb565.rc";
            QFile originalSource(filepath);
            originalSource.copy(rgb565path);
            QSettings * rgb565source = new QSettings(rgb565path, QSettings::IniFormat);
            if (!rgb565Icon.isEmpty()) {
                rgb565source->setValue("info/icon", rgb565Icon);
            }
            if (!rgb565SmallIcon.isEmpty()) {
                rgb565source->setValue("info/smallicon", rgb565SmallIcon);
            }
            if (!rgb565TopTexture.isEmpty()) {
                rgb565source->setValue("textures/top.image", rgb565TopTexture);
            }
            if (!rgb565BottomTexture.isEmpty()) {
                rgb565source->setValue("textures/bottom.image", rgb565BottomTexture);
            }

            if (!hasShortTitle) {
                for (int i = 0; i < DEFAULT_SHORTTITLE.size(); i++) {
                    rgb565source->setValue("info/" + SourceFile::LANGUAGES[i] + "shorttitle", DEFAULT_SHORTTITLE[i].trimmed());
                }
            }
            if (!hasLongTitle) {
                for (int i = 0; i < DEFAULT_LONGTITLE.size(); i++) {
                    rgb565source->setValue("info/" + SourceFile::LANGUAGES[i] + "longtitle", DEFAULT_LONGTITLE[i].trimmed());
                }
            }
            if (!hasPublisher) {
                for (int i = 0; i < DEFAULT_PUBLISHER.size(); i++) {
                    rgb565source->setValue("info/" + SourceFile::LANGUAGES[i] + "publisher", DEFAULT_PUBLISHER[i].trimmed());
                }
            }

            rgb565source->sync();
            delete rgb565source;
        }
        return true;
    }
    return false;
}

void ConsoleMenu::checkSourceFile() {
    _isvalid = validateSourceFile();
    if (_state == State::DEPLOY) {
        updateSaveButton();
    }
}

void ConsoleMenu::autosaveChanged(int interval) {
    if (interval > 0) {
        _autosavetimer->start(interval * 1000 * 60);
    } else {
        _autosavetimer->stop();
    }
}

void ConsoleMenu::restoreAutosave(QString path) {
    QString cachePath = QFileInfo(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/" + path).filePath();
    if (SourceFile::recentlyModified) {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, _unsavedChangesTitle,
                _unsavedChanges,
                QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if(reply == QMessageBox::Save) {
            saveSourceFile();
        }
        if (reply == QMessageBox::Cancel) {
            return;
        }
    }
    if (checkMissingResources(cachePath)) {
        deleteAutosave();
        _filepath = cachePath;
        _currentAutosavePath = cachePath;
        loadSourceFile();
        emit sourceLoaded();
    }
}

void ConsoleMenu::changeDeployType(bool enabled) {
    _deploytype = enabled;
}

void ConsoleMenu::updateSaveButton() {
    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);
    QString path = ":/resources/skins/" + _skinid + "/button-save.png";
    bool isRow = skin.value("menu/type").toString() == "row";
    if (_isvalid && _kametoolsFound) {
        if (isRow) {
            _rightButton->setIcon(QIcon(_warnLossy.isEmpty() ? path : warningTexture(path)));
        } else {
            drawOptionButton(_rightButton, "save", 2, !_warnLossy.isEmpty());
        }
        _rightButton->setToolTip(tr("Deploy Theme Archive", "Display a tooltip for the deploy theme button") + _warnLossy);
    } else {
        if (isRow) {
            _rightButton->setIcon(QIcon(warningTexture(path)));
        } else {
            drawOptionButton(_rightButton, "save", 2, true);
        }
        _rightButton->setToolTip(_validateSaveMessage + _warnLossy);
    }
}

void ConsoleMenu::updateLoadButton() {
    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);
    QString path = ":/resources/skins/" + _skinid + "/button-load.png";
    bool isRow = skin.value("menu/type").toString() == "row";
    if (_kametoolsFound && _importEnabled) {
        if (isRow) {
            _leftButton->setIcon(QIcon(path));
        } else {
            drawOptionButton(_leftButton, "load", 1);
        }
        _leftButton->setToolTip(tr("Import Theme Archive", "Display a tooltip for the import theme button"));
    } else {
        if (isRow) {
            _leftButton->setIcon(QIcon(warningTexture(path)));
        } else {
            drawOptionButton(_leftButton, "load", 1, true);
        }
        _leftButton->setToolTip(_validateLoadMessage);
    }
}

bool ConsoleMenu::validateSourceFile() {
    bool foundShortTitle = false;
    bool foundLongTitle = false;
    bool foundPublisher = false;
    for (int i = 0; i < SourceFile::LANGUAGES.size(); i++) {
        if (!SourceFile::shorttitle[i].isEmpty()) {
            foundShortTitle = true;
        }
        if (!SourceFile::longtitle[i].isEmpty()) {
            foundLongTitle = true;
        }
        if (!SourceFile::publisher[i].isEmpty()) {
            foundPublisher = true;
        }
    }
    bool foundIcon = !SourceFile::smdhIconPath.isEmpty();

    bool invalidTopTexture = false;
    bool invalidBottomTexture = false;
    int index = 0;
    TopView::DrawType topdrawtype = static_cast<TopView::DrawType>(SourceFile::topdraw);
    if (topdrawtype == TopView::DrawType::TEXTURE || topdrawtype == TopView::DrawType::COLORTEXTURE) {
        index = topdrawtype == TopView::DrawType::COLORTEXTURE ? 2 : std::min(SourceFile::topframe, 1);
        invalidTopTexture = SourceFile::toptexture[index].isEmpty();
    }

    if (static_cast<BottomView::DrawType>(SourceFile::bottomdraw) == BottomView::DrawType::TEXTURE) {
        index = std::min(SourceFile::bottomframe, 1);
        invalidBottomTexture = SourceFile::bottomtexture[index].isEmpty();
    }

    if (_kametoolsFound) {
        _validateSaveMessage = "<html><head/><body><p>" + tr("The following properties are missing and required to deploy this theme:", "Displays a validation message in the deploy button tooltip") + " </p><ul>";

        _validateSaveMessage += foundShortTitle ? "" : "<li>" + tr("Short Title", "Name of the short title SMDH field") + "</li>";
        _validateSaveMessage += foundLongTitle ? "" : "<li>" + tr("Long Title", "Name of the long title SMDH field") + "</li>";
        _validateSaveMessage += foundPublisher ? "" : "<li>" + tr("Publisher", "Name of the publisher SMDH field") + "</li>";
        _validateSaveMessage += foundIcon ? "" : "<li>" + tr("Theme Icon", "Name of the SMDH image ") + "</li>";
        _validateSaveMessage += invalidTopTexture ? "<li>" + tr("Top Screen Color Texture/Texture Type Image", "Indicates one of the top screen variables is not valid") + "</li>" : "";
        _validateSaveMessage += invalidBottomTexture ? "<li>" + tr("Bottom Screen Texture Type Image", "Indicates one of the bottom screen variables is not valid") + "</li>" : "";
        _validateSaveMessage += "</ul>";
        _validateSaveMessage += tr("Kame Editor will fill in the required missing fields with sensible defaults, but it's recommended to fill in those fields yourself.", "Validation message note at the bottom");
        _validateSaveMessage += "</body></html>";
    }

    return (foundShortTitle && foundLongTitle && foundPublisher && foundIcon && !invalidTopTexture && !invalidBottomTexture);
}

QPixmap ConsoleMenu::combineIcons(QPixmap button, QPixmap option) {
    int maxHeight = std::max(button.height(), option.height());
    QPixmap combined = QPixmap(button.width() + option.width(), maxHeight);
    combined.fill(QColor(Qt::transparent));
    QPainter p;
    p.begin(&combined);
    p.drawPixmap(0, (maxHeight - button.height()) / 2, button);
    p.drawPixmap(button.width(), (maxHeight - option.height()) / 2, option);
    p.end();
    return combined;
}

QPixmap ConsoleMenu::overlayIcons(QPixmap button, QPixmap icon, int offsetY) {
    QPixmap overlay = QPixmap(button.width(), button.height());
    overlay.fill(QColor(Qt::transparent));
    QPainter p;
    p.begin(&overlay);
    p.drawPixmap(0, 0, button);
    p.drawPixmap((button.width() - icon.width()) / 2, (button.height() - icon.height()) / 2 + offsetY, icon);
    p.end();
    return overlay;
}

QPixmap ConsoleMenu::disabledTexture(QString path) {
    QPixmap pixmap = QPixmap(path);
    QPixmap result = QPixmap(pixmap.width(), pixmap.height());
    result.fill(Qt::transparent);
    QPainter painter;

    painter.begin(&result);
    painter.setOpacity(0.5);
    painter.drawPixmap(0, 0, pixmap);
    painter.end();

    return result;
}

QPixmap ConsoleMenu::warningTexture(QString path) {
    QSettings skin(":/resources/skins/" + _skinid + ".conf", QSettings::IniFormat);

    QPixmap pixmap = QPixmap(path);
    QPixmap result = QPixmap(pixmap.width() + 20, pixmap.height());
    result.fill(Qt::transparent);
    QPainter painter;

    painter.begin(&result);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    painter.drawPixmap(0, 0, pixmap);

    QFont font = QFont("Nimbus Sans");
    font.setPixelSize(11);
    font.setWeight(QFont::Bold);
    QPainterPath ppath;
    ppath.addEllipse(pixmap.width() + 4, 2, 12, 12);

    QPainterPath text;
    text.addText(pixmap.width() + 8, 12, font, "!");

    ppath = ppath.subtracted(text);

    painter.setPen(QColor(Qt::transparent));
    painter.setBrush(QColor(skin.value("menu/color", QColor(Qt::white)).toString()));
    painter.drawPath(ppath);
    painter.end();

    return result;
}

std::unique_ptr<QFileDialog> ConsoleMenu::getSaveDialog() {
    QFileDialog* _filedialog = new QFileDialog();
    _filedialog->setAcceptMode(QFileDialog::AcceptSave);
    _filedialog->setWindowTitle(tr("Save Source File...", "Dialogue title when choosing to save the theme source file."));
    _filedialog->setDefaultSuffix("rc");
    _filedialog->setNameFilters(QStringList() << tr("Source Files (*.rc)"));
    return std::unique_ptr<QFileDialog>(_filedialog);
}

std::unique_ptr<QFileDialog> ConsoleMenu::getLoadDialog() {
    QFileDialog* _filedialog = new QFileDialog();
    _filedialog->setAcceptMode(QFileDialog::AcceptOpen);
    _filedialog->setWindowTitle(tr("Load Source File...", "Dialogue title when choosing to load a theme source file."));
    _filedialog->setDefaultSuffix(ResourcesDialog::FILETYPES["rc"].defaultExt);
    _filedialog->setNameFilters(ResourcesDialog::FILETYPES["rc"].filters);
    return std::unique_ptr<QFileDialog>(_filedialog);
}

std::unique_ptr<QFileDialog> ConsoleMenu::getDeployDialog() {
    QFileDialog* _filedialog = new QFileDialog();
    _filedialog->setAcceptMode(QFileDialog::AcceptSave);
    _filedialog->setWindowTitle(tr("Deploy Theme File...", "Dialogue title when choosing to deploy a theme."));
    _filedialog->setDefaultSuffix(ResourcesDialog::FILETYPES["zip"].defaultExt);
    _filedialog->setNameFilters(ResourcesDialog::FILETYPES["zip"].filters);
    return std::unique_ptr<QFileDialog>(_filedialog);
}

std::unique_ptr<QFileDialog> ConsoleMenu::getImportDialog() {
    QFileDialog* _filedialog = new QFileDialog();
    _filedialog->setAcceptMode(QFileDialog::AcceptOpen);
    _filedialog->setFileMode(QFileDialog::ExistingFile);
    _filedialog->setOption(QFileDialog::ShowDirsOnly, false);
    _filedialog->setWindowTitle(tr("Import Theme File...", "Dialogue title when choosing to import a theme zip."));
    _filedialog->setDefaultSuffix("zip");
    _filedialog->setNameFilters(QStringList() << tr("Zip Archives (*.zip)"));
    return std::unique_ptr<QFileDialog>(_filedialog);
}

void ConsoleMenu::mouseMoveEvent(QMouseEvent *event) {
    if (_leftButton->underMouse() || _rightButton->underMouse() || _middleButton->underMouse()) {
        event->accept();
        _leftButton->clearFocus();
        _rightButton->clearFocus();
        _middleButton->clearFocus();
    } else {
        event->ignore();
    }
}
