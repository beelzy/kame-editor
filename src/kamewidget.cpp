// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QMouseEvent>
#include <QPainter>
#include <QVBoxLayout>
#include <QFontDatabase>
#include <QListWidget>
#include <QApplication>
#include <QScreen>
#include <QOffscreenSurface>
#include <QOpenGLFunctions>
#include <QOpenGLContext>
#include <QMessageBox>

#include "kamewidget.h"
#include "sourcefile.h"
#include "color.h"
#include "dock/kameaccordion/kameaccordion.h"
#include "dock/kameslider.h"
#include "dock/helpwidget.h"

KameWidget::KameWidget(QWidget * parent) : QMainWindow(parent, Qt::FramelessWindowHint|Qt::WindowSystemMenuHint) {
    Q_INIT_RESOURCE(kameeditor);
    QFontDatabase::addApplicationFont(":/resources/fonts/NimbusSans-Regular.otf");

    _consoleWidget = NULL;
    isValid = true;

    // Check if OpenGL support is adequate before we get too far ahead
    QOffscreenSurface surf;
    surf.create();

    QOpenGLContext ctx;
    ctx.create();
    ctx.makeCurrent(&surf);
    QOpenGLFunctions *f = ctx.functions();
    GLint version = 0;
    f->glGetIntegerv(GL_MAJOR_VERSION, &version);
    QString platform = (const char*)f->glGetString(GL_VERSION);
    ctx.doneCurrent();
    surf.destroy();
    if (version < 3) {
        QMessageBox msgBox(QMessageBox::Critical, tr("OpenGL Version Not Supported", "Title for message box warning users about not having adequate OpenGL support"), tr("Your system is currently using OpenGL %1. Kame Editor requires OpenGL 3.0+ to work. Now closing.").arg(platform), QFlag(0), this);
        msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
        msgBox.exec();
        QApplication::quit();
        isValid = false;
        return;
    }

    setAttribute(Qt::WA_TranslucentBackground);
    setAttribute(Qt::WA_StyledBackground);

    setWindowTitle(tr("Kame Editor", "Application title"));

    _consoleWidget = new ConsoleWidget();
    setCentralWidget(_consoleWidget);

    setDockNestingEnabled(true);
    setTabPosition(Qt::AllDockWidgetAreas, QTabWidget::North);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
    QRect screen = QGuiApplication::screenAt(QCursor::pos())->geometry();
#else
    QRect screen = QGuiApplication::primaryScreen()->geometry();
#endif
    setGeometry(x(), y(), std::min(1600, screen.width()), height());

    SourceFile::reset();

    createDockWidgets();

    HelpWidget *helpwidget = new HelpWidget(this);

    connect(_consoleWidget->smdhButton(), &QAbstractButton::toggled, _smdhDockWidget, &QWidget::setVisible);
    connect(_smdhDockWidget, &QDockWidget::visibilityChanged, _consoleWidget->smdhButton(), &ConsoleButton::updateChecked);
    connect(_consoleWidget->colorsButton(), &QAbstractButton::toggled, _colorDockWidget, &QWidget::setVisible);
    connect(_colorDockWidget, &QDockWidget::visibilityChanged, _consoleWidget->colorsButton(), &ConsoleButton::updateChecked);
    connect(_consoleWidget->texturesButton(), &QAbstractButton::toggled, _textureDockWidget, &QWidget::setVisible);
    connect(_textureDockWidget, &QDockWidget::visibilityChanged, _consoleWidget->texturesButton(), &ConsoleButton::updateChecked);
    connect(_consoleWidget->audioButton(), &QAbstractButton::toggled, _audioDockWidget, &QWidget::setVisible);
    connect(_audioDockWidget, &QDockWidget::visibilityChanged, _consoleWidget->audioButton(), &ConsoleButton::updateChecked);

    connect(_consoleWidget->pad()->up(), &QAbstractButton::toggled, _aboutDockWidget, &QWidget::setVisible);
    connect(_aboutDockWidget, &QDockWidget::visibilityChanged, _consoleWidget->pad(), &ConsolePad::updateCheckedUp);
    connect(_consoleWidget->pad()->down(), &QAbstractButton::toggled, _settingsDockWidget, &QWidget::setVisible);
    connect(_settingsDockWidget, &QDockWidget::visibilityChanged, _consoleWidget->pad(), &ConsolePad::updateCheckedDown);
    connect(_consoleWidget->pad()->right(), &QAbstractButton::toggled, helpwidget, &QWidget::setVisible);
    connect(helpwidget, &HelpWidget::visibilityChanged, _consoleWidget->pad(), &ConsolePad::updateCheckedRight);
    connect(_consoleWidget->pad()->left(), &QAbstractButton::toggled, this, &KameWidget::toggleWidgets);
    if (_settingsWidget->autosaveFilesPending) {
        _settingsDockWidget->show();
    }
}

KameWidget::~KameWidget() {
    if (_consoleWidget != NULL) {
        delete _consoleWidget;
        _aboutDockWidget->deleteLater();
        _aboutWidget->deleteLater();
        _settingsDockWidget->deleteLater();
        _settingsWidget->deleteLater();
        _colorDockWidget->deleteLater();
        _colorWidget->deleteLater();
        _smdhDockWidget->deleteLater();
        _smdhWidget->deleteLater();
        _audioDockWidget->deleteLater();
        _audioWidget->deleteLater();
        _textureDockWidget->deleteLater();
        _textureWidget->deleteLater();
    }
}

void KameWidget::handleFocus(Qt::ApplicationState state) {
    if (state == Qt::ApplicationInactive) {
        _consoleWidget->setAnimated(false);
        emit windowInactive();
    } else {
        _consoleWidget->setAnimated(true);
        emit windowActive();
    }
}

void KameWidget::saveWindowState() {
    QSettings settings;
    settings.setValue("kamewidget/geometry", saveGeometry());
    settings.setValue("kamewidget/windowState", saveState());
}

void KameWidget::createDockWidgets() {
    _aboutWidget = new AboutWidget();
    _aboutDockWidget = new KameDockWidget(_aboutWidget, tr("About", "About Dock Widget title"), this);
    addDockWidget(Qt::LeftDockWidgetArea, _aboutDockWidget);
    _aboutDockWidget->setObjectName("about");

    _settingsWidget = new SettingsWidget();
    _settingsDockWidget = new KameDockWidget(_settingsWidget, tr("Settings", "Kame Editor configuration"), this);
    addDockWidget(Qt::LeftDockWidgetArea, _settingsDockWidget);
    _settingsDockWidget->setObjectName("settings");
    _settingsDockWidget->setFloating(true);

    _smdhWidget = new SMDHWidget();
    _smdhDockWidget = new KameDockWidget(_smdhWidget->contents(), tr("Theme Info", "Theme Info Dock Widget title"), this);

    addDockWidget(Qt::LeftDockWidgetArea, _smdhDockWidget);
    _smdhDockWidget->setObjectName("smdh");

    _smdhWidget->setDockWidget(_smdhDockWidget);

    _audioWidget = new AudioWidget();
    _audioDockWidget = new KameDockWidget(_audioWidget->contents(), tr("Audio", "Audio Dock Widget title"), this);

    addDockWidget(Qt::LeftDockWidgetArea, _audioDockWidget);
    _audioDockWidget->setObjectName("audio");

    _audioWidget->setDockWidget(_audioDockWidget);

    _textureWidget = new TextureWidget();
    _textureDockWidget = new KameDockWidget(_textureWidget->contents(), tr("Textures and Screen Appearance", "Textures and Screen Appearance Dock Widget title"), this);
    addDockWidget(Qt::RightDockWidgetArea, _textureDockWidget);
    _textureDockWidget->setObjectName("texture");

    _textureWidget->setDockWidget(_textureDockWidget);

    _colorWidget = new ColorWidget();
    _colorDockWidget = new KameDockWidget(_colorWidget->contents(), tr("Colors", "Colors Dock Widget title"), this);
    addDockWidget(Qt::RightDockWidgetArea, _colorDockWidget);
    _colorDockWidget->setObjectName("color");

    _colorWidget->setDockWidget(_colorDockWidget);

    QSettings settings;
    restoreGeometry(settings.value("kamewidget/geometry").toByteArray());
    restoreState(settings.value("kamewidget/windowState").toByteArray());

    _consoleWidget->pad()->up()->setChecked(_aboutDockWidget->isVisible());
    _consoleWidget->pad()->down()->setChecked(_settingsDockWidget->isVisible());
    _consoleWidget->smdhButton()->setChecked(_smdhDockWidget->isVisible());
    _consoleWidget->audioButton()->setChecked(_audioDockWidget->isVisible());
    _consoleWidget->texturesButton()->setChecked(_textureDockWidget->isVisible());
    _consoleWidget->colorsButton()->setChecked(_colorDockWidget->isVisible());

    connect(_smdhWidget, &SMDHWidget::smdhRequiredFieldsChanged, _consoleWidget->consoleMenu(), &ConsoleMenu::checkSourceFile);
    connect(this, &KameWidget::windowActive, _smdhWidget, &SMDHWidget::onActive);

    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::resetSource, _settingsWidget, &SettingsWidget::resetPath);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::resetSource, _smdhWidget, &SMDHWidget::resetFields);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::resetSource, _textureWidget, &TextureWidget::reset);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::resetSource, _colorWidget, &ColorWidget::reset);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::resetSource, _audioWidget, &AudioWidget::reset);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::sourceLoaded, _settingsWidget, &SettingsWidget::updatePath);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::deployPathSelected, _settingsWidget, &SettingsWidget::updatePath);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::sourceLoaded, _smdhWidget, &SMDHWidget::loadFields);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::sourceLoaded, _textureWidget, &TextureWidget::load);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::sourceLoaded, _colorWidget, &ColorWidget::load);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::sourceLoaded, _audioWidget, &AudioWidget::load);
    if (settings.contains("kame-tools/available") && settings.contains("kame-tools/nodither")) {
        _textureWidget->onKameTools();
        _smdhWidget->onKameTools();
    }
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::kameToolsCheckComplete, _textureWidget, &TextureWidget::onKameTools);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::kameToolsCheckComplete, _smdhWidget, &SMDHWidget::onKameTools);

    connect(_settingsWidget, &SettingsWidget::bgmAutoplayChanged, _audioWidget, &AudioWidget::enableBGMAutoplay);
    connect(_settingsWidget, &SettingsWidget::skinChanged, _consoleWidget, &ConsoleWidget::setSkinId);
    connect(_settingsWidget, &SettingsWidget::autosaveChanged, _consoleWidget->consoleMenu(), &ConsoleMenu::autosaveChanged);
    connect(_settingsWidget, &SettingsWidget::folderLetterChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::onFolderLetterChanged);
    connect(_settingsWidget, &SettingsWidget::autosaveRestored, _consoleWidget->consoleMenu(), &ConsoleMenu::restoreAutosave);
    connect(_settingsWidget, &SettingsWidget::deployTypeChanged, _consoleWidget->consoleMenu(), &ConsoleMenu::changeDeployType);
    connect(_settingsWidget, &SettingsWidget::restart, _consoleWidget, &ConsoleWidget::onRestart);
    connect(_consoleWidget->consoleMenu(), &ConsoleMenu::autosaveDeleted, _settingsWidget, &SettingsWidget::autosaveDeleted);

    connectColorWidget(_colorWidget);

    connectTextureWidget(_textureWidget);

    connectAudioWidget(_audioWidget);
}

void KameWidget::connectColorWidget(ColorWidget* colorWidget) {
    connect(colorWidget->colorSwatch("cursor", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->cursor(), &CursorWidget::setDarkColor);
    connect(colorWidget->colorSwatch("cursor", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->cursor(), &CursorWidget::setMainColor);
    connect(colorWidget->colorSwatch("cursor", "glow"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->cursor(), &CursorWidget::setGlowColor);
    connect(colorWidget->colorSwatch("cursor", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->cursor(), &CursorWidget::setLightColor);
    connect(colorWidget, &ColorWidget::cursorColorChanged, _consoleWidget->bottomwidget()->cursor(), &CursorWidget::updateColors);

    connect(colorWidget->colorSwatch("folder", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setFolderDarkColor);
    connect(colorWidget->colorSwatch("folder", "dark"), &ColorSwatch::colorChanged, _consoleWidget->topview(), &TopView::setFolderDarkColor);
    connect(colorWidget->colorSwatch("folder", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setFolderMainColor);
    connect(colorWidget->colorSwatch("folder", "main"), &ColorSwatch::colorChanged, _consoleWidget->topview(), &TopView::setFolderMainColor);
    connect(colorWidget->colorSwatch("folder", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setFolderLightColor);
    connect(colorWidget->colorSwatch("folder", "light"), &ColorSwatch::colorChanged, _consoleWidget->topview(), &TopView::setFolderLightColor);
    // unused?
    //connect(colorWidget->colorSwatch("folder", "shadow"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setFolderShadowColor);
    connect(colorWidget, &ColorWidget::folderColorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::updateFolderColors);
    connect(colorWidget, &ColorWidget::folderColorChanged, _consoleWidget->topview(), &TopView::updateFolderColors);

    connect(colorWidget->colorSwatch("file", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setFileDarkColor);
    connect(colorWidget->colorSwatch("file", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setFileMainColor);
    connect(colorWidget->colorSwatch("file", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::setFileDarkColor);
    connect(colorWidget->colorSwatch("file", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::setFileMainColor);
    connect(colorWidget->colorSwatch("file", "main"), &ColorSwatch::colorChanged, _consoleWidget->topview(), &TopView::setFileColor);
    connect(colorWidget->colorSwatch("file", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setFileLightColor);
    connect(colorWidget->colorSwatch("file", "shadow"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setFileShadowColor);
    connect(colorWidget->colorSwatch("file", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::setFileLightColor);
    connect(colorWidget->colorSwatch("file", "shadow"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::setFileShadowColor);
    connect(colorWidget, &ColorWidget::fileColorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::updateFileColors);
    connect(colorWidget, &ColorWidget::fileColorChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::updateFileColors);
    connect(colorWidget, &ColorWidget::fileColorChanged, _consoleWidget->topview(), &TopView::updateFileColors);

    connect(colorWidget->colorSwatch("open", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setDarkColor);
    connect(colorWidget->colorSwatch("open", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setMainColor);
    connect(colorWidget->colorSwatch("open", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setLightColor);
    connect(colorWidget->colorSwatch("open", "textshadow"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setTextShadowColor);
    connect(colorWidget->colorSwatch("open", "textmain"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setTextMainColor);
    connect(colorWidget->colorSwatch("open", "textselected"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setTextSelectedColor);
    connect(colorWidget->slider("open"), &KameSlider::valueChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setTextShadowPos);

    connect(colorWidget->colorSwatch("close", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setCloseDarkColor);
    connect(colorWidget->colorSwatch("close", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setCloseMainColor);
    connect(colorWidget->colorSwatch("close", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setCloseLightColor);
    connect(colorWidget->colorSwatch("close", "textshadow"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setCloseTextShadowColor);
    connect(colorWidget->colorSwatch("close", "textmain"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setCloseTextMainColor);
    connect(colorWidget->colorSwatch("close", "textselected"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setCloseTextSelectedColor);
    connect(colorWidget->slider("close"), &KameSlider::valueChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::setCloseTextShadowPos);

    connect(colorWidget, &ColorWidget::opencloseColorChanged, _consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::updateColors);

    connect(colorWidget->colorSwatch("folderbackbutton", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->folderbackbutton(), &FolderBackButton::setDarkColor);
    connect(colorWidget->colorSwatch("folderbackbutton", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->folderbackbutton(), &FolderBackButton::setMainColor);
    connect(colorWidget->colorSwatch("folderbackbutton", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->folderbackbutton(), &FolderBackButton::setLightColor);
    connect(colorWidget->colorSwatch("folderbackbutton", "textshadow"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->folderbackbutton(), &FolderBackButton::setTextShadowColor);
    connect(colorWidget->colorSwatch("folderbackbutton", "textmain"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->folderbackbutton(), &FolderBackButton::setTextMainColor);
    connect(colorWidget->colorSwatch("folderbackbutton", "textselected"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->folderbackbutton(), &FolderBackButton::setTextSelectedColor);
    connect(colorWidget->slider("folderbackbutton"), &KameSlider::valueChanged, _consoleWidget->bottomwidget()->folderbackbutton(), &FolderBackButton::setTextShadowPos);

    connect(colorWidget, &ColorWidget::folderbackbuttonColorChanged, _consoleWidget->bottomwidget()->folderbackbutton(), &FolderBackButton::updateColors);

    connect(colorWidget->colorSwatch("arrowbuttonbase", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setArrowDarkColor);
    connect(colorWidget->colorSwatch("arrowbuttonbase", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setArrowMainColor);
    connect(colorWidget->colorSwatch("arrowbuttonbase", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setArrowLightColor);
    connect(colorWidget, &ColorWidget::arrowbuttonbaseColorChanged, _consoleWidget->bottomwidget(), &BottomWidget::updateArrowBaseColors);

    connect(colorWidget->colorSwatch("arrowbuttonarrow", "border"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setArrowBorderColor);
    connect(colorWidget->colorSwatch("arrowbuttonarrow", "unpressed"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setArrowUnpressedColor);
    connect(colorWidget->colorSwatch("arrowbuttonarrow", "pressed"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setArrowPressedColor);
    connect(colorWidget, &ColorWidget::arrowbuttonarrowColorChanged, _consoleWidget->bottomwidget(), &BottomWidget::updateArrowColors);

    connect(colorWidget->colorSwatch("bottomcorner", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setFakeButtonDarkColor);
    connect(colorWidget->colorSwatch("bottomcorner", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setFakeButtonMainColor);
    connect(colorWidget->colorSwatch("bottomcorner", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setFakeButtonLightColor);
    connect(colorWidget->colorSwatch("bottomcorner", "iconmain"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setFakeButtonIconMainColor);
    connect(colorWidget->colorSwatch("bottomcorner", "iconlight"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget(), &BottomWidget::setFakeButtonIconLightColor);
    connect(colorWidget, &ColorWidget::bottomcornerColorChanged, _consoleWidget->bottomwidget(), &BottomWidget::updateFakeButtonColors);

    connect(colorWidget->colorSwatch("topcorner", "main"), &ColorSwatch::colorChanged, _consoleWidget->topview(), &TopView::setMainColor);
    connect(colorWidget->colorSwatch("topcorner", "shadow"), &ColorSwatch::colorChanged, _consoleWidget->topview(), &TopView::setShadowColor);
    connect(colorWidget->colorSwatch("topcorner", "textmain"), &ColorSwatch::colorChanged, _consoleWidget->topview(), &TopView::setTextMainColor);
    connect(colorWidget, &ColorWidget::topcornerColorChanged, _consoleWidget->topview(), &TopView::updateColors);

    connect(colorWidget->colorSwatch("folderview", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::setIconDarkColor);
    connect(colorWidget->colorSwatch("folderview", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::setMainColor);
    connect(colorWidget->colorSwatch("folderview", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::setIconLightColor);
    connect(colorWidget->colorSwatch("folderview", "shadow"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::setShadowColor);
    connect(colorWidget, &ColorWidget::folderviewColorChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::updateColors);

    connect(colorWidget->colorSwatch("gametext", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gametextwidget(), &GameTextWidget::setMainColor);
    connect(colorWidget->colorSwatch("gametext", "textmain"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gametextwidget(), &GameTextWidget::setTextMainColor);
    connect(colorWidget, &ColorWidget::gametextColorChanged, _consoleWidget->bottomwidget()->gametextwidget(), &GameTextWidget::updateColors);

    connect(colorWidget->colorSwatch("demotext", "main"), &ColorSwatch::colorChanged, _consoleWidget->topview(), &TopView::setDemoMainColor);
    connect(colorWidget->colorSwatch("demotext", "textmain"), &ColorSwatch::colorChanged, _consoleWidget->topview(), &TopView::setDemoTextMainColor);
    connect(colorWidget, &ColorWidget::demotextColorChanged, _consoleWidget->topview(), &TopView::updateDemoColors);

}

void KameWidget::connectTextureWidget(TextureWidget* textureWidget) {
    connect(textureWidget, &TextureWidget::topDrawTypeChanged, _consoleWidget, &ConsoleWidget::onTopDrawTypeChanged);
    connect(textureWidget->topColorSwatch(), &ColorSwatch::colorChanged, _consoleWidget->topview(), &TopView::setColor);
    connect(textureWidget, &TextureWidget::bottomDrawTypeChanged, _consoleWidget, &ConsoleWidget::onBottomDrawTypeChanged);

    connect(textureWidget->bottomColorSwatch("outer", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomview(), &BottomView::setDarkColor);
    connect(textureWidget->bottomColorSwatch("outer", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomview(), &BottomView::setMainColor);
    connect(textureWidget->bottomColorSwatch("inner", "main"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setMainColor);
    connect(textureWidget->bottomColorSwatch("inner", "shadow"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setShadowColor);
    connect(textureWidget->bottomColorSwatch("inner", "dark"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setIconDarkColor);
    connect(textureWidget->bottomColorSwatch("inner", "light"), &ColorSwatch::colorChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::setIconLightColor);

    connect(textureWidget, &TextureWidget::folderImageChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::onFolderTextureChanged);
    connect(textureWidget, &TextureWidget::fileImageChanged, _consoleWidget->bottomwidget()->gridwidget(), &GridWidget::onFileTextureChanged);
    connect(textureWidget, &TextureWidget::fileImageChanged, _consoleWidget->bottomwidget()->foldergrid(), &GridWidget::onFileTextureChanged);

    connect(this, &KameWidget::windowActive, textureWidget, &TextureWidget::onActive);
}

void KameWidget::connectAudioWidget(AudioWidget *audioWidget) {
    connect(_consoleWidget->bottomwidget()->gridwidget(), &GridWidget::playCursor, audioWidget->picker("cursor"), &AudioPicker::play);
    connect(_consoleWidget->bottomwidget()->foldergrid(), &GridWidget::playCursor, audioWidget->picker("cursor"), &AudioPicker::play);
    connect(_consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::playLaunch, audioWidget->picker("launch"), &AudioPicker::play);
    connect(_consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::playFolder, audioWidget->picker("folder"), &AudioPicker::play);
    connect(_consoleWidget->bottomwidget()->openclosewidget(), &OpenCloseWidget::playClose, audioWidget->picker("close"), &AudioPicker::play);
    connect(_consoleWidget->bottomwidget()->gridwidget(), &GridWidget::playFrame0, audioWidget->picker("frame0"), &AudioPicker::play);
    connect(_consoleWidget->bottomwidget()->gridwidget(), &GridWidget::playFrame1, audioWidget->picker("frame1"), &AudioPicker::play);
    connect(_consoleWidget->bottomwidget()->gridwidget(), &GridWidget::playFrame2, audioWidget->picker("frame2"), &AudioPicker::play);
    connect(_consoleWidget->bottomwidget()->foldergrid(), &GridWidget::playFrame0, audioWidget->picker("frame0"), &AudioPicker::play);
    connect(_consoleWidget->bottomwidget()->foldergrid(), &GridWidget::playFrame1, audioWidget->picker("frame1"), &AudioPicker::play);
    connect(_consoleWidget->bottomwidget()->foldergrid(), &GridWidget::playFrame2, audioWidget->picker("frame2"), &AudioPicker::play);
    connect(this, &KameWidget::windowActive, audioWidget, &AudioWidget::playResume);
    connect(this, &KameWidget::windowInactive, audioWidget, &AudioWidget::pauseAll);
}

void KameWidget::toggleWidgets(bool locked) {
    QFlags<QDockWidget::DockWidgetFeature> flags = locked ? QDockWidget::NoDockWidgetFeatures : QDockWidget::DockWidgetClosable|QDockWidget::DockWidgetMovable|QDockWidget::DockWidgetFloatable;
    _aboutDockWidget->setFeatures(flags);
    _audioDockWidget->setFeatures(flags);
    _settingsDockWidget->setFeatures(flags);
    _smdhDockWidget->setFeatures(flags);
    _colorDockWidget->setFeatures(flags);
    _textureDockWidget->setFeatures(flags);
}

ConsoleWidget *KameWidget::consolewidget() {
    return _consoleWidget;
}

void KameWidget::resizeEvent(QResizeEvent *) {
    setMinimumSize(0, 0);
    setMaximumSize(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX);
}

void KameWidget::closeEvent(QCloseEvent *event) {
    if (_consoleWidget != NULL && _consoleWidget->checkQuit()) {
        saveWindowState();
        event->accept();
    } else {
        event->ignore();
    }
}

void KameWidget::mouseMoveEvent(QMouseEvent *event) {
    if (event->buttons() & Qt::LeftButton) {
        move(event->globalPos() - _dragPosition);
        event->accept();
    }
}

void KameWidget::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton) {
        _dragPosition = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
}

void KameWidget::paintEvent(QPaintEvent *) {
    // Draw the dockable areas
    QPainter p(this);
    QPen pen;
    pen.setColor(Color::MAIN);
    QVector<qreal> dashes;
    qreal space = 2;
    dashes << 4 << space;
    pen.setDashPattern(dashes);
    p.setPen(pen);
    p.setBrush(QColor(Qt::transparent));
    QColor color = Color::BLACK;
    color.setAlpha(0xC0);
    p.fillRect(0, 0, width() - 1, height() - 1, color);
    p.drawRect(0, 0, width() - 1, height() - 1);

    dashes = QVector<qreal>();

    dashes << 1 << 3;
    pen.setDashPattern(dashes);
    color = Color::MAIN;
    color.setAlpha(0xF0);
    pen.setColor(color);
    p.setPen(pen);
    p.drawRect(80, 80, width() - 1 - 160, height() - 1 - 160);
}
