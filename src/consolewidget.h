// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CONSOLEWIDGET_H
#define CONSOLEWIDGET_H

#include <QWidget>
#include <QFrame>
#include <QPixmap>
#include <QSizeGrip>

#include "consolebutton.h"
#include "consolepad.h"
#include "consolejoystick.h"
#include "consolemenu.h"
#include "preview/topview.h"
#include "preview/bottomview.h"
#include "preview/bottomwidget.h"

class ConsoleWidget : public QFrame {
    Q_OBJECT

    public:
        explicit ConsoleWidget(QWidget *parent = 0);
        ~ConsoleWidget();

        ConsoleButton *smdhButton();
        ConsoleButton *colorsButton();
        ConsoleButton *texturesButton();
        ConsoleButton *audioButton();

        ConsoleMenu *consoleMenu();

        ConsolePad *pad();

        TopView *topview();
        BottomView *bottomview();
        BottomWidget *bottomwidget();

        void setAnimated(bool value);
        QPixmap capturePreview();
        bool checkQuit();

    public slots:
        void onTopDrawTypeChanged(QString path);
        void onBottomDrawTypeChanged(QString path);

        void updateSkin();
        void setSkinId(QString value);
        void onRestart();

    protected:
        void paintEvent(QPaintEvent *event);

    protected slots:
        void onPowerButton();
        void saveAnimationState();

    private:
        QWidget *_widget;
        TopView *_topview;
        BottomView *_bottomview;
        BottomWidget *_bottomwidget;
        ConsoleButton *_smdhbutton;
        ConsoleButton *_colorsbutton;
        ConsoleButton *_texturesbutton;
        ConsoleButton *_audiobutton;
        ConsoleJoystick *_animationbutton;
        ConsoleButton *_powerbutton;
        ConsolePad *_consolepad;
        ConsoleMenu *_consolemenu;
        QSizeGrip *_sizegrip;
        QPixmap _pixmap;
        QString _skinid;
};

#endif // CONSOLEWIDGET_H
