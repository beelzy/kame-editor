// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CONSOLEPAD_H
#define CONSOLEPAD_H

#include <QPushButton>
#include <QPixmap>
#include <QWidget>
#include <QFrame>
#include <QDockWidget>

class ConsolePad : public QWidget {
    Q_OBJECT
    public:
        explicit ConsolePad(QWidget *parent = 0);
        ~ConsolePad();

        void updateSkin();
        void setSkinId(QString value);

        QPushButton *up();
        QPushButton *down();
        QPushButton *left();
        QPushButton *right();

    public slots:
        void updateCheckedUp(bool isVisible);
        void updateCheckedDown(bool isVisible);
        void updateCheckedLeft(bool isVisible);
        void updateCheckedRight(bool isVisible);

    protected:
        void mouseMoveEvent(QMouseEvent *event);
        void paintEvent(QPaintEvent *event);
        bool updateChecked(bool isVisible, QDockWidget *dockwidget, QMainWindow *main);
        QIcon makeStripe(QRect rect, bool isVertical);

    private:
        QPixmap _base;
        QPixmap _baseup;
        QPixmap _basedown;
        QPixmap _baseleft;
        QPixmap _baseright;
        QFrame *_container;
        QPushButton *_up;
        QPushButton *_down;
        QPushButton *_left;
        QPushButton *_right;
        QString _skinid;
};

#endif // CONSOLEPAD_H
