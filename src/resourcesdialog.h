// This file is part of Kame Editor.
// Copyright (C) 2021 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef RESOURCESDIALOG_H
#define RESOURCESDIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QTableWidget>
#include <QPushButton>
#include <QMap>
#include <QSettings>

struct missingResource {
    QString title;
    QString type;
    QString id;
};

struct fileType {
    QString defaultExt;
    QString icon;
    QStringList filters;
};

class ResourcesDialog : public QDialog {
    Q_OBJECT

    public:
        ResourcesDialog(QWidget *parent = 0);
        ~ResourcesDialog();

        static QMap<QString, fileType> FILETYPES;

        void setList(QVector<missingResource>, QSettings *sourcefile);

    protected:
        QIcon colorIcon(QString path);

    protected slots:
        void cancel();
        void resolve();
        void selectFile();

    private:
        QTableWidget *_list;
        QVector<missingResource> _sources;
        QSettings *_sourcefile;
        QPushButton *_resolve;
        QPushButton *_cancel;
};

#endif // RESOURCESDIALOG_H
