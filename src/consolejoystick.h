// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CONSOLEJOYSTICK_H
#define CONSOLEJOYSTICK_H

#include <QAbstractButton>
#include <QPixmap>
#include <QPainterPath>

class ConsoleJoystick : public QAbstractButton {
    Q_OBJECT
    public:
        explicit ConsoleJoystick(QWidget *parent = 0);

        void setSkinId(QString value);
        void updateSkin();

    protected:
        void paintEvent(QPaintEvent *event);
        void mouseMoveEvent(QMouseEvent *event);

    private:
        QPixmap _base;
        QString _skinid;
};

#endif // CONSOLEJOYSTICK_H
