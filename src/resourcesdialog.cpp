// This file is part of Kame Editor.
// Copyright (C) 2021 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QHeaderView>

#include "resourcesdialog.h"
#include "dock/icons/folder.h"
#include "color.h"

QMap<QString, fileType> ResourcesDialog::FILETYPES{
    {"rc", {"rc", ":/resources/skins/icons/smdh.png", QStringList() << QT_TRANSLATE_NOOP("File Types", "Source Files (*.rc)")}},
    {"zip", {"zip", "", QStringList() << QT_TRANSLATE_NOOP("File Types", "Zip Archives (*.zip)")}},
    {"texture", {"", ":/resources/skins/icons/textures.png", QStringList() << QT_TRANSLATE_NOOP("File Types", "Image Files (*.png *.jpg *.jpeg *.gif *.bmp)")}},
        {"sfx", {"", ":/resources/skins/icons/audio.png", QStringList() << QT_TRANSLATE_NOOP("File Types", "Nintendo BCWAV Audio Files (*.bcwav *cwav)")}},
        {"bgm", {"", ":/resources/skins/icons/audio.png", QStringList() << QT_TRANSLATE_NOOP("File Types", "Nintendo BCSTM Audio Files (*.bcstm *cstm)")}}
};

ResourcesDialog::ResourcesDialog(QWidget *parent) : QDialog(parent) {
    setWindowTitle(tr("Missing Theme Resources", "title for missing theme resources dialog"));
    setMinimumWidth(800);
    QLabel * explanation = new QLabel();
    explanation->setText(tr("Kame Editor cannot find some resources used in this theme. Please correct the paths to the missing resources before continuing.", ""));
    explanation->setWordWrap(true);
    _list = new QTableWidget(0, 4, this);
    _list->setAlternatingRowColors(true);
    _list->setShowGrid(false);
    _list->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    _list->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    _list->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    _list->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    _list->verticalHeader()->hide();
    _list->setObjectName("readonly");
    _list->setIconSize(QSize(43, 43));
    QTableWidgetItem *resourceHeader = new QTableWidgetItem(tr("Resource", "Missing resource list header title"));
    QTableWidgetItem *oldpath = new QTableWidgetItem(tr("Old Path", "Missing resource list header title"));
    QTableWidgetItem *newpath = new QTableWidgetItem(tr("New Path", "Missing resource list header title"));
    QTableWidgetItem *select = new QTableWidgetItem(tr("Select...", "Missing resource list header title"));
    _list->setHorizontalHeaderItem(0, resourceHeader);
    _list->setHorizontalHeaderItem(1, oldpath);
    _list->setHorizontalHeaderItem(2, newpath);
    _list->setHorizontalHeaderItem(3, select);
    _list->setSelectionMode(QAbstractItemView::NoSelection);

    _resolve = new QPushButton(tr("Resolve", "resolve button"));
    _cancel = new QPushButton(tr("Cancel", "cancel button"));

    QHBoxLayout *controlsLayout = new QHBoxLayout();
    controlsLayout->addWidget(_cancel);
    controlsLayout->addWidget(_resolve);

    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(explanation);
    layout->addWidget(_list);
    layout->addLayout(controlsLayout);

    _sourcefile = nullptr;

    setLayout(layout);

    connect(_resolve, &QAbstractButton::clicked, this, &ResourcesDialog::resolve);
    connect(_cancel, &QAbstractButton::clicked, this, &ResourcesDialog::cancel);
}

ResourcesDialog::~ResourcesDialog() {
    delete _list;
    delete _resolve;
    delete _cancel;
    delete _sourcefile;
}

void ResourcesDialog::setList(QVector<missingResource> list, QSettings *sourcefile) {
    _sources = list;
    _sourcefile = sourcefile;
    QDir sourceDir = QFileInfo(sourcefile->fileName()).dir();
    Folder *folder = new Folder();
    QIcon folderIcon(folder->icon());
    for (int i = 0; i < list.size(); ++i) {
        _list->setRowCount(_list->rowCount() + 1);
        QTableWidgetItem *title = new QTableWidgetItem(list.at(i).title);
        title->setFlags(Qt::ItemIsEditable|Qt::ItemIsSelectable);
        title->setToolTip(title->text());
        title->setIcon(colorIcon(FILETYPES[list.at(i).type].icon));
        QString path = _sourcefile->value(list.at(i).id).toString();
        QTableWidgetItem *pathItem = new QTableWidgetItem(sourceDir.cleanPath(sourceDir.absoluteFilePath(path)));
        pathItem->setFlags(Qt::ItemIsEditable|Qt::ItemIsSelectable);
        pathItem->setToolTip(pathItem->text());
        _list->setItem(i, 0, title);
        _list->setItem(i, 1, pathItem);
        QPushButton *select = new QPushButton();
        select->setIcon(folderIcon);
        select->setIconSize(folder->icon().rect().size());
        select->setObjectName("select");
        select->setStyleSheet(folder->selectStyle());
        select->setProperty("row", QString::number(i));
        connect(select, &QAbstractButton::clicked, this, &ResourcesDialog::selectFile);
        _list->setCellWidget(i, 3, select);
    }
    delete folder;
}

void ResourcesDialog::cancel() {
    reject();
}

void ResourcesDialog::resolve() {
    QDir sourceDir = QFileInfo(_sourcefile->fileName()).dir();
    for (int i = 0; i < _list->rowCount(); i++) {
        QString newpath = _list->item(i, 2) == nullptr ? "" : _list->item(i, 2)->text();
        _sourcefile->setValue(_sources[i].id, newpath.isEmpty() ? "" : sourceDir.cleanPath(sourceDir.relativeFilePath(newpath)));
    }
    _sourcefile->sync();
    accept();
}

QIcon ResourcesDialog::colorIcon(QString path) {
    QPixmap source(path);
    QPixmap transformed = source;

    QPainter painter(&transformed);
    painter.setCompositionMode(QPainter::CompositionMode_SourceIn);
    painter.fillRect(transformed.rect(), Color::MAIN);
    painter.end();

    QIcon icon(transformed);
    icon.addPixmap(transformed, QIcon::Disabled);
    return icon;
}

void ResourcesDialog::selectFile() {
    QPushButton *select = qobject_cast<QPushButton *>(QObject::sender());
    int index = select->property("row").toInt();
    fileType type = FILETYPES[_sources[index].type];

    QFileDialog* filedialog = new QFileDialog();
    filedialog->setAcceptMode(QFileDialog::AcceptOpen);
    filedialog->setWindowTitle(tr("Load Missing Resource...", "Dialogue title when choosing to load a missing resource file."));
    if (!type.defaultExt.isEmpty()) {
        filedialog->setDefaultSuffix(type.defaultExt);
    }
    filedialog->setNameFilters(type.filters);
    if (filedialog->exec()) {
        QTableWidgetItem *path = new QTableWidgetItem(filedialog->selectedFiles().first());
        path->setFlags(Qt::ItemIsEditable|Qt::ItemIsSelectable);
        path->setToolTip(path->text());
        _list->setItem(index, 2, path);
        _resolve->setEnabled(true);
    }
}
