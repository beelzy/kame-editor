// This file is part of Kame Editor.
// Copyright (C) 2019 beelzy
// 
// Kame Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Kame Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Kame Editor.  If not, see <https://www.gnu.org/licenses/>.

#ifndef KAMEWIDGET_H
#define KAMEWIDGET_H

#include <QMainWindow>
#include <QPixmap>

#include "consolewidget.h"
#include "dock/audiowidget.h"
#include "dock/colorwidget.h"
#include "dock/smdhwidget.h"
#include "dock/texturewidget.h"
#include "dock/kamedockwidget.h"
#include "dock/aboutwidget.h"
#include "dock/settingswidget.h"

class KameWidget : public QMainWindow {
    Q_OBJECT
    public:
        explicit KameWidget(QWidget * parent);
        ~KameWidget();
        void createDockWidgets();

        virtual void paintEvent(QPaintEvent * e);

        void handleFocus(Qt::ApplicationState state);
        ConsoleWidget* consolewidget();

        void saveWindowState();

        bool isValid;

    signals:
        void windowActive();
        void windowInactive();

    protected:
        void mouseMoveEvent(QMouseEvent *event) override;
        void mousePressEvent(QMouseEvent *event) override;
        void resizeEvent(QResizeEvent *event) override;
        void closeEvent(QCloseEvent *event) override;
        void connectColorWidget(ColorWidget* colorwidget);
        void connectTextureWidget(TextureWidget* texturewidget);
        void connectAudioWidget(AudioWidget* audiowidget);

    protected slots:
        void toggleWidgets(bool toggled);

    private:
        ConsoleWidget *_consoleWidget;
        QPixmap _pixmap;
        QPoint _dragPosition;
        KameDockWidget *_aboutDockWidget;
        KameDockWidget *_settingsDockWidget;
        KameDockWidget *_smdhDockWidget;
        KameDockWidget *_colorDockWidget;
        KameDockWidget *_textureDockWidget;
        KameDockWidget *_audioDockWidget;

        AboutWidget *_aboutWidget;
        SettingsWidget *_settingsWidget;
        ColorWidget *_colorWidget;
        SMDHWidget *_smdhWidget;
        TextureWidget *_textureWidget;
        AudioWidget *_audioWidget;
};

#endif // KAMEWIDGET_H
