<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutWidget</name>
    <message>
        <source>Programming and graphics: %1
Made with Qt5.
Special Thanks:
%2</source>
        <comment>Credits contents</comment>
        <translation type="vanished">Programmierung und Grafiken: %1
Mit Qt5 gemacht.
Danksagung:
%2</translation>
    </message>
    <message>
        <source>Credits</source>
        <comment>Title for Credits accordion in about widget.</comment>
        <translation type="vanished">Mitwirkenden</translation>
    </message>
</context>
<context>
    <name>AccordionWidget</name>
    <message>
        <location filename="../src/dock/accordionwidget.cpp" line="43"/>
        <source>Quick load palette</source>
        <comment>tooltip for accordion quick load palette</comment>
        <translation>Palette schnellladen</translation>
    </message>
    <message>
        <location filename="../src/dock/accordionwidget.cpp" line="44"/>
        <source>Save colors to palette</source>
        <comment>tooltip for accordion save palette</comment>
        <translation>Farbe zum Palette speichern</translation>
    </message>
</context>
<context>
    <name>AudioPicker</name>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="568"/>
        <source>(No file selected)</source>
        <translation>(Keine Datei ausgewählt)</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="31"/>
        <source></source>
        <comment>Used to select audio files (BGM, SFX) in the Audio Dock Widget.</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="44"/>
        <source>(No file selected)</source>
        <comment>Displayed when no file in the audio picker is selected.</comment>
        <translation>(Keine Datei ausgewählt)</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="106"/>
        <source>Information</source>
        <comment>Title for the audio picker meta data accordion.</comment>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="150"/>
        <source>Start</source>
        <comment>Loop start text in the audio converter section.</comment>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="151"/>
        <source>End</source>
        <comment>Loop end text in the audio converter section.</comment>
        <translation>Ende</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="299"/>
        <source>vgmstream-cli cannot be found. You will still be able to manage your theme&apos;s audio, but Kame Editor cannot preview them.</source>
        <comment>This warning appears in a tooltip in a ! icon where the play button was if the program cannot find vgmstream-cli.</comment>
        <translation>vgmstream-cli nicht gefunden. Sie dürfen noch Ihre Themenaudio verwalten, aber Kame Editor kann sie nicht abspielen.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="354"/>
        <source>WARNING: Your selected .bcstm file is over 3.3MB in size. Please optimize your file before including it or else it will not be included during deploy.</source>
        <comment>This warning shows up in a tooltip in a ! icon near the title if users select a bgm file that&apos;s over 3.3MB.</comment>
        <translation>HINWEIS: Ihre ausgewählte bcstm Datei ist über 3.3MB zu groß. Bitte die Datei optimieren vor Sie es hinzufügen, sonst ist es während das Deploy verzichten.</translation>
    </message>
    <message>
        <source>Failed to load file</source>
        <comment>Title for the error message</comment>
        <translation type="vanished">Dateiladen Fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="517"/>
        <source>Not a valid WAV file. The file you have chosen is either corrupted or does not contain a WAV header.</source>
        <comment>This message appears when users select an audio file that isn&apos;t really a WAV.</comment>
        <translation>Nicht ein gültige Datei. Die Datei die Sie ausgewählt hat ist entweder kaputt oder hat kein WAV Header.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="517"/>
        <source>Incompatible bits per sample. The file you have chosen needs to have a bits per sample of 8 or 16.</source>
        <comment>This message appears when users select a WAV file that doesn&apos;t have 8 or 16 bits per sample.</comment>
        <translation>Inkompatibel Samplingtiefe. Die Datei die Sie ausgewählt haben muss einen Samplingtiefe von 8 oder 6 haben.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="519"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Nintendo BCWAV Audio Files (*.bcwav *cwav)</source>
        <translation type="vanished">Nintendo BCWAV Audiodatei (*.bcwav *cwav)</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="546"/>
        <source>SFX File Selection</source>
        <comment>Dialog title for selecting SFX files.</comment>
        <translation>SFXDatei Auswahl</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="549"/>
        <source>WAV Input File Selection</source>
        <comment>Dialog title for selecting WAV files for conversion.</comment>
        <translation>WAV Eingabedatei Auswahl</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="552"/>
        <source>BGM File Selection</source>
        <comment>Dialog title for selecting BGM files.</comment>
        <translation>BGMDatei Auswahl</translation>
    </message>
    <message>
        <source>WARNING: Your selected .bcwav file is over 1 second long. The file will still be included in the theme, but may not play correctly on 3DS hardware.</source>
        <comment>Warns users through a tooltip icon near the title if they try to select a WAV file as an SFX that&apos;s too long.</comment>
        <translation type="vanished">HINWEIS: Ihre ausgewählte bcwav Datei ist über 1 Sekunde zu lang. Die Datei wurde noch in das Theme hinzufügt, aber darf nicht richtig auf das 3DS Gerät abspielen.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="548"/>
        <source>WAV Audio Files (*.wav)</source>
        <translation>WAV Audiodatei (*.wav)</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="516"/>
        <source>Failed to load file</source>
        <comment>Title for WAV loading error message</comment>
        <translation>Dateiladen Fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/dock/audiopicker.cpp" line="516"/>
        <source>Wrong Bits per Sample</source>
        <comment>Title for WAV loading error message</comment>
        <translation>Falsche Samplingtiefe</translation>
    </message>
    <message>
        <source>Nintendo BCSTM Audio Files (*.bcstm *cstm)</source>
        <translation type="vanished">Nintendo BCSTM Audiodatei (*.bcstm *cstm)</translation>
    </message>
</context>
<context>
    <name>AudioWidget</name>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="124"/>
        <source>rstmcpp could not be found. Audio file conversion is not possible without it.</source>
        <comment>This message is in a tooltip in an icon in the convert button if the program can&apos;t find rstmcpp.</comment>
        <translation>rstmcpp nicht gefunden.Ohne ihn ist Audiodateikonvertierung ist leider nicht möglich.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="448"/>
        <source>Nintendo BCWAV Audio Files (*.bcwav *cwav)</source>
        <translation>Nintendo BCWAV Audiodatei (*.bcwav *cwav)</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="449"/>
        <source>Select BCSTM/BCWAV Destination</source>
        <comment>Title for the file dialog when selecting the destination path for audio conversion.</comment>
        <translation>BCSTM/BCWAV Zielort auswählen</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="482"/>
        <source>Converting...</source>
        <comment>Indicates in a message box that the audio is being converted.</comment>
        <translation>konvertieren...</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="501"/>
        <source>Audio Conversion Results</source>
        <comment>Title for the messagebox displaying the results.</comment>
        <translation>Audiokonvertierung Ergebnisse</translation>
    </message>
    <message>
        <source>%s file successfully converted.</source>
        <comment>Message to display when audio conversion succeeds.</comment>
        <translation type="vanished">%s Datei erfolgreich konvertiert.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="501"/>
        <source>%1 file successfully converted.</source>
        <comment>Message to display when audio conversion succeeds.</comment>
        <translation>%1 Datei erfolgreich konvertiert.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="503"/>
        <source>Audio conversion failed.</source>
        <comment>Messagebox gets displayed when audio conversion fails.</comment>
        <translation>Audiokonvertierung fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="561"/>
        <source>Source File</source>
        <comment>Title for the audio converter input file.</comment>
        <translation>Quelledatei</translation>
    </message>
    <message>
        <source>Source WAV file to convert. Loop values are in samples.</source>
        <comment>Tooltip that appears when hovering over the title icon for the coverter.</comment>
        <translation type="vanished">Quelle WAV Datei zu konvertieren. Loopwerte sind in Samples.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="565"/>
        <source>Destination:</source>
        <comment>Text indicating where the output selection for the audio converter apppears.</comment>
        <translation>Zielort:</translation>
    </message>
    <message>
        <source>(No file selected)</source>
        <comment>Tooltip that shows up when no destination has been selected for the audio converter.</comment>
        <translation type="vanished">(Keine Datei ausgewählt)</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="583"/>
        <source>Format:</source>
        <comment>Text in the audio converter that indicates where the options for changing the converter format are.</comment>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="588"/>
        <source>Assign to:</source>
        <comment>Text in the audio converter that indicates where the options for changing the audio assignment start.</comment>
        <translation>verwenden als:</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="591"/>
        <source>Do not assign</source>
        <comment>Initial option in the combobox for assigning the bgm after conversion.</comment>
        <translation>Nicht verwenden</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="594"/>
        <source>Do not assign</source>
        <comment>Initial option in the combobx for assigning the sfx after conversion.</comment>
        <translation>Nicht verwenden</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="599"/>
        <source>Convert</source>
        <comment>Button for starting audio conversion.</comment>
        <translation>Konvertieren</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="602"/>
        <source>Converter</source>
        <comment>Title for audio converter accordion.</comment>
        <translation>Konverter</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="602"/>
        <source>Converts .wav files to .bcstm or .bcwav.</source>
        <comment>Tooltip for audio converter accordion</comment>
        <translation>WAV Datei zum bcstm oder bcwav konvertieren.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="626"/>
        <source>Background music track</source>
        <comment>Tooltip describes what the bgm audiopicker is for.</comment>
        <translation>Hintergrundmusik Track</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="629"/>
        <source>Background Music</source>
        <comment>Title for BGM accordion</comment>
        <translation>Hintergrundmusik</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="629"/>
        <source>Looping music track that plays in the background. In .bcstm/.cstm format.</source>
        <comment>Tooltip that appears in the Background Music accordion.</comment>
        <translation>Musikschleife die im Hintergrund läuft. In .bcstm/.cstm Format.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="644"/>
        <source>SFX Limit</source>
        <comment>Title for SFX limit section</comment>
        <translation>SFX Begrenzung</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="661"/>
        <source>The total size of all the SFXes in this theme is over the 183KB limit. Your theme may not have sound effects on deploy.</source>
        <comment>Tooltip warning when users have a total SFX size &gt; 183KB.</comment>
        <translation>Die gesamtgroße von alle SFXdateien in dieser Theme ist über die 183KB Begrenzung. Ihre endgültige Theme darf keine Soundeffekte haben.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="677"/>
        <source>Triggering Sound Effects</source>
        <comment>Title for sfx detailed help</comment>
        <translation>Soundeffekte aktivieren</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="677"/>
        <source>Launch</source>
        <translation>Starten</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="679"/>
        <source>Close</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="680"/>
        <source>Folder (On real devices, the folder SFX can also be played while clicking on certain buttons, not just for creating folders.)</source>
        <translation>Verzeichnis (Auf echte Geräte, der Verzeichnis SFX darf auch durch klicken auf bestimmte Knöpfe abgespielt geworden, nicht nur für Verzeichnis erstellen.)</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="681"/>
        <source>Frame 0 - 2 (Plays while scrolling/dragging)</source>
        <translation>Rahmen 0 - 2 (Spielt durch scrollen/ziehen)</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="676"/>
        <source>SFX Help</source>
        <comment>Window title for SFX detailed help</comment>
        <translation>SFX Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="28"/>
        <source>(No file selected)</source>
        <translation>(Keine Datei ausgewählt)</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="562"/>
        <source>Source WAV file to convert. Loop values are in samples. Please note that WAV files must be in 8 or 16-bit encoding. For best results, use stereo channel WAVs. You can make the necessary conversions in programs like Audacity or ffmpeg.</source>
        <comment>Tooltip that appears when hovering over the title icon for the coverter.</comment>
        <translation>Quelle WAV Datei zu konvertieren. Loopwerte sind in Samples. Bitte achten Sie dass WAV Dateien muss als 8 oder 16-bit enkodiert sein. Für beste Ergebnisse, bitte WAV Dateien mit Stereokanälen benutzen. Sie können die nötige Veränderungen mit Programme wie Audacity oder ffmpeg machen.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="684"/>
        <source>Cursor</source>
        <comment>Cursor audio title</comment>
        <translation>Anzeiger</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="685"/>
        <source>Sound that plays when you move the cursor.</source>
        <comment>Tooltip for the cursor audio picker.</comment>
        <translation>Schall spielt ab als Sie den Anzeiger bewegt.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="691"/>
        <source>Launch</source>
        <comment>Launch audio title</comment>
        <translation>Starten</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="692"/>
        <source>Sound that plays when opening an application. Click for more details.</source>
        <comment>Tooltip for the launch audio picker.</comment>
        <translation>Schall die abspielt wenn Sie ein Programm starten. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="700"/>
        <source>Sound that plays when creating a folder. Click for more details.</source>
        <comment>Tooltip for the folder audio picker.</comment>
        <translation>Schall spielt ab wenn Sie ein neue Verzeichnis erstellen. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="708"/>
        <source>Sound that plays when closing an application. Click for more details.</source>
        <comment>Tooltip for the close sound audio picker.</comment>
        <translation>Schall spielt ab wenn Sie ein Programm beenden. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="716"/>
        <location filename="../src/dock/audiowidget.cpp" line="724"/>
        <location filename="../src/dock/audiowidget.cpp" line="732"/>
        <source>Sound that plays when scrolling between bottom screens. Applies to any bottom screen frame type. Click for more details.</source>
        <comment>Tooltip for page scrolling sound audio picker.</comment>
        <translation>Schall spielt ab wenn Sie zwischen untere Bildschirme scrollen. Gilt für alle Arten von unteres Bildschirm Rahmen. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="740"/>
        <source>Sound that plays when opening the 3DS. In this preview, it will play when Kame Editor receives focus.</source>
        <comment>Tooltip for open lid audio picker.</comment>
        <translation>Schall die abspielt als Sie das 3DS aufmacht. In diese Vorschau, spielt es ab als Kame Editor fokusiert geworden ist.</translation>
    </message>
    <message>
        <source>Sound that plays when opening an application.</source>
        <comment>Tooltip for the launch audio picker.</comment>
        <translation type="vanished">Schall die abspielt wenn Sie ein Programm starten.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="699"/>
        <source>Folder</source>
        <comment>Folder audio title</comment>
        <translation>Verzeichnis</translation>
    </message>
    <message>
        <source>Sound that plays when creating a folder.</source>
        <comment>Tooltip for the folder audio picker.</comment>
        <translation type="vanished">Schall spielt ab wenn Sie ein neue Verzeichnis erstellen.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="707"/>
        <source>Close</source>
        <comment>Close audio title</comment>
        <translation>Beenden</translation>
    </message>
    <message>
        <source>Sound that plays when closing an application.</source>
        <comment>Tooltip for the close sound audio picker.</comment>
        <translation type="vanished">Schall spielt ab wenn Sie ein Programm beenden.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="715"/>
        <source>Frame 0</source>
        <comment>Frame 0 audio title</comment>
        <translation>Rahmen 0</translation>
    </message>
    <message>
        <source>Sound that plays when scrolling between bottom screens. Applies to any bottom screen frame type.</source>
        <comment>Tooltip for page scrolling sound audio picker.</comment>
        <translation type="vanished">Schall spielt ab wenn Sie zwischen untere Bildschirme scrollen. Gilt für alle Arten von unteres Bildschirm Rahmen.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="723"/>
        <source>Frame 1</source>
        <comment>Frame 1 audio title</comment>
        <translation>Rahmen 1</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="731"/>
        <source>Frame 2</source>
        <comment>Frame 2 audio title</comment>
        <translation>Rahmen 2</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="739"/>
        <source>Open Lid</source>
        <comment>Open lid audio title</comment>
        <translation>Aufmachen</translation>
    </message>
    <message>
        <source>Sound that plays when opening the 3DS.</source>
        <comment>Tooltip for open lid audio picker.</comment>
        <translation type="vanished">Schall die abspielt als Sie das 3DS aufmacht.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="745"/>
        <source>Sound Effects</source>
        <comment>Title for SFX accordion.</comment>
        <translation>Soundeffekte</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="745"/>
        <source>Sound effects for various theme actions. In .bcwav/.cwav format.</source>
        <comment>Tooltip for SFX accordion.</comment>
        <translation>Soundeffekte für verschiedene Themenaktionen. In .bcwav/.cwav Format.</translation>
    </message>
    <message>
        <location filename="../src/dock/audiowidget.cpp" line="555"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>ColorDialog</name>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.ui" line="14"/>
        <source>Select Color</source>
        <translation>Farbe auswählen</translation>
    </message>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.ui" line="30"/>
        <source>Recently Used Colors</source>
        <translation>Farbeverwendungverlauf</translation>
    </message>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.ui" line="116"/>
        <source>Saturation</source>
        <translation>Sättigung</translation>
    </message>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.ui" line="123"/>
        <source>Hue</source>
        <translation>Farbwert</translation>
    </message>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.ui" line="140"/>
        <source>Hex</source>
        <translation>Hex</translation>
    </message>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.ui" line="147"/>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.ui" line="184"/>
        <source>Value</source>
        <translation>Hellwert</translation>
    </message>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.ui" line="191"/>
        <source>Green</source>
        <translation>Grün</translation>
    </message>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.ui" line="198"/>
        <source>Alpha</source>
        <translation>Alpha</translation>
    </message>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.ui" line="205"/>
        <source>Red</source>
        <translation>Rot</translation>
    </message>
</context>
<context>
    <name>ColorWidget</name>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="909"/>
        <source>Dark</source>
        <comment>Cursor dark color title</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="912"/>
        <source>Main</source>
        <comment>Cursor main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="915"/>
        <source>Light</source>
        <comment>Cursor light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="918"/>
        <source>Glow</source>
        <comment>Cursor glow color title</comment>
        <translation>Beleuchten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="28"/>
        <source>Cursor</source>
        <comment>Title for cursor color accordion.</comment>
        <translation>Anzeiger</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="921"/>
        <source>Colors for the bottom screen cursor.</source>
        <comment>Tooltip for cursor color accordion.</comment>
        <translation>Farben für untere Bildschirm Anzeiger.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="951"/>
        <source>Dark</source>
        <comment>3D folder dark color title</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="954"/>
        <source>Main</source>
        <comment>3D folder main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="957"/>
        <source>Light</source>
        <comment>3D folder light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="960"/>
        <source>Shadow</source>
        <comment>3D folder shadow color title</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="29"/>
        <source>3D Folder</source>
        <comment>Title for 3D Folder accordion.</comment>
        <translation>3D Verzeichnis</translation>
    </message>
    <message>
        <source>Colors for the 3D folder in the top screen.</source>
        <comment>Tooltip for 3D folder accordion.</comment>
        <translation type="vanished">Farbe für das 3D Verzeichnis in obere Bildschirm.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="999"/>
        <source>Dark</source>
        <comment>File dark color title</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1002"/>
        <source>Main</source>
        <comment>File main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1005"/>
        <source>Light</source>
        <comment>File light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1008"/>
        <source>Shadow</source>
        <comment>File shadow color title</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="30"/>
        <source>File Frames</source>
        <comment>Title for file frames accordion.</comment>
        <translation>Dateirahmen</translation>
    </message>
    <message>
        <source>Colors for the frame around the file icons in the bottom screen.</source>
        <comment>Tooltip for file frames accordion.</comment>
        <translation type="vanished">Farbe für den Rahmen um die Dateiicons in untere Bildschirm.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="964"/>
        <source>Colors for the 3D folder in the top screen. Click for more details.</source>
        <comment>Tooltip for 3D folder accordion.</comment>
        <translation>Farben für das 3D Verzeichnis in obere Bildschirm. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="973"/>
        <source>3D Folder Colors</source>
        <comment>Title for 3D folder color detailed help</comment>
        <extracomment>3D folder color detailed help</extracomment>
        <translation>3D Verzeichnisfarben</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="976"/>
        <source>3D Folder Colors Help</source>
        <comment>Window title for 3D folder colors detailed help</comment>
        <translation>3D Verzeichnisfarben Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1012"/>
        <source>Colors for the frame around the file icons in the bottom screen. Click for more details.</source>
        <comment>Tooltip for file frames accordion.</comment>
        <translation>Farben für den Rahmen um die Dateiicons in untere Bildschirm. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1021"/>
        <source>File Frames Colors</source>
        <comment>Title for file frames color detailed help</comment>
        <extracomment>File frames color detailed help</extracomment>
        <translation>Dateirahmenfarben</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="974"/>
        <location filename="../src/dock/colorwidget.cpp" line="1021"/>
        <source>Top Screen Preview</source>
        <translation>Obere Bildschirm Vorschau</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="975"/>
        <source>Default Folder Colors (Only visible when folder textures are not used)</source>
        <translation>Standard Verzeichnisfarben (Nur sichtbar wenn Verzeichnistexturen unbenutzt sind)</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="976"/>
        <source>Opened Folder Colors (Only visible when folder textures are not used)</source>
        <translation>Geöffnete Verzeichnisfarben (Nur sichtbar wenn Verzeichnistexturen unbenutzt sind)</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1023"/>
        <source>Folder File Colors (Default and Opened)</source>
        <translation>Dateirahmenfarben (Standard und Geöffnet)</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1024"/>
        <source>File Frames Colors Help</source>
        <comment>Window title for file frames colors detailed help</comment>
        <translation>Dateirahmenfarben Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1024"/>
        <source>File Frame Colors (Only visible on zoom levels where the corresponding large or small file textures are not used)</source>
        <translation>Dateirahmenfarben (Nur sichtbar auf Dateiskalierungsstufe worin Groß oder Klein Dateitexturen unbenutzt sind)</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1047"/>
        <source>Dark</source>
        <comment>Arrow button base dark color title</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1050"/>
        <source>Main</source>
        <comment>Arrow button base main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1053"/>
        <source>Light</source>
        <comment>Arrow button base light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1056"/>
        <source>Shadow</source>
        <comment>Arrow button base shadow color title</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="31"/>
        <source>Arrow Buttons (Base)</source>
        <comment>Title for Arrow Buttons (Base) accordion.</comment>
        <translation>Pfeilknöpfe (Hintergrund)</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1061"/>
        <source>Background colors for the arrow buttons in the bottom screen.</source>
        <comment>Tooltip for Arrow Buttons (Base) accordion.</comment>
        <translation>Hintergrundfarben für die Pfeilknöpfe in untere Bildschirm.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1091"/>
        <source>Border</source>
        <comment>Border color title</comment>
        <translation>Rand</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1094"/>
        <source>Unpressed</source>
        <comment>Unpressed color title</comment>
        <translation>Nicht gedrückt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1097"/>
        <source>Pressed</source>
        <comment>Pressed color title</comment>
        <translation>Gedrückt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="32"/>
        <source>Arrow Buttons (Arrow)</source>
        <comment>Title for Arrow Buttons (Arrow) accordion.</comment>
        <translation>Pfeilknöpfe (Pfeil)</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="415"/>
        <source>Failed to load palette</source>
        <comment>Title for loading palette error message</comment>
        <translation>Paletteladen fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="416"/>
        <source>Not a valid palette. The file you have chosen is either corrupted or does not contain valid palette data.</source>
        <translation>Nicht ein gültige Palette. Die Palette die Sie ausgewählt hat ist entweder kaputt oder hat keine gültige Palettedaten.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="418"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="438"/>
        <location filename="../src/dock/colorwidget.cpp" line="870"/>
        <source>(No file selected)</source>
        <comment>Palette file path text.</comment>
        <translation>(Keine Datei ausgewählt)</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="458"/>
        <source>(Not applicable for this palette)</source>
        <comment>Combo box warning that the selected palette does not apply to any color sets.</comment>
        <translation>(Nicht verwendbare Palette)</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="410"/>
        <source>Load Palette File...</source>
        <comment>Dialog title when choosing to load a color palette.</comment>
        <translation>Palettedatei laden...</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="505"/>
        <source>Save Palette...</source>
        <comment>Dialog title when saving palette colors.</comment>
        <translation>Palette Speichern...</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="869"/>
        <source>Palette Files (*.pal)</source>
        <translation>Palettedatei (*.pal)</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="885"/>
        <source>Assign to:</source>
        <comment>Assign to palette text.</comment>
        <translation>zuweisen:</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="892"/>
        <source>Apply</source>
        <comment>palette accordion apply button text</comment>
        <translation>anwenden</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="895"/>
        <source>Palette Management</source>
        <comment>Title for palette accordion.</comment>
        <translation>Palette Verwaltung</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="895"/>
        <source>Color palette management.</source>
        <comment>Tooltip for palette accordion.</comment>
        <translation>Farbpalette Verwaltung.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1100"/>
        <source>Colors for the icon on the arrow buttons in the bottom screen.</source>
        <comment>Tooltip for Arrow Buttons (Arrow) accordion.</comment>
        <translation>Farben für die Icons auf der Pfeilknöpfe in untere Bildschirm.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1127"/>
        <source>Open Button</source>
        <comment>Title for Open button.</comment>
        <translation>Öffnenknopf</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1131"/>
        <source>Text Shadow Position</source>
        <comment>Text shadow position title for open button.</comment>
        <translation>Textschatten Position</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1137"/>
        <source>Dark</source>
        <comment>Open button dark color title.</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1140"/>
        <source>Main</source>
        <comment>Open button main color title.</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1143"/>
        <source>Light</source>
        <comment>Open button light color title.</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1146"/>
        <source>Shadow</source>
        <comment>Open button shadow color title.</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1151"/>
        <source>Glow</source>
        <comment>Open button glow color title.</comment>
        <translation>Beleuchten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1155"/>
        <source>Text Shadow</source>
        <comment>Open button text shadow color title.</comment>
        <translation>Textschatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1158"/>
        <source>Text Main</source>
        <comment>Open button text main color title.</comment>
        <translation>Haupttext</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1161"/>
        <source>Text Selected</source>
        <comment>Open button text selected color title.</comment>
        <translation>Ausgewählte Text</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1164"/>
        <source>Close Button</source>
        <comment>Title for Close Button</comment>
        <translation>Schließenknopf</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1168"/>
        <source>Text Shadow Position</source>
        <comment>Text shadow position title for close button.</comment>
        <translation>Textschatten Position</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1174"/>
        <source>Dark</source>
        <comment>Close button dark color title</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1177"/>
        <source>Main</source>
        <comment>Close button main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1180"/>
        <source>Light</source>
        <comment>Close button light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1183"/>
        <source>Shadow</source>
        <comment>Close button shadow color title</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1188"/>
        <source>Glow</source>
        <comment>Close button glow color title</comment>
        <translation>Beleuchten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1192"/>
        <source>Text Shadow</source>
        <comment>Close button text shadow color title</comment>
        <translation>Textschatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1195"/>
        <source>Text Main</source>
        <comment>Close button text main color title</comment>
        <translation>Haupttext</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1198"/>
        <source>Text Selected</source>
        <comment>Close button text selected color title</comment>
        <translation>Ausgewählte Text</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="33"/>
        <source>Open/Close Buttons</source>
        <comment>Title for Open/Close Buttons accordion</comment>
        <translation>Öffnen/Schließenknöpfe</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1201"/>
        <source>Colors for the bottom buttons in the bottom screen.</source>
        <comment>Tooltip for Open/Close Buttons accordion</comment>
        <translation>Farben für untere Knöpfe in untere Bildschirm.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1283"/>
        <source>Main</source>
        <comment>Game text main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1286"/>
        <source>Light</source>
        <comment>Game text light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1290"/>
        <source>Shadow</source>
        <comment>Game text shadow color title</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1295"/>
        <source>Text</source>
        <comment>Game text text color title</comment>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="34"/>
        <source>Game Text</source>
        <comment>Title for game text accordion</comment>
        <translation>Spieltext</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1298"/>
        <source>Colors for the tooltip that appears above bottom screen icons on the highest zoom level. Click for more details.</source>
        <comment>Tooltip for game text accordion</comment>
        <translation>Farben für den Tooltip der über den untere Bildschirmicons auf höchste Dateiskalierung erscheint. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1307"/>
        <source>Game Text Colors</source>
        <comment>Title for game text color detailed help</comment>
        <extracomment>Game text colors detailed help</extracomment>
        <translation>Spieltextfarben</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1308"/>
        <source>Game Text</source>
        <translation>Spieltext</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1356"/>
        <location filename="../src/dock/colorwidget.cpp" line="1427"/>
        <source>Select any folder.</source>
        <translation>Ein Verzeichnis auswählen.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1486"/>
        <source>Top Corner Buttons</source>
        <translation>Oberer Eckeknopf</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1589"/>
        <source>Demo Text</source>
        <translation>Demotext</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1590"/>
        <source>Only appears when certain items are selected. In this preview, any file icon will have a demo text, but not every app on a real device has a demo text.</source>
        <translation>Nur gezeigt wenn bestimmte Gegenstände ausgewählt sind. In diese Vorschau, alle Dateiicons haben ein Demotext, aber nicht jedes Programm auf ein echtes Gerät hat ein Demotext.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1309"/>
        <source>Game Text Colors Help</source>
        <comment>Window title for game text colors detailed help</comment>
        <translation>Spieltextfarben Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1309"/>
        <source>Only visible on the largest zoom level. Click to change it.</source>
        <translation>Nur sichtbar auf das höchste Dateiskalierungstufe. Klicken Sie darauf um es zu verändern.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1347"/>
        <source>Colors for the bottom screen when you open a folder. Click for more details.</source>
        <comment>Tooltip for folder view accordion.</comment>
        <translation>Farben für untere Bildschirm als Sie ein Verzeichnis öffnen. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1356"/>
        <location filename="../src/dock/colorwidget.cpp" line="1427"/>
        <source>Displaying Folder View and Back Button Colors</source>
        <comment>Title for folder view and back button colors detailed help</comment>
        <extracomment>Folder view and back button colors detailed help</extracomment>
        <translation>Verzeichnisansicht und Zurückknopf Farben anzeigen</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1357"/>
        <location filename="../src/dock/colorwidget.cpp" line="1428"/>
        <source>Folder View and Back Button Colors Help</source>
        <comment>Window title for folder view and back button colors detailed help</comment>
        <translation>Verzeichnisansicht und Zurückknopf Farben Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1357"/>
        <location filename="../src/dock/colorwidget.cpp" line="1428"/>
        <source>Click on the open button.</source>
        <translation>Klicken Sie auf den Startknopf.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1414"/>
        <source>Colors for the folder view back button. Click for more details.</source>
        <comment>Tooltip for folder back button accordion.</comment>
        <translation>Farben für den Verzeichnisansicht Zurückknopf. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1476"/>
        <source>Colors for the corner buttons in the top screen. Click for more details.</source>
        <comment>Tooltip for top corner buttons accordion</comment>
        <translation>Farben für Eckeknöpfe in obere Bildschirm. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1485"/>
        <source>Top Corner Button Colors</source>
        <comment>Title for top corner button colors detailed help</comment>
        <extracomment>Top corner button colors detailed help</extracomment>
        <translation>Oberer Eckeknopffarben</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1487"/>
        <source>Top Corner Button Colors Help</source>
        <comment>Window title for top corner button colors detailed help</comment>
        <translation>Oberer Eckeknopffarben Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1487"/>
        <source>In this preview, select any folder or empty slot to see them.</source>
        <translation>In diese Vorschau, wählen Sie ein Verzeichnis oder Leericon aus um ihm zu sehen.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1581"/>
        <source>Colors for the text that appears at the bottom of the top screen when certain apps in the bottom screen are selected. Click for more details.</source>
        <comment>Tooltip for demo text accordion.</comment>
        <translation>Farben für den Text der erscheint auf der untere Seite von der obere Bildschirm wenn manche Programme in untere Bildschirm ausgewählt sind. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1588"/>
        <source>Demo Text Colors</source>
        <comment>Title for demo text color detailed help</comment>
        <extracomment>Demo text colors detailed help</extracomment>
        <translation>Demotextfarben</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1590"/>
        <source>Demo Text Colors Help</source>
        <comment>Window title for demo text colors detailed help</comment>
        <translation>Demotextfarben Hilfe</translation>
    </message>
    <message>
        <source>Colors for the tooltip that appears above bottom screen icons on the highest zoom level.</source>
        <comment>Tooltip for game text accordion</comment>
        <translation type="vanished">Farbe für den Tooltip der über den untere Bildschirmicons auf höchste Dateiskalierung erscheint.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1334"/>
        <source>Dark</source>
        <comment>Folder view dark color title</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1337"/>
        <source>Main</source>
        <comment>Folder view main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1340"/>
        <source>Light</source>
        <comment>Folder view light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1343"/>
        <source>Shadow</source>
        <comment>Folder view shadow color title</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="35"/>
        <source>Folder View</source>
        <comment>Title for folder view accordion.</comment>
        <translation>Verzeichnisansicht</translation>
    </message>
    <message>
        <source>Colors for the bottom screen when you open a folder.</source>
        <comment>Tooltip for folder view accordion.</comment>
        <translation type="vanished">Farbe für untere Bildschirm als Sie ein Verzeichnis öffnen.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1381"/>
        <source>Text Shadow Position</source>
        <comment>Text shadow position for folder back button.</comment>
        <translation>Textschatten Position</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1387"/>
        <source>Dark</source>
        <comment>Folder back button dark color title</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1390"/>
        <source>Main</source>
        <comment>Folder back button main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1393"/>
        <source>Light</source>
        <comment>Folder back button light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1396"/>
        <source>Shadow</source>
        <comment>Folder back button shadow color title</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1401"/>
        <source>Glow</source>
        <comment>Folder back button glow color title</comment>
        <translation>Beleuchten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1405"/>
        <source>Text Shadow</source>
        <comment>Folder back button text shadow color title</comment>
        <translation>Textschatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1408"/>
        <source>Text Main</source>
        <comment>Folder back button text main color title</comment>
        <translation>Haupttext</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1411"/>
        <source>Text Selected</source>
        <comment>Folder back button text selected color title</comment>
        <translation>Ausgewählte Text</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="36"/>
        <source>Folder Back Button</source>
        <comment>Title for folder back button accordion.</comment>
        <translation>Verzeichnis Zurückknopf</translation>
    </message>
    <message>
        <source>Colors for the folder view back button.</source>
        <comment>Tooltip for folder back button accordion.</comment>
        <translation type="vanished">Farbe für den Verzeichnisansicht Zurückknopf.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1463"/>
        <source>Main</source>
        <comment>Top corner button main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1466"/>
        <source>Light</source>
        <comment>Top corner button light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1470"/>
        <source>Shadow</source>
        <comment>Top corner button shadow color title</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1473"/>
        <source>Text</source>
        <comment>Top corner button text color title</comment>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="37"/>
        <source>Top Corner Buttons</source>
        <comment>Title for top corner buttons accordion</comment>
        <translation>Oberer Eckeknopf</translation>
    </message>
    <message>
        <source>Colors for the corner buttons in the top screen.</source>
        <comment>Tooltip for top corner buttons accordion</comment>
        <translation type="vanished">Farbe für Eckeknöpfe in obere Bildschirm.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1511"/>
        <source>Dark</source>
        <comment>Bottom corner button dark color title</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1514"/>
        <source>Main</source>
        <comment>Bottom corner button main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1517"/>
        <source>Light</source>
        <comment>Bottom corner button light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1520"/>
        <source>Shadow</source>
        <comment>Bottom corner button shadow color title</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1524"/>
        <source>Icon Main</source>
        <comment>Bottom corner button icon main color title</comment>
        <translation>Icon haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1527"/>
        <source>Icon Light</source>
        <comment>Bottom corner button icon light color title</comment>
        <translation>Icon hell</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1530"/>
        <source>Icon Text Main</source>
        <comment>Bottom corner button icon text main color title</comment>
        <translation>Icontext haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="38"/>
        <source>Bottom Corner Buttons</source>
        <comment>Title for bottom corner buttons accordion.</comment>
        <translation>Unterer Eckeknöpfe</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1534"/>
        <source>Colors for the corner buttons on the bottom screen.</source>
        <comment>Tooltip for bottom corner buttons accordion.</comment>
        <translation>Farbe für die Eckeknöpfe auf der untere Bildschirm.</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1575"/>
        <source>Main</source>
        <comment>Demo text main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="1578"/>
        <source>Text</source>
        <comment>Demo text text color title</comment>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="../src/dock/colorwidget.cpp" line="39"/>
        <source>Demo Text</source>
        <comment>Title for demo text accordion.</comment>
        <translation>Demotext</translation>
    </message>
    <message>
        <source>Colors for the text that appears at the bottom of the top screen when certain apps in the bottom screen are selected.</source>
        <comment>Tooltip for demo text accordion.</comment>
        <translation type="vanished">Farbe für den Text der erscheint auf der untere Seite von der obere Bildschirm wenn manche Programme in untere Bildschirm ausgewählt sind.</translation>
    </message>
</context>
<context>
    <name>ConsoleMenu</name>
    <message>
        <source>Load Theme Archive (Not Implemented)</source>
        <translation type="vanished">Themenarchiv laden (Nicht entwickelt)</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="389"/>
        <source>Deploy</source>
        <translation>Deployen</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="31"/>
        <source></source>
        <comment>The three buttons at the bottom of the console. They change when you click on the middle source button. There are three modes: Source 1 - contains options for creating a new theme or loading a theme source file Source 2 - contains options for saving theme source files Deploy - contains options for deploying theme files</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="299"/>
        <location filename="../src/consolemenu.cpp" line="432"/>
        <location filename="../src/consolemenu.cpp" line="502"/>
        <location filename="../src/consolemenu.cpp" line="541"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Deploy Theme Archive</source>
        <translation type="vanished">Themenarchiv deployen</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="67"/>
        <source>Theme has unsaved changes</source>
        <comment>Title for the dialog</comment>
        <translation>Das Theme hat ungespeicherte Veränderungen</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="207"/>
        <source>Create New Theme</source>
        <comment>Display a tooltip when hovering over the New button.</comment>
        <translation>Neue Theme erstellen</translation>
    </message>
    <message>
        <source>Source</source>
        <comment>Display a tooltip when hovering over the Source button.</comment>
        <translation type="vanished">Quelle</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="209"/>
        <source>Load Theme Source</source>
        <comment>Display a tooltip when hovering over the Load button.</comment>
        <translation>Theme Quelle laden</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="68"/>
        <source>The current theme has unsaved changes. Would you like to save them before loading or starting a new one?</source>
        <comment>Asks the user whether or not they want to save the current theme before creating a new theme.</comment>
        <translation>Das jetzige Theme hat ungespeicherte Veränderungen. Möchten Sie sie speichern vor Sie neue Theme erstellen oder laden?</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="321"/>
        <source>Save Source File</source>
        <comment>Display a tooltip when hovering over the save button</comment>
        <translation>Quelledatei speichern</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="322"/>
        <source>Save Source File As...</source>
        <comment>Display a tooltip when hovering over the save as button</comment>
        <translation>Quelledatei speichern als...</translation>
    </message>
    <message>
        <source>Load Theme Archive (Not Implemented)</source>
        <comment>Display a tooltip when hovering over the load button (deploy)</comment>
        <translation type="vanished">Themenarchiv laden (Nicht entwickelt)</translation>
    </message>
    <message>
        <source>Deploy</source>
        <comment>Display a tooltip when hovering over the middle button for toggling sections</comment>
        <translation type="vanished">Deployen</translation>
    </message>
    <message>
        <source>The current theme has unsaved changes. Would you like to save them before loading or starting a new one?</source>
        <comment>Asks the user whether or not they want to save the current theme before loading a theme source file.</comment>
        <translation type="vanished">Das jetzige Theme hat ungespeicherte Veränderungen. Möchten Sie sie speichern vor Sie neue Theme erstellen oder laden?</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="288"/>
        <location filename="../src/consolemenu.cpp" line="421"/>
        <source>Kame Editor</source>
        <comment>Titlebar for deploying progress dialog.</comment>
        <translation>Kame Editor</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="422"/>
        <source>Deploying...</source>
        <comment>Click on the deploy button. Indicates the theme is being deployed in a progress dialog.</comment>
        <translation>deployen...</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="431"/>
        <source>Theme Required Fields Missing</source>
        <comment>Displays a warning message when trying to click on the deploy button and some required fields are missing.</comment>
        <translation>Theme erförderliche Eingabe fehlt</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="298"/>
        <location filename="../src/consolemenu.cpp" line="431"/>
        <source>kame-tools Missing</source>
        <comment>In the same warning message as the previous item. Displayed if kame-tools is not found in the PATH variable.</comment>
        <translation>kame-tools fehlt</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="208"/>
        <source>Source Menu: Manage source files used by Kame Editor. - Click here to toggle more menus.</source>
        <comment>Display a tooltip when hovering over the Source button.</comment>
        <translation>Quellemenü: Quelledateien von Kame Editor verwalten - Hier klicken um mehr Menüs zu sehen.</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="268"/>
        <source>Import Theme from Folder...</source>
        <comment>Dialog when choosing theme folder for import.</comment>
        <translation>Theme von Verzeichnis importieren...</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="289"/>
        <source>Importing...</source>
        <comment>Click on the load (import) button. Indicates the theme is being imported in a progress dialog.</comment>
        <translation>Importieren...</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="298"/>
        <source>kame-tools Version</source>
        <comment>Displays a warning message when trying to click on the load button and kame-tools is &lt; v1.3.1.</comment>
        <translation>kame-tools Version</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="335"/>
        <source>Deploy Menu: Manage theme files used on an actual 3DS. - Click here to toggle more menus.</source>
        <comment>Display a tooltip when hovering over the middle button for toggling sections</comment>
        <translation>Deploymenu: Themendateien wie auf ein 3DS Gerät verwalten - Hier klicken um mehr Menüs zu sehen.</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="380"/>
        <source>Deploy Theme Folder...</source>
        <comment>Dialogue title when choosing to deploy a theme to a folder.</comment>
        <translation>Theme Verzeichnis deployen...</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="388"/>
        <source>Theme Files Already Exist</source>
        <comment>Title for message box warning users deploy folder already contains theme files</comment>
        <translation>Theme Dateien sind bereits vorhanden</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="388"/>
        <source>The folder you have chosen for theme deploy already contains theme files. These files will be overwritten if you deploy your current theme. Continue with theme deploy anyways?</source>
        <translation>Das ausgewählte Verzeichnis enthalt Theme Dateien, die bereits vorhanden sind. Deployen Sie Ihre jetzige Theme, wird diese Dateien übergeschrieben geworden. Mit Theme Deploy trotzdem weitermachen?</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="390"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="448"/>
        <source>Theme has an autosave file</source>
        <comment>Title for the autosave detected dialog</comment>
        <translation>Das Theme hat ein Autospeicherndatei</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="449"/>
        <source>The current theme has an autosave file. Would you like to load it? If you load it, the original file will still be intact, but the autosave file will disappear if you close it without saving it. The autosave file will be deleted if you choose not to open it.</source>
        <comment>Asks the user whether or not they want to load the autosaved theme.</comment>
        <translation>Das jetzige Theme hat ein Autospeicherndatei. Möchten Sie es laden? Laden Sie das Autospeicherndatei, wird das ursprüngliche Datei noch verfügbar sein. Schließen Sie das Autospeicherndatei ohne zu speichern, wird es entfernt geworden. Das Autospeicherndatei wird gelöscht geworden, falls Sie öffnet es nicht.</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="471"/>
        <source>Theme Import Results</source>
        <comment>Title of the message that appears after a theme finished importing</comment>
        <translation>Themenarchiv importieren Ergebnisse</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="471"/>
        <source>Theme imported.</source>
        <comment>Message that appears when a theme finishes importing</comment>
        <translation>Themenarchiv erfolgreich importiert.</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="508"/>
        <source>Theme Deploy Results</source>
        <comment>Title of the message that appears after a theme finished deploying</comment>
        <translation>Theme deployen Ergebnisse</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="508"/>
        <source>Theme deployed.</source>
        <comment>Message that appears when a theme finishes deploying</comment>
        <translation>Themedeployen ist beendet.</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="591"/>
        <source>kame-tools v%1.%2.%3 currently installed. Importing theme archives requires kame-tools v%4 or later. Please upgrade kame-tools to use this functionality.</source>
        <comment>Warn users about the required version of kame-tools for importing theme archives.</comment>
        <translation>kame-tools v%1.%2.%3 ist gerade installiert. kame-tools v%4 oder höher ist erförderlich um Themenarchive zu importieren. Bitte kame-tools aktualisieren um die benötige Funktionalität zu verwenden.</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="595"/>
        <source>kame-tools v%1.%2.%3 currently installed. Subsequent importing and deploying of themes will cause lossy behavior in texture images. Please upgrade kame-tools to at least v%4 to prevent this from happening.</source>
        <comment>Warn users about newer version of kame-tools which prevents images from being saved in a lossy manner when using deploy options.</comment>
        <translation>kame-tools v%1.%2.%3 ist gerade installiert. Mehrmals Themen importieren und deployen kann zum schlechtere Bildqualität führen. Bitte kame-tools zum mindestens v%4 aktualisieren um dieses Problem aufzuheben.</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="600"/>
        <source>kame-tools could not be found. You can still save theme source files, but you won&apos;t be able to deploy them and use them on actual 3DS hardware.</source>
        <comment>Warn users when kame-tools is not available.</comment>
        <translation>kame-tools nicht gefunden. Sie dürfen noch Themenquelledateien speichern, aber deployen und auf ein echtes 3DS Gerät benutzen können Sie nicht.</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="601"/>
        <source>kame-tools could not be found. You can still load theme source files, but you won&apos;t be able to import theme archives.</source>
        <comment>Warn users when kame-tools is not available.</comment>
        <translation>kame-tools nicht gefunden. Sie dürfen noch Themenquelledateien laden, aber Themenarchive importieren können Sie nicht.</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="636"/>
        <source>Theme Icon</source>
        <comment>Title for missing theme icon resource</comment>
        <translation>Themenicon</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="637"/>
        <source>Small Theme Icon</source>
        <comment>Title for missing small theme icon resource</comment>
        <translation>Kleines Themenicon</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="642"/>
        <source>Top Screen Texture</source>
        <comment>Title for missing top screen texture</comment>
        <translation>Obere Bildschirm Textur</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="643"/>
        <source>Bottom Screen Texture</source>
        <comment>Title for missing bottom screen texture</comment>
        <translation>Untere Bildschirm Textur</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="651"/>
        <source>Closed Folder Texture</source>
        <comment>Title for missing closed folder texture</comment>
        <translation>Standardverzeichnis Textur</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="652"/>
        <source>Opened Folder Texture</source>
        <comment>Title for missing opened folder texture</comment>
        <translation>geöffnete Verzeichnis Textur</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="654"/>
        <source>Large File Texture</source>
        <comment>Title for missing large file texture</comment>
        <translation>großes Datei Textur</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="655"/>
        <source>Small File Texture</source>
        <comment>Title for missing small file texture</comment>
        <translation>kleines Datei Textur</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="660"/>
        <source>Background Music</source>
        <comment>Title for missing BGM</comment>
        <translation>Hintergrundmusik</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="671"/>
        <source>Cursor SFX</source>
        <comment>Title for missing cursor SFX</comment>
        <translation>Anzeiger SFX</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="672"/>
        <source>Launch SFX</source>
        <comment>Title for missing launch SFX</comment>
        <translation>Starten SFX</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="673"/>
        <source>Folder SFX</source>
        <comment>Title for missing folder SFX</comment>
        <translation>Verzeichnis SFX</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="674"/>
        <source>Close SFX</source>
        <comment>Title for missing close SFX</comment>
        <translation>Beenden SFX</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="675"/>
        <source>Frame 0 SFX</source>
        <comment>Title for missing frame0 SFX</comment>
        <translation>Rahmen 0 SFX</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="676"/>
        <source>Frame 1 SFX</source>
        <comment>Title for missing frame1 SFX</comment>
        <translation>Rahmen 1 SFX</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="677"/>
        <source>Frame 2 SFX</source>
        <comment>Title for missing frame2 SFX</comment>
        <translation>Rahmen 2 SFX</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="678"/>
        <source>Open Lid SFX</source>
        <comment>Title for missing open lid SFX</comment>
        <translation>Aufmachen SFX</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1447"/>
        <source>Deploy Theme Archive</source>
        <comment>Display a tooltip for the deploy theme button</comment>
        <translation>Themenarchiv deployen</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1468"/>
        <source>Import Theme Archive</source>
        <comment>Display a tooltip for the import theme button</comment>
        <translation>Themenarchiv importieren</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1511"/>
        <source>The following properties are missing and required to deploy this theme:</source>
        <comment>Displays a validation message in the deploy button tooltip</comment>
        <translation>Die folgende Eigenschaften fehlt, und sind erförderlich, um dieses Theme zu deployen:</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1513"/>
        <source>Short Title</source>
        <comment>Name of the short title SMDH field</comment>
        <translation>Kurztitel</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1514"/>
        <source>Long Title</source>
        <comment>Name of the long title SMDH field</comment>
        <translation>Langtitel</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1515"/>
        <source>Publisher</source>
        <comment>Name of the publisher SMDH field</comment>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1516"/>
        <source>Theme Icon</source>
        <comment>Name of the SMDH image </comment>
        <translation>Themenicon</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1517"/>
        <source>Top Screen Color Texture/Texture Type Image</source>
        <comment>Indicates one of the top screen variables is not valid</comment>
        <translation>Obere Bildschirm Farbetexturmodus/Texturmodus Bild</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1518"/>
        <source>Bottom Screen Texture Type Image</source>
        <comment>Indicates one of the bottom screen variables is not valid</comment>
        <translation>Untere Bildschirm Texturmodus Bild</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1520"/>
        <source>Kame Editor will fill in the required missing fields with sensible defaults, but it&apos;s recommended to fill in those fields yourself.</source>
        <comment>Validation message note at the bottom</comment>
        <translation>Kame Editor wird die fehlende Eigenschaften ergänzen, aber es ist empfehlenswert Ihr selbst die Felder zu ergänzen.</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1598"/>
        <source>Save Source File...</source>
        <comment>Dialogue title when choosing to save the theme source file.</comment>
        <translation>Quelledatei speichern...</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1607"/>
        <source>Load Source File...</source>
        <comment>Dialogue title when choosing to load a theme source file.</comment>
        <translation>Quelledatei laden...</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1616"/>
        <source>Deploy Theme File...</source>
        <comment>Dialogue title when choosing to deploy a theme.</comment>
        <translation>Theme Datei deployen...</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1627"/>
        <source>Import Theme File...</source>
        <comment>Dialogue title when choosing to import a theme zip.</comment>
        <translation>Themedatei importieren...</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1600"/>
        <source>Source Files (*.rc)</source>
        <translation>Quelledatei (*.rc)</translation>
    </message>
    <message>
        <location filename="../src/consolemenu.cpp" line="1629"/>
        <source>Zip Archives (*.zip)</source>
        <translation>Ziparchive (*.zip)</translation>
    </message>
</context>
<context>
    <name>ConsolePad</name>
    <message>
        <location filename="../src/consolepad.cpp" line="31"/>
        <source>About Kame Editor</source>
        <comment>Tooltip for D-Pad up on the left side of the console.</comment>
        <translation>Über Kame Editor</translation>
    </message>
    <message>
        <location filename="../src/consolepad.cpp" line="36"/>
        <source>Settings</source>
        <comment>Tooltip for D-Pad down on the left side of the console</comment>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/consolepad.cpp" line="41"/>
        <source>Toggle Widget Lock</source>
        <comment>Tooltip for D-Pad left on the left side of the console</comment>
        <translation>Widget Bewegungen an/ausschalten</translation>
    </message>
    <message>
        <location filename="../src/consolepad.cpp" line="46"/>
        <source>Help</source>
        <comment>Tooltip for displaying help overlay</comment>
        <translation>Hilfe</translation>
    </message>
</context>
<context>
    <name>ConsoleWidget</name>
    <message>
        <location filename="../src/consolewidget.cpp" line="29"/>
        <source></source>
        <comment>Console Button - The four buttons on the right side of the console. Hover your cursor over them to see the tooltips. Power Button - Bottom button on the right side of the console. Used to quit the program. Console Joystick - Joystick on the left side of the console. Hover your cursor over it to see its tooltip.</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/consolewidget.cpp" line="44"/>
        <source>Show/Hide Theme Info Widget</source>
        <comment>Console Button</comment>
        <translation>Themeninfowidget einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/consolewidget.cpp" line="48"/>
        <source>Show/Hide Colors Widget</source>
        <comment>Console Button</comment>
        <translation>Farbewidget einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/consolewidget.cpp" line="52"/>
        <source>Show/Hide Textures and Screen Appearance Widget</source>
        <comment>Console Button</comment>
        <translation>Texturen und Bildschirm Erscheinung Widget einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/consolewidget.cpp" line="56"/>
        <source>Show/Hide Audio Widget</source>
        <comment>Console Button</comment>
        <translation>Audiowidget einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/consolewidget.cpp" line="63"/>
        <source>Quit</source>
        <comment>Power Button</comment>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../src/consolewidget.cpp" line="67"/>
        <source>Play/Pause Animations</source>
        <comment>Console Joystick</comment>
        <translation>Animationen abspielen/pausieren</translation>
    </message>
    <message>
        <location filename="../src/consolewidget.cpp" line="362"/>
        <source>Theme has unsaved changes</source>
        <comment>Title for the dialog</comment>
        <translation>Theme hat ungespeicherte Veränderungen</translation>
    </message>
    <message>
        <location filename="../src/consolewidget.cpp" line="363"/>
        <source>The current theme has unsaved changes. Would you like to save them before loading or starting a new one?</source>
        <comment>Asks the user whether or not they want to save the current theme before quitting.</comment>
        <translation>Das jetzige Theme hat ungespeicherte Veränderungen. Möchten Sie es speichern vor Sie ein neues Theme erstellen?</translation>
    </message>
</context>
<context>
    <name>File Types</name>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="27"/>
        <source>Source Files (*.rc)</source>
        <translation>Quelledatei (*.rc)</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="28"/>
        <source>Zip Archives (*.zip)</source>
        <translation>Ziparchive (*.zip)</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="29"/>
        <source>Image Files (*.png *.jpg *.jpeg *.gif *.bmp)</source>
        <translation>Bilderdatei (*.png *.jpg *.gif *.bmp)</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="30"/>
        <source>Nintendo BCWAV Audio Files (*.bcwav *cwav)</source>
        <translation>Nintendo BCWAV Audiodatei (*.bcwav *cwav)</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="31"/>
        <source>Nintendo BCSTM Audio Files (*.bcstm *cstm)</source>
        <translation>Nintendo BCSTM Audiodatei (*.bcstm *cstm)</translation>
    </message>
</context>
<context>
    <name>GameTextWidget</name>
    <message>
        <location filename="../src/preview/gametextwidget.cpp" line="90"/>
        <source>(New)</source>
        <comment>Appears in the game text tooltip when the grid zoom level is at its highest and a folder is selected.</comment>
        <translation>(Neu)</translation>
    </message>
</context>
<context>
    <name>GridWidget</name>
    <message>
        <location filename="../src/preview/gridwidget.cpp" line="68"/>
        <source>Try interacting with this preview. Use the mouse to click and drag on various elements.</source>
        <comment>Demo text that appears in the top screen when the first item in the bottom screen is selected.</comment>
        <translation>Versuchen Sie mit diese Vorschau zu interagieren. Benutzen Sie das Mausanzeiger um auf verschiedene Elemente zu klicken und ziehen.</translation>
    </message>
    <message>
        <location filename="../src/preview/gridwidget.cpp" line="97"/>
        <source>Turtle Game</source>
        <comment>Title of the second item in the bottom screen</comment>
        <translation>Schildkrötenspiel</translation>
    </message>
    <message>
        <location filename="../src/preview/gridwidget.cpp" line="99"/>
        <source>Some buttons in this preview don&apos;t do anything, but you can still click on them to see their pressed states.</source>
        <comment>Demo text that belongs to the Turtle Game item.</comment>
        <translation>Manche Knöpfe hatte in dieser Vorschau keinen Verwendungszweck, aber Sie können noch auf ihr klicken, um die Knöpfe gedrückt zu sehen.</translation>
    </message>
    <message>
        <location filename="../src/preview/gridwidget.cpp" line="101"/>
        <source>Warning</source>
        <comment>Title of the third item in the bottom screen.</comment>
        <translation>Hinweis</translation>
    </message>
    <message>
        <location filename="../src/preview/gridwidget.cpp" line="102"/>
        <source>This preview may not be 100% accurate. Sorry for any disappointment this may cause.</source>
        <comment>Demo text that belongs to the Warning item.</comment>
        <translation>Diese Vorschau darf nicht 100% genau sein. Tut uns Leid, wenn es Ihr nicht gefällt.</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="28"/>
        <source>Help</source>
        <comment>Title for help overlay</comment>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="33"/>
        <source>All interactive elements on the console frame have help tooltips.</source>
        <comment>Console tooltip hint in help overlay.</comment>
        <translation>Alle interaktive Elemente auf das Konsolerahmen haben Tooltips.</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="36"/>
        <source>1) Menu Options</source>
        <comment>Help overlay description</comment>
        <translation>1) Menü Optionen</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="37"/>
        <source>2) Change Menu Options</source>
        <comment>Help overlay description</comment>
        <translation>2) Menü Optionen wechseln</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="38"/>
        <source>3) 3DS Top Screen Preview</source>
        <comment>Help overlay description</comment>
        <translation>3) 3DS obere Bildschirm Vorschau</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="39"/>
        <source>4) 3DS Bottom Screen Preview</source>
        <comment>Help overlay description</comment>
        <translation>4) 3DS untere Bildschirm Vorschau</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="40"/>
        <source>5) Show/Hide Theme Info Dock Widget</source>
        <comment>Help overlay description</comment>
        <translation>5) Themeninfowidget einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="41"/>
        <source>6) Show/Hide Colors Dock Widget</source>
        <comment>Help overlay description</comment>
        <translation>6) Farbewidget einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="42"/>
        <source>7) Show/Hide Textures and Screen Appearance Dock Widget</source>
        <comment>Help overlay description</comment>
        <translation>7) Texturen und Bildschirm Erscheinung Widget einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="43"/>
        <source>8) Show/Hide Audio Dock Widget</source>
        <comment>Help overlay description</comment>
        <translation>8) Audiowidget einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="44"/>
        <source>9) Quit Application</source>
        <comment>Help overlay description</comment>
        <translation>9) Programm beenden</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="45"/>
        <source>10) Show/Hide Application Information</source>
        <comment>Help overlay description</comment>
        <translation>10) Programminfo einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="46"/>
        <source>11) Show/Hide This Help Overlay</source>
        <comment>Help overlay description</comment>
        <translation>11) Dieses Hilfefenster einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="47"/>
        <source>12) Show/Hide Settings Widget</source>
        <comment>Help overlay description</comment>
        <translation>12) Einstellungenwidget einblenden/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="48"/>
        <source>13) Play/Pause Preview Animations</source>
        <comment>Help overlay description</comment>
        <translation>13) Vorschau Animationen spielen/stoppen</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="49"/>
        <source>14) Dock Widgets can be docked or tabbed</source>
        <comment>Help overlay description</comment>
        <translation>14) Dockwidgets dürfen als Dock oder Tab angezeigt geworden</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="50"/>
        <source>15) Dockable regions indicated with a dotted line</source>
        <comment>Help overlay description</comment>
        <translation>15) Dockbare Stelle sind mit gepunktete Linien markiert</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="51"/>
        <source>16) Hover over (i) icons for help tooltips</source>
        <comment>Help overlay description</comment>
        <translation>16) Anzeiger über (i) Icons für Tooltiphilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/helpwidget.cpp" line="52"/>
        <source>17) Show Custom/Default Styles</source>
        <comment>Help overlay description</comment>
        <translation>17) Custom/Standard Stile anzeigen</translation>
    </message>
    <message>
        <source>Close</source>
        <comment>Close button in help overlay</comment>
        <translation type="vanished">Schließen</translation>
    </message>
</context>
<context>
    <name>ImagePicker</name>
    <message>
        <source>Image Files (*.png *.jpg *.gif *.bmp)</source>
        <translation type="vanished">Bilderdatei (*.png *.jpg *.gif *.bmp)</translation>
    </message>
    <message>
        <source>This image appears to already be in RGB565 color mode, so it has been loaded without being dithered. If you want to dither it, change the file extension to exclude .rgb565 and load it again.</source>
        <comment>Warn users that they&apos;re using an image that may already have been dithered, so Kame Editor does not dither it.</comment>
        <translation type="vanished">Dieses Bild ist bereits im RGB565 Modus, und war ohne Dithering geladen. Wollen Sie das Dithering, bitte &quot;.rgb565&quot; von Ende des Dateiname entfernen und laden Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../src/dock/imagepicker.cpp" line="29"/>
        <source>This image appears to already be in RGB565 color mode, so it has been loaded without being dithered. If you want to dither it, change the file extension to exclude &quot;.rgb565&quot; and load it again.</source>
        <comment>Warn users that they&apos;re using an image that may already have been dithered, so Kame Editor does not dither it.</comment>
        <translation>Dieses Bild ist bereits im RGB565 Modus, und ist somit ohne Dithering geladen. Wollen Sie das Dithering, bitte &quot;.rgb565&quot; von Ende des Dateinames entfernen und laden Sie es erneut.</translation>
    </message>
    <message>
        <source>Image Files (*.png *.jpg *.jpeg *.gif *.bmp)</source>
        <translation type="vanished">Bilderdatei (*.png *.jpg *.gif *.bmp)</translation>
    </message>
    <message>
        <location filename="../src/dock/imagepicker.cpp" line="44"/>
        <source>Texture Image Selection</source>
        <comment>Title for image selection dialog</comment>
        <translation>Texturbild Auswahl</translation>
    </message>
    <message>
        <location filename="../src/dock/imagepicker.cpp" line="68"/>
        <source>(No file selected)</source>
        <comment>Displayed near the file selection icon when no file is selected.</comment>
        <translation>(Keine Datei ausgewählt)</translation>
    </message>
    <message>
        <location filename="../src/dock/imagepicker.cpp" line="144"/>
        <location filename="../src/dock/imagepicker.cpp" line="273"/>
        <source>(No file selected)</source>
        <translation>(Keine Datei ausgewählt)</translation>
    </message>
    <message>
        <source>The loaded image appears to have come from an earlier version of Kame Editor. To ensure the images are dithered properly for the preview and deploy, please click on the &quot;Dither Images&quot; button in the settings menu.</source>
        <comment>Warn users that they have images from earlier versions of Kame Editor that may not be dithered in the final deployed theme.</comment>
        <translation type="vanished">Das ausgewähltes Bild stammt möglicherweise aus ein altere Version von Kame Editor. Um zu sichern daß die Bilder für die Vorschau und Deploy richtig aussieht, bitte auf &quot;Bilder dithern&quot; im Einstellungensmenü klicken.</translation>
    </message>
</context>
<context>
    <name>ImportDialog</name>
    <message>
        <source>Import Theme Archive</source>
        <comment>Import dialog title</comment>
        <translation type="vanished">Themenarchiv importieren</translation>
    </message>
    <message>
        <source>Archive file:</source>
        <comment>Label for selecting zip archive.</comment>
        <translation type="vanished">Archivdatei:</translation>
    </message>
    <message>
        <source>.zip file of the theme archive.</source>
        <comment>Tooltip for theme zip archive selection.</comment>
        <translation type="vanished">.zip Datei von Themenarchiv.</translation>
    </message>
    <message>
        <source>Resources folder:</source>
        <comment>Label for selecting resources path.</comment>
        <translation type="vanished">Ressourcenverzeichnis:</translation>
    </message>
    <message>
        <source>Folder where theme resources like sound effects and images should be stored.</source>
        <comment>Tooltip for resources folder selection.</comment>
        <translation type="vanished">Verzeichnis worin Themenressourcen wie Soundeffekte und Bilder gespeichern sind.</translation>
    </message>
    <message>
        <source>.rc output path:</source>
        <comment>Label for selecting where the rc file should go.</comment>
        <translation type="vanished">.rc Ausgabenpfad:</translation>
    </message>
    <message>
        <source>Path for the resulting configuration file.</source>
        <comment>Tooltip for rc file output path.</comment>
        <translation type="vanished">Pfad zum endgültiges RC Quelledatei.</translation>
    </message>
    <message>
        <source>Import from folder</source>
        <comment>Text for checkbox for toggling between archive zip and folder input.</comment>
        <translation type="vanished">von Verzeichnis importieren</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>cancel button</comment>
        <translation type="vanished">Abbrechen</translation>
    </message>
    <message>
        <source>Import</source>
        <comment>import button</comment>
        <translation type="vanished">Importieren</translation>
    </message>
    <message>
        <source>Archive folder:</source>
        <comment>Label for selecting folder archive.</comment>
        <translation type="vanished">Archivverzeichnis:</translation>
    </message>
    <message>
        <source>Folder where theme archive files are.</source>
        <comment>Tooltip for theme folder archive selection.</comment>
        <translation type="vanished">Verzeichnis das Themenarchivdateien enthalt.</translation>
    </message>
    <message>
        <source>Theme Archive Folder...</source>
        <comment>Dialog when choosing the theme folder to import.</comment>
        <translation type="vanished">Themenarchivverzeichnis...</translation>
    </message>
    <message>
        <source>Theme Archive File...</source>
        <comment>Dialog when choosing the theme zip file to import.</comment>
        <translation type="vanished">Themenarchivdatei...</translation>
    </message>
    <message>
        <source>Zip Archives (*.zip)</source>
        <translation type="vanished">Ziparchive (*.zip)</translation>
    </message>
    <message>
        <source>.rc Output File...</source>
        <comment>Dialog when choosing the rc output path to import.</comment>
        <translation type="vanished">.rc Ausgabendatei...</translation>
    </message>
    <message>
        <source>Source Files (*.rc)</source>
        <translation type="vanished">Quelledatei (*.rc)</translation>
    </message>
    <message>
        <source>Resources Folder...</source>
        <comment>Dialog when choosing the resources folder to import into.</comment>
        <translation type="vanished">Ressourcenverzeichnis...</translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../src/dock/infodialog.cpp" line="29"/>
        <source>Texture Templates</source>
        <translation>Texturvorlagen</translation>
    </message>
    <message>
        <location filename="../src/dock/infodialog.cpp" line="35"/>
        <source>Close</source>
        <comment>Close button in info dialogs</comment>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../src/dock/infodialog.cpp" line="38"/>
        <source>Documentation</source>
        <comment>Documentation button in info dialogs</comment>
        <translation>Dokumentation</translation>
    </message>
    <message>
        <location filename="../src/dock/infodialog.cpp" line="100"/>
        <source>Download Texture Templates...</source>
        <comment>Dialogue title when choosing to download templates</comment>
        <translation>Texturvorlagen herunterladen...</translation>
    </message>
    <message>
        <location filename="../src/dock/infodialog.cpp" line="108"/>
        <source>File Write Error</source>
        <comment>Dialog title when attempting to open texture templates file for download failed</comment>
        <translation>Datei Schreibfehler</translation>
    </message>
    <message>
        <location filename="../src/dock/infodialog.cpp" line="108"/>
        <source>Unable to write to destination file for texture templates download: %1</source>
        <translation>Kann Zieldatei für Textuvorlagen herunterladen nicht schreiben: %1</translation>
    </message>
    <message>
        <location filename="../src/dock/infodialog.cpp" line="109"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Downloading...</source>
        <comment>Texture Templates button downloading text</comment>
        <translation type="vanished">herunterladen...</translation>
    </message>
    <message>
        <source>Kame Editor</source>
        <comment>Titlebar for downloading texture templates</comment>
        <translation type="vanished">Kame Editor</translation>
    </message>
    <message>
        <source>Downloading Texture Templates...</source>
        <comment>Main text in the progress dialog for downloading texture templates</comment>
        <translation type="vanished">Texturvorlagen herunterladen...</translation>
    </message>
    <message>
        <source>Download Textures Template Failed</source>
        <comment>Dialog title when downloading textures templates failed</comment>
        <translation type="vanished">herunterladen von Texturvorlagen fehlgeschlagen</translation>
    </message>
    <message>
        <source>Downloading texture templates failed: %1</source>
        <translation type="vanished">Herunterladen von Texturvorlagen ist fehlgeschlagen: %1</translation>
    </message>
</context>
<context>
    <name>KameWidget</name>
    <message>
        <location filename="../src/kamewidget.cpp" line="57"/>
        <source>OpenGL Version Not Supported</source>
        <comment>Title for message box warning users about not having adequate OpenGL support</comment>
        <translation>OpenGL Version nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../src/kamewidget.cpp" line="57"/>
        <source>Your system is currently using OpenGL %1. Kame Editor requires OpenGL 3.0+ to work. Now closing.</source>
        <translation>Ihre System unterstützt OpenGL %1. OpenGL 3.0+ ist erforderlich, um Kame Editor richtig zu verwenden. Das Program wird jetzt beenden.</translation>
    </message>
    <message>
        <location filename="../src/kamewidget.cpp" line="58"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/kamewidget.cpp" line="68"/>
        <source>Kame Editor</source>
        <comment>Application title</comment>
        <translation>Kame Editor</translation>
    </message>
    <message>
        <location filename="../src/kamewidget.cpp" line="146"/>
        <source>About</source>
        <comment>About Dock Widget title</comment>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/kamewidget.cpp" line="151"/>
        <source>Settings</source>
        <comment>Kame Editor configuration</comment>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/kamewidget.cpp" line="157"/>
        <source>Theme Info</source>
        <comment>Theme Info Dock Widget title</comment>
        <translation>Themeninfo</translation>
    </message>
    <message>
        <location filename="../src/kamewidget.cpp" line="166"/>
        <source>Audio</source>
        <comment>Audio Dock Widget title</comment>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/kamewidget.cpp" line="174"/>
        <source>Textures and Screen Appearance</source>
        <comment>Textures and Screen Appearance Dock Widget title</comment>
        <translation>Textur und Bildschirm Erscheinung</translation>
    </message>
    <message>
        <location filename="../src/kamewidget.cpp" line="181"/>
        <source>Colors</source>
        <comment>Colors Dock Widget title</comment>
        <translation>Farbe</translation>
    </message>
</context>
<context>
    <name>OpenCloseWidget</name>
    <message>
        <location filename="../src/preview/openclosewidget.cpp" line="34"/>
        <source>Close</source>
        <comment>Bottom screen close button</comment>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../src/preview/openclosewidget.cpp" line="61"/>
        <source>Open</source>
        <comment>Bottom screen open button (when a folder is selected)</comment>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../src/preview/openclosewidget.cpp" line="64"/>
        <source>Settings</source>
        <comment>Bottom screen open button (on the left when a folder is selected)</comment>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/preview/openclosewidget.cpp" line="74"/>
        <source>Start</source>
        <comment>Bottom screen open button</comment>
        <translation>Starten</translation>
    </message>
    <message>
        <location filename="../src/preview/openclosewidget.cpp" line="84"/>
        <source>Interactive Preview</source>
        <comment>Bottom screen open/close button (when nothing is selected)</comment>
        <translation>Interaktive Vorschau</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Credits</source>
        <translation type="obsolete">Mitwirkenden</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">Über</translation>
    </message>
    <message>
        <source>Programming and graphics: %1
Made with Qt5.
Special Thanks:
%2</source>
        <comment>Credits contents</comment>
        <translation type="vanished">Programmierung und Grafiken: %1
Mit Qt5 gemacht.
Danksagung:
%2</translation>
    </message>
    <message>
        <location filename="../src/dock/aboutwidget.cpp" line="81"/>
        <source>Programming and graphics: %1
Made with Qt5 and PortAudio.
Other frameworks and libraries:
%2
Special Thanks:
%3</source>
        <comment>Credits contents</comment>
        <translation>Programmierung und Grafiken: %1
Mit Qt5 gemacht.
Andere Abhängikeiten und Bibliotheke:
%2
Danksagung:
%3</translation>
    </message>
    <message>
        <location filename="../src/dock/aboutwidget.cpp" line="93"/>
        <source>Credits</source>
        <comment>Title for Credits accordion in about widget.</comment>
        <translation>Mitwirkenden</translation>
    </message>
</context>
<context>
    <name>ResourcesDialog</name>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="35"/>
        <source>Missing Theme Resources</source>
        <comment>title for missing theme resources dialog</comment>
        <translation>Fehlende Themenressourcen</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="38"/>
        <source>Kame Editor cannot find some resources used in this theme. Please correct the paths to the missing resources before continuing.</source>
        <translation>Kame Editor können nicht alle Ressourcen in dieses Theme finden. Bevor Sie das Theme bearbeiten können, bitte die Pfade zum fehlende Ressourcen korrigieren.</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="50"/>
        <source>Resource</source>
        <comment>Missing resource list header title</comment>
        <translation>Ressource</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="51"/>
        <source>Old Path</source>
        <comment>Missing resource list header title</comment>
        <translation>alter Pfad</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="52"/>
        <source>New Path</source>
        <comment>Missing resource list header title</comment>
        <translation>neuer Pfad</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="53"/>
        <source>Select...</source>
        <comment>Missing resource list header title</comment>
        <translation>auswählen...</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="60"/>
        <source>Resolve</source>
        <comment>resolve button</comment>
        <translation>Korrigieren</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="61"/>
        <source>Cancel</source>
        <comment>cancel button</comment>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/resourcesdialog.cpp" line="152"/>
        <source>Load Missing Resource...</source>
        <comment>Dialogue title when choosing to load a missing resource file.</comment>
        <translation>fehlende Ressource laden...</translation>
    </message>
</context>
<context>
    <name>SMDHWidget</name>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="295"/>
        <source>Default</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="296"/>
        <source>Japanese</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Japanisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="297"/>
        <source>English</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="298"/>
        <source>French</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Französisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="299"/>
        <source>German</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="300"/>
        <source>Italian</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Italienisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="301"/>
        <source>Spanish</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Spanisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="302"/>
        <source>Simplified Chinese</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>vereinfachtes Chinesisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="303"/>
        <source>Korean</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Koreanisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="304"/>
        <source>Dutch</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Niederländisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="305"/>
        <source>Portuguese</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Portugiesisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="306"/>
        <source>Russian</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>Russisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="307"/>
        <source>Traditional Chinese</source>
        <comment>Language selection for SMDH titles/publisher</comment>
        <translation>traditionelles Chinesisch</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="339"/>
        <source>Icon</source>
        <comment>Title for SMDH icon image selection.</comment>
        <translation>Icon</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="340"/>
        <source>Icon used for the entry displayed in Anemone 3DS.</source>
        <comment>Tooltip for SMDH icon image selection.</comment>
        <translation>Icon die in Eintrag in Anemone 3DS angezeigt wird.</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="343"/>
        <source>Short Title</source>
        <comment>Title for SMDH short title.</comment>
        <translation>Kurztitel</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="345"/>
        <source>Long Title</source>
        <comment>Title for SMDH long title.</comment>
        <translation>Langtitel</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="347"/>
        <source>Publisher</source>
        <comment>Title for SMDH publisher.</comment>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="350"/>
        <source>Required Fields</source>
        <comment>Title for Theme Info widget required fields accordion.</comment>
        <translation>Erförderliche Eingaben</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="350"/>
        <source>Required information metadata.</source>
        <comment>Tooltip for Theme Info widget required fields accordion.</comment>
        <translation>Erförderliche Metadaten.</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="371"/>
        <source>Small Icon</source>
        <comment>Title for SMDH small icon image selection.</comment>
        <translation>Kleines Icon</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="372"/>
        <source>Small icon used for the entry displayed in Anemone 3DS.</source>
        <comment>Tooltip for SMDH small icon image selection.</comment>
        <translation>Kleines Icon die in Eintrag in Anemone 3DS angezeigt wird.</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="376"/>
        <source>Optional Fields</source>
        <comment>Title for Theme Info widget optional fields accordion.</comment>
        <translation>Optionale Eingaben</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="376"/>
        <source>Optional information metadata.</source>
        <comment>Tooltip for Theme Info widget optional fields accordion.</comment>
        <translation>Optionale Metadaten.</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="384"/>
        <source>Japan</source>
        <comment>Name of the country/region for which the SMDH rating field belongs to.</comment>
        <translation>Japan</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="385"/>
        <source>USA</source>
        <comment>Name of the country/region for which the SMDH rating field belongs to.</comment>
        <translation>USA</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="386"/>
        <source>Germany</source>
        <comment>Name of the country/region for which the SMDH rating field belongs to.</comment>
        <translation>Deutschland</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="387"/>
        <source>Europe</source>
        <comment>Name of the country/region for which the SMDH rating field belongs to.</comment>
        <translation>Europa</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="388"/>
        <source>Portugal</source>
        <comment>Name of the country/region for which the SMDH rating field belongs to.</comment>
        <translation>Portugal</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="389"/>
        <source>UK</source>
        <comment>Name of the country/region for which the SMDH rating field belongs to.</comment>
        <translation>UK</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="390"/>
        <source>Australia</source>
        <comment>Name of the country/region for which the SMDH rating field belongs to.</comment>
        <translation>Australien</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="391"/>
        <source>South Korea</source>
        <comment>Name of the country/region for which the SMDH rating field belongs to.</comment>
        <translation>Südkorea</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="392"/>
        <source>Taiwan</source>
        <comment>Name of the country/region for which the SMDH rating field belongs to.</comment>
        <translation>Taiwan</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="398"/>
        <source>Age Limit</source>
        <comment>Title for age limit in SMDH rating.</comment>
        <translation>Altersbeschränkung</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="399"/>
        <source>years</source>
        <comment>Units for the age limit in SMDH rating.</comment>
        <translation>Jahren</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="405"/>
        <source>Active</source>
        <comment>Option for SMDH rating.</comment>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="406"/>
        <source>Rating Pending</source>
        <comment>Option for SMDH rating.</comment>
        <translation>Altersfreigabe anstehend</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="407"/>
        <source>No Age Restriction</source>
        <comment>Option for SMDH rating.</comment>
        <translation>Keine Altersbeschränkung</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="414"/>
        <source>Region</source>
        <comment>Title for SMDH region.</comment>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="416"/>
        <source>Region Free</source>
        <comment>Option for SMDH region free.</comment>
        <translation>Regionsfrei</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="419"/>
        <source>Japan</source>
        <comment>Region selection for SMDH.</comment>
        <translation>Japan</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="420"/>
        <source>North America</source>
        <comment>Region selection for SMDH.</comment>
        <translation>Nord Amerika</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="421"/>
        <source>Europe</source>
        <comment>Region selection for SMDH.</comment>
        <translation>Europa</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="422"/>
        <source>Australia</source>
        <comment>Region selection for SMDH.</comment>
        <translation>Australien</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="423"/>
        <source>China</source>
        <comment>Region selection for SMDH.</comment>
        <translation>China</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="424"/>
        <source>Korea</source>
        <comment>Region selection for SMDH.</comment>
        <translation>Korea</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="425"/>
        <source>Taiwan</source>
        <comment>Region selection for SMDH.</comment>
        <translation>Taiwan</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="432"/>
        <source>Match Maker ID</source>
        <translation>Match Maker ID</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="436"/>
        <source>Match Maker BIT ID</source>
        <translation>Match Maker BIT ID</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="440"/>
        <source>EULA Version</source>
        <comment>Label for EULA version in SMDH.</comment>
        <translation>EULA Version</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="449"/>
        <source>Flags</source>
        <comment>Title for flag options in SMDH.</comment>
        <translation>Flags</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="451"/>
        <source>Visible</source>
        <comment>SMDH flag option</comment>
        <translation>Sichtbar</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="452"/>
        <source>Autoboot</source>
        <comment>SMDH flag option</comment>
        <translation>Autobooten</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="453"/>
        <source>Allow 3D</source>
        <comment>SMDH flag option</comment>
        <translation>3D erlauben</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="454"/>
        <source>Require EULA</source>
        <comment>SMDH flag option</comment>
        <translation>EULA erförderlich</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="455"/>
        <source>Autosave</source>
        <comment>SMDH flag option</comment>
        <translation>Autospeichern</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="456"/>
        <source>Extended Banner</source>
        <comment>SMDH flag option</comment>
        <translation>Erweitertes Banner</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="457"/>
        <source>Rating Required</source>
        <comment>SMDH flag option</comment>
        <translation>Altersfreigabe erförderlich</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="458"/>
        <source>Save Data</source>
        <comment>SMDH flag option</comment>
        <translation>Daten speichern</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="459"/>
        <source>Record Usage</source>
        <comment>SMDH flag option</comment>
        <translation>Verwendung aufnehmen</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="460"/>
        <source>No Save Backups</source>
        <comment>SMDH flag option</comment>
        <translation>Keine Speicherdaten Sicherungskopie</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="461"/>
        <source>New 3DS</source>
        <comment>SMDH flag option</comment>
        <translation>New 3DS</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="476"/>
        <source>Optimal Animation Default Frame (for BNR)</source>
        <comment>Title for SMDH option.</comment>
        <translation>Optimale Animation Standard Zeitleiste (für BNR)</translation>
    </message>
    <message>
        <location filename="../src/dock/smdhwidget.cpp" line="481"/>
        <source>CEC (StreetPass) ID</source>
        <comment>Title for SMDH option.</comment>
        <translation>CEC (StreetPass) ID</translation>
    </message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <source>Language: </source>
        <translation type="vanished">Sprache: </translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">Englisch</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="vanished">Deutsch</translation>
    </message>
    <message>
        <source>Console Skin: </source>
        <translation type="vanished">Konsolenstil: </translation>
    </message>
    <message>
        <source>3DS</source>
        <translation type="vanished">3DS</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="37"/>
        <source>Language: </source>
        <comment>Language selection title</comment>
        <translation>Sprache: </translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="41"/>
        <source>English</source>
        <comment>Language option</comment>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="41"/>
        <source>German</source>
        <comment>Language option</comment>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="51"/>
        <source>Console Skin: </source>
        <comment>Title for selecting console appearance.</comment>
        <translation>Konsolenstil: </translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="53"/>
        <source>3DS</source>
        <comment>Console skin option</comment>
        <translation>3DS</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="53"/>
        <source>2DS</source>
        <comment>Console skin option</comment>
        <translation>2DS</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="53"/>
        <source>3DS XL</source>
        <comment>Console skin option</comment>
        <translation>3DS XL</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="53"/>
        <source>New 3DS XL</source>
        <comment>Console skin option</comment>
        <translation>New 3DS XL</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="53"/>
        <source>New 2DS XL</source>
        <comment>Console skin option</comment>
        <translation>New 2DS XL</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="61"/>
        <source>Autoplay BGM</source>
        <comment>Settings option for enabling autoplaying BGMs</comment>
        <translation>BGM automatisch spielen</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="65"/>
        <source>Show folder letter</source>
        <comment>Settings option for displaying first letter on bottom screen folders.</comment>
        <translation>Verzeichnis Buchstabe anzeigen</translation>
    </message>
    <message>
        <source>Generates dithered images in RGB565 format. You generally shouldn&apos;t need this, but if your theme was made with an earlier version of Kame Editor &lt; 1.3.0, then click on the button below to ensure top and bottom textures are dithered properly when the theme is deployed.</source>
        <comment>Explain what the dither images button is for</comment>
        <translation type="vanished">Generiert geditherte Bilder in RGB565 Format. Es ist normalerweise nicht gebraucht, aber wenn Ihre Theme mit ein altere Version von Kame Editor &lt; 1.3.0 erstellt war, dann klicken Sie auf das Knopf unten um zu sichern daß die obere und untere Texturen bei deployen richtig aussieht.</translation>
    </message>
    <message>
        <source>Dither Images</source>
        <comment>Button for dithering all RGB565 images</comment>
        <translation type="vanished">Bilder dithern</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="77"/>
        <source>Custom Import Path: </source>
        <comment>Title for import path</comment>
        <translation>benutzerdefiniertes Importpfad: </translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="79"/>
        <source>Custom location to save source files for imported themes. If it&apos;s disabled, the editor picks a temporary location for these, and will be removed when the app closes. Enable this if you want to keep the source files.</source>
        <comment>Tooltip to explain to users the point of having a custom import path.</comment>
        <translation>Benutzerdefiniertes Pfad um Quelldateien von Themenimport zu speichern. Deaktiviert, wird das App ein anderen Pfad für die Quelldateien auswählen, und wird nach Beendigung des Apps gelöscht. Aktivieren Sie dieses Option wenn Sie die Quelldateien behalten wollen.</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="80"/>
        <source>Import from Folder</source>
        <comment>Title for whether to import from folder or zip</comment>
        <translation>von Verzeichnis importieren</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="81"/>
        <source>Import themes from a folder instead of from a zip file.</source>
        <comment>Tooltip for import from folder option</comment>
        <translation>Themen von Verzeichnis statt Zipdatei importieren.</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="108"/>
        <source>Deploy Path: </source>
        <comment>Title for deploy path</comment>
        <translation>Deploypfad: </translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="109"/>
        <source>Deploy Target to Folder</source>
        <comment>Title for deploy target</comment>
        <translation>zum Verzeichnis deployen</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="126"/>
        <source>Autosave</source>
        <comment>autosave label in settings widget</comment>
        <translation>Autospeichern</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="127"/>
        <source>Autosave interval:</source>
        <comment>Autosave interval text in settings widget</comment>
        <translation>Autospeichern Intervall:</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="128"/>
        <source>min(s)</source>
        <comment>autosave interval in minutes</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="138"/>
        <source>Untitled Autosave Files</source>
        <comment>Title for list of untitled autosave files</comment>
        <translation>Unbennante Autospeicherndateien</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="147"/>
        <source>Restore</source>
        <comment>button for restoring autosave files</comment>
        <translation>Wiederherstellen</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="148"/>
        <source>Delete</source>
        <comment>button for deleting autosave files</comment>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="153"/>
        <source>Untitled Autosave Files Pending</source>
        <comment>Title for message box warning users about existing autosave files</comment>
        <translation>Ungelöste Unbenannte Autospeicherndateien</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="153"/>
        <source>Autosave files from previously unsaved rc files have been cached. Please look in the settings widget to resolve them.</source>
        <translation>Autospeicherndateien von frühere ungespeicherte RC Dateien sind gecached. Bitte im Einstellungswidget anschauen um sie zu lösen.</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="154"/>
        <location filename="../src/dock/settingswidget.cpp" line="396"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="169"/>
        <location filename="../src/dock/settingswidget.cpp" line="275"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="173"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="177"/>
        <source>Restart</source>
        <translation>Neu starten</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="180"/>
        <source>Branded Preview</source>
        <comment>Option to add branding to preview screenshots</comment>
        <translation>Vorschau mit Marke</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="181"/>
        <source>Preview screenshot contains additional &quot;Made with Kame Editor&quot; text.</source>
        <comment>Tooltip for branded preview option</comment>
        <translation>Vorschauaufnahme enthalt die Texte &quot;Made with Kame Editor&quot;.</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="267"/>
        <source>Requires restart to take effect.</source>
        <translation>Neustart ist erförderlich.</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="308"/>
        <source>Saved</source>
        <translation>Gespeichert</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="357"/>
        <source>(No folder selected)</source>
        <comment>Tooltip that shows up when no destination has been selected for the deploy folder.</comment>
        <translation>(Keines Verzeichnis ausgewählt)</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="381"/>
        <source>Select import location</source>
        <comment>Title for file dialog to select import folder</comment>
        <translation>Importpfad auswählen</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="390"/>
        <source>Select deploy folder</source>
        <comment>Title for file dialog to select deploy folder</comment>
        <translation>Deployverzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="395"/>
        <source>Theme Files Already Exist</source>
        <comment>Title for message box warning users deploy folder already contains theme files</comment>
        <translation>Theme Dateien sind bereits vorhanden</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="395"/>
        <source>The folder you have chosen for theme deploy already contains theme files. Please note that these files will be overwritten. If this is not what you want to happen, please pick a different deploy folder.</source>
        <translation>Das ausgewählte Verzeichnis enthalt Theme Dateien, die bereits vorhanden sind. Bitte bemerken Sie, dass diese Dateien wird übergeschrieben geworden. Falls das nicht was Sie wollen, bitte einen anderen Deployverzeichnis auswählen.</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="404"/>
        <source>Zip archive (*.zip)</source>
        <translation>Ziparchive (*.zip)</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="405"/>
        <source>Select deploy path</source>
        <comment>Title for file dialog to select deploy path</comment>
        <translation>Deploypfad auswählen</translation>
    </message>
    <message>
        <source>Select deploy path</source>
        <translation type="vanished">Deploypfad auswählen</translation>
    </message>
    <message>
        <location filename="../src/dock/settingswidget.cpp" line="357"/>
        <source>(No file selected)</source>
        <comment>Tooltip that shows up when no destination has been selected for the deploy path.</comment>
        <translation>(Keine Datei ausgewählt)</translation>
    </message>
</context>
<context>
    <name>SwatchWidget</name>
    <message>
        <location filename="../src/dock/swatchwidget.cpp" line="22"/>
        <source></source>
        <comment>Unused Color - Tooltip that appears next to a ! icon in a color swatch. Unknown Color - Tooltip that appears next to a (?) icon in a color swatch.</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/dock/swatchwidget.cpp" line="28"/>
        <source>This color appears to be unused. You can modify it, and it will be saved into your theme, but it will not appear in the preview and may not show up on your 3DS.</source>
        <translation>Diese Farbe scheint unbenutzt zu sein. Sie dürfen es bearbeiten, und es wird auch in Ihre Theme gespeichert geworden, aber es wird nicht in Vorschau angezeigt, und darf auch nicht auf Ihre 3DS erscheinen.</translation>
    </message>
    <message>
        <location filename="../src/dock/swatchwidget.cpp" line="29"/>
        <source>It is unknown what this color does exactly, so it will not show up in the preview. It may show up on your 3DS or in other editors.</source>
        <translation>Es ist unbekannt, was genau diese Farbe wirkt, also ist es nicht im Vorschau angezeit. Es darf aber auf andere Editoren oder auf Ihre 3DS erscheinen.</translation>
    </message>
</context>
<context>
    <name>TextureWidget</name>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="532"/>
        <source>Draw Type</source>
        <comment>Top screen draw type selection</comment>
        <translation>Rendermodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="533"/>
        <source>Frame Type</source>
        <comment>Top screen frame type selection</comment>
        <translation>Rahmenmodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="537"/>
        <source>None</source>
        <comment>Top screen draw type selection option</comment>
        <translation>Keine</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="538"/>
        <source>Color</source>
        <comment>Top screen draw type selection option</comment>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="539"/>
        <source>Color Texture</source>
        <comment>Top screen draw type selection option</comment>
        <translation>Farbetextur</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="540"/>
        <source>Texture</source>
        <comment>Top screen draw type selection option</comment>
        <translation>Textur</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="545"/>
        <source>Image</source>
        <comment>Title for top screen images. Used by top screen frame types TEXTURE and COLOR TEXTURE (main texture).</comment>
        <translation>Bild</translation>
    </message>
    <message>
        <source>Image file for the top screen background, or the moving tiles pattern for the color texture draw type.</source>
        <comment>Tooltip for top screen main texture selection.</comment>
        <translation type="vanished">Bilddatei für das obere Bildschirm, oder die bewegende Tafelmuster für Farbtexturmodus.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="555"/>
        <source>Image file for the top screen background, or the moving tiles pattern for the color texture draw type. Click for more details.</source>
        <comment>Tooltip for top screen main texture selection.</comment>
        <translation>Bilddatei für das obere Bildschirm, oder die bewegende Tafelmuster für Farbtexturmodus. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="546"/>
        <source>Top Screen with Texture Draw Type</source>
        <comment>Title caption for detailed top texture help</comment>
        <translation>Obere Bildschirm auf Texturmodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="546"/>
        <location filename="../src/dock/texturewidget.cpp" line="690"/>
        <source>Frame Type Single</source>
        <translation>Einzelrahmenmodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="547"/>
        <location filename="../src/dock/texturewidget.cpp" line="691"/>
        <source>Frame Type Fast Scroll or Slow Scroll</source>
        <translation>Schnellscrollen oder Langsamscrollen Rahmenmodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="548"/>
        <location filename="../src/dock/texturewidget.cpp" line="693"/>
        <source>Single Frame Image Dimensions</source>
        <translation>Einzelrahmen Bildgröße</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="549"/>
        <location filename="../src/dock/texturewidget.cpp" line="694"/>
        <source>Single Frame Visible Region (green = visible, gray = not visible)</source>
        <translation>Einzelrahmen sichtbarer Bereich (grün = sichtbar, grau = nicht sichtbar)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="550"/>
        <source>3D Holographic region bleed</source>
        <translation>3D Hologramm Überfüllung</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="551"/>
        <source>Scrolling Frames Image Dimensions</source>
        <translation>Scrollrahmen Bildgröße</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="552"/>
        <source>Top Screen with Color Texture Draw Type</source>
        <comment>Title caption for detailed top texture help</comment>
        <translation>Obere Bildschirm auf Farbtexturmodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="552"/>
        <source>Scrolling Frames Visible Region (green = visible, gray = not visible)</source>
        <translation>Scrollrahmen sichtbarer Bereich (grün = sichtbar, grau = nicht sichtbar)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="553"/>
        <source>Tiling images are grayscale images where white is used to represent opaque pixels, and black is used for transparent pixels. For best results, do not use images with transparent pixels.</source>
        <comment>Detailed information about top color textures.</comment>
        <translation>Getafelte Bilder sind Graustufebilder, worin weiß soll sichtbarer Pixel zeigen, und schwarz soll unsichtbarer Pixel zeigen. Für beste Ergebnisse, bitte keine Bilder mit Transparente Pixel benutzen.</translation>
    </message>
    <message>
        <source>Top Texture Help</source>
        <comment>Title for detailed top texture help</comment>
        <translation type="vanished">Obere Bildschirm Texture Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="554"/>
        <source>Top Screen Texture Help</source>
        <comment>Title for detailed top texture help</comment>
        <translation>Obere Bildschirm Textur Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="562"/>
        <source>Image Extra</source>
        <comment>Title for top screen ext image. Only used in top screen frame type COLOR TEXTURE.</comment>
        <translation>Zusätzliches Bild</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="563"/>
        <source>Image file for the static tiles.</source>
        <comment>Tooltip for top screen ext texture selection.</comment>
        <translation>Bilddatei für statische Tafeln.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="572"/>
        <source>Single</source>
        <comment>Top screen frame type selection option</comment>
        <translation>Einzel</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="573"/>
        <source>Fast Scroll</source>
        <comment>Top screen frame type selection option</comment>
        <translation>Schnellscrollen</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="574"/>
        <source>Slow Scroll</source>
        <comment>Top screen frame type selection option</comment>
        <translation>Langsamscrollen</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="599"/>
        <source>Top Screen</source>
        <comment>Title for top screen accordion.</comment>
        <translation>Obere Bildschirm</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="599"/>
        <source>Textures for the top screen.</source>
        <comment>Tooltip for top screen accordion.</comment>
        <translation>Textur für obere Bildschirm.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="629"/>
        <source>Background Color</source>
        <comment>Top screen background color text in frame types COLOR and COLOR TEXTURE.</comment>
        <translation>Hintergrundfarbe</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="635"/>
        <source>Gradient</source>
        <comment>Top screen gradient text in frame types COLOR and COLOR TEXTURE.</comment>
        <translation>Verlauf</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="640"/>
        <source>Opacity</source>
        <comment>Top screen opacity text in frame types COLOR and COLOR TEXTURE.</comment>
        <translation>Deckkraft</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="646"/>
        <source>Static Texture Opacity</source>
        <comment>Top screen static texture opacity in frame types COLOR and COLOR TEXTURE.</comment>
        <translation>Statische Textur Deckkraft</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="652"/>
        <source>Gradient Brightness</source>
        <comment>Top screen gradient brightness in frame types COLOR and COLOR TEXTURE.</comment>
        <translation>Verlauf Helligkeit</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="673"/>
        <source>Draw Type</source>
        <comment>Bottom screen draw type selection</comment>
        <translation>Rendermodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="674"/>
        <source>Frame Type</source>
        <comment>Bottom screen frame type selection</comment>
        <translation>Rahmenmodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="678"/>
        <source>None</source>
        <comment>Bottom screen draw type selection option</comment>
        <translation>Keine</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="679"/>
        <source>Color</source>
        <comment>Bottom screen draw type selection option</comment>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="680"/>
        <source>Texture</source>
        <comment>Bottom screen draw type selection option</comment>
        <translation>Textur</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="687"/>
        <source>Image</source>
        <comment>Title for bottom screen images. Used by bottom screen frame type TEXTURE.</comment>
        <translation>Bild</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="688"/>
        <source>Image file for the bottom screen background. Click for more details.</source>
        <comment>Tooltip for bottom screen texture selection.</comment>
        <translation>Bilddatei für unterem Bildschirmhintergrund. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="690"/>
        <source>Bottom Screen with Texture Draw Type</source>
        <comment>Title caption for detailed bottom texture help</comment>
        <extracomment>Bottom screen texture detailed help</extracomment>
        <translation>Untere Bildschirm auf Texturmodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="850"/>
        <source>Exact templates can be found at %1.</source>
        <comment>Default folder texture detailed help note</comment>
        <translation>Genauer Vorlagen finden Sie am %1.</translation>
    </message>
    <message>
        <source>Frame Type Fast Scroll, Slow Scroll, Page Scroll or Bounce Scroll</source>
        <translation type="vanished">Schnellscrollen, Langsamscrollen, Seitenscrollen oder Spiegelscrollen Rahmenmodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="698"/>
        <source>Page or Bounce Scroll Frame 0 (within the dotted lines)</source>
        <translation>Seiten oder Spiegelscrollen Rahmen 0 (in gepunkte Linien)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="699"/>
        <source>Page or Bounce Scroll Frame 1 (within the dotted lines)</source>
        <translation>Seiten oder Spiegelscrollen Rahmen 1 (in gepunkte Linien)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="700"/>
        <source>Page or Bounce Scroll Frame 2 (within the dotted lines)</source>
        <translation>Seiten oder Spiegelscrollen Rahmen 2 (in gepunkte Linien)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="702"/>
        <source>Bottom Screen Texture Help</source>
        <comment>Title for detailed bottom texture help</comment>
        <translation>Untere Bildschirm Textur Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="582"/>
        <location filename="../src/dock/texturewidget.cpp" line="719"/>
        <source>Single - Display texture as is</source>
        <extracomment>Top frame type tooltip explanation
----------
Bottom frame type explanation</extracomment>
        <translation>Einzel - Textur ist einfach angezeigt</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="583"/>
        <location filename="../src/dock/texturewidget.cpp" line="720"/>
        <source>Fast Scroll - A longer texture scrolls over the screen</source>
        <translation>Schnellscrollen - Langere Textur über den Schirm scrollen</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="584"/>
        <location filename="../src/dock/texturewidget.cpp" line="721"/>
        <source>Slow Scroll - Same as fast scroll, but scrolling speed is reduced to 0.8x</source>
        <translation>Langsamscrollen - Gleich wie Schnellscrollen, aber scrollgeschwindigkeit ist 0.8x reduziert</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="701"/>
        <source>Page or Bounce Scroll Frames Image Dimensions</source>
        <translation>Seitenscrollen oder Spiegelscrollen Rahmen Bildgröße</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="702"/>
        <source>Page or Bounce Scroll Frames Visible Region (green = visible, gray = not visible)</source>
        <translation>Seitenscrollen oder Spiegelscrollen Rahmen sichtbarer Bereich (grün = sichtbar, grau = nicht sichtbar)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="722"/>
        <source>Page Scroll - Scrolling/dragging through the bottom screen causes a frame animation to play. The order of the frames is 0, 1, 2, 0, 1, 2...</source>
        <translation>Seitenscrollen - Scrollen/zeihen auf den untere Bildschirm zeigt ein Frameanimation. Die Reihenfolge des Rahmens ist 0, 1, 2, 0, 1, 2...</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="723"/>
        <source>Bounce Scroll - Same as Page Scroll, but the order of the frames is 0, 1, 2, 1, 0...</source>
        <translation>Spiegelscrollen - Gleich wie Seitenscrollen, aber die Reihenfolge des Rahmens ist 0, 1, 2, 1, 0, ...</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="838"/>
        <location filename="../src/dock/texturewidget.cpp" line="849"/>
        <source>Folder Foreground Texture (green)</source>
        <translation>Verzeichnis Vordergrundtextur (grün)</translation>
    </message>
    <message>
        <source>Exact templates can be found at %1</source>
        <comment>Opened folder texture detailed help note</comment>
        <translation type="vanished">Genauer Vorlagen finden Sie am %1</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="873"/>
        <location filename="../src/dock/texturewidget.cpp" line="884"/>
        <source>Icon Frame Region (gray)</source>
        <translation>Iconrahmen Bereich (grau)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="874"/>
        <location filename="../src/dock/texturewidget.cpp" line="885"/>
        <source>Icon Region (green)</source>
        <translation>Icon Bereich (grün)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="835"/>
        <source>Image for the folder icon when it is opened. Click for more details.</source>
        <comment>Tooltip for open folder image selection.</comment>
        <translation>Bild für Verzeichnisicon wenn es öffnet sich. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="692"/>
        <source>Frame Type Page Scroll or Bounce Scroll</source>
        <translation>Seitenscrollen oder Spiegelscrollen Rahmenmodus</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="695"/>
        <source>Fast or Slow Scroll Frame Image Dimensions</source>
        <translation>Schnellscrollen oder Langsamscrollen Rahmen Bildgröße</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="696"/>
        <source>Fast or Slow Scroll Frame Visible Region (green = visible, gray = not visible)</source>
        <translation>Schnellscrollen oder Langsamscrollen Rahmen sichtbarer Bereich (grün = sichtbar, grau = nicht sichtbar)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="697"/>
        <source>Offset image left 45 pixels to align with top screen (must also have the same frame type)</source>
        <translation>Bild 45 Pixel Abstand und nach links wiederholen um mit Oberes Bildschirm zu passen (Beide mussen auch der gleiche Rahmenmodus haben)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="837"/>
        <source>Folder Texture: Opened</source>
        <comment>Title caption for detailed folder texture help.</comment>
        <extracomment>Open folder texture detailed help</extracomment>
        <translation>Verzeichnistextur: Geöffnet</translation>
    </message>
    <message>
        <source>Exact templates can be found at https://gitlab.io/beelzy/kame-editor.</source>
        <comment>Opened folder texture detailed help note</comment>
        <translation type="vanished">Genauer Vorlagen finden Sie am https://gitlab.io/beelzy/kame-editor.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="839"/>
        <location filename="../src/dock/texturewidget.cpp" line="850"/>
        <source>Folder Texture Help</source>
        <comment>Window title for folder texture detailed help</comment>
        <translation>Verzeichnistextur Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="839"/>
        <location filename="../src/dock/texturewidget.cpp" line="850"/>
        <source>Folder Background Texture (gray)</source>
        <translation>Verzeichnis Hintergrundtextur (grau)</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="839"/>
        <source>Exact templates can be found at %1.</source>
        <comment>Opened folder texture detailed help note</comment>
        <translation>Genauer Vorlagen finden Sie am %1.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="846"/>
        <source>Image for the folder icon when it is closed. Click for more details.</source>
        <comment>Tooltip for closed folder image selection.</comment>
        <translation>Bild für Verzeichnisicon wenn es geschlossen ist. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="848"/>
        <source>Folder Texture: Default</source>
        <comment>Title caption for detailed folder texture help.</comment>
        <extracomment>Default folder texture detailed help</extracomment>
        <translation>Verzeichnistextur: Standard</translation>
    </message>
    <message>
        <source>Exact templates can be found at https://gitlab.io/beelzy/kame-editor.</source>
        <comment>Default folder texture detailed help note</comment>
        <translation type="vanished">Genauer Vorlagen finden Sie am https://gitlab.io/beelzy/kame-editor.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="870"/>
        <source>Image for the file icon when bottom screen grid zoom is large enough. Click for more details.</source>
        <comment>Tooltip for large file frame image selection.</comment>
        <translation>Bild für das Dateiicon wenn die untere Bildschirm Dateiskalierung groß genug ist. Klicken Sie für weitere Einzelheiten.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="872"/>
        <source>Large File Icon</source>
        <comment>Title caption for large file icon detailed help.</comment>
        <extracomment>Large file icon detailed help</extracomment>
        <translation>Großes Dateiicon</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="874"/>
        <source>The 3DS renders icon frames with rounded corners, so some portion of the corners will not be visible. The icon is mirrored horizontally, so the image contains only the right half of the icon. The icon may also be scaled to fit in different zoom levels.</source>
        <comment>Large file icon detailed help note</comment>
        <translation>Das 3DS zeigt Iconrahmen mit Rundecken, also Teile von die Ecken dürfen unsichtbar sein. Das Icon ist horizontal gespiegelt, also steht auf das Bild nur die richtige Seite von das Icon. Das Icon darf auch skaliert um verschiedene Dateiskalierungstufe anzupassen.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="874"/>
        <source>Large File Icon Help</source>
        <comment>Window title for large file icon detailed help</comment>
        <translation>Großes Dateiicon Hilfe</translation>
    </message>
    <message>
        <source>Image file for the bottom screen background.</source>
        <comment>Tooltip for bottom screen texture selection.</comment>
        <translation type="vanished">Bilddatei für unterem Bildschirmhintergrund.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="707"/>
        <source>Single</source>
        <comment>Bottom screen frame type selection option</comment>
        <translation>Einzel</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="708"/>
        <source>Fast Scroll</source>
        <comment>Bottom screen frame type selection option</comment>
        <translation>Schnellscrollen</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="709"/>
        <source>Slow Scroll</source>
        <comment>Bottom screen frame type selection option</comment>
        <translation>Langsamscrollen</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="710"/>
        <source>Page Scroll</source>
        <comment>Bottom screen frame type selection option</comment>
        <translation>Seitenscrollen</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="711"/>
        <source>Bounce Scroll</source>
        <comment>Bottom screen frame type selection option</comment>
        <translation>Spiegelscrollen</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="735"/>
        <source>Bottom Screen</source>
        <comment>Title for bottom screen accordion.</comment>
        <translation>Untere Bildschirm</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="735"/>
        <source>Textures for the bottom screen.</source>
        <comment>Tooltip for bottom screen accordion.</comment>
        <translation>Texturen für untere Bildschirm.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="756"/>
        <source>Inner Frame</source>
        <comment>Title for inner frame colors in the bottom screen with frame type COLOR.</comment>
        <translation>Innerrahmen</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="760"/>
        <source>Dark</source>
        <comment>Bottom screen inner frame dark color title</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="763"/>
        <source>Main</source>
        <comment>Bottom screen inner frame main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="766"/>
        <source>Light</source>
        <comment>Bottom screen inner frame light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="769"/>
        <source>Shadow</source>
        <comment>Bottom screen inner frame shadow color title</comment>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="773"/>
        <source>Background</source>
        <comment>Title for background colors in the bottom screen with frame type COLOR.</comment>
        <translation>Hintergrund</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="777"/>
        <source>Dark</source>
        <comment>Bottom screen background dark color title</comment>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="780"/>
        <source>Main</source>
        <comment>Bottom screen background main color title</comment>
        <translation>Haupt</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="783"/>
        <source>Light</source>
        <comment>Bottom screen background light color title</comment>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="833"/>
        <source>Open</source>
        <comment>Title for open folder image selection.</comment>
        <translation>Öffnen</translation>
    </message>
    <message>
        <source>Image for the folder icon when it is opened.</source>
        <comment>Tooltip for open folder image selection.</comment>
        <translation type="vanished">Bild für Verzeichnisicon wenn es öffnet sich.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="844"/>
        <source>Default</source>
        <comment>Title for closed folder image selection.</comment>
        <translation>Standard</translation>
    </message>
    <message>
        <source>Image for the folder icon when it is closed.</source>
        <comment>Tooltip for closed folder image selection.</comment>
        <translation type="vanished">Bild für Verzeichnisicon wenn es geschlossen ist.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="853"/>
        <source>Folder Icons</source>
        <comment>Title for folder icons accordion.</comment>
        <translation>Verzeichnisicons</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="853"/>
        <source>Textures for the folder icons in the bottom screen.</source>
        <comment>Tooltip for folder icons accordion.</comment>
        <translation>Texturen für die Verzeichnisicons in untere Bildschirm.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="868"/>
        <source>Large</source>
        <comment>Title for large file frame image selection.</comment>
        <translation>Groß</translation>
    </message>
    <message>
        <source>Image for the file icon when bottom screen grid zoom is large enough.</source>
        <comment>Tooltip for large file frame image selection.</comment>
        <translation type="vanished">Bild für das Dateiicon wenn die untere Bildschirm Dateiskalierung groß genug ist.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="879"/>
        <source>Small</source>
        <comment>Title for small file frame image selection.</comment>
        <translation>Klein</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="881"/>
        <source>Image for the file icon when bottom screen grid zoom is small enough.</source>
        <comment>Tooltip for small file frame image selection.</comment>
        <translation>Bild für Dateiicon wenn untere Bildschirm Dateiskalierung klein genug ist.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="883"/>
        <source>Small File Icon</source>
        <comment>Title caption for small file icon detailed help.</comment>
        <extracomment>Small file icon detailed help</extracomment>
        <translation>Kleines Dateiicon</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="885"/>
        <source>The 3DS renders icon frames with rounded corners, so some portion of the corners will not be visible. The icon is mirrored horizontally, so the image contains only the right half of the icon. The icon may also be scaled to fit in different zoom levels.</source>
        <comment>Small file icon detailed help note</comment>
        <translation>Das 3DS zeigt Iconrahmen mit Rundecken, also Teile von die Ecken dürfen unsichtbar sein. Das Icon ist horizontal gespiegelt, also steht auf das Bild nur die richtige Seite von das Icon. Das Icon darf auch skaliert um verschiedene Dateiskalierungstufe anzupassen.</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="885"/>
        <source>Small File Icon Help</source>
        <comment>Window title for small file icon detailed help</comment>
        <translation>Kleines Dateiicon Hilfe</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="888"/>
        <source>File Icons</source>
        <comment>Title for file icons accordion.</comment>
        <translation>Dateiicons</translation>
    </message>
    <message>
        <location filename="../src/dock/texturewidget.cpp" line="888"/>
        <source>Textures for the file icons in the bottom screen.</source>
        <comment>Tooltip for file icons accordion.</comment>
        <translation>Texturen für Dateiicons in untere Bildschirm.</translation>
    </message>
</context>
<context>
    <name>ThumbnailWidget</name>
    <message>
        <source>Incorrect Texture Image Size</source>
        <translation type="vanished">Falsche Texturbildgroße</translation>
    </message>
    <message>
        <location filename="../src/dock/thumbnailwidget.cpp" line="46"/>
        <source>Incorrect Texture Image Size</source>
        <comment>Title for error message.</comment>
        <translation>Falsche Texturbildgröße</translation>
    </message>
    <message>
        <location filename="../src/dock/thumbnailwidget.cpp" line="46"/>
        <source>The image you have selected is the wrong dimensions. Please use an image with a resolution of %1 x %2.</source>
        <comment>This message shows up when you try to select an image in the texture widget that&apos;s not the correct dimensions.</comment>
        <translation>Das Bild die Sie ausgewählt haben ist das falsche Große. Bitte ein Bild mit %1 x %2 Auflösung verwenden.</translation>
    </message>
    <message>
        <location filename="../src/dock/thumbnailwidget.cpp" line="47"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>TopView</name>
    <message>
        <source>Internet</source>
        <translation type="vanished">Internet</translation>
    </message>
    <message>
        <location filename="../src/preview/topview.cpp" line="798"/>
        <source>Internet</source>
        <comment>Internet status text in the top screen.</comment>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="../src/preview/topview.cpp" line="933"/>
        <source>(New)</source>
        <comment>Appears in the folder tooltip in the top screen when a folder is selected.</comment>
        <translation>(Neu)</translation>
    </message>
</context>
<context>
    <name>color_widgets::ColorDialog</name>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_dialog.cpp" line="66"/>
        <source>Pick</source>
        <translation>Auswählen</translation>
    </message>
</context>
<context>
    <name>color_widgets::ColorPalette</name>
    <message>
        <location filename="../src/dock/QtColorWidgets/color_palette.cpp" line="428"/>
        <source>Unnamed</source>
        <translation>Unbenannt</translation>
    </message>
</context>
<context>
    <name>color_widgets::Swatch</name>
    <message>
        <location filename="../src/dock/QtColorWidgets/swatch.cpp" line="761"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
</context>
<context>
    <name>global</name>
    <message>
        <source>Image Files (*.png *.jpg *.jpeg *.gif *.bmp)</source>
        <translation type="obsolete">Bilderdatei (*.png *.jpg *.gif *.bmp)</translation>
    </message>
    <message>
        <source>Nintendo BCWAV Audio Files (*.bcwav *cwav)</source>
        <translation type="obsolete">Nintendo BCWAV Audiodatei (*.bcwav *cwav)</translation>
    </message>
    <message>
        <source>Nintendo BCSTM Audio Files (*.bcstm *cstm)</source>
        <translation type="obsolete">Nintendo BCSTM Audiodatei (*.bcstm *cstm)</translation>
    </message>
</context>
</TS>
