#!/bin/sh

version="$(git describe --abbrev=0)";

sed -e "s/\${VERSION}/$version/g" -e "s/\${WIN_VERSION}/1.3.1/g" public/index.template.html > public/index.html
