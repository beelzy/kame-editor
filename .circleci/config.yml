version: 2.1
commands:
    unit-tests:
        steps:
            - run:
                name: unit tests
                command: |
                    cd ~/project/test
                    qmake ../kame-editor.pro "CONFIG += test"
                    make -j4
                    export PATH=$PATH:/tmp/rstmcpp/:/tmp/kame-tools/output/mac-x86_64/
                    ./test/build/kame-tests > results
            - store_artifacts:
                path: ~/project/test/results
    kame-tools:
        steps:
            - run:
                name: build kame-tools
                command: |
                    cd /tmp
                    git clone https://gitlab.com/beelzy/kame-tools.git
                    cd kame-tools
                    git submodule init
                    git submodule update
                    cd buildtools
                    git apply ../aarch64.patch
                    cd ..
                    make
    vgmstream:
        steps:
            - run:
                name: build vgmstream
                command: |
                    cd /tmp
                    git clone https://github.com/losnoco/vgmstream.git
                    cd vgmstream
                    mkdir -p build
                    cd build
                    cmake -D BUILD_AUDACIOUS=OFF -D USE_CELT=OFF ..
                    make
    rstmcpp:
        steps:
            - run:
                name: build rstmcpp
                command: |
                    cd /tmp
                    git clone https://gitlab.com/beelzy/rstmcpp.git
                    cd rstmcpp
                    git submodule init
                    git submodule update
                    make
    build-qt:
        steps:
            - run:
                name: build qt6 base
                command: |
                    cd /tmp
                    curl https://download.qt.io/official_releases/qt/6.6/6.6.2/submodules/qtbase-everywhere-src-6.6.2.tar.xz -L -o qtbase-everywhere-src-6.6.2.tar.xz
                    tar -xf qtbase-everywhere-src-6.6.2.tar.xz
                    mkdir -p ~/dev/qt-build
                    cd ~/dev/qt-build
                    /tmp/qtbase-everywhere-src-6.6.2/configure -nomake examples -nomake tests -prefix /usr/local -release
                    cmake --build . --parallel
                    sudo cmake --install .
    build-portaudio:
        steps:
            - run:
                name: build portaudio
                command: |
                    cd /tmp
                    curl http://files.portaudio.com/archives/pa_stable_v190700_20210406.tgz -L -o portaudio.tgz
                    tar -xf portaudio.tgz
                    cd portaudio
                    ./configure --disable-debug --disable-dependency-tracking --enable-mac-universal=no --enable-cxx
                    make
                    sudo make install
    macbundle:
        steps:
            - run:
                name: bundle Kame Editor
                command: |
                    cd ~/project
                    cp /tmp/kame-tools/output/mac-arm64/kame-tools build/kame-editor.app/Contents/MacOS/
                    cp /tmp/vgmstream-cli build/kame-editor.app/Contents/MacOS/
                    cp /tmp/rstmcpp/rstmcpp build/kame-editor.app/Contents/MacOS/
                    macdeployqt build/kame-editor.app/
                    mkdir Kame\ Editor
                    cp -a build/kame-editor.app/ Kame\ Editor/kame-editor.app/
                    cp LICENSE.txt Kame\ Editor/
                    ln -s /Applications Kame\ Editor/Applications
                    hdiutil create -format UDZO -srcfolder Kame\ Editor/ -fs HFS+ kame-editor.dmg

jobs:
    build:
        macos:
            xcode: "15.0.0"
            resource_class: macos.m1.medium.gen1
        steps:
            - run:
                name: install cmake
                command: HOMEBREW_NO_AUTO_UPDATE=1 brew install cmake
            - run:
                name: install ninja
                command: HOMEBREW_NO_AUTO_UPDATE=1 brew install ninja
            - run:
                name: link brew properly
                command: HOMEBREW_NO_AUTO_UPDATE=1 brew install leptonica
            - build-qt
            - build-portaudio
            - kame-tools
            - run:
                name: install vgmstream
                command: |
                    cd /tmp
                    curl -O -L https://github.com/vgmstream/vgmstream-releases/releases/download/nightly/vgmstream-mac-cli.tar.gz
                    tar -xvf vgmstream-mac-cli.tar.gz
            - rstmcpp
            - checkout
            - run:
                name: build Kame Editor
                command: |
                    cd ~/project
                    qmake
                    make -j4
            - macbundle
            - store_artifacts:
                path: ~/project/kame-editor.dmg
    build-debs:
        docker:
            - image: cimg/base:2024.02
        steps:
            - run:
                name: "Update apt-get"
                command: sudo apt-get update
            - run:
                name: "install libgl"
                command: sudo apt-get install libgl1-mesa-dev
            - run:
                name: "install vgmstream dependencies"
                command: sudo apt-get install libspeex-dev libvorbis-dev libtool yasm
            - run:
                name: "install qt6"
                command: sudo apt-get install qt6-base-dev
            - run:
                name: "install portaudio"
                command: sudo apt-get install portaudio19-dev
            - kame-tools
            - vgmstream
            - rstmcpp
            - checkout
            - run:
                name: build Kame Editor
                command: |
                    cd ~/project/
                    qmake6
                    make -j4
                    objcopy --strip-unneeded --strip-debug build/kame-editor
                    ./buildicons.sh
            - run:
                name: package Kame Editor
                command: |
                    sudo apt-get install fakeroot
                    cd ~/project/
                    mkdir -p kame-editor_version_amd64/DEBIAN
                    mkdir -p kame-editor_version_amd64/usr/bin
                    mkdir -p kame-editor_version_amd64/usr/share/applications
                    mkdir -p kame-editor_version_amd64/usr/share/icons/hicolor
                    mkdir -p kame-editor_version_amd64/usr/share/doc/kame-editor
                    cp .circleci/deb/control kame-editor_version_amd64/DEBIAN/
                    cp build/kame-editor kame-editor_version_amd64/usr/bin/
                    cp /tmp/kame-tools/output/linux-x86_64/kame-tools kame-editor_version_amd64/usr/bin/
                    objcopy --strip-unneeded --strip-debug kame-editor_version_amd64/usr/bin/kame-tools
                    cp /tmp/rstmcpp/rstmcpp kame-editor_version_amd64/usr/bin/
                    objcopy --strip-unneeded --strip-debug kame-editor_version_amd64/usr/bin/rstmcpp
                    cp /tmp/vgmstream/build/cli/vgmstream-cli kame-editor_version_amd64/usr/bin/
                    cp kame-editor.desktop kame-editor_version_amd64/usr/share/applications/
                    cp .circleci/deb/copyright kame-editor_version_amd64/usr/share/doc/kame-editor/
                    cp -a icons/. kame-editor_version_amd64/usr/share/icons/hicolor
                    fakeroot dpkg-deb --build kame-editor_version_amd64
            - store_artifacts:
                path: ~/project/kame-editor_version_amd64.deb
    build-aarch64:
        machine:
            image: ubuntu-2204:current
        resource_class: arm.medium
        steps:
            - run:
                name: "Update apt-get"
                command: sudo apt-get update
            - run:
                name: "Disable apt-get interactive mode"
                command: sudo sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf
            - run:
                name: "install libgl"
                command: sudo apt-get install libgl1-mesa-dev
            - run:
                name: "install vgmstream dependencies"
                command: sudo apt-get install libspeex-dev libvorbis-dev libtool yasm
            - run:
                name: "install qt6"
                command: sudo apt-get install qt6-base-dev
            - run:
                name: "install portaudio"
                command: sudo apt-get install portaudio19-dev
            - kame-tools
            - vgmstream
            - rstmcpp
            - checkout
            - run:
                name: build Kame Editor
                command: |
                    cd ~/project/
                    qmake6
                    make -j$(nproc)
                    objcopy --strip-unneeded --strip-debug build/kame-editor
                    ./buildicons.sh
            - run:
                name: package Kame Editor
                command: |
                    sudo apt-get install fakeroot
                    cd ~/project/
                    mkdir -p kame-editor_version_aarch64/DEBIAN
                    mkdir -p kame-editor_version_aarch64/usr/bin
                    mkdir -p kame-editor_version_aarch64/usr/share/applications
                    mkdir -p kame-editor_version_aarch64/usr/share/icons/hicolor
                    mkdir -p kame-editor_version_aarch64/usr/share/doc/kame-editor
                    cp .circleci/aarch64/control kame-editor_version_aarch64/DEBIAN/
                    cp build/kame-editor kame-editor_version_aarch64/usr/bin/
                    cp /tmp/kame-tools/output/linux-aarch64/kame-tools kame-editor_version_aarch64/usr/bin/
                    objcopy --strip-unneeded --strip-debug kame-editor_version_aarch64/usr/bin/kame-tools
                    cp /tmp/rstmcpp/rstmcpp kame-editor_version_aarch64/usr/bin/
                    objcopy --strip-unneeded --strip-debug kame-editor_version_aarch64/usr/bin/rstmcpp
                    cp /tmp/vgmstream/build/cli/vgmstream-cli kame-editor_version_aarch64/usr/bin/
                    cp kame-editor.desktop kame-editor_version_aarch64/usr/share/applications/
                    cp .circleci/aarch64/copyright kame-editor_version_aarch64/usr/share/doc/kame-editor/
                    cp -a icons/. kame-editor_version_aarch64/usr/share/icons/hicolor
                    fakeroot dpkg-deb --build kame-editor_version_aarch64
            - store_artifacts:
                path: ~/project/kame-editor_version_aarch64.deb

workflows:
    version: 2
    appbundle:
        jobs:
            - build
            - build-debs
            - build-aarch64
