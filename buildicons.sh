#!/bin/bash

cd "${0%/*}"

rm -rf icons;
mkdir icons;

shopt -s nullglob
list=(resources/icons/icon-*.png);

for i in "${list[@]}";
do
   size=$(echo "$i" | grep -o -E "[0-9]+x[0-9]+");
   mkdir -p icons/"$size"/apps;
   cp "$i" icons/"$size"/apps/kame-editor.png;
done

mkdir -p icons/scalable/apps;
cp resources/icons/icon.svg icons/scalable/apps/kame-editor.svg;
