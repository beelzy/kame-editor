# Creating binaries for Windows with Mingw-w64

When building with mingw-w64, the mingw-w64 versions of QT5 and portaudio have to be built or installed, and GCC as well. This can be either when cross-building from linux (like [Arch Linux](https://www.archlinux.org) with AUR repo entries) or building from [MSYS2](https://www.msys2.org) on Windows.

Crossbuilding from Arch Linux and building from MSYS2 were both tested.

## Mingw-w64 package names

From Arch\* AUR:
 - mingw-w64-qt5
 - mingw-w64-portaudio
 - mingw-w64-environment
 - mingw-w64-gcc

From MSYS2, if building for 32bit:
 - mingw-w64-i686-qt5
 - mingw-w64-i686-portaudio
 - mingw-w64-i686-gcc
 - mingw-w64-i686-make

From MSYS2, if building for 64bit:
 - mingw-w64-x86_64-qt5
 - mingw-w64-x86_64-portaudio
 - mingw-w64-x86_64-gcc
 - mingw-w64-x86_64-make


\* A thing to note about using Arch is that you'll need to build every dependency in the chain, and deal with some circular dependencies.

## Building with Mingw-w64

Provided that we use qmake, on MSYS2, open the shortcut for the desired mingw architecture and run:

```bash
qmake
mingw32-make
```

On Arch Linux, we use the prefixed qmake of the desired architecture instead, as well add a set of flags from `mingw-env`:

x86:

```bash
# mingw-env loads new flags but keeps previous ones too, skip these unsets if intended to keep extra flags
unset CFLAGS
unset CPPFLAGS
unset CXXFLAGS
unset LDFLAGS

source mingw-env i686-w64-mingw32
i686-w64-mingw32-qmake-qt5 QMAKE_CXXFLAGS+="$CPPFLAGS $CXXFLAGS" QMAKE_CFLAGS+="$CPPFLAGS $CFLAGS" QMAKE_LFLAGS="$CPPFLAGS $LDFLAGS"
make
```

x64:

```bash
# mingw-env loads new flags but keeps previous ones too, skip these unsets if intended to keep extra flags
unset CFLAGS
unset CPPFLAGS
unset CXXFLAGS
unset LDFLAGS

source mingw-env x86_64-w64-mingw32
x86_64-w64-mingw32-qmake-qt5 QMAKE_CXXFLAGS+="$CPPFLAGS $CXXFLAGS" QMAKE_CFLAGS+="$CPPFLAGS $CFLAGS" QMAKE_LFLAGS="$CPPFLAGS $LDFLAGS"
make
```

After successful build, exe will be placed under `build/`.
Any needed dlls will have to be copied out of the build environment.
You will also need at least the `platforms` and `imageformats` folders from your QT5's plugin folder.
You can skip getting dlls and plugins if building with MSYS2, and intend to keep it, just point folder with DLLs in PATH.

DLLs can be found under:
 - MSYS2 mingw-w64 i686: `/mingw32/bin`
 - MSYS2 mingw-w64 x86_64: `/mingw64/bin`
 - Arch mingw-w64 i686: `/usr/i686-w64-mingw32/bin`
 - Arch mingw-w64 x86_64: `/usr/x86_64-w64-mingw32/bin`

QT5 plugin folders could be found under:
 - MSYS2 mingw-w64 i686: `/mingw32/share/qt5/plugins/`
 - MSYS2 mingw-w64 x86_64: `/mingw64/share/qt5/plugins/`
 - Arch mingw-w64 i686: `/usr/i686-w64-mingw32/lib/qt/plugins/`
 - Arch mingw-w64 x86_64: `/usr/x86_64-w64-mingw32/lib/qt/plugins/`

Notes:
 - MSYS2 folders are relative to its installation folder.
 - These may be different as they may change over time with changes on how the libs are built or a change of your working environment.
