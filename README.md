# <img src="https://gitlab.com/beelzy/kame-editor/raw/master/resources/icons/icon-64x64.png"> Kame Editor

A 3DS theme editor made in Qt

![screenshot](https://gitlab.com/beelzy/kame-editor/raw/master/screenshot.png)

This is the frontend for the command line tool, [kame-tools](https://gitlab.com/beelzy/kame-tools). You can make themes with all the graphical functionality provided by kame-tools. Experimental audio support is now included.
There are currently some release builds available for macOS 10.12+, Windows 64-bit&ast; and Debian/Ubuntu&ast;&ast; on [Bitbucket](https://bitbucket.org/beelzy/kame-editor/downloads/) and for Archlinux in the [AUR](https://aur.archlinux.org/packages/kame-editor-git/). Linux users of other distros are welcome to contribute their own builds. See the section below on building and packaging considerations for more details.
Theoretically, it should be possible to build Kame Editor on Windows since Qt6 is supposed to be cross-platform, but I don't really have a good way of testing such builds.

&ast; Windows builds are provided by other contributors who were nice enough to put the time and effort in getting it to work. The builds are provided as-is in the hopes that they will be useful to whoever is using them, and so long as these contributors are around to help, any Windows specific bugs may be considered, but I myself would be unable to look at them personally. I don't have the expertise or experience in this area to do so.

&ast;&ast; The .deb builds are not in any way meant to function as official Debian/Ubuntu packages, so they may not conform to packaging guidelines or even work correctly. If you are interested in seeing an official .deb package for your distro, feel free to create one yourself or try to get a distro maintainer interested in it. See the section below on packaging considerations for more details.

## Features

* Interactive preview
* Preview can display different icon zoom levels
* Preview supports various top and bottom screen texture scrolling modes
* Animations can be paused to reduce CPU usage
* Editor is organized into flexible dock widgets with collapsible items
* Preview is displayed in a 3DS skin
* Helpful tooltips indicate what each property does, or if they are unused/unknown
* Integrates with kame-tools in the backend to create theme files
* Generates a preview image when themes are deployed
* Supports previewing and managing audio
* Provides a converter for converting wav files to the appropriate format required for themes
* Displays a 3D folder in the preview when a folder is selected
* Color set palette management
* Import theme archives and extract their image and audio resources

## Building

### Dependencies

The following libraries should be installed to a location your compiler can find them in. On Linux, this should be provided by the package manager of your distro. We've added extra search paths for Mac users, so they can also have it in `/usr/local` if they are building from Homebrew.

* [Qt6](https://www.qt.io/download-qt-installer)
* [portaudio](http://www.portaudio.com/download.html)

Please make sure you put these tools somewhere on your PATH or in the same directory as the final kame-editor executable.
* [kame-tools](https://gitlab.com/beelzy/kame-tools) - for actually bundling and making the final theme files
* [vgmstream](https://github.com/losnoco/vgmstream) - for previewing audio
* [rstmcpp](https://gitlab.com/beelzy/rstmcpp) - for converting audio files

To build Kame Editor, clone this project and just run `qmake` and then `make` in the root directory of the project. You can now run the `kame-editor` binary. Please note that you can still run Kame Editor without having the tools in your PATH listed above, but the features provided by them will be missing.

### Windows (MinGW)

Some build instructions for making Windows builds on MinGW have been provided by [luigoalma](https://github.com/luigoalma). You can find the build instructions [here](https://gitlab.com/beelzy/kame-editor/-/blob/master/MINGW.md).

## Packaging Considerations

### Shared Library
Kame-Editor is configured to create shared library builds, which means out of the box, it requires libraries and the Qt6 framework to reside in a shared library location. For most Linux users, this is not normally a problem (the dependency requirements are not too complicated). There is a [PKGBUILD](https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=kame-editor-git) for this in the AUR, so distro maintainers who want to package Kame Editor for their distro can look at it. You may also need to look at the PKGBUILDs of [kame-tools](https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=kame-tools-git), [rstmcpp](https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=rstmcpp-git) and [vgmstream](https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=vgmstream-kode54-git) since they aren't normally provided by most distros.

### Standalone/Static
For users who want to bundle Kame Editor as a standalone app, you can use [macdeployqt](https://doc.qt.io/qt-5.9/osx-deployment.html) or [windeployqt](https://doc.qt.io/qt-5/windows-deployment.html) for non-Linux platforms, so that the Qt6 frameworks and portaudio get bundled with the app. Additionally, you should include the tools listed in the build section above in the same location as the kame-editor executable.

I never actually tried building Kame Editor statically, but you're welcome to try it.

### Deb Packages
There is a circleci configuration for this. See the `build-debs` job in the `.circleci/config.yml` file for more details. This will probably create a working .deb package, but I am not well versed enough in deb packaging to make it conform to particular distro packaging guidelines.

## Issues

On some graphics configurations, [OpenGL](https://stackoverflow.com/questions/53768408/qt3d-qt-glx-qglx-findconfig-failed-to-finding-matching-fbconfig-warning) [multisampling](https://forum.qt.io/topic/102442/linux-build-qt-glx-problem) [doesn't](https://github.com/YosysHQ/nextpnr/issues/240) work properly if you build with Qt5.12 and up, so an extra argument has been provided to disable it if this happens to you:

`kame-editor --no-msaa`

## Unit Tests

Go into the `test` folder and run `qmake ../kame-editor.pro "CONFIG += test"`. Run `make` and then run `test/build/kame-tests` to build and run the tests.

## Translations

German is the only translation currently available at the moment, and it has room for improvement (my German isn't that great). People are free to offer improvements or add translations for other languages. See the [Qt6 guide](https://doc.qt.io/qt-6/linguist-translators.html) for doing this. You will need Qt Linguist, which you can get [here](https://github.com/lelegard/qtlinguist-installers/releases) to avoid installing the entire Qt framework, which as a translator, you probably won't need if you don't plan to build the program yourself. You can find the translation files in [translations/](https://gitlab.com/beelzy/kame-editor/tree/master/translations).

### Qt6 Core Translations

Qt6 provides some of the translations for standard buttons, so we don't have to provide those. To include those, install qt6 translations and use `lconvert -o translations/qtbase_[locale].qm [path/to/qtbase_locale.qm]`. `[locale]` is the two letter locale code for the language you want to include. Also don't forget to include the qm file in `kameeditor.qrc`.

## Attribution

### Qt6

* modified and updated for Qt6 [qAccordion](https://github.com/crapp/qaccordion) by Christian Rapp, GPL 3.0
* ColorDialog updated for Qt6 from [Qt Color Widgets](https://gitlab.com/mattbas/Qt-Color-Widgets) by Mattia Basaglia, LGPLv3+

### Other libs

* [portaudio](http://www.portaudio.com) by Ross Bencina and Phil Burk, MIT
* [miniz](https://github.com/richgel999/miniz) by richgel999, MIT
* [tinyobjloader](https://github.com/syoyo/tinyobjloader) by Syoyo Fujita, MIT

### Fonts

* Nimbus Sans L by URW++, GPL

### Images

- [topExt.png](https://github.com/usagirei/3DS-Theme-Editor/blob/master/ThemeEditor.WPF/Resources/TOPALT_DEFMASK.png) by usagirei, MIT
- [battery.png](https://github.com/exelix11/YATA-PLUS/blob/master/Resources/HudBatBase_01.png) by exelix, ISC

### Meshes

- [folder.obj](https://github.com/usagirei/3DS-Theme-Editor/blob/master/ThemeEditor.WPF/Resources/folder.obj) by usagirei, MIT
- [file.obj](https://github.com/usagirei/3DS-Theme-Editor/blob/master/ThemeEditor.WPF/Resources/file.obj) by usagirei, MIT

All other graphics and other assets were made specifically for this program.

## License

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
