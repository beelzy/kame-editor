#version 330 core
layout(location = 0)in vec3 vertex;
layout(location = 1)in vec3 aNormal;

uniform mat4 mvpMatrix;
uniform mat4 mMatrix;

out vec3 fragPos;
out vec3 lightPos;
out vec3 normal;
out vec3 vertexOut;

void main() {
    gl_Position = mvpMatrix * vec4(vertex.x, vertex.y, vertex.z, 1.0);
    fragPos = vec3(mMatrix * vec4(vertex, 1.0));
    lightPos = vec3(mMatrix * vec4(0, 15, 1, 1.0));
    normal = aNormal;
    vertexOut = vertex;
}
