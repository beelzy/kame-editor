#version 330 core
in vec2 uv;

uniform sampler2D image;
uniform float time;

out vec4 fragColor;

void main() {
    vec2 newuv = vec2(uv.x - time / 1008.0, uv.y + 16 / 256.0);
    fragColor = texture(image, newuv);
}
