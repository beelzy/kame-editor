#version 330 core
in vec3 fragPos;
in vec3 lightPos;
in vec3 normal;
in vec3 vertexOut;

uniform vec4 maincolor;
uniform vec4 dark;
uniform vec4 light;

out vec4 fragColor;

void main() {
    vec3 ambientcolor = mix(vec3(maincolor), vec3(light), min(1, abs(vertexOut.z - normal.b)));
    ambientcolor = mix(ambientcolor, vec3(dark), min(1, abs(normal.r)));
    vec3 lightColor = vec3(1, 1, 1);
    float ambientStrength = 0.7;
    vec3 ambient = ambientStrength * lightColor;

    // diffuse 
    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(lightPos - fragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    vec3 result = (ambient + diffuse) * ambientcolor;
    fragColor = vec4(result, 1.0);
}
