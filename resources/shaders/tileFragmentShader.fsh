#version 330 core
in vec2 uv;

uniform vec4 color;
uniform sampler2D image;
uniform sampler2D ext;
uniform float alpha;
uniform float gradientbrightness;
uniform float gradientalpha;
uniform float alphaext;
uniform float time;

out vec4 fragColor;

void main() {
    vec2 newuv = uv;
    float ratioX = uv.x / 6.40625;
    float rate = 1 / 8000.0;
    newuv.x += 0.4;
    newuv.y += 0.4 + 0.3 * (-2.0 * newuv.y + 0.5) * ratioX * (ratioX - 1.0);
    vec2 offset = vec2(newuv.x - (time * rate), newuv.y + time * rate);
    // Moving tiles
    vec4 c = texture(image, offset);
    // Ensure textures are grayscale
    c.rgb = vec3(dot(c.rgb, vec3(0.299, 0.587, 0.114)));
    c.a = min(c.r, c.a) * alpha;
    c.rgb = vec3(1);
    // Static tiles
    vec4 fixedTexture = texture(ext, newuv);
    fixedTexture.rgb = vec3(dot(fixedTexture.rgb, vec3(0.299, 0.587, 0.114)));
    fixedTexture.a = min(fixedTexture.a, fixedTexture.r) * alphaext;
    fixedTexture.rgb = vec3(1);

    // Background color
    vec4 bottom = color;
    bottom.a = 1;
    vec4 top = mix(bottom, vec4(gradientbrightness, gradientbrightness, gradientbrightness, 1), gradientalpha);
    top.a = 1;
    vec4 gradient = bottom * (1 - mod(uv.y / 3.75, 1.0)) + top * mod(uv.y / 3.75, 1.0);
    gradient.a = 1;
    vec4 opaque = mix(c, fixedTexture, 0.5);
    opaque.a = min(1, opaque.a * 2);
    opaque.rgb = vec3(gradientbrightness, gradientbrightness, gradientbrightness);
    fragColor = vec4(mix(gradient.rgb, opaque.rgb, opaque.a), gradient.a + opaque.a);
}
