#version 330 core
layout(location = 0)in vec3 vertex;
layout(location = 1)in vec2 texCoords;

out vec2 uv;

uniform mat4 mvpMatrix;

void main() {
    uv = texCoords;
    gl_Position = mvpMatrix * vec4(vertex.x, vertex.y, vertex.z, 1.0);
}
